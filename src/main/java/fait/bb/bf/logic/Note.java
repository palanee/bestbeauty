package fait.bb.bf.logic;

public class Note {
	
	private int id;
	private String title;
	private String text;
	private int customerID;
	
	Note(int id, String text, int customerID) {
		
		this.id = id;
		this.text = text;
		this.customerID = customerID;
		
	}
	
	public int getId() {
		
		return id;
		
	}
	
	public String getText() {
		
		return text;
		
	}
	
	public int getCustomer() {
		
		return customerID;
		
	}

}
