package fait.bb.bf.logic;

public class Treatment {
	
	
	private String number;
	private String name;
	private Integer time;
	private Float price;
	private int order;
	private int id;
	private Formula formula;
	private int idRelation;
	
	
	public Treatment(String number, String name, int time, float price,
			int order, int id) {
		
		super();
		setNumber(number);
		this.name = name;
		this.time = time;
		this.price = price;
		this.order = order;
		this.id = id;
	}
	
	public Treatment() {
		
		super();
	}
	
	public Treatment(String number, String name, int time, float price) {
		
		super();
		setNumber(number);
		this.name = name;
		this.time = time;
		this.price = price;
		
	}
	
	public Treatment(String number, String name, String time, String price, int id) {
		
		super();
		setNumber(number);
		setName(name);
		if(!time.isEmpty())
			this.time = Integer.parseInt(time);
		if(!price.isEmpty())
			this.price = Float.parseFloat(price);
		this.id = id;
	}
	
	public Treatment(String number, String name, String time, String price) {
		
		super();
		setNumber(number);
		setName(name);
		if(!time.isEmpty())
			this.time = Integer.parseInt(time);
		if(!price.isEmpty())
			this.price = Float.parseFloat(price);
		
	}

	public Treatment(int order) {
		
		super();
		setNumber("");
		this.order = order;
		this.name = "";
		this.time = 0;
		
	}

	public int getId() {
		return id;
	}

	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		
		if(number==null)
			this.number="";
		else
			this.number = number;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getTime() {
		if(this.time!=null)
			return time;
		else
			return 30;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	
	public float getPrice() {
		if(price!=null)
			return price;
		else
			return 0f;
		
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
	
	public int getOrder() {
		return order;
	}
	
	public void setOrder(int order) {
		this.order = order;
	}
	
	
	public String toString() {
		if(name.isEmpty())
			return " ";
		else
			return this.name + ", "; 
		
	}
	
	public void setFormula(int idRelation) {
		
		System.out.println("Ustawiamy formu��.... ");
		ServiceData serviceData = new ServiceData();
		
		formula = serviceData.getFormula(idRelation);
		System.out.println("Formula idRelation: " + formula.getIdTreats());
		
		this.idRelation = idRelation;
		
		
		
		
	}
	
	public int getRelation() {
		
		return idRelation;
		
	}
	
	public void setRelation(int id) {
		idRelation = id;
		if(formula!=null)
			formula.setIdTreats(id);
		else {
			ServiceData serviceData = new ServiceData();
			formula = serviceData.getFormula(idRelation);
		}
	}
	
	public Boolean equals(Treatment t) {
		
		Boolean isEqual = true;
		
		if(t.getPrice()!=price)
			return false;
		else if(t.getTime()!=time)
			return false;
		else
			return true;
		
	}

	public Formula getFormula() {
		return formula;
	}

	public void setFormula(Formula formula) {
		
		this.formula = formula;
		System.out.println("W klasie formula: " + this.formula);
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	
	

}
