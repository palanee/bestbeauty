package fait.bb.bf.logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import fait.bb.bf.graphics.ConfigureApp;


public class CalendarApp {
	
	private static DateOfDay date;
	
	public enum Day {
		SUNDAY("Nd", "Niedziela"), MONDAY("Pn", "Poniedzia�ek"),
		TUESDAY("Wt", "Wtorek"), WEDNESDAY("�r", "�roda"), 
		THURSDAY("Czw", "Czwartek"), FRIDAY("Pt", "Pi�tek"), SATURDAY("Sb", "Sobota")
		;
		
		private String name;
		private String fullName;
		
		
		Day(String name, String fullName) {
			
			this.name = name;
			this.fullName = fullName;
	
		}
		
		public String getName() {
			return name;
		}
		
		public String getFullName() {
			return fullName;
		}
	};
	
	
	
	public enum Month {
		JANUARY("stycze�"),
		FEBRUARY("luty"),
		MARCH("marzec"),
		APRIL("kwiecie�"),
		MAY("maj"),
		JUNE("czerwiec"),
		JULY("lipiec"),
		AUGUST("sierpie�"),
		SEPTEMBER("wrzesie�"),
		OCTOBER("pa�dziernik"),
		NOVEMBER("listopad"),
		DECEMBER("grudzie�");
		
		private String name;
		
		Month(String name) {
			this.name = name;
		}
		
		public String getName() {
			return this.name;
		}
		
	}
	
	
	
	public static String getNameOfDay(int i) {
		
		System.out.println("Warto�� przes�ana: " + i);
		
		Day [] tab = Day.values();
		for(int j = 0; j<tab.length; j++) {
			
			if(tab[j].ordinal()==i) {
				System.out.println("Znalaz�em!!!");
				return tab[j].getName();
				
			}
		}
	
		return null;
	}
	
	public static String getNameOfMonth(int i) {
		
		Month [] tab = Month.values();
		for(int j = 0; j<tab.length; j++) {
			if(tab[j].ordinal()==i) 
				return tab[j].getName();
		}
	
		return null;
		
	}
	
	public static DateOfDay setTodayDate(Calendar data) {
		
		
		
		if(data==null) {
			data = Calendar.getInstance(Locale.getDefault());
			System.out.println("Data do przetworzenia: null");
		}
		else {
			System.out.println("Data do przetworzenia: " + data.getTime());
		}
		
		//System.out.println("Ustawianie daty w CalendarApp: " + data.get(Calendar.DAY_OF_WEEK));
		String name = getNameOfDay(data.get(Calendar.DAY_OF_WEEK) - 1);
		int day = data.get(Calendar.DAY_OF_MONTH);
		String month = getNameOfMonth(data.get(Calendar.MONTH));
		int year = data.get(Calendar.YEAR);
		System.out.println("Dzie�: " + day);
		
		return (new DateOfDay(name, day, month, year, data));
		
	}
	
	public CalendarApp() {
		
		 date = setTodayDate(null);
		 
		 ConfigureApp.date = date;
	}

	/**
	 *
	 * @param i ilo�� dni, jaka zmie�ci�a si� na ekranie
	 * @param currentDate aktualna data / dzisiejsza data
	 * @return list� dni z datami, kt�re maj� by� wy�wietlane na ekranie
	 */

	public List<DateOfDay> getStringTopDate(int i, Calendar currentDate) {
		
		List<DateOfDay> list = new ArrayList<DateOfDay>();
		DateOfDay tempDate;
		
		if(currentDate!=null)
			tempDate = setTodayDate(currentDate);
		else
			tempDate = date;
		
		
		int j = 0;
		while(j<i) {

			list.add(tempDate);
			j++;
			tempDate = addDay(1, tempDate.getCalDate());
			
		}
		
		
		
		return list;
		
	}

	/**
	 *
	 * @param i
	 * @param date
	 * @return
	 */
	public DateOfDay addDay(int i, Calendar date) {
		
		
		date.add(Calendar.DAY_OF_MONTH, i);
		Calendar tempDate = date;
		
		
		//System.out.println("Data wygenerowana: " + tempDate.get(Calendar.DAY_OF_MONTH));
		
		DateOfDay temp = new DateOfDay(getNameOfDay(date.get(Calendar.DAY_OF_WEEK)-1),
									   date.get(Calendar.DAY_OF_MONTH),
									   getNameOfMonth(date.get(Calendar.MONTH)),
									   date.get(Calendar.YEAR),
									   tempDate);
		
		
		return temp;
	}
	
	public DateOfDay changeDate(Calendar date) {
		
		DateOfDay temp = new DateOfDay(getNameOfDay(date.get(Calendar.DAY_OF_WEEK)-1),
				   			date.get(Calendar.DAY_OF_MONTH),
				   			getNameOfMonth(date.get(Calendar.MONTH)),
				   			date.get(Calendar.YEAR),
				   			date);
		
		return temp;
		
	}
	
	public String getFullName(String name) {
		
		Day [] tab = Day.values();
		for(int j = 0; j<tab.length; j++) {
			if(tab[j].getName()==name) 
				return tab[j].getFullName();
		}
	
		return null;
		
	}
	
	public static long diffInDays(Date d1, Date d2) {
           
		return (d1.getTime() - d2.getTime()) / 86400000;
    
	}
	
	
 
}
