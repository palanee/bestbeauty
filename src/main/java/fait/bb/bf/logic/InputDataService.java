package fait.bb.bf.logic;


import javax.swing.text.JTextComponent;

public class InputDataService {
	
	public static Boolean textInField(JTextComponent field) {
		
		Boolean ok = false;
		if(!field.getText().isEmpty())
			ok = true;
		
		
		return ok;
		
	}
	
	public static String isGoodNumber(JTextComponent field) {
		
		String comm = "";
		//je�li 0 - brak wpisu
		//je�li 1 - poprawnie wpisany numer
		//je�li 2 - zosta�y wpisane niedozwolone znaki
		//je�li 3 - numer jest zbyt kr�tki
		//je�li 4 - numer jest zbyt d�ugi
		
		
		if(!field.getText().isEmpty()) {
			String s = field.getText();
			s.trim();
			s.replace("-", "");
			char[] temp = s.toCharArray();
			int j = 0;
			if(temp.length>11) 
				comm = "Wpisany numer jest za d�ugi";
			else if(temp.length<7)
				comm = "Wpisany numer jest za kr�tki";
			while(j<temp.length) {
				if(Character.isDigit(temp[j]))
					comm = "Wpisano niedozwolone znaki";
				j++;
			}
			
			
		}
		
		return comm;
		
	}
	
	public static Boolean goodPrice(String price) {
		
		//0 - je�li dobrze, 1 - je�li �le
		if(price.isEmpty()) {
			return true;
		}
		else if(price.equals("Cena")) {
			return true;
		}
		else
			return true;
		
	}
	
	public static Boolean goodTime(String time) {
		
		//0 - je�li dobrze, 1 - je�li �le
		if(time.isEmpty()) {
			return true;
		}
		else if(time.equals("Czas")) {
			return true;
		}
		else
			return true;
		
	}
	
	public static String formatCustomerID(int id) {
		
		String idFormat = Integer.toString(id);
		
		switch(idFormat.length()) {
			case 1: 
					idFormat = "#000" + idFormat;
					break;
			case 2: 
				idFormat = "#00" + idFormat;
				break;
			case 3: 
				idFormat = "#0" + idFormat;
				break;
			case 4: 
				idFormat = "#" + idFormat;
				break;
			default:
				idFormat = "#" + idFormat;
				break;
		}
		
		return idFormat;
		
	}
	

}
