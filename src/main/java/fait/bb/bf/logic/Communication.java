package fait.bb.bf.logic;

import java.awt.Frame;

import javax.swing.JOptionPane;

public class Communication {
	
	private Frame frame;
	
	public Communication(Frame frame) {
			this.frame = frame;
	}


	public Boolean youSure(String name, int i) {

		Object[] options = {"Tak",
		"Nie"};

		int n = JOptionPane.showOptionDialog(
				frame,
				name,
				"Zapytanie",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[i]);

		if(n==0) {
			return true;
		}

		else
			return false;

	}
	
	public Boolean youSure(String name, int i, String...strings) {

		Object[] options = new Object[strings.length];
		
		for(int j=0; j<strings.length; j++)
			options[j] = strings[j];

		int n = JOptionPane.showOptionDialog(
				frame,
				name,
				"Zapytanie",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[i]);

		if(n==0) {
			return true;
		}

		else
			return false;

	}
	
	public void dont(String name) {

		JOptionPane.showMessageDialog(frame,
	    name);

	}

}
