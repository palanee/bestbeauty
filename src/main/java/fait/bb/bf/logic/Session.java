package fait.bb.bf.logic;

import java.util.Calendar;

/**
 * Klasa tworz�ca sesj� uzytkownika na dany dzie�
 */

public class Session {
	
	
	private int id;
	private User user;
	private DateOfDay date;
	private Calendar calendar;
	
	
	public Session(User user) {
		
		this.user = user;
		calendar = Calendar.getInstance();
		date = new DateOfDay(calendar);
		
	}
	
	public void setCalendar(Calendar cal) {
		
		this.calendar = cal;
		
	}
	
	public void setDate(Calendar cal) {
		
		date = new DateOfDay(cal);
		
	}
	
	public DateOfDay getDate() {
		
		return date;
		
	}
	
	public User getUser() {
		
		return user;
		
	}
	
	
	
	
	
}
