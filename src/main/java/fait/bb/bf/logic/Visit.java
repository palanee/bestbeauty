package fait.bb.bf.logic;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Visit implements Comparable<Visit> {
	
	private int id;
	private int customerID;
	private String date;
	private String text;
	private String userCode;
	private int duration;
	private Formula formula;
	private VisitTemp visitRelation;
	private String isRealized;



	private Customer customer;
	private User user;
	
	private String number;
	
	private Float allPrice;
	
	private Boolean active;
	
	private List<Treatment> listTreat;
	private List<VisitTemp> listVisit;
	

	
	public Visit(int id, int customerID, String date, String text,
				 String userCode, int duration, String isRealized) {
		
		super();
		this.id = id;
		this.customerID = customerID;
		this.date = date;
		this.text = text;
		this.userCode = userCode;
		this.duration = duration;
		setActive(isRealized);
		
		ServiceData serviceData = new ServiceData();
		
		//this.active = true;
		
		user = serviceData.getUserFromString(userCode); 
		customer = serviceData.getCustomer(customerID);
		listTreat = new ArrayList<Treatment>();
		listVisit = new ArrayList<VisitTemp>();
	}
	
	
	
	public Visit(int customerID, String date, String userCode,
			     String treat, float price, int time, String text) {
		super();
		this.customerID = customerID;
		this.date = date;
		this.text = text;
		this.userCode = userCode;
		this.duration = time;
		
		ServiceData serviceData = new ServiceData();
		
		this.active = true;
		
		user = serviceData.getUserFromString(userCode); 
		customer = serviceData.getCustomer(customerID);
		listTreat = new ArrayList<Treatment>();
	}
	
	
	public Visit(Customer customer, String text) {
		
		this.customer = customer;
		this.text = text;
		
	}
	
	
	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}
	
	public Formula getFormula() {
		return formula;
	}

	public void setFormula(Formula formula) {
		this.formula = formula;
	}

	public Treatment getTreatment() {
		ServiceData serviceData = new ServiceData();
		if(listVisit.size()==0)
			listVisit = serviceData.getListTempVisit(id);
		
		Treatment t = null;
		
		if(listVisit.size()>0) {
			System.out.println("Wizyta: " + id);
			t = serviceData.getTreatment(listVisit.get(0).getIdTreat());
			visitRelation = listVisit.get(0);
		}
		
		
		return t;
	}
	
	public VisitTemp getFirstTreat() {
		
		return visitRelation;
	}
	
	public List<Treatment> getListTreatment() {
		
		ServiceData serviceData = new ServiceData();
		listVisit = serviceData.getListTempVisit(id);
		
		
		
		
		for(VisitTemp v : listVisit) {
			
			Treatment t = serviceData.getTreatment(v.getIdTreat());
			if(t.getOrder()!=1)
				listTreat.add(t);
			
		}
		
		return listTreat;
		
	}
	
	public List<VisitTemp> getListTempVisit() {
		
		ServiceData serviceData = new ServiceData();
		listVisit = serviceData.getListTempVisit(id);
		
		return listVisit;
	}
	
	public List<Treatment> getAllTreatments() {
		
		if(listTreat.size()>0)
			return listTreat;
		
		ServiceData serviceData = new ServiceData();
		
		listVisit = serviceData.getListTempVisit(id);
		
		listTreat.clear();
		
		System.out.println("No a teraz jeste�my w klasie Visit i metodzie getAllTreatments");
		
		for(VisitTemp v : listVisit) {
			
			Treatment t = serviceData.getTreatment(v.getIdTreat(), v.getId());
			listTreat.add(t);
			
			
		}
		
		return listTreat;
		
	}

	public int getDuration() {
		return duration;
	}
	
	//true- aktywny
	//false- nieaktywny
	
	public void setActive(Boolean isActive) {
		active = isActive;
	}
	
	public void setActive(String text) {
		
		if(text.equals("no"))
			active = true;
		else
			active = false;
		
	}
	
	public Boolean getActive() {
		
		System.out.println("Aktywne? " + active);
		return active;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}


	public User getUser() {
		return user;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getCustomerID() {
		return customerID;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	
	public String getDate() {
		return date;
	}
	
	public Date getDateToString() {
		
		String[] calendar = date.split(" ");
		
		String[] fields = calendar[0].split("-");
		
		String[] time = calendar[1].split(":");
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		
		if(time[0].contains(".")) {
			String[] temp= time[0].split("\\.");
			time[0] = temp[0];
		}
		
		cal.set(Integer.parseInt(fields[0]), Integer.parseInt(fields[1]), 
				Integer.parseInt(fields[2]), Integer.parseInt(time[0]), 
				Integer.parseInt(time[1]));
		
		return cal.getTime();
		
	}
	
	
	
	public String getDateWithout() {
		
		String[] calendar = date.split(" ");
		String[] date = calendar[0].split("-");
		
		int temp = Integer.parseInt(date[1]) + 1;
		if(temp<10)
			date[1] = "0" + temp;
		else
			date[1] = "" + temp;
		
		return date[2] + "-" + date[1] + "-" + date[0];
		
	}
	
	public String getDateText() {
		
		String[] calendar = date.split(" ");
		String[] date = calendar[0].split("-");
		
		CalendarApp calendarApp = new CalendarApp();
		String month = calendarApp.getNameOfMonth(Integer.parseInt(date[1]));
		
		return date[2] + " " + month + " " + date[0] + " godz: " + calendar[1];
		
	}
	
	public Calendar getCalendar() {
		String[] calendar = date.split(" ");
		
		String[] fields = calendar[0].split("-");
		
		String[] time = calendar[1].split(":");
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		
		if(time[0].contains(".")) {
			String[] temp= time[0].split("\\.");
			time[0] = temp[0];
		}
		
		cal.set(Integer.parseInt(fields[0]), Integer.parseInt(fields[1]), 
				Integer.parseInt(fields[2]), Integer.parseInt(time[0]), 
				Integer.parseInt(time[1]));
		
		return cal;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		
		if(text==null)
			text="";
		
		this.text = text;
	}
	
	public String getUserCode() {
		return userCode;
	}
	
	public float getHour() {
		
		
		
		
		String[] temp = date.split(" ");
		String[] temp2 = temp[1].split(":");
		
		System.out.println("Godzina: " + temp2[0]);
		
		return Float.parseFloat(temp2[0]);
		
	}
	
	public String getTime() {
		
		String[] temp = date.split(" ");
		
		return temp[1];
		
		
		
	}
	
	public float getAllPrice() {
		
		
		if(allPrice==null) {
			ServiceData serviceData = new ServiceData();


			allPrice = 0f;
			
			listVisit = serviceData.getListTempVisit(id);
			listTreat.clear();
			
			
			for(VisitTemp v : listVisit) {
				
				Treatment t = serviceData.getTreatment(v.getIdTreat());
				listTreat.add(t);
				
			}

			int i = 0;
			while(i<listTreat.size()) {
				allPrice += listTreat.get(i).getPrice();
				i++;
			}
		}
		
		return allPrice;
		
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public String toString() {
		
		return "Visit: " + userCode + " " + text + " " + date;
	}

	@Override
	public int compareTo(Visit o) {
		
		/*if(this.getHour()>o.getHour())
			return 1;
		else if(this.getHour()<o.getHour())
			return -1;
		else
			return 0;*/
		
		return this.getDateToString().compareTo(o.getDateToString());
		
	}
	
	public void setTreatments(List<Treatment> list) {
		
		listTreat = list;
		
	}
	
	
	
	public List<Treatment> getTreatments() {
		return listTreat;
	}
	

}
