package fait.bb.bf.logic;

import java.util.Calendar;


public class Customer implements Comparable<Customer> {
	
	private String name;
	private String surname;
	private String number;
	private String email;
	private String birthday;
	private String city;
	private String lastDate;
	private String natural;
	private String grey;
	private String condition;
	private int id;
	
	Customer(String name, String surname, String number, String natural, String grey, String condition) {
		
		setName(name);
		setSurname(surname);
		setNumber(number);
		setLastDate();
		setNatural(natural);
		setGrey(grey);
		setCondition(condition);
		
	}
	
	Customer(String name, String surname, String number, int id) {
		
		setName(name);
		setSurname(surname);
		setNumber(number);
		setId(id);
		
		
	}
	
	Customer(String name, String surname, String number, String email, String birthday, String city, String natural, String grey, int id) {
		
		setName(name);
		setSurname(surname);
		setNumber(number);
		setLastDate();
		setId(id);
		setNatural(natural);
		setGrey(grey);
		setEmail(email);
		setBirthday(birthday);
		setCity(city);
		
	}
	
	Customer(String name, String surname, String number, String email, String birthday, String city, int id) {
		
		setName(name);
		setSurname(surname);
		setNumber(number);
		setLastDate();
		setId(id);
		setNatural(natural);
		setGrey(grey);
		setEmail(email);
		setBirthday(birthday);
		setCity(city);
		
	}
	
	Customer(String name, String surname, String number, String email, String birthday, String city, String natural, String grey, String condition) {
		
		setName(name);
		setSurname(surname);
		setNumber(number);
		setLastDate();
		setId(id);
		setNatural(natural);
		setGrey(grey);
		setEmail(email);
		setBirthday(birthday);
		setCity(city);
		setCondition(condition);
		
	}

	
	public Customer(Customer c, Hair h) {
		
		setName(c.getName());
		setSurname(c.getSurname());
		setNumber(c.getNumber());
		setId(c.getId());
		setNatural(h.getNatural());
		setGrey(h.getGrey());
	}

	public Customer(String name, String surname, String number, String natural, String grey) {

		setName(name);
		setSurname(surname);
		setNumber(number);
		setNatural(natural);
		setGrey(grey);

	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name==null)
			this.name="";
		else
			this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		if(surname==null)
			this.surname="";
		else
			this.surname = surname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		if(number==null)
			number="-";
		this.number = number;
	}
	
	

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getLastDate() {
		
		if(lastDate.matches("[^\\.]")) {
		
			char[] temp = lastDate.toCharArray();
			String s = null;

			s += temp[0] + temp[1] + ".";
			s += temp[3] + temp[4] + ".";
			s += temp[6] + temp[7];
			lastDate = s;
			
			System.out.println("Po zamianie: " + lastDate);
			
		}
		return lastDate;
	}

	public void setLastDate() {
		
		Calendar data = Calendar.getInstance();
		int day = data.get(Calendar.DAY_OF_MONTH);
		Integer month = data.get(Calendar.MONTH);
		Integer year = data.get(Calendar.YEAR);
		month++;
		String miesiac = month.toString();
		if(month<10) {
			miesiac = "0" + month;
		}
		
		char[] temp = year.toString().toCharArray();
		String s = day + "." + miesiac + "." + temp[2]+temp[3];
		System.out.println("Dzisiejsza data: " + s);
		
		lastDate = s;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNatural() {
		return natural;
	}

	public void setNatural(String natural) {
		this.natural = natural;
	}

	public String getGrey() {
		return grey;
	}

	public void setGrey(String grey) {
		this.grey = grey;
	}
	
	public String toString() {
		return "ID " + id + ", name: " + name + ", nazwisko: " + surname + "\n";
	}
	
	public String getCustomerMenu() {
		return surname + " " + name + " " + number;
	}

	@Override
	public int compareTo(Customer o) {

		if(this.id == o.getId())
			return 0;
		else
			return 1;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setHair(Hair h) {
		
		this.natural = h.getNatural();
		this.grey = h.getGrey();
		this.condition = h.getCondition();
		
	}

}
