package fait.bb.bf.logic;

public class VisitTemp {
	
	private int id;
	private int idDate;
	private int idTreat;
	
	
	
	public VisitTemp(int id, int idDate, int idTreat) {
		
		super();
		this.id = id;
		this.idDate = idDate;
		this.idTreat = idTreat;
		
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdDate() {
		return idDate;
	}
	
	public void setIdDate(int idDate) {
		this.idDate = idDate;
	}
	
	public int getIdTreat() {
		return idTreat;
	}
	public void setIdTreat(int idTreat) {
		this.idTreat = idTreat;
	}
	
	

}
