package fait.bb.bf.logic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Formula {
		
	private int id;
	//private String price;
	//private String time;
	private String content;
	private int idTreats;
	private float price;
	private int time;
	private Treatment treat;
	private String date;
	//private int customerID;
	//private int userID;
	//private String userCode;
	
	
	//do tworzenia wpisu do bazy
	public Formula(String content, int idTreats) {
		
		setContent(content);
		setIdTreats(idTreats);
		setPrice(null);
		setTime(null);
		
	}
	
	public Formula(Float price, int time) {
		
		setPrice(price);
		setTime(time);
		this.content = "";
		this.idTreats = 0;
		
	}
	
	public Formula(Float price, Integer time, String content) {
		
		setPrice(price);
		setTime(time);
		setContent(content);
		this.idTreats = 0;
		
	}
	
	
	
	//gdy pobierany jest wpis z bazy
	public Formula(int id, String content, int idTreats, float price, int time) {
				
		setId(id);
		setContent(content);
		setIdTreats(idTreats);
		setPrice(price);
		setTime(time);
		//setTreat();
	}

	public Formula(int id, String content, float price, int time) {
		setId(id);
		setContent(content);
		setPrice(price);
		setTime(time);
		setTreat();
	}


	
	/*public Formula(String price, String time, String content) {
		
		setPrice(price);
		setTime(time);
		setContent(content);
		
	}*/
	
	public void setTreat() {
		
		ServiceData serviceData = new ServiceData();
		treat = serviceData.getTreatmentFromList(idTreats);
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		if(!content.isEmpty())
			this.content = content;
		else
			this.content = "";
	}
	
	
	
	public int getIdTreats() {
		return idTreats;
	}

	public void setIdTreats(int idTreats) {
		this.idTreats = idTreats;
	}
	
	

	public String getDate(Boolean format) {
		
		String temp;
		String[] tabtemp;
		
		if(format) {
			String[] tabDate = date.split("-");
			if(tabDate[2].contains(" ")) {
				tabtemp = tabDate[2].split(" ");
				temp = tabtemp[0] + "-" + tabDate[1] + "-" + tabDate[0];
			}
			else {
				temp = date;
			}
			
			System.out.println("Data po sformatowaniu: " + temp);
		}
		else
			temp = date;
		
		return temp;


	}
	
	
	
	public void setDate() {
		
		Calendar data = Calendar.getInstance(Locale.getDefault());
		int day = data.get(Calendar.DAY_OF_MONTH);
		Integer month = data.get(Calendar.MONTH);
		Integer year = data.get(Calendar.YEAR);
		Integer hour = data.get(Calendar.HOUR_OF_DAY);
		Integer minutes = data.get(Calendar.MINUTE);
		Integer secundes = data.get(Calendar.SECOND);
		month++;
		String miesiac = month.toString();
		if(month<10) {
			miesiac = "0" + month;
		}
		
		char[] temp = year.toString().toCharArray();
		String s = year + "-" + miesiac + "-" + day + " " + hour + ":" + minutes + ":" + secundes;
		//String s = day + "-" + miesiac + "-" + temp[2]+temp[3];
		System.out.println("Dzisiejsza data: " + s);
		this.date = s;
	}
	
	
	
	
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		if(price!=null)
			this.price = price;
		else
			this.price = 0f;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		if(time!=null)
			this.time = time;
		else
			this.time = 0;
	}

	public String toString() {
		return "ID " + id + ", content: " + content +  "; time: " + time + "; price: " + price + "\n";
	}
	
	
}
