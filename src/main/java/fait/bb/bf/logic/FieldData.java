package fait.bb.bf.logic;

/**
 * Klasa wykorzystywana przez DataBase do stworzenia p�l w bazie danych
 */

public class FieldData {
	
	String name;
	String type;
	String dflt;
	
	public FieldData(String name, String type, String dflt) {
		super();
		this.name = name;
		this.type = type;
		this.dflt = dflt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDflt() {
		return dflt;
	}

	public void setDflt(String dflt) {
		this.dflt = dflt;
	}
	
	
	
	

}
