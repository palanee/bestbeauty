package fait.bb.bf.logic;

import java.util.Calendar;
import java.util.Date;

public class DateOfDay {
	
	private String nameOfDay;
	private int day;
	private String month;
	private int year;
	private String date;
	private Calendar calDate;
	private Date dateDate;
	private int dayOfWeek;
	
	DateOfDay(String nameOfDay, int day, String month, int year, Calendar calDate) {
		
		this.nameOfDay = nameOfDay;
		this.day = day;
		this.month = month;
		this.year = year;
		this.dayOfWeek = calDate.get(Calendar.DAY_OF_WEEK);
		this.calDate = calDate;
		dateDate = calDate.getTime();
		setStringDate();
		
		System.out.println("dateDate: " + calDate.get(Calendar.DAY_OF_MONTH) + " " 
						   + calDate.get(Calendar.DAY_OF_WEEK));
		
	}
	
	public DateOfDay(Calendar calDate) {
		
		this.calDate = calDate;
		
	}
	
	
	//zwraca dzie� miesi�ca
	public int getDay() {
		
		return day;
	}
	
	//zwraca dzie� tygodnia
	public int getDayWeek() {
		
		return dayOfWeek;
		
	}

	public String getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public String getDate() {
		return date;
	}
	
	public String getDateWithoutDay() {
		
		return day + " " + month + " " + year;
		
	}
	
	public void setStringDate() {
			
		date = nameOfDay + 
		  			  ", " + 
		  			  day + 
		  			  " " + 
		  			  month + 
		  			  " " + 
		  			  year;
	}
	
	public Calendar getCalDate() {
		return calDate;
	}
	
	public Date getDateDate() {
		return dateDate;
	}
	
	public String getNameOfDay() {
		
		return nameOfDay;
	}
	
	public String toString() {
		
		return date;
		
	}
	
	

}
