package fait.bb.bf.logic;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;


public class User implements Comparable<User> {
	
	private String name;
	private String pass;
	private int isAdmin;
	private String surname;
	private String code;
	private byte[] avatar;
	private String status;
	
	private Date startDate;
	private Date endDate;
	private String freeDate;
	
	public User(String login, String pass, int isAdmin) {
		super();
		this.name = login;
		setPass(pass);
		this.isAdmin = isAdmin;
		this.avatar = null;
	}
	
	public User() {
		this.name = "";
		this.code = "";
		this.surname = "";
	}
	
	public User(String login, String pass) {
		super();
		this.name = login;
		this.pass = pass;
		
	}
	
	public User(String name, String surname, String code, byte[] avatar, String status) {
		
		super();
		setName(ServiceData.checkString(name));
		if(surname!=null)
			setSurname(ServiceData.checkString(surname));
		setCode(code);
		this.avatar = avatar;
		this.status = status;
		
	}
	
	public User(String name, String surname, String code, String status) {
		
		super();
		setName(ServiceData.checkString(name));
		setSurname(ServiceData.checkString(surname));
		setCode(code);
		this.status = status;
		this.avatar = null;
		
	}
	
	public User(String name, String surname, String code, byte[] avatar) {
		
		super();
		setName(ServiceData.checkString(name));
		setSurname(ServiceData.checkString(surname));
		setCode(code);
		this.avatar = avatar;
		
		
	}
	
	
	
	public User(int id, String name, String surname, String code) {
		
		super();
		setName(ServiceData.checkString(name));
		setSurname(ServiceData.checkString(surname));
		setCode(code);
		this.avatar = null;
		
	}

	
	public String getLogin() {
		return name;
	}
	
	public void setLogin(String login) {
		this.name = login;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		
		try {
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(pass.getBytes(),0,pass.length());
			BigInteger bigint = new BigInteger(1,md.digest());
			this.pass = bigint.toString();
			System.out.println("Password: " + this.pass);
			
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public int getIsAdmin() {
		return isAdmin;
	}
	
	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public Boolean comparePass(String password) {
		
		Boolean isGood = false;
		
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes(),0,password.length());
			BigInteger bigint = new BigInteger(1,md.digest());
			password = bigint.toString();
			if(password.equals(this.pass))
				isGood = true;
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("UDA�O si�? " + isGood);
		
		return isGood;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public ImageIcon getAvatar() {
		if(avatar==null) {
			System.out.println("Avatar: " + avatar);
			return null;
		}
		else {
			System.out.println("Avatar: " + avatar);
			return new ImageIcon(avatar);
		}
	}
	
	public byte[] getAvatarByte() {
		return avatar;
	}
	
	public String getStatus() {
		return status;
	}

	public int compareTo(User u) {
		
		final int EQUAL = 0;
		final int NOT_EQUAL = 1;
		
		System.out.println("Stary user: " + this);
		System.out.println("Nowy user: " + u);
		
		if(!this.name.equals(u.getName())) {
			return NOT_EQUAL;
		}
		else if(!this.surname.equals(u.getSurname())) {
			return NOT_EQUAL;
		}
		else if(!this.code.equals(u.getCode())) {
			return NOT_EQUAL;
		}
		else if((this.avatar==null) && (u.getAvatarByte()!=null)) {
			return NOT_EQUAL;
		}
		else if(!Arrays.equals(this.avatar, u.getAvatarByte())) {
			return NOT_EQUAL;
		}
		/*else if(!this.status.equals(u.getStatus())) {
			return NOT_EQUAL;
		}*/
		else
			return EQUAL;
		
	}
	
	public String toString() {
		
		return "[" + this.code + "] " + this.name + " " + this.surname + " ";
		
	}
	
	
	
	public void setFreeDay(Boolean isSelected) {
		
		ServiceData service = new ServiceData();
		
		System.out.println("Pobieramy dzie� wolny");
		
		if(isSelected)
			freeDate = service.getUsersMeta(code);
		String[] tab;
		if(freeDate!=null) {
			tab = service.getUsersMeta(code).split(";;");
			DateFormat df = DateFormat.getDateInstance();
			try {
				System.out.println("Mamy urlop!!!!");
				startDate = df.parse(tab[0]);
				endDate = df.parse(tab[1]);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(endDate);
				cal.add(Calendar.DAY_OF_MONTH, 1);
				endDate = cal.getTime();
				if(cal.getTime().after(endDate)) {
					
					System.out.println("Niestety ju� po urlopie...");
					service.updateUserMeta(code);
				}
				
				
				
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else {
			startDate = null;
			endDate = null;
			
		}
		
		
		
	}

	/**
	 * Pobiera informacj� na temat dni wolnych danego pracownika - urlop
	 * TO-DO wykorzysta� inn� klas� do godzin i dat?
	 * A co gdy kilka zaplanowanych dni urlopowych? Zwolnienia?
	 */
	
	public void setFreeDay() {
		
		ServiceData service = new ServiceData();

		freeDate = service.getUsersMeta(code);
		String[] tab;
		if(freeDate!=null) {
			tab = service.getUsersMeta(code).split(";;");
			DateFormat df = DateFormat.getDateInstance();
			try {
				startDate = df.parse(tab[0]);
				endDate = df.parse(tab[1]);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(endDate);
				cal.add(Calendar.DAY_OF_MONTH, 1);
				endDate = cal.getTime();
				if(cal.getTime().after(endDate)) {

					service.updateUserMeta(code);
				}
				
				
				
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else {
			startDate = null;
			endDate = null;
			
		}
		
		
		
	}
	
	public Boolean isFreeDay(Date date) {
		
		System.out.println("Por�wnujemy daty...." + endDate);
		
		
		
		if(startDate==null || endDate==null) {
			System.out.println("Brak pobranego urlopu...");
			return false;
		}
		else if((date.after(startDate) || date.equals(startDate)) && date.compareTo(endDate)<=0) {
			System.out.println("Jeste�my w czasie urlopu");
			return true;
		}
		else {
			System.out.println("Urlop pobrany, ale to nie ten dzie�");
			return false;
		}
		
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		
		if(endDate!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			cal.add(Calendar.DAY_OF_MONTH, -1);
			Date temp = cal.getTime();
			return temp;
		}
		else
			return null;
		
		
	}
	
	public String getFreeDate() {
		return freeDate;
	}

	

}
