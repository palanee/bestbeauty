package fait.bb.bf.logic;

public class DayWork implements Comparable<DayWork> {

	private int day;
	private int start;
	private int end;
	private int id;
	
	public DayWork(int day)  {
		
		setDay(day);
		
	}
	
	public DayWork(int id, String value) {
		
		System.out.println("Konstruktor DayWork: " + value);
		String[] temp = value.split(";");
		setDay(Integer.parseInt(temp[0]));
		setStart(Integer.parseInt(temp[1].trim()));
		setEnd(Integer.parseInt(temp[2].trim()));
		setId(id);
		
	}
	
	public DayWork(int id, int day, int start, int end) {
		
		setDay(day);
		setStart(start);
		setEnd(end);
		setId(id);
		
	}
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(DayWork o) {
		// TODO Auto-generated method stub
		if(this.getStart()>o.getStart())
			return 1;
		else if(this.getStart()<o.getStart())
			return -1;
		else
			return 0;
		
	}
	
	
	
}
