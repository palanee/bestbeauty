package fait.bb.bf.logic;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;



/**
 * Klasa obs�uguj�ca baz� danyych / zapytania SQL
 *
 * @author Paulina Miko�ajczyk
 */


public class DataBase {
	
	private static String path;

	/**
	 * Uaktualnia baz� wed�ug poni�szej struktury
	 */

	public static void updateDatabase() {
		
	
		
		try {
			
			//tworzenie tabeli klienci
			List<FieldData> fieldData = new ArrayList<FieldData>();
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("name", "text", null));
			fieldData.add(new FieldData("surname", "text", null));
			fieldData.add(new FieldData("phone", "text", null));
			fieldData.add(new FieldData("email", "text", "-"));
			fieldData.add(new FieldData("status", "text", null));
			fieldData.add(new FieldData("avatar", "blob", null));
			fieldData.add(new FieldData("user_display", "text", null));
			fieldData.add(new FieldData("birthday", "text", null));
			fieldData.add(new FieldData("city", "text", null));
			
			updateTable("klienci", fieldData);
			
			fieldData.clear();
			
			//tworzenie tabeli klienci_meta
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("priority", "integer", null));
			fieldData.add(new FieldData("key", "text", null));
			fieldData.add(new FieldData("value", "text", null));
			fieldData.add(new FieldData("parent", "text", null));
			fieldData.add(new FieldData("customerID", "integer", null));
			
			updateTable("klienci_meta", fieldData);
			
			fieldData.clear();
			
			//tworzenie tabeli lista zabieg�w
			fieldData.add(new FieldData("idRelacji", "integer", null));
			fieldData.add(new FieldData("idTermin", "integer", null));
			fieldData.add(new FieldData("idZabieg", "integer", null));
			
			updateTable("lista_zabiegow", fieldData);
			
			fieldData.clear();
			
			//tworzenie tabeli pomoc
			fieldData.add(new FieldData("panelName", "integer", null));
			fieldData.add(new FieldData("content", "text", null));
			
			updateTable("pomoc", fieldData);
			
			fieldData.clear();
			
			//tworzenie tabeli receptury
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("content", "text", null));
			fieldData.add(new FieldData("idRelacji", "integer", null));
			fieldData.add(new FieldData("cena", "float", null));
			fieldData.add(new FieldData("czas", "integer", null));
			
			updateTable("receptury", fieldData);
			
			fieldData.clear();
			
			//tworzenie tabeli receptury_meta
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("priority", "integer", null));
			fieldData.add(new FieldData("key", "text", null));
			fieldData.add(new FieldData("value", "text", null));
			fieldData.add(new FieldData("parent", "text", null));
			fieldData.add(new FieldData("receptID", "integer", null));
			
			updateTable("receptury_meta", fieldData);
			
			fieldData.clear();

			fieldData.add(new FieldData("idTermin", "integer", null));
			fieldData.add(new FieldData("customerID", "integer", null));
			fieldData.add(new FieldData("v_date", "text", null));
			fieldData.add(new FieldData("text", "text", null));
			fieldData.add(new FieldData("user_ID", "text", null));
			fieldData.add(new FieldData("duration", "integer", "30"));
			fieldData.add(new FieldData("isRealized", "text", "no"));

			updateTable("terminarz", fieldData);
			
			fieldData.clear();
			
			fieldData.add(new FieldData("name", "text", null));
			fieldData.add(new FieldData("surname", "text", null));
			fieldData.add(new FieldData("avatar", "blob", null));
			fieldData.add(new FieldData("permissions", "text", null));
			fieldData.add(new FieldData("is_admin", "integer", null));
			fieldData.add(new FieldData("pass", "text", ""));
			fieldData.add(new FieldData("code", "text", "ADMIN"));
			fieldData.add(new FieldData("status", "text", "AKTYWNY"));

			updateTable("users", fieldData);
			
			fieldData.clear();
			
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("priority", "integer", null));
			fieldData.add(new FieldData("key", "text", null));
			fieldData.add(new FieldData("value", "text", null));
			fieldData.add(new FieldData("parent", "text", null));
			
			updateTable("vars", fieldData);
			
			fieldData.clear();
			
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("nr_kasowy", "text", null));
			fieldData.add(new FieldData("nazwa", "text", null));
			fieldData.add(new FieldData("czas", "integer", "30"));
			fieldData.add(new FieldData("cena", "float", null));
			fieldData.add(new FieldData("order_by", "integer", "2"));
			
			updateTable("zabieg", fieldData);
			
			testUsersMeta();
			
			fieldData.add(new FieldData("id", "integer", null));
			fieldData.add(new FieldData("priority", "integer", null));
			fieldData.add(new FieldData("key", "text", null));
			fieldData.add(new FieldData("value", "text", null));
			fieldData.add(new FieldData("parent", "text", null));
			fieldData.add(new FieldData("userID", "integer", null));
			
			updateTable("users_meta", fieldData);
			
			updateVersionData();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	/**
	 * ��czy si� z baz� danych w celub pobrania aktualnej wersji bazy danych
	 * przechowywanej w tabeli vars ze specjalnymi zmiennymi
	 * @return wersj� utworzonej bazy danych jako String
	 * @throws SQLException
	 */
	public static String getVersionData() throws SQLException {
		
		Connection connect = null;
		String version = "";
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM vars " +
							   "WHERE key = 'versionDatabase'";
			
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				version = rs.getString("value");
			}
			else
				version = null;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		return version;
		
	}
	
	public static void updateVersionData() throws SQLException, ClassNotFoundException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			if(getVersionData()==null) {
				setVars("versionDatabase", ConfigureApp.versionDatabase);
			}
			else {
				String s = "UPDATE vars SET value='" + ConfigureApp.versionDatabase + "' " + 
				   "WHERE key = 'versionDatabase'";
		
				PreparedStatement prepare;
				prepare = connect.prepareStatement(s);
				prepare.executeUpdate();
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			setVars("versionDatabase", ConfigureApp.versionDatabase);
			
		} catch (SQLException e) {
			
			setVars("versionDatabase", ConfigureApp.versionDatabase);
		} finally {
			connect.close();
		}
		
	}

	/**
	 *
	 * Uniwersalna metoda tworz�ca tabele w bazie danych
	 *
	 * @param table nazwa tworzonej tabeli
	 * @param list lista p�l potrzebnych do stworzenia tabeli
	 * @throws SQLException
	 *
	 * TO-DO: zastanowi� si�, czy aktualizacja rzeczywi�cie przebiega prawid�owo - co je�li
	 * jaka� tabela istnieje, ale nie ze wszystkimi polami (jest ich mniej, wi�cej?)
	 */
	
	private static void updateTable(String table, List<FieldData> list) throws SQLException {
		
		Connection connect = null;
		
		try {

			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = "PRAGMA table_info(" + table + ")";
			rs = stat.executeQuery(statement);
			
			int i = 0;
			Boolean isThere = false;
			
			while(rs.next()) {
				String name = rs.getString("name");
				while(i<list.size() && !isThere) {
					
					if(name.equals(list.get(i).getName())) {
						isThere = true;
						list.remove(i);
						System.out.println("Usuwamy " + name);
					}
					
					i++;
				}

				i = 0;
				isThere = false;
				
			}
			
			if(list.size()>0) {
				
					for(FieldData f : list) {
						PreparedStatement prepare;
						
						statement = "ALTER TABLE " + table + " ADD COLUMN '" +
									f.getName() + "' " + f.getType() + " DEFAULT '" + f.getDflt() + "'";
						
						prepare = connect.prepareStatement(statement);
						prepare.executeUpdate();
						
					}

			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
			connect.close();
			
		}
		
		
	}
	
	public static Boolean testUsersMeta() throws SQLException, ClassNotFoundException {
		
		Connection connect = null;
		Boolean check = false;
		
		
		
		
		try {
			Class.forName("org.sqlite.JDBC");
			
			
			
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
			
			
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT COUNT(1) FROM users";

			ResultSet rs = stat.executeQuery(statement);
			
			check = true;
			
		} catch (ClassNotFoundException e) {
			createUsersMeta();
			System.out.println("Nie tabeli users_meta");
			check = true;
		} catch (SQLException e) {
			createUsersMeta();
			check = true;
		} finally {
			
			connect.close();
		}
		
		return check;
	}

	/**
	 * sprawdza czy istnieje zadana baza danych
	 * je�li nie istnieje, tworzona jest baza wed�ug zadanej struktury
	 *
	 * TO-DO: pobranie nazwy bazy danych w pliku konfiguracyjnym
	 */
	
	public static void testDatabase() throws SQLException, ClassNotFoundException {
		
		Connection connect = null;
		
		path = System.getenv("APPDATA") + "\\faIT\\BestBeauty\\BF";
		System.out.println(path);
		
		try {
			Class.forName("org.sqlite.JDBC");
			
			boolean files = new File(path).mkdirs();
			
			path = path + "\\baza.db";
			
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);

			String statement = "SELECT COUNT(1) FROM users_meta";
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = stat.executeQuery(statement);
			
		} catch (ClassNotFoundException e) {
			createUsersMeta();
		} catch (SQLException e) {
			createDatabase();
		} finally {
			connect.close();
		}
	}

	/**
	 * TO-DO: zmieni� nazwy metod i p�l w bazach na agnielskie
	 */
	private static void createDatabase() {
		
		
		
		
		try {
			
			createKlienci();
			createKlienciMeta();
			createListaZabiegow();
			createPomoc();
			createReceptury();
			createRecepturyMeta();
			createTerminarz();
			createUsers();
			createUsersMeta();
			createVars();
			createZabieg();
			
			
		} catch (ClassNotFoundException e) {
			
			System.out.println("Nie ma bazy b�d� jest uszkodzona");
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			System.out.println("B��d zapytania przy tworzeniu bazy");
			
		} 
		
		
	}
		
	private static void createKlienci() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE klienci " +
										  "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										  "name TEXT," +
										  "surname TEXT," +
										  "phone TEXT," +
										  "email TEXT DEFAULT '-'," +
										  "status TEXT DEFAULT 'normal'," +
										  "avatar BLOB, " +
										  "user_display text," +
										  "birthday text," +
										  "city text)");
		prepare.executeUpdate();
		
	}
	
	private static void createUsersMeta() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE users_meta " +
										   "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "priority integer," +
										   "key TEXT," +
										   "value TEXT," +
										   "parent TEXT," +
										   "userID integer REFERENCES users (code))");
		prepare.executeUpdate();
		
		
	}
	
	private static void createKlienciMeta() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE klienci_meta " +
										   "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "priority integer," +
										   "key TEXT," +
										   "value TEXT," +
										   "parent TEXT," +
										   "customerID integer REFERENCES klienci (id))");
		prepare.executeUpdate();
		
	}
	
	private static void createListaZabiegow() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE lista_zabiegow " +
										   "(idRelacji integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "idTermin integer NOT NULL," +
										   "idZabieg integer NOT NULL)");
		prepare.executeUpdate();
		
	}
	
	private static void createPomoc() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE pomoc " +
										   "(panelName integer PRIMARY KEY UNIQUE," +
										   "content text)");
		prepare.executeUpdate();
		
		List<Help> listHelp = ServiceData.setHelp("resources/data/help.txt");
		Iterator<Help> it = listHelp.iterator();
		while(it.hasNext()) {
			
			Help h = it.next();
			
			System.out.println("H - " + h);
			
			prepare = connect.prepareStatement("INSERT INTO pomoc (panelName, content) " +
			   								   "VALUES ((?), (?))");
			prepare.setInt(1, h.getName());
			prepare.setString(2, h.getContent());
			prepare.executeUpdate();
			
		}
		
		
		
	}
	
	private static void createReceptury() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE receptury " +
										   "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "content TEXT," +
										   "idRelacji integer REFERENCES lista_zabiegow (idRelacji)," +
										   "cena FLOAT," +
										   "czas INTEGER)");
		prepare.executeUpdate();
		
	}
	
	private static void createRecepturyMeta() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE receptury_meta " +
										   "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "priority integer, " +
										   "key text, " +
										   "value text, " +
										   "parent text, " +
										   "receptID integer REFERENCES receptury (id))");
		prepare.executeUpdate();
		
	}
	
	private static void createTerminarz() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE terminarz " +
										   "(idTermin integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "customerID integer REFERENCES klienci(id)," +
										   "v_date text," +
										   "'text' text," +
										   "user_ID text," +
										   "duration integer DEFAULT 30," +
										   "isRealized text DEFAULT no)");
		prepare.executeUpdate();
		
	}
	
	private static void createUsers() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE users " +
										   "(name TEXT," +
										   "surname TEXT," +
										   "avatar BLOB," +
										   "permissions TEXT," +
										   "is_admin integer," +
										   "pass text DEFAULT ''," +
										   "code text PRIMARY KEY UNIQUE DEFAULT 'ADMIN'," +
										   "status text DEFAULT 'AKTYWNY')");
		prepare.executeUpdate();
		
		
		prepare = connect.prepareStatement("INSERT INTO users (name, surname, is_admin, code) " +
										   "VALUES ((?), (?), (?), (?))");
		prepare.setString(1, "U�ytkownik");
		prepare.setString(2, "Domy�lny");
		prepare.setInt(3, 0);
		prepare.setString(4, "DOM");
		
		prepare.executeUpdate();
		
		
	}
	
	private static void createZabieg() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE zabieg " +
										   "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
										   "nr_kasowy TEXT," +
										   "nazwa TEXT," +
										   "czas INTEGER DEFAULT 30, " +
										   "cena FLOAT, " +
										   "order_by INTEGER )");
		prepare.executeUpdate();
		
		
		prepare = connect.prepareStatement("INSERT INTO zabieg (nazwa, nr_kasowy, czas, order_by) " +
										   "VALUES ((?), (?), (?), (?))");
		prepare.setString(1, "15 min");
		prepare.setString(2, "t1");
		prepare.setInt(3, 15);
		prepare.setInt(4, 1);
		prepare.executeUpdate();
		
		prepare = connect.prepareStatement("INSERT INTO zabieg (nazwa, nr_kasowy, czas, order_by) " +
		   		 						   "VALUES ((?), (?), (?), (?))");
		prepare.setString(1, "30 min");
		prepare.setString(2, "t2");
		prepare.setInt(3, 30);
		prepare.setInt(4, 1);
		prepare.executeUpdate();
		
		prepare = connect.prepareStatement("INSERT INTO zabieg (nazwa, nr_kasowy, czas, order_by) " +
			   							   "VALUES ((?), (?), (?), (?))");
		prepare.setString(1, "45 min");
		prepare.setString(2, "t3");
		prepare.setInt(3, 45);
		prepare.setInt(4, 1);
		prepare.executeUpdate();     
		
		prepare = connect.prepareStatement("INSERT INTO zabieg (nazwa, nr_kasowy, czas, order_by) " +
		   								   "VALUES ((?), (?), (?), (?))");
		prepare.setString(1, "60 min");
		prepare.setString(2, "t4");
		prepare.setInt(3, 60);
		prepare.setInt(4, 1);
		prepare.executeUpdate();
		
	}

	/**
	 * Tworzy tabel� vars -
	 * TO-DO: dane powinny by� pobrane z zewn�trznego pliku konfiguracyjnego
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	
	private static void createVars() throws ClassNotFoundException, SQLException {
		
		Class.forName("org.sqlite.JDBC");
		Connection connect = DriverManager.getConnection("jdbc:sqlite:" + path);
		
		PreparedStatement prepare;
		prepare = connect.prepareStatement("CREATE TABLE vars " +
										   "(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
										   "priority integer," +
										   "key TEXT," +
										   "value TEXT," +
										   "parent TEXT)");
		
		
		
		prepare.executeUpdate();
		
		String temp = "w";
		
		for(int i=1; i<=7; i++) {
			
			if(i>1)
				temp = "p";
			setVars("daywork",i + ";8;18;" + temp);
			
		}
		
		setVars("versionDatabase", ConfigureApp.versionDatabase);
		
	}
	
	public static Customer addCustomer(Customer c, String user_display) throws SQLException {
		
		Connection connect = null;
		Customer cc = null;
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO klienci (name, surname, phone, email, birthday, city, user_display) " +
											   "VALUES ((?), (?), (?), (?), (?), (?), (?))");
			prepare.setString(1, c.getName());
			prepare.setString(2, c.getSurname());
			prepare.setString(3, c.getNumber());
			prepare.setString(4, c.getEmail());
			prepare.setString(5, c.getBirthday());
			prepare.setString(6, c.getCity());
			prepare.setString(7, user_display);
			prepare.executeUpdate();
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = "SELECT * FROM klienci ORDER BY id DESC LIMIT 1";
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				cc = new Customer(rs.getString("name"), rs.getString("surname"),
								  rs.getString("phone"), rs.getString("email"), 
								  rs.getString("birthday"), rs.getString("city"),
								  c.getNatural(), c.getGrey(),
								  rs.getInt("id"));
			}
			
			if(!c.getNatural().equals("")) {
				prepare = connect.prepareStatement("INSERT INTO klienci_meta (key, value, customerID) " +
				   "VALUES ((?), (?), (?))");
				prepare.setString(1, "natural");
				prepare.setString(2, c.getNatural());
				prepare.setInt(3, cc.getId());
				prepare.executeUpdate();
			}
			
			
			if(!c.getGrey().equals("")) {
				prepare = connect.prepareStatement("INSERT INTO klienci_meta (key, value, customerID) " +
				   "VALUES ((?), (?), (?))");
				prepare.setString(1, "siwe");
				prepare.setString(2, c.getGrey());
				prepare.setInt(3, cc.getId());
				prepare.executeUpdate();
			}
			
			if(!c.getCondition().equals("")) {
				prepare = connect.prepareStatement("INSERT INTO klienci_meta (key, value, customerID) " +
				   "VALUES ((?), (?), (?))");
				prepare.setString(1, "kondycja");
				prepare.setString(2, c.getCondition());
				prepare.setInt(3, cc.getId());
				prepare.executeUpdate();
			}
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		System.out.println("Customer cc: " + cc.getId());
		return cc;
		
	}
	
	public static Formula setFormula(String content, int idTreats, float price, int time) throws SQLException {
		
		Connection connect = null;
		Formula f = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO receptury (content, idRelacji, cena, czas) " +
											   "VALUES ((?), (?), (?), (?))");
			prepare.setString(1, content);
			prepare.setInt(2, idTreats);
			prepare.setFloat(3, price);
			prepare.setInt(4, time);
			prepare.executeUpdate();
			
			//wybranie ostatnio dodanej receptury
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = "SELECT * FROM receptury ORDER BY id DESC LIMIT 1";
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				int id = rs.getInt("id");
				f = new Formula(rs.getInt("id"), rs.getString("content"), rs.getInt("idRelacji"), 
								rs.getFloat("cena"), rs.getInt("czas"));
			}
			
			//setRecepturaMeta("userReceptura", f.getUser(), id);
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return f;
	}
	
	public static void setRecepturaMeta(String key, String value, int idRec) throws SQLException {
		
		
		Connection connect = null;

		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO receptury_meta (key, value, " +
											   "receptId) " +
											   "VALUES ((?), (?), (?))");
			prepare.setString(1, key);
			prepare.setString(2, value);
			prepare.setInt(3, idRec);
			prepare.executeUpdate();
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static Boolean isAnyUser() throws SQLException {
		
		Boolean isThere = false;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT COUNT(1) FROM users";
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				
				int i = rs.getInt(1);
				System.out.println("Ilo�� rekord�w w users: " + i);
				if(i>1) {
					isThere = true;
				}
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return isThere;
		
	}
	
	public static void addAdmin(User u) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO users (name, pass, is_admin) " +
											   "VALUES ((?), (?), (?))");
			prepare.setString(1, u.getLogin());
			prepare.setString(2, u.getPass());
			prepare.setInt(3, u.getIsAdmin());
			prepare.executeUpdate();
			
			
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void updateNameCustomer(String name, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci SET name='" + name + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateSurnameCustomer(String surname, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci SET surname='" + surname + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateNumberCustomer(String number, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci SET phone='" + number + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateFieldCustomer(String text, int id, String field) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci SET " +  field + " = '" + text + "' " +
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateFieldVisit(String text, int id, String field) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE terminarz SET " +  field + " = '" + text + "' " +
					   "WHERE idTermin = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateListTreat(int idRelation, int idTreat) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE lista_zabiegow SET idZabieg=" + idTreat + " " + 
					   "WHERE idRelacji = " + idRelation;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	
	public static void updateNaturalCustomer(String natural, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci_meta SET value='" + natural + "' " + 
					   "WHERE customerID = " + id + 
					   " AND key='natural'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateGreyCustomer(String grey, int id) {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci_meta SET value='" + grey + "' " + 
					   "WHERE customerID = " + id + 
					   " AND key='siwe'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void updateConditionCustomer(String condition, int id) {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci_meta SET value='" + condition + "' " + 
					   "WHERE customerID = " + id + 
					   " AND key='kondycja'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void updateUserDisplay(String user_display, int id) {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci SET user_display='" + user_display + "' " + 
					   "WHERE id = " + id;
					   
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static void updateContent(String content, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE receptury SET content='" + content + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateFormula(String content, float price, int time, int id) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE receptury SET content='" + content + "', " + 
					   "cena = " + price + ", czas = " + time + " " +
					   "WHERE idRelacji = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}

	
	public static List<Customer> searchCustomer(String s, int customer, String search) throws SQLException {
		
		List<Customer> list = new ArrayList<Customer>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM klienci " +
							   search +
							   "ORDER BY lower(surname) " + 
							   "LIMIT " + customer;
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				
				list.add(new Customer(rs.getString("name"), rs.getString("surname"), 
									  rs.getString("phone"), rs.getInt("id")));
			}
			
			System.out.println(list);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static List<Customer> searchCustomer(String s, String search) throws SQLException {
		
		List<Customer> list = new ArrayList<Customer>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM klienci " +
							   search +
							   "ORDER BY lower(surname) ";
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				
				list.add(new Customer(rs.getString("name"), rs.getString("surname"), 
									  rs.getString("phone"), rs.getString("email"), 
									  rs.getString("birthday"), rs.getString("city"),
									  rs.getInt("id")));
			}
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	
	
	public static Hair getHair(int id) throws SQLException {
		
		Hair h = null;
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * FROM klienci_meta " +
							   "WHERE key = 'natural' " + 
							   "AND customerID = " + id;
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			String nat = "";
			String value = "";
			String condition = "";
			
			if(rs.next()) {
				value = rs.getString("value");
			}
			
			statement = "SELECT * FROM klienci_meta " +
			   			"WHERE key = 'siwe' " + 
			   			"AND customerID = " + id;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				nat = rs.getString("value");
				
			}
			
			statement = "SELECT * FROM klienci_meta " +
   			"WHERE key = 'siwe' " + 
   			"AND customerID = " + id;

			rs = stat.executeQuery(statement);

			if(rs.next()) {
				nat = rs.getString("value");
	
			}
			
			statement = "SELECT * FROM klienci_meta " +
   			"WHERE key = 'kondycja' " + 
   			"AND customerID = " + id;

			rs = stat.executeQuery(statement);

			if(rs.next()) {
				condition = rs.getString("value");
	
			}
			
			h = new Hair(value, nat, condition);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		return h;
		
	}
	
	public static List<Formula> getFormulasList(int id) throws SQLException {
		
		List<Formula> list = new ArrayList<Formula>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT A.id, A.price, A.content, A.time, A.date, B.value " +
							   "FROM receptury A " +
							   "INNER JOIN receptury_meta B " +
							   "ON A.customerID = " + id + " AND A.id = B.receptID " + 
							   "ORDER BY A.id DESC " + 
							   "LIMIT 3" +
							   "WHERE customerID = " + id + " " +
							   "ORDER BY id DESC " +
							   "LIMIT 3";
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				
				list.add(new Formula(rs.getInt("id"), rs.getString("content"),
									 Float.parseFloat(rs.getString("price")),
						             Integer.parseInt(rs.getString("time"))));
				
			}
			
			System.out.println(list);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static User getAdmin(String name) throws SQLException {
		
		User u = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM users " +
							   "WHERE name = '" + name + "' " + 
							   "AND is_admin = 1";
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				u = new User(rs.getString("name"), rs.getString("pass"));
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return u;
		
	}
	
	public static User addUser(String name, String surname, String code, byte[] bytes, String status) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO users (name, surname, " +
											   "is_admin, code, avatar, status) " +
											   "VALUES ((?), (?), (?), (?), (?), (?))");
			prepare.setString(1, name);
			prepare.setString(2, surname);
			prepare.setInt(3, 0);
			prepare.setString(4, code);
			prepare.setBytes(5, bytes);
			prepare.setString(6, status);
			prepare.executeUpdate();
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = "SELECT * FROM users ORDER BY rowid DESC LIMIT 1";
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				u = new User(rs.getString("name"), rs.getString("surname"),
							 rs.getString("code"), rs.getBytes("avatar"), rs.getString("status"));
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			u = null;
			//Communication comm = new Communication(MainWindow.getFrame());
			
		} finally {
			connect.close();
		}
		
		return u;
		
	}
	
	public static User addUser(String name, String surname, String code, byte[] bytes) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO users (name, surname, " +
											   "is_admin, code, avatar) " +
											   "VALUES ((?), (?), (?), (?), (?))");
			prepare.setString(1, name);
			prepare.setString(2, surname);
			prepare.setInt(3, 0);
			prepare.setString(4, code);
			prepare.setBytes(5, bytes);
			prepare.executeUpdate();
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = "SELECT * FROM users ORDER BY rowid DESC LIMIT 1";
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				u = new User(rs.getString("name"), rs.getString("surname"),
							 rs.getString("code"), rs.getBytes("avatar"), rs.getString("status"));
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			u = null;
			//Communication comm = new Communication(MainWindow.getFrame());
			
		} finally {
			connect.close();
		}
		
		return u;
		
	}
	
	public static void changePass(User user) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("UPDATE users " +
											   "SET pass = '" + user.getPass() + "' " + 
											   "WHERE name = '" + user.getLogin() + "'");
			
			prepare.executeUpdate();
			
			
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		

		
	}
	
	public static List<User> getUsers(String action) throws SQLException {
		
		List<User> list = new ArrayList<User>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = null;
			
			statement = "SELECT * FROM users " +
						"WHERE is_admin!=1 " + action + 
						"ORDER BY surname";
			
			/*if(action.equals("SAWF")) {
				statement = "SELECT * FROM users " +
							"WHERE is_admin!=1 AND status!='ZWOLNIONY' " +
							"ORDER BY userID";
			}
			else if(action.equals("SAU")) {
				statement = "SELECT * FROM users " + 
							"WHERE is_admin!=1 AND status='AKTYWNY' " +
							"ORDER BY userID";
			}*/
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				 list.add(new User(rs.getString("name"), rs.getString("surname"),
							 rs.getString("code"), rs.getBytes("avatar"), rs.getString("status")));
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static String checkNumberUser(String c) throws SQLException {
		
		String code = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = null;
			
			statement = "SELECT * FROM users " +
						"WHERE code ='" + c + "'" ;
						
			
			/*if(action.equals("SAWF")) {
				statement = "SELECT * FROM users " +
							"WHERE is_admin!=1 AND status!='ZWOLNIONY' " +
							"ORDER BY userID";
			}
			else if(action.equals("SAU")) {
				statement = "SELECT * FROM users " + 
							"WHERE is_admin!=1 AND status='AKTYWNY' " +
							"ORDER BY userID";
			}*/
			
			rs = stat.executeQuery(statement);
			
			if(rs.next())
				code = rs.getString("code");
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return code;
		
	}
	
	public static String getAdminName(String code) throws SQLException {
		
		String name = "";
		
		Connection connect = null;
		
		System.out.println("Code, kt�ry otrzymales: " + code);
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = null;
			
			statement = "SELECT name FROM users " +
						"WHERE code = '" + code + "'";
			
			/*if(action.equals("SAWF")) {
				statement = "SELECT * FROM users " +
							"WHERE is_admin!=1 AND status!='ZWOLNIONY' " +
							"ORDER BY userID";
			}
			else if(action.equals("SAU")) {
				statement = "SELECT * FROM users " + 
							"WHERE is_admin!=1 AND status='AKTYWNY' " +
							"ORDER BY userID";
			}*/
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				name = rs.getString("name");
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		return name;
		
	}
	
	public static User getUser(String code) throws SQLException {
		
		User user = null;
		Connection connect = null;
		
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			
			
			java.sql.Statement stat = connect.createStatement();
			ResultSet rs = null;
			String statement = null;
			
			statement = "SELECT * FROM users " +
						"WHERE code = '" + code + "'";
			
			/*if(action.equals("SAWF")) {
				statement = "SELECT * FROM users " +
							"WHERE is_admin!=1 AND status!='ZWOLNIONY' " +
							"ORDER BY userID";
			}
			else if(action.equals("SAU")) {
				statement = "SELECT * FROM users " + 
							"WHERE is_admin!=1 AND status='AKTYWNY' " +
							"ORDER BY userID";
			}*/
			
			rs = stat.executeQuery(statement);
			while(rs.next()) {
				user = new User(rs.getString("name"),
								rs.getString("surname"), rs.getString("code"), 
								rs.getBytes("avatar"), rs.getString("status"));
			}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return user;
		
	}
	
	
	
	public static void deleteUser(String code) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("UPDATE users SET status='ZWOLNIONY' " +
											   "WHERE code = '" + code + "'");
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void deleteTreat(String number) throws SQLException {
		
		Connection connect = null;
		
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM zabieg WHERE nr_kasowy='" + number + "'");
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void deletListRelation(int idRelation) throws SQLException {
		
		Connection connect = null;
		
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM lista_zabiegow " +
											   "WHERE idRelacji = " + idRelation);
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void deleteVisit(int id) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			System.out.println("Usuwamy wizyt� z bazy...");
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM terminarz WHERE idTermin=" + id);
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void deleteCustomer(int id) throws SQLException {
		
		Connection connect = null;
		
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM klienci " +
											   "WHERE id = " + id + " ");								   
			prepare.executeUpdate();
			
			prepare = connect.prepareStatement("DELETE FROM klienci_meta " + 
					   						   "WHERE customerID = " + id);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void deleteFormula(int id) throws SQLException {
		
		Connection connect = null;
		
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM receptury WHERE idRelacji =" + id);
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void dropUser(String code) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("DELETE FROM users " +
											   "WHERE code = '" + code + "'");
			prepare.executeUpdate();
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void updateUser(User userOld, User userNew) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			ServiceData service = new ServiceData();
			
				
				if(!userOld.getName().equals(userNew.getName())) {	
					prepare = connect.prepareStatement("UPDATE users " +
													   "SET name=' " + userNew.getName() + "' " +
													   "WHERE code = '" + userOld.getCode() +"'");
					prepare.executeUpdate();
				}
				if(!userOld.getSurname().equals(userNew.getSurname())) {	
					prepare = connect.prepareStatement("UPDATE users " +
													   "SET surname=' " + userNew.getSurname() + "' " +
												   	   "WHERE code = '" + userOld.getCode() + "'");
					prepare.executeUpdate();
				
				}
				if(!userOld.getStatus().equals(userNew.getStatus())) {	
					if(userNew.getStatus().equals("AKTYWNY")) {
						service.updateUserMeta(userNew.getCode());
						ConfigureApp.updateSession(userNew);
					}
					prepare = connect.prepareStatement("UPDATE users " +
													   "SET status=' " + userNew.getStatus() + "' " +
												   	   "WHERE code = '" + userOld.getCode() +"'");
					prepare.executeUpdate();
				
				}
				if(!userOld.getCode().equals(userNew.getCode())) {	
					prepare = connect.prepareStatement("UPDATE users " +
													   "SET code=' " + userNew.getCode() + "' " +
												   	   "WHERE code = '" + userOld.getCode() +"'");
					prepare.executeUpdate();
				
				}
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static String getTextHtml(Integer id) throws SQLException {
		
		String text = null;
		Connection connect = null;
		
		try {
			
			System.out.println("ID: " + id);
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT content " +
							   "FROM pomoc " +
							   "WHERE panelName =" +  id;
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				text = rs.getString("content");
			}
			
			System.out.println("Text: " + text);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return text;
		
	}
	
	public static int addDate(int idC, String date, String text, String code, int time, String isRealized) throws SQLException {
		
		Connection connect = null;
		int id = 0;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			
				if(time==0)
					time = 30;
			
				prepare = connect.prepareStatement("INSERT INTO terminarz (customerID, v_date, " +
						"text, user_ID, duration, isRealized) " +
				"VALUES ((?), (?), (?), (?), (?), (?))");
				prepare.setInt(1, idC);
				prepare.setString(2, date);
				prepare.setString(3, text);
				prepare.setString(4, code);
				prepare.setInt(5, time);
				prepare.setString(6, isRealized);
			
			
				prepare.executeUpdate();
			
				java.sql.Statement stat = connect.createStatement();
				String statement = "SELECT idTermin " +
								   "FROM terminarz " +
								   "WHERE v_date = '" + date + "' " +
								   "AND customerID = " + idC + " " +  
								   "AND user_ID = '" + code + "' " +
								   "ORDER BY idTermin " + 
								   "LIMIT 1";
				
				ResultSet rs = stat.executeQuery(statement);	
				if(rs.next())
					id = rs.getInt("idTermin");
				
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		return id;
		
	}
	
	public static int addListTreat(int idDate, int idTreat) throws SQLException {
		
		Connection connect = null;
		int id = 0;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			System.out.println("idTreat: " + idTreat);
			
			PreparedStatement prepare;
			
				if(idTreat==0) {
					prepare = connect.prepareStatement("INSERT INTO lista_zabiegow (idTermin, idZabieg) " +
													   "VALUES ((?), (?))");
					prepare.setInt(1, idDate);
					prepare.setInt(2, 2);
				}
				else {
					prepare = connect.prepareStatement("INSERT INTO lista_zabiegow (idTermin, idZabieg) " +
					   "VALUES ((?), (?))");
					prepare.setInt(1, idDate);
					prepare.setInt(2, idTreat);
				}
			
				prepare.executeUpdate();
				
				String statement = "SELECT * " +
				   				   "FROM lista_zabiegow " +
				   				   "ORDER BY idRelacji DESC " +
				   				   "LIMIT 1";

				ResultSet rs = null;
				java.sql.Statement stat = connect.createStatement();
				rs = stat.executeQuery(statement);
				
				if(rs.next()) {
					id = rs.getInt("idRelacji");
				}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		return id;
		
	}
	
	public static List<Visit> getListVisit(String user, String date) throws SQLException {
		
		List<Visit> list = new ArrayList<Visit>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			System.out.println("Wybieramy z bazy wizyty z dat�: " + date);
			
			String statement = "SELECT * " +
							   "FROM terminarz " +
							   "WHERE user_ID = '" + user + "' " +
							   "AND v_date LIKE '" + date + " %'";
			
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				list.add(new Visit(rs.getInt("idTermin"), rs.getInt("customerID"),
						 rs.getString("v_date"), rs.getString("text"), rs.getString("user_ID"),
						 rs.getInt("duration"), rs.getString("isRealized")));
			}
			
			Collections.sort(list);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static Boolean isExistVisit(int idVisit) throws SQLException {
		
		Boolean isExist = false;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM terminarz " +
							   "WHERE idTermin = " + idVisit;
							  
			
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				isExist = true;
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return isExist;
		
	}
	
	public static List<Visit> getListVisit(int id, String date) throws SQLException {
		
		List<Visit> list = new ArrayList<Visit>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM terminarz " +
							   "WHERE customerID = '" + id + "' ";
			
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				list.add(new Visit(rs.getInt("idTermin"), rs.getInt("customerID"),
						 rs.getString("v_date"), rs.getString("text"), rs.getString("user_ID"),
						 rs.getInt("duration"), rs.getString("isRealized")));
			}
			
			Collections.sort(list);
			
			System.out.println("Lista" + list);
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static List<Visit> getListVisit(int id) throws SQLException {
		
		List<Visit> list = new ArrayList<Visit>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM terminarz " +
							   "WHERE customerID = '" + id + "' ";
			
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				list.add(new Visit(rs.getInt("idTermin"), rs.getInt("customerID"),
						 rs.getString("v_date"), rs.getString("text"), rs.getString("user_ID"),
						 rs.getInt("duration"), rs.getString("isRealized")));
			}
			
			//Collections.sort(list);
			//Collections.reverse(list);
			
			System.out.println("Lista" + list);
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static void setCustomerMeta(String key, String value, int id) throws SQLException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO klienci_meta (key, value, customerID) " +
											   "VALUES ((?), (?), (?))");
			prepare.setString(1, key);
			prepare.setString(2, value);
			prepare.setInt(3, id);
			prepare.executeUpdate();
			
		
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static Note getNote(int customerID) throws SQLException {
		
		Note note = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT id, value, customerID " +
							   "FROM klienci_meta " +
							   "WHERE customerID = " + customerID + " " + 
							   "AND key = 'note'";
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				note = new Note(rs.getInt("id"), 
								rs.getString("value"), rs.getInt("customerID"));
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return note;
		
	}
	
	public static void updateCustomerMeta(int id, String value) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			System.out.println("Warto��: " + value + " id: " + id);
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE klienci_meta SET value='" + value + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static Customer getCustomer(int id) throws SQLException {
		
		Customer customer=null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM klienci " +
							   "WHERE id = " + id;
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				
				customer = new Customer(rs.getString("name"), rs.getString("surname"),
										rs.getString("phone"), rs.getString("email"), 
										rs.getString("birthday"), rs.getString("city"),
										rs.getInt("id"));
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return customer;
		
	}
	
	public static Customer checkNumber(String number) throws SQLException {
		
		Customer customer=null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM klienci " +
							   "WHERE phone = '" + number + "'";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				
				customer = new Customer(rs.getString("name"), rs.getString("surname"),
										rs.getString("phone"), rs.getInt("id"));
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return customer;
		
	}
	
	public static Customer checkClient(String phone, String surname, String name) throws SQLException {
		
		Customer customer=null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM klienci " +
							   "WHERE surname = '" + surname + "' " +
							   "AND name = '" + name + "' ";
							   
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				
				customer = new Customer(rs.getString("name"), rs.getString("surname"),
										rs.getString("phone"), rs.getString("email"),
										rs.getString("birthday"), rs.getString("city"),
										rs.getInt("id"));
				
				
				
				if(!customer.getNumber().equals(phone) && (customer.equals("-") || customer.equals("")))
					customer = null;
				else {
					System.out.println("Pobieramy w�osy...");
					customer.setHair(DataBase.getHair(customer.getId()));
					
				}
			}
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return customer;
		
	}
	
	public static List<DayWork> getDayWork() throws SQLException, ClassNotFoundException {
		
		List<DayWork> list = new ArrayList<DayWork>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			String statement = "SELECT * " +
							   "FROM vars " +
							   "WHERE key = 'daywork'";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				list.add(new DayWork(rs.getInt("id"), rs.getString("value")));
			}
			
			
			
		} catch (SQLException e) {
			System.out.println("Nie ma tabeli vars...");
			createVars();
			list = getDayWork();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static String getWork(String value, int day) throws SQLException, ClassNotFoundException {
		
		String work = "";
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT value " +
							   "FROM vars " +
							   "WHERE key = 'daywork' AND value LIKE '" + day + ";%'";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				work = rs.getString("value");
			}
			
			
			
		} catch (SQLException e) {
			
			System.out.println("B��d ze znalezieniem worka w tabeli vars...");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return work;
		
	}
	
	public static List<Treatment> getTreatments() throws SQLException, ClassNotFoundException {
		
		List<Treatment> list = new ArrayList<Treatment>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			list.add(new Treatment(0));
			
			String statement = "SELECT * " +
							   "FROM zabieg ";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				list.add(new Treatment(rs.getString("nr_kasowy"), rs.getString("nazwa"), 
						 rs.getInt("czas"), rs.getFloat("cena"), rs.getInt("order_by"), rs.getInt("id")));
			}
			
			
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static List<Treatment> getTreatments(String where) throws SQLException, ClassNotFoundException {
		
		List<Treatment> list = new ArrayList<Treatment>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
		
			
			String statement = "SELECT * " +
							   "FROM zabieg " + where;
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			
			
			while(rs.next()) {

				list.add(new Treatment(rs.getString("nr_kasowy"), rs.getString("nazwa"), 
						 rs.getInt("czas"), rs.getFloat("cena"), rs.getInt("order_by"), rs.getInt("id")));
			}
			
			
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	public static Treatment getTreatment(String number) throws SQLException, ClassNotFoundException {
		
		Treatment treat = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT * " +
							   "FROM zabieg " +
							   "WHERE nr_kasowy = '" + number + "'";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				treat = new Treatment(rs.getString("nr_kasowy"), rs.getString("nazwa"), 
						 rs.getInt("czas"), rs.getFloat("cena"), rs.getInt("order_by"), rs.getInt("id"));
			}
			
			
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return treat;
		
	}
	
	public static Treatment getTreatmentFromList(int id) throws SQLException, ClassNotFoundException {
		
		Treatment treat = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT * " +
							   "FROM zabieg " +
							   "WHERE id = (" + 
							   				"SELECT idZabieg " + 
							   				"FROM lista_zabiegow " + 
							   				"WHERE idRelacji = " + id + ")";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				treat = new Treatment(rs.getString("nr_kasowy"), rs.getString("nazwa"), 
						 rs.getInt("czas"), rs.getFloat("cena"), rs.getInt("order_by"), rs.getInt("id"));
			}
			
			
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return treat;
		
	}
	
	public static Formula getFormulaFromList(int id) throws SQLException, ClassNotFoundException {
		
		Formula formula = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT * " +
							   "FROM receptury " +
							   "WHERE idRelacji = " + id;
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			System.out.println("idRelacji w DataBase: " + id);
			
			if(rs.next()) {
				formula = new Formula(rs.getInt("id"), rs.getString("content"), 
									  rs.getInt("idRelacji"), rs.getFloat("cena"), rs.getInt("czas"));
			}
			
			System.out.println("Formula: " + formula);
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return formula;
		
	}
	
	public static Treatment getTreatment(int number) throws SQLException, ClassNotFoundException {
		
		Treatment treat = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT * " +
							   "FROM zabieg " +
							   "WHERE id = " + number;
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				treat = new Treatment(rs.getString("nr_kasowy"), rs.getString("nazwa"), 
						 rs.getInt("czas"), rs.getFloat("cena"), rs.getInt("order_by"), rs.getInt("id"));
			}
			
			
			
		} catch (SQLException e) {
			
			//System.out.println("B��d ze znalezieniem zabiegu w tabeli zabiegi...");
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return treat;
		
	}
	
	public static void setVars(String key, String value) throws SQLException, ClassNotFoundException {
		
		Connection connect = null;
		User u = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO vars (key, value) " +
											   "VALUES ((?), (?))");
			prepare.setString(1, key);
			prepare.setString(2, value);
			prepare.executeUpdate();
			
		
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			
			System.out.println("Nie ma tabeli vars...");
			
			createVars();
			setVars(key, value);
			
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void updateVars(int id, String value) throws SQLException, ClassNotFoundException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE vars SET value='" + value + "' " + 
					   "WHERE id = " + id;
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Nie ma tabeli vars...");
			
			createVars();
			updateVars(id, value);
		} finally {
			connect.close();
		}
		
	}
	
	public static void changeUserInVisit(String oldCode, String newCode) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE terminarz SET user_ID='" + newCode + "' " + 
					   "WHERE user_ID = '" + oldCode + "'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateVisit(Visit visit) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE terminarz SET user_ID='" + visit.getUserCode() + "', " +
					   "v_date = '" +  visit.getDate() + "', " +
					   "text = '" + visit.getText() + "', " +  
					   "customerID = " + visit.getCustomerID() + " ";
			
			if(visit.getDuration()>0)  {
				s+= ", duration = " + visit.getDuration() + " ";
				
			}
			
			s+= "WHERE idTermin = '" + visit.getId() + "'";
			
			
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			List<Treatment> listTreat = visit.getTreatments();
			List<VisitTemp> listTemp = visit.getListTempVisit();
			
			int i = 0;
			
			while(i<listTemp.size() && i<listTreat.size()) {
				s = "UPDATE lista_zabiegow SET idZabieg = " + listTreat.get(i).getId() + 
					" WHERE idRelacji = " + listTemp.get(i).getId();
				prepare = connect.prepareStatement(s);
				prepare.executeUpdate();
				i++;
			}
			
			System.out.println("Listemp size: " + listTemp.size() + " i = " + i);
			System.out.println("Listreat size: " + listTreat.size() + " i = " + i);
			
			if(i<listTemp.size()) {
				
				while(i<listTemp.size()) {
				
					s = "DELETE FROM lista_zabiegow WHERE idRelacji = " + listTemp.get(i).getId();

					prepare = connect.prepareStatement(s);
					prepare.executeUpdate();
					i++;
				}
			}

			while(i<listTreat.size()) {
				
				System.out.println("Trzeba doda� list� relacji now�");
				
				DataBase.addListTreat(visit.getId(), listTreat.get(i).getId());
				i++;
			}
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void updateTreat(Treatment treat) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE zabieg SET nr_kasowy ='" + treat.getNumber() + "', " +
					   "nazwa = '" +  treat.getName() + "', " +
					   "czas = " + treat.getTime() + ", " +  
					   "cena = " + treat.getPrice() + " ";
			
		
			
			s+= "WHERE id = " + treat.getId();
			
			
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static void addTreat(Treatment t) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO zabieg (nr_kasowy, nazwa, czas, cena, order_by) " +
											   "VALUES ((?), (?), (?), (?), (?))");
			prepare.setString(1, t.getNumber());
			prepare.setString(2, t.getName());
			prepare.setInt(3, t.getTime());
			prepare.setFloat(4, t.getPrice());
			prepare.setInt(5, 1);
			prepare.executeUpdate();
			
			
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void setUserMeta(String code, String key, String value) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement("INSERT INTO users_meta (key, value, userID, priority) " +
											   "VALUES ((?), (?), (?), (?))");
			prepare.setString(1, key);
			prepare.setString(2, value);
			prepare.setString(3, code);
			prepare.setInt(4, 1);
			prepare.executeUpdate();
			
			
			
			
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static void updateUserMeta(String code, String key, String value) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			
			
			String s = "UPDATE users_meta SET value = ' " + value + "' " +
					   "WHERE userID = '" + code + "' AND priority=1 AND key = 'urlop'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			

			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		
	}
	
	public static String getUserMeta(String code) throws SQLException, ClassNotFoundException {
		
		String date = null;
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT value " +
							   "FROM users_meta " +
							   "WHERE userID = '" + code + "' AND priority = 1 AND key = 'urlop'";
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			if(rs.next()) {
				date = rs.getString("value");
			}
			
			
			
		} catch (SQLException e) {
			
			System.out.println("B��d ze znalezieniem urlopu...");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return date;
		
	}
	
	public static void updateUserMeta(String code) throws SQLException {
		
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			
			String s = "UPDATE users_meta SET priority=0 " +
					   "WHERE userID = '" + code + "' AND priority=1 AND key = 'urlop'";
			
			PreparedStatement prepare;
			prepare = connect.prepareStatement(s);
			prepare.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
	}
	
	public static List<VisitTemp> getListTempVisit(int id) throws SQLException, ClassNotFoundException {
		
		List<VisitTemp> list = new ArrayList<VisitTemp>();
		Connection connect = null;
		
		try {
			
			Class.forName("org.sqlite.JDBC");
			connect = DriverManager.getConnection("jdbc:sqlite:" + path);
			java.sql.Statement stat = connect.createStatement();
			
			
			
			String statement = "SELECT * " +
							   "FROM lista_zabiegow " +
							   "WHERE idTermin = " + id;
							  
			ResultSet rs = null;
			
			rs = stat.executeQuery(statement);
			
			while(rs.next()) {
				
				list.add(new VisitTemp(rs.getInt("idRelacji"), rs.getInt("idTermin"), 
									   rs.getInt("idZabieg")));
				
			}
			
			
			
		} catch (SQLException e) {
			
			System.out.println("B��d z tabel� lista zabieg�w");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		
		
		return list;
		
	}
	
	
}
