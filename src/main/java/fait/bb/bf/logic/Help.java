package fait.bb.bf.logic;

public class Help {
	
	int name;
	String content;
	
	Help(int name, String content) {
		
		this.name = name;
		this.content = content;
		
	}

	public int getName() {
		return name;
	}

	public String getContent() {
		return content;
	}
	
	public String toString() {
		return name + " " + content;
	}
	
}
