package fait.bb.bf.logic;

public class Hair {
	
	private String natural;
	private String grey;
	private String condition;
	
	public Hair(String natural, String grey, String condition) {
		super();
		this.natural = natural;
		this.grey = grey;
		this.condition = condition;
	}
	
	public String getNatural() {
		return natural;
	}
	
	public void setNatural(String natural) {
		this.natural = natural;
	}
	
	public String getGrey() {
		return grey;
	}
	
	public void setGrey(String grey) {
		this.grey = grey;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	

}
