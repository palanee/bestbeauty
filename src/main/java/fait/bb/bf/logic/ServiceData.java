package fait.bb.bf.logic;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;

import kryptografia.zad2.rsa.RSA;
import kryptografia.zad2.rsa.Texter;


import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.components.JHDresserBar;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.standard.HexString;
import fait.standard.ManageFiles;
import fait.standard.Numbers;

/**
 * Klasa obrabiaj�ca dane
 *
 * @author Paulina Miko�ajczyk
 */


public class ServiceData {
	
	private final static String keyApp = "0000-0000-0000-0000";
	private final static BigInteger en = new BigInteger("71004835961449063455630356638756640843775172129862296213190471352190631031691");
	private final static String SALT = "@tx)qtt1txwhf@x#l*mk(trsn@)3w6^imuwjw*1v85p(0nt_ln";
	
	private String name;
	private String surname;
	
	//metoda usuwaj�ca bia�e znaki z pocz�tku i zamieniaj�ca pierwsz� liter� na du��
	
	public static String checkString(String s) {
		
		String newString = "";
		
		if(!s.isEmpty()) {
			
			char[] temp = s.toCharArray();
			int i = 0;
			
			Boolean isLetter = false;
			
			while(i<temp.length) {
				
				if(Character.isDigit(temp[i]));
				else if(Character.isWhitespace(temp[i]));
				else if(Character.isLetter(temp[i]) && !isLetter) {
					temp[i] = Character.toUpperCase(temp[i]);
					newString+=temp[i];
					isLetter = true;
				}
				else if(Character.isLetter(temp[i]) && isLetter) {
					temp[i] = Character.toLowerCase(temp[i]);
					newString+=temp[i];
				}
				i++;
				
			}
			
		}
		
	
		
		
		
		
		return newString;
		
	}
	
	public String removeAccent(String string) {
		
		final Pattern NONLATIN = Pattern.compile("[^\\w-]");
		
		string = string.replaceAll("�", "l");
		String normalized = Normalizer.normalize(string, Form.NFD);
	    String slug = NONLATIN.matcher(normalized).replaceAll("");
	    
	    return slug.toLowerCase(Locale.ENGLISH);
		
		
	}
	
	public String getName() {
		return name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public String setStringCustomer(String string) {
		
		if((string.isEmpty()) || (string.equals(ConfigureApp.number)) || string.equals(" ")) {
			string = "";
		}
		
		return string;
	}

	/**
	 * TO-DO: zrobi� varargs
	 * @param name
	 * @param surname
	 * @param number
	 * @param email
	 * @param birthday
	 * @param city
	 * @param natural
	 * @param grey
	 * @param condition
	 * @return
	 */
	public Customer addCustomer(String name, String surname, String number, String email, String birthday, String city, String natural, String grey, String condition) {
		
		Customer c = null;
		
		try {
			number = setStringCustomer(number);
			email = setStringCustomer(email);
			birthday = setStringCustomer(birthday);
			city = setStringCustomer(city);
			
			//sprawdzam, czy przed imieniem wystepuje spacja:
			name = checkString(name);
			surname = checkString(surname);
			this.name = name;
			this.surname = surname;
			
			if(name.equals("") || surname.equals("")) {
				
				return c;
			}
			
			
			c = new Customer(name, surname, number.replaceAll("-", ""), email, birthday, city, natural, grey, condition);
			surname = removeAccent(surname.toLowerCase());
			name = removeAccent(name.toLowerCase());
			String user_display = surname + " " + name;
			
			
			
			
			c = DataBase.addCustomer(c, user_display);
		
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
		
	}
	
	public Customer addCustomer(String name, String surname, String number, String natural, String grey, String condition) {
		
		Customer c = null;
		
		try {
			number = setStringCustomer(number);
			
			
			//sprawdzam, czy przed imieniem wystepuje spacja:
			name = checkString(name);
			surname = checkString(surname);
			this.name = name;
			this.surname = surname;
			
			if(name.equals("") || surname.equals("")) {
				
				return c;
			}
			
			
			c = new Customer(name, surname, number.replaceAll("-", ""), natural, grey, condition);
			surname = removeAccent(surname.toLowerCase());
			name = removeAccent(name.toLowerCase());
			String user_display = surname + " " + name;
			
			
			
			
			c = DataBase.addCustomer(c, user_display);
		
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
		
	}

	public Customer addCustomer(String name, String surname, String number, String natural, String grey) {

		Customer c = null;

		try {
			number = setStringCustomer(number);


			//sprawdzam, czy przed imieniem wystepuje spacja:
			name = checkString(name);
			surname = checkString(surname);
			this.name = name;
			this.surname = surname;

			if(name.equals("") || surname.equals("")) {

				return c;
			}


			c = new Customer(name, surname, number.replaceAll("-", ""), natural, grey);
			surname = removeAccent(surname.toLowerCase());
			name = removeAccent(name.toLowerCase());
			String user_display = surname + " " + name;




			c = DataBase.addCustomer(c, user_display);



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return c;

	}
	
	public Customer checkNumber(String number) {
		
		Customer c = null;
		
		try {
			c = DataBase.checkNumber(number.replaceAll("-", ""));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
		
	}
	
	public Customer checkClient(String phone, String surname, String name) {
		
		Customer c = null;
		surname = checkString(surname);
		name = checkString(name);
		
		try {
			if(!name.equals("") || !surname.equals(""))
				c = DataBase.checkClient(phone.replaceAll("-", ""), surname, name);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
		
	}
	
	public User getUserFromString(String code) {
		
		User user = null;
		
		try {
			user = DataBase.getUser(code);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return user;
		
	}
	
	//metoda pobieraj�ca kod u�ytkownika ze Stringa
	public String getUserCodee(String user) {
		
		String[] temp = user.split("\\[");
		user = temp[1];
		temp = user.split("\\]");
		user = temp[0];
		return user;
		
	}
	
	public Formula addFormula(String content, String time, String price, Customer c, String user) {
		
		int idTreat = 0;
		
		if(user.equals(JHDresserBar.getTextOnBar())) 
			user = "";
		else {
			user = getUserCodee(user);
		}
		
		Formula f = null;
		
		try {
			if((!content.isEmpty())) {
				if(time.equals(ConfigureApp.time))
					time = "";
				if(price.equals(ConfigureApp.price))
					price = "";
				//f = new Formula(content, idTreat);
				f = DataBase.setFormula(content, idTreat, Float.parseFloat(price), Integer.parseInt(time));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return f;
		
	}
	
	public Formula getFormula(int idRelation) {
		
		Formula formula = null;
		
		System.out.println("A teraz jeste�my w getFormula w klasie ServiceData");
		try {
			formula =  DataBase.getFormulaFromList(idRelation);
			if(formula==null) {
				
				Treatment treat = getTreatmentFromList(idRelation);
				if(treat!=null)
					formula = new Formula(treat.getPrice(), treat.getTime());
				
			}
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formula;
	}
	
	public static Boolean isAnyUser() {
		
		Boolean isUser = false;
		
		try {
			isUser = DataBase.isAnyUser();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isUser;
	}
	
	
	
	public Customer checkAndchange(Customer c, String name, String surname, 
								   String number, String email, String birthday, 
								   String city, String natural, String grey, String condition) {
		
			
			name = checkString(name);
			surname = checkString(surname);
			this.name = name;
			this.surname = surname;
		
			Boolean changeDisplay = false;
			try {
				
				if(!c.getName().equalsIgnoreCase(name)) {
					DataBase.updateNameCustomer(name, c.getId());
					changeDisplay = true;
					c.setName(name);
				}
				if(!c.getSurname().equalsIgnoreCase(surname)) {
					DataBase.updateSurnameCustomer(surname, c.getId());
					changeDisplay = true;
					c.setSurname(surname);
				}
					
				if(!c.getNumber().equalsIgnoreCase(number.replaceAll("-", "")) && !number.equals(ConfigureApp.number)) {
					DataBase.updateNumberCustomer(number, c.getId());
					c.setNumber(number);
				}
				
				if(!c.getEmail().equalsIgnoreCase(email) && !email.equals(ConfigureApp.email)) {
					DataBase.updateFieldCustomer(email, c.getId(), "email");
					c.setEmail(email);
				}
				
				if(!c.getBirthday().equalsIgnoreCase(birthday)) {
					DataBase.updateFieldCustomer(birthday, c.getId(), "birthday");
					c.setBirthday(birthday);
				}
				
				if(!c.getCity().equalsIgnoreCase(city)) {
					DataBase.updateFieldCustomer(city, c.getId(), "city");
					c.setCity(city);
				}
					
				
				if(c.getNatural().equals("") && !natural.isEmpty() && !natural.equals(ConfigureApp.natural)) {
					DataBase.setCustomerMeta("natural", natural, c.getId());
					c.setNatural(natural);
				}
				else if(!c.getNatural().equals(natural) && !natural.equals(ConfigureApp.natural)) {
					DataBase.updateNaturalCustomer(natural, c.getId());
					c.setNatural(natural);
				}
				
				if(c.getGrey().equals("") && !grey.isEmpty() && !grey.equals(ConfigureApp.grey)) {
					DataBase.setCustomerMeta("siwe", grey, c.getId());
					c.setGrey(grey);
				}
				else if(!c.getGrey().equalsIgnoreCase(grey) && !grey.equals(ConfigureApp.grey)) {
					DataBase.updateGreyCustomer(grey, c.getId());
					c.setGrey(grey);
				}	
				
				if(c.getCondition().equals("") && !condition.isEmpty() && !condition.equals(ConfigureApp.condition)) {
					DataBase.setCustomerMeta("kondycja", condition, c.getId());
					c.setCondition(condition);
				}
				else if(!c.getCondition().equalsIgnoreCase(condition) && !grey.equals(ConfigureApp.condition)) {
					DataBase.updateConditionCustomer(condition, c.getId());
					c.setCondition(condition);
				}	
				
				
				if(changeDisplay) {
				
					
					surname = removeAccent(surname.toLowerCase());
					name = removeAccent(name.toLowerCase());
					String user_display = surname + " " + name;
					DataBase.updateUserDisplay(user_display, c.getId());
					
					
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		
		
		
		return c;
		
	}

	public Customer checkAndchange(Customer c, String name, String surname,
								   String number, String natural, String grey) {


		name = checkString(name);
		surname = checkString(surname);
		this.name = name;
		this.surname = surname;

		Boolean changeDisplay = false;
		try {

			if(!c.getName().equalsIgnoreCase(name)) {
				DataBase.updateNameCustomer(name, c.getId());
				changeDisplay = true;
				c.setName(name);
			}
			if(!c.getSurname().equalsIgnoreCase(surname)) {
				DataBase.updateSurnameCustomer(surname, c.getId());
				changeDisplay = true;
				c.setSurname(surname);
			}

			if(!c.getNumber().equalsIgnoreCase(number.replaceAll("-", "")) && !number.equals(ConfigureApp.number)) {
				DataBase.updateNumberCustomer(number, c.getId());
				c.setNumber(number);
			}


			if(c.getNatural().equals("") && !natural.isEmpty() && !natural.equals(ConfigureApp.natural)) {
				DataBase.setCustomerMeta("natural", natural, c.getId());
				c.setNatural(natural);
			}
			else if(!c.getNatural().equals(natural) && !natural.equals(ConfigureApp.natural)) {
				DataBase.updateNaturalCustomer(natural, c.getId());
				c.setNatural(natural);
			}

			if(c.getGrey().equals("") && !grey.isEmpty() && !grey.equals(ConfigureApp.grey)) {
				DataBase.setCustomerMeta("siwe", grey, c.getId());
				c.setGrey(grey);
			}
			else if(!c.getGrey().equalsIgnoreCase(grey) && !grey.equals(ConfigureApp.grey)) {
				DataBase.updateGreyCustomer(grey, c.getId());
				c.setGrey(grey);
			}



			if(changeDisplay) {


				surname = removeAccent(surname.toLowerCase());
				name = removeAccent(name.toLowerCase());
				String user_display = surname + " " + name;
				DataBase.updateUserDisplay(user_display, c.getId());


			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}





		return c;

	}

	//f - poprzednia receptura
	//content, price, time - wpisane przez u�ytkownika
	
	public Formula checkFormulaAndSave(Formula f, String content) {
		
			
			try {
				if(!f.getContent().equalsIgnoreCase(content)) {
					DataBase.updateContent(content, f.getId());
					f.setContent(content);
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return f;

}
	
	
	public Boolean checkPass(String pass, String passTwo) {
		
		int isAdmin = 1;
		Boolean isGood = false;
		if(!pass.equals(passTwo)) {
			isGood = false;
		}
		else
			isGood = true;
		
		
		return isGood;
	}
	
	public Boolean checkKey(String key, String login) {
		
		Boolean isGood = false;
		/*if(keyApp.replace("-", "").equalsIgnoreCase(key.replace("-", "")))
				isGood = true;*/
		
		XMLRpc xml = new XMLRpc();
		
		//zaszyfrowanie klucza
		//czas pobrany z serwera
		/*String result = "";
		char [] temp = result.toCharArray();
		String time="";
		if(temp[11]=='0') {
			time = "" + temp[12];
		}
		else {
			time = temp[11] + "" + temp[12];
		}
		
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			String input = key + time + SALT;
			md.update(input.getBytes());
			key = HexString.bufferToHex(md.digest());
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		
		return xml.activation(login, key);
		
		
	}
	
	public void addUser(String login, String pass, int admin) {
		
		
		try {
			
			DataBase.addAdmin(new User(login, pass, admin));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void changePass(String login, String pass) {
		
		
		try {
			User user = new User(login, pass);
			user.setPass(pass);
			DataBase.changePass(user);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getAdmin() {
		
		String name = "";
		
		try {
			name = DataBase.getAdminName("ADMIN");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return name;
		
	}
	
	private int formatString(String string) {
		
		int what = 1;
		//najpierw sprawdzamy, czym jest dana fraza
		//0  - string
		//1 - numer
		
		if(string!=null && !string.isEmpty()) {
			char[] temp = string.toCharArray();
			int i = 0;
			while(i<temp.length) {
			
				if(Character.isLetter(temp[i])) {
					what = 0;
					return what;
				}
				i++;
			}
		}
		else
			what = 0;
		
		
		return what;
	}
	
	public String removeCharsString(String string) {
		
		string = string.trim();
		char[] temp = string.toCharArray();
		String tempString = "";
		Boolean isSpace = false;

		
		for(int i=0; i<temp.length; i++) {
			if(Character.isLetter(temp[i]))  {
				tempString+=temp[i];
			}
			else if(temp[i]=='-') {
				tempString+=temp[i];
			}
			else if(Character.isWhitespace(temp[i])) {
				isSpace = true;
				tempString+=" ";
			}
			
		}
		
		if(isSpace) {
			String[] noSpace = tempString.split(" ");
			
			tempString = "";
			int i = 0;
			
			while(i<noSpace.length) {
				tempString += removeAccent(noSpace[i]);
				if(i<noSpace.length-1)
					tempString +=" ";
				i++;
			}
		}
		else {
			tempString = removeAccent(tempString);
		}
		
		return tempString;
	}
	
	public String removeCharsNumber(String string) {
		
		char[] temp = string.toCharArray();
		String tempString = "";
		
		
		for(int i=0; i<temp.length; i++) {
			
			if((Character.isDigit(temp[i])) || (temp[i]=='-') || (temp[i]=='+')) {
				tempString +=temp[i];
			}
			
		}
		
		return tempString;
	}
	
	public List<Customer> searchCustomer(String s, int customer) {

		List<Customer> list = new ArrayList<Customer>();
		String searchPhrase = null;
		
		
		try {
			int what = formatString(s);
			if(what==0) {
				
				s = removeCharsString(s);
			
				
				searchPhrase = "WHERE user_display LIKE '" + s + "%' ";
			}
			else if(what==1) {
				s = removeCharsNumber(s);
				searchPhrase = "WHERE phone LIKE '" + s + "%' ";
			}
			list = DataBase.searchCustomer(s, customer, searchPhrase);
		} catch (SQLException e) {
			list=null;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	public List<Customer> searchCustomer(String s) {

		List<Customer> list = new ArrayList<Customer>();
		String searchPhrase = null;
		
		
		try {
			int what = formatString(s);
		
			if(what==0) {
				
				if(s!=null)
					s = removeCharsString(s);
				else
					s = "";
				
				searchPhrase = "WHERE user_display LIKE '" + s + "%' ";
			}
			else if(what==1) {
				s = removeCharsNumber(s);
				searchPhrase = "WHERE phone LIKE '" + s + "%' ";
			}
			
			list = DataBase.searchCustomer(s, searchPhrase);
		} catch (SQLException e) {
			list=null;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Hair getHair(int id) {
		
		Hair h = null;
		
		try {
			
			h = DataBase.getHair(id);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return h;
		
	}
	
	public List<Formula> getFormulasList(int id) {
		
		List<Formula> list = new ArrayList<Formula>();
		
		try {
			
			list = DataBase.getFormulasList(id);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public int checkSearchString(String s) {
		int code=0;
		//code = 0 - niepoprawny format
		//code = 1 - string to nazwisko b�d� imi�
		//code = 2 - string to fragment numeru telefonu
		//code = 3 - zbyt ma�o cyfr
		
		/*if(!s.matches("[A-Za-z0-9+-]*")) {
			System.out.println("Nie pasuje do wzorca");
			return code;
		}
		*/
		char[] temp = s.toCharArray();
		int i = 0;
		int sum = 0;
		
		while(i<temp.length) {
			if(!Character.isDigit(temp[i])) {
				sum++;
			}
			i++;
		}
		
		if(sum==temp.length)
			code = 1;
		else if(sum==0) {
			if(temp.length<4)
				code = 1;
			else
				code = 1;
		}
		else
			code = 1;
		
		
		return code;
	}
	
	public Boolean logInAdmin(String login, String pass) {
		
		Boolean good = false;
		try {
			
			User u = DataBase.getAdmin(login);
			if(u!=null)
				good = u.comparePass(pass);
			else
				good = false;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return good;
		
	}
	
	public byte[] changeFileToByte(File file) {
		
		byte[] buf = new byte[1024];
		byte[] bytes = null;
		
		try {
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			for (int readNum; (readNum = fis.read(buf)) != -1;) {
        		bos.write(buf, 0, readNum); 
        		
        	}
        	bytes = bos.toByteArray();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return bytes;
		
	}
	
	public User addUser(String name, String surname, String code, File avatar) {
		User u = null;
		byte[] bytes;
		try {
			if(avatar!=null) {
				bytes = changeFileToByte(avatar);
			}
			else {
				bytes = null;
			}
			this.name = name = checkString(name);
			this.surname = surname = checkString(surname);
			
			if(name.equals("") || surname.equals("") || code.equals(""))
				return u;
			
			
			u = DataBase.addUser(name, surname, code, bytes);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			u = null;
		}
		return u;
		
	}
	
	public User addUser(String name, String surname, String code, File avatar, String status) {
		User u = null;
		byte[] bytes;
		try {
			if(avatar!=null) {
				bytes = changeFileToByte(avatar);
			}
			else {
				bytes = null;
			}
			name = checkString(name);
			surname = checkString(surname);
			u = DataBase.addUser(name, surname, code, bytes, status);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			u = null;
		}
		return u;
		
	}
	
	public List<User> getUsers(String action) {
		
		List<User> list = new ArrayList<User>();
		
		try {
			
			list = DataBase.getUsers(action);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public String[] parseToString(List<User> listUser) {
		
		Iterator<User> it = listUser.iterator();
		String[] tab = new String[listUser.size()];
		int i = 0;
		
		while(it.hasNext()) {
			User u = it.next();
			//String temp  = new String("[" + u.getCode() + "] " + u.getName() + " " + u.getSurname());
			String temp = u.toString();
			tab[i] = temp;
			i++;
		}
		
		
		
		return tab;
	}
	
	public String parseToString(User u) {
		
		return new String(" [" + u.getCode() + "] " + u.getName() + " " + u.getSurname());
		
		
		
	}
	
	public void deleteUser(String code) {
		
		try {
			
			DataBase.deleteUser(code);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void deleteVisit(int id) {
		
		try {
			
			DataBase.deleteVisit(id);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void updateUser(User userOld, User userNew) {
		
		try {
			userNew.setName(checkString(userNew.getName()));
			userNew.setSurname(checkString(userNew.getSurname()));
			DataBase.updateUser(userOld, userNew);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public User updateAvatar(String name, String surname, String code, File avatar) {
		
		User u = null;
		
		try {
			
			byte[] bytes = changeFileToByte(avatar);
			DataBase.dropUser(code);
			name = checkString(name);
			surname = checkString(surname);
			u = new User(name, surname, code, bytes);
			DataBase.addUser(name, surname, code, bytes);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return u;
	}
	
	public void deleteCustomer(int id) {
		
		try {
			DataBase.deleteCustomer(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getTextHtml(int id) {
		
		String text = null;
		
		try {
			text = DataBase.getTextHtml(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return text;
		
	}
	
	public String roundNumber(int n, float x) {
		
		
		java.text.DecimalFormat df=new java.text.DecimalFormat();
		df.setMaximumFractionDigits(n);
		df.setMinimumFractionDigits(n);
		String number = df.format(x).replace(',', '.');
		
		return number;
		
	}
	
	public String getUserCode(String user) {
		
		String code = "";
		String[] temp = user.split(" ");
		char[] tab = temp[0].toCharArray();
		for(int i=0; i<tab.length; i++) {
			if((tab[i]=='[') || (tab[i]==']')) {
				
			}
			else {
				code = code + tab[i];
			}	
		}
		return code;
		
	}
	
	private String createStringDate(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		return cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH)) + "-" + 
		 	   cal.get(Calendar.DAY_OF_MONTH);
		
	}
	
	private String createStringDate(Date date, String time) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		return cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH)) + "-" + 
		 	   cal.get(Calendar.DAY_OF_MONTH) + " " + time;
		
	}
	
	public Visit addDate(Customer c, String hour, Date date, String text, String user, List<Treatment> treats) {
		
		Visit visit = null;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		int idCustomer = c.getId();
		String newDate = createStringDate(date) + " " + hour;
		
		int time = 0;
		
		
		Iterator<Treatment> it = treats.iterator();
		
		System.out.println("Lista zabieg�w przed zapisaniem: " + treats.size());
		
		while(it.hasNext()) {
			Treatment treat = it.next();
			time  += treat.getTime();
			
		}
		
		System.out.println("Czas ��czny zabieg�w: " + time);
		
		//String newDateString = 
		
		
		
		try {
			
			
			int idDate = DataBase.addDate(idCustomer, newDate, text, getUserCode(user), time, "no");
		    visit = new Visit(idDate, idCustomer, newDate, text, getUserCode(user), time, "no");
			
		    System.out.println("Id terminu po dodaniu terminu: " + idDate);
			int idRelation = 0;
			it = treats.iterator();
			while(it.hasNext()) {
				Treatment treat = it.next();
				
				idRelation = DataBase.addListTreat(idDate, treat.getId());
				System.out.println("idRelation po dodaniu terminu: " + idRelation);
			}
			
			if(idRelation == 0) { 
				DataBase.deleteVisit(idDate);
				Communication comm = new Communication(MainWindow.getFrame());
				comm.dont("Niepoprawny zapis terminu wizyty do bazy. Spr�buj jeszcze raz");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return visit;
		
	}
	
	
	
	public List<Visit> getListVisit(String user, Date date) {
		
		List<Visit> list = new ArrayList<Visit>();
		
		
		try {
			list = DataBase.getListVisit(getUserCode(user), createStringDate(date));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public List<Visit> getListVisit(String user, Date date, String time) {
		
		List<Visit> list = new ArrayList<Visit>();
		
		
		try {
			list = DataBase.getListVisit(getUserCode(user), createStringDate(date, time));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public List<Visit> getListVisit(User user, Date date) {
		
		List<Visit> list = new ArrayList<Visit>();
		
		
		try {
			list = DataBase.getListVisit(user.getCode(), createStringDate(date));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	
	//pobieranie wizyt dotyczacych zaplanowanych dla konkretnego klienta
	public List<Visit> getListVisit(int id) {
		
		List<Visit> list = new ArrayList<Visit>();
		
		
		try {
			List<Visit> lista = DataBase.getListVisit(id, createStringDate(Calendar.getInstance().getTime()));
			Collections.sort(lista);
			
			Date date = Calendar.getInstance().getTime();
			Iterator<Visit> it = lista.iterator();
			
			while(it.hasNext()) {
				
				Visit visit = it.next();
				if(visit.getDateToString().compareTo(date)>=0) {
					list.add(visit);
				}
					
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public List<Visit> getListVisit(int id, int number) {
		
		List<Visit> list = new ArrayList<Visit>();
		
		
		try {
			List<Visit> lista = DataBase.getListVisit(id);
			Collections.sort(lista);
			Collections.reverse(lista);
			
			List<Visit> temp = new ArrayList<Visit>();
			
			if(number<lista.size()) {
				temp = lista.subList(0, number);
			}
			else {
				temp.addAll(lista);
			}
			
			Date date = Calendar.getInstance().getTime();
			Iterator<Visit> it = temp.iterator();
			
			int i = 0;
		
			while(it.hasNext()) {
				
				Visit visit = it.next();
				if(visit.getDateToString().compareTo(date)<0) 
					visit.setActive(false);
				
				list.add(visit);	
				i++;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public Visit getNextVisit(int id) {
		
		Visit visit = null;;
		
		
		try {
			List<Visit> list = DataBase.getListVisit(id);
			Collections.sort(list);
			System.out.println("Lista posortowana: " + list);
			//Collections.reverse(list);
			//System.out.println("Lista odwr�cona: " + list);
			
			Date date = Calendar.getInstance().getTime();
			Iterator<Visit> it = list.iterator();
			
			Boolean isNext = false;
			
			
			while(!isNext && it.hasNext()) {
				
				Visit v = it.next();
				if(v.getDateToString().compareTo(date)>=0)  {
					isNext = true;
					visit = v;
				}
				
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return visit;
		
	}
	
	
	
	
	public void setCustomerMeta(String key, String value, Customer customer) {
		
		try {
			DataBase.setCustomerMeta(key, value, customer.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Customer getCustomer(int id) {
		
		Customer c = null;
		
		try {
			c = DataBase.getCustomer(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
		
	}
	
	public Note getNote(int customerID) {
		
		Note note = null;
		
		try {
			note = DataBase.getNote(customerID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return note;
		
	}
	
	public void updateNote(Note note, String text) {
		
		if(!note.getText().equals(text)) {
			try {
				DataBase.updateCustomerMeta(note.getId(), text);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public ImageIcon resizeImage(ImageIcon imgIc, Dimension dest) {
		
		Image img = imgIc.getImage();  
		float y = (float) (dest.getHeight()-6)/(float) imgIc.getIconHeight();
		float x = imgIc.getIconWidth()*y;
		
		Image newimg = img.getScaledInstance((int) x, 
											 (int) (dest.getHeight() - 6),  
											 java.awt.Image.SCALE_SMOOTH);  
		imgIc = new ImageIcon(newimg); 
		
		return imgIc;
		
	}
	
	public Boolean isGoodNumber(String number) {
	
	  if(!number.isEmpty()) {
		if(number.equals(ConfigureApp.number)) 
				return true;
		else if(number.length()>14 || number.length()<7)
			return false;
		else if(!number.matches("[0-9\\-]*"))
			return false;
	
		else
			return true;
	  }
	  else
		  return true;
	}
	
	
	
	public static Boolean inRange(String i, int start, int end) {
		
		int num = Integer.parseInt(i);
	
		if(num<=end && num>=start) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Boolean checkProperlyHour(String from, String to) {
		
		from = from.trim();
		to = to.trim();
		
		int f = Integer.parseInt(from);
		int t = Integer.parseInt(to);
		
		if(f>t)
			return false;
		else if(f==t)
			return false;
		else
			return true;
		
	}
	
	public void setVars(int day, String start, String end) {
		
		try {
			DataBase.setVars("daywork", day + ";" + start + ";" + end + ";p");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Boolean updateVars(int id, int day, String start, String end) {
		
		try {
			String free = "p";
			Numbers num = new Numbers();
			if(start.isEmpty() && end.isEmpty()) {
				
				start = Integer.toString(ConfigureApp.minDayStart);
				end = Integer.toString(ConfigureApp.maxDayEnd);
				free="w";
			}
			if(num.isInteger(start) && num.isInteger(end)) {
				DataBase.updateVars(id, day + ";" + start + ";" + end + ";" + free);
				return true;
			}
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	public String getWork(String key, int day) {
		
		String temp="";
		try {
			temp = DataBase.getWork(key, day);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] tab = temp.split(";");
		
		
		return tab[3];
		
	}

	/**
	 *
	 * @param list lista godzin potrzebnych do wy�wietlania kalendarza
	 *  jaka jest najwcze�niejsza godzina spo�r�d wszystkich wy�wietlanych
	 *  jaka jest najp�niejsza godzina spo�r�d wszystkich wy�wietlanych dni
	 */
	public static void compareDayWork(List<DayWork> list) {
		
		Collections.sort(list);
		if(list.size()>0) 
			ConfigureApp.minDayStart = list.get(0).getStart(); 
		else 
			ConfigureApp.minDayStart = 8;
			
		
		
		Iterator<DayWork> it = list.iterator();
		//ConfigureApp.maxDayEnd = 0;
		
		ConfigureApp.maxDayEnd = 9;
		
		while(it.hasNext()) {
			
			DayWork temp = it.next();
			if(temp.getEnd()>ConfigureApp.maxDayEnd)
				ConfigureApp.maxDayEnd = temp.getEnd();
			
		}
		
		if(list.size()==0) {
			ConfigureApp.maxDayEnd = 18;
		}
		
	}

	/**
	 *
	 * @return list� godzin pracy w poszczeg�lne dni
	 */

	public List<DayWork> getDayWork() {
		
		List<DayWork> list = new ArrayList<DayWork>();
		
		try {
			list = DataBase.getDayWork();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public void changeUserInVisit(String codeOld, String codeNew) {
		
		try {
			DataBase.changeUserInVisit(codeOld, codeNew);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Visit updateVisit(Visit visit, int customerID, String hour, Date date, 
							String text, String code, List<Treatment> treats) {
		
		Visit v = null;
		
		try {
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			
			
			String newDate = createStringDate(date) + " " + hour;
			
			visit.setCustomerID(customerID);
			visit.setDate(newDate);
			visit.setText(text);
			visit.setUserCode(getUserCode(code));
			
			Integer time = 0;
			String zabiegi = "";
			int i=0;
			Iterator<Treatment> it = treats.iterator();
			
			while(it.hasNext()) {
				Treatment treat = it.next();
				
				time  += treat.getTime();
				
			}
			
			visit.setTreatments(treats);
			
			if(time!=0) {
				visit.setDuration(time);
				visit.setNumber(zabiegi);
				
			}
			else {
				visit.setDuration(0);
				visit.setNumber("t2");
			}
			
			
			
			if(isExistVisit(visit.getId())) {
				DataBase.updateVisit(visit);
				v = visit;
			}
			else {
				v = addDate(getCustomer(customerID), hour, date, text, code, treats);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return v;
	}
	
	public Boolean isExistVisit(int id) {
		
		Boolean isExist = false;
		
		try {
			isExist = DataBase.isExistVisit(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isExist;
		
	}
	
	public void updateVisitFormula(Visit visit, String code, List<Treatment> treats, 
								   int sizeOld) {

		try {
			
			visit.setUserCode(getUserCode(code));
			
			int i = 0;
			
			//System.out.println("Wielko�ci list - treats: " + treats.size() + " old: " + oldTreats.size());
			
			while(i<treats.size() && i<sizeOld) {
				Treatment t = treats.get(i);
				System.out.println("Id relacji: " + t.getRelation());
				if(t.getRelation()!=0)
					DataBase.updateListTreat(t.getRelation(), t.getId());
				else
					t.setRelation(DataBase.addListTreat(visit.getId(), t.getId()));
				
				Formula f = t.getFormula();
				
				System.out.println("Ju� dodajemy do bazy w pierwszej p�tli... " + f.getIdTreats());
				
				if(t.getFormula().getIdTreats()==0)
					DataBase.setFormula(f.getContent(), t.getRelation(), f.getPrice(), f.getTime());
				else
					DataBase.updateFormula(f.getContent(), f.getPrice(), f.getTime(), f.getIdTreats());
				i++;
			}
			
			if(i<treats.size()) {
				while(i<treats.size()) {
					Treatment t = treats.get(i);
					int id = DataBase.addListTreat(visit.getId(), t.getId());
					Formula f = t.getFormula();
					DataBase.setFormula(f.getContent(), id, f.getPrice(), f.getTime());
					i++;
				}
			}
			else if(i<sizeOld) {
				while(i<sizeOld) {
					List<Treatment> oldTreats = new ArrayList<Treatment>();
					oldTreats = visit.getAllTreatments();
					Treatment t = oldTreats.get(i);
					DataBase.deletListRelation(t.getRelation());
					DataBase.deleteFormula(t.getRelation());
					i++;
				}
			}
			
			
			
			Integer time = 0;
			
			for(Treatment t : treats) {
				
				Formula f = t.getFormula();
				time += f.getTime();
				
				
			}
			
			DataBase.updateFieldVisit(Integer.toString(time), visit.getId(), "duration");
			DataBase.updateFieldVisit(visit.getUserCode(), visit.getId(), "user_ID");
			DataBase.updateFieldVisit("yes", visit.getId(), "isRealized");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String setHour(Calendar cal) {
		
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		
		String time = hour + ":";
		
		if(min>=0 && min<15)
			time += "00";
		else if(min>=15 && min<30)
			time += "15";
		else if(min>=30 && min<45)
			time += "30";
		else
			time += "45";
		
		return time;
		
	}
	
	public void addVisitFormula(String code, Customer customer, List<Treatment> treats) {

		try {

			code = getUserCode(code);
			
			Integer duration = 0;
			
			for(Treatment t : treats) {
				
				if(t.getFormula()!=null)
					duration += t.getFormula().getTime();
				else
					duration += t.getTime();
				
			}
			
			//tworzymy dzisiejsz� dat�:
			Calendar cal = Calendar.getInstance();
			String newDate = createStringDate(cal.getTime()) + " " + setHour(cal);
			
			int idDate = DataBase.addDate(customer.getId(), newDate, "", code, duration, "yes");
			
			for(Treatment t : treats) {
				
				int id = DataBase.addListTreat(idDate, t.getId());
				Formula f = t.getFormula();
				if(f==null)
					f = new Formula(t.getPrice(), t.getTime(), "");
				DataBase.setFormula(f.getContent(), id, f.getPrice(), f.getTime());
			}

			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String formatUserToBar(User u) {
		
		return u.getName() + " " + u.getSurname() + " [" + 
		   u.getCode() + "]";
		
	}
	
	public List<Treatment> getTreatments() {
		
		try {
			return DataBase.getTreatments();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public List<Treatment> getTreatments(String where) {
		
		
		
		try {
			return  DataBase.getTreatments(where);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public Treatment getTreatment(String number) {
		
		try {
			return DataBase.getTreatment(number);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Treatment getTreatment(int number) {
		
		Treatment t = null;
		
		try {
			t =  DataBase.getTreatment(number);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}
	
	public Treatment getTreatment(int number, int idRelation) {
		
		Treatment t = null;
		
		try {
			System.out.println("Jeste�my w klasie ServiceData metodzie getTreatment");
			t =  DataBase.getTreatment(number);
			if(t!=null) {
				t.setFormula(idRelation);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}
	
	
	public String[] getStringTreat() {
		
		List<Treatment> list = getTreatments();
		
		Iterator<Treatment> it = list.iterator();
		String[] tab = new String[list.size()];
		int i = 0;
		
		while(it.hasNext()) {
			
			Treatment t = it.next();
			tab[i] = t.toString();
			i++;
		}
		
		return tab;
	}
	
	public String readFile(String name) {
		
		return new ManageFiles().readFile(name);
	
	}
	
	public static List<Help> setHelp(String helper) {
		
		
		String help = new ManageFiles().readFile(helper);
		
		String[] temp = help.split("</name>\n");
		List<Help> helpList = new ArrayList<Help>();
		int i=0;
		int name;
		String content;
		while(i<temp.length) {
			name = Integer.parseInt(temp[i].replaceFirst("(.)*<name='", "").replaceFirst("'>(.)*", "").replaceAll("[^0-9]", ""));
			content = temp[i].replaceFirst("<name='(.)*'>", "");
		
			helpList.add(new Help(name, content));
			
			i++;
		}
		
		return helpList;
		
	}
	
	public void deleteTreat(String number) {
		
		try {
			DataBase.deleteTreat(number);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void addTreat(Treatment treat) {
		
		try {
			
			DataBase.addTreat(treat);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void updateTreat(Treatment treat) {
		
		try {
			
			DataBase.updateTreat(treat);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Boolean checkCrypto(String message) {
		
		Boolean isGood = false;
		
		
		return isGood;
		
	}
	
	public Boolean checkKeyMaster(String master) {
		
		Boolean isGood = false;
		
		
		
		
		if(master.equals(ConfigureApp.getMaster()))
			return true;
		
		
		return isGood;
		
	}
	
	public Date checkDateFromString(String text, String splitter) {
		
		Numbers number = new Numbers();
		
		
		String[] dateTextFrom = text.split(splitter);
		
		Date date = null;
		int i = 0;
		
		
		Calendar cal = Calendar.getInstance();
		
		if(number.isInteger(dateTextFrom[0])) {
			i = Integer.parseInt(dateTextFrom[0]);
			cal.set(Calendar.DAY_OF_MONTH, i);
		}
		else
			return null;
		
		if(number.isInteger(dateTextFrom[1]))
			cal.set(Calendar.MONTH, Integer.parseInt(dateTextFrom[1])-1);
		else
			return null;
		
		
		
		if(number.isInteger(dateTextFrom[2]))
			cal.set(Calendar.YEAR, Integer.parseInt(dateTextFrom[2]));
		else
			return null;
		
		
		date = cal.getTime();
		
		return date;
		
	}
	
	
	public void setUserMeta(String code, String value, String key) {
		
		try {
			DataBase.setUserMeta(code, key, value);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public String getUsersMeta(String code) {
		
		try {
			return DataBase.getUserMeta(code);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public User getUserFromSession(User user) {
		
		List<Session> list = ConfigureApp.getSessions();
		for(Session s : list) {
			
			if(s.getUser().getCode().equals(user.getCode()))
				return s.getUser();
			
		}
		
		return null;
		
	}
	
	public void updateUserMeta(String code) {
		
		try {
			DataBase.updateUserMeta(code);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void updateUserMeta(String code, String value, String key) {
		
		try {
			DataBase.updateUserMeta(code, key, value);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Boolean checkFree(String from, String to) {
		
		if(from.contains("_") && to.contains("_")) 
			return false;
		else
			return true;
	}
	
	
	public void deleteFormula(int id) {
		
		try {
			DataBase.deleteFormula(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String checkNumberUser(String surname, String name) {
		
			String code = "";
			Boolean isDone = false;
			
			char[] surTab = surname.toCharArray();
			char[] nameTab = name.toCharArray();
			
			int i = 0;
			
			code += Character.toUpperCase(nameTab[i]);
		
			try {
				while(!isDone && i<surTab.length) {
					
					code += Character.toUpperCase(surTab[i]);
					
					if(DataBase.checkNumberUser(code)==null)
						isDone = true;
					i++;
				}
				
				if(isDone==false)
					code = null;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		return code;
		
	}
	
	public List<VisitTemp> getListTempVisit(int id) {
		
		List<VisitTemp> list = new ArrayList<VisitTemp>();
		try {
			list = DataBase.getListTempVisit(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
		
		
	}
		
	public Customer getCustomerMeta(Customer c) {
			
		try {
			
			Hair h = DataBase.getHair(c.getId());
			c.setNatural(h.getNatural());
			c.setGrey(h.getGrey());
			c.setCondition(h.getCondition());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return c;
			
	}
	
	public Treatment getTreatmentFromList(int id) {
		
		try {
			return DataBase.getTreatmentFromList(id);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
}
