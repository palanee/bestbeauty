package fait.bb.bf.widgets;

import java.awt.Dimension;

import javax.swing.JButton;

public interface Widget {
	
	public void setView();
	
	public void action();
	
	public JButton getWidget();
	
	public Dimension getSize();
	
	

}
