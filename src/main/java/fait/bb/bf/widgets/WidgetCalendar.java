package fait.bb.bf.widgets;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.graphics.panels.mainpanels.SearchView;
import fait.bb.bf.logic.Communication;


public class WidgetCalendar {
	
	public static final String name = "Terminarz";
	private static final String code = "0003";
	public static int indexCal = -1;
	
	private JButton widget;
	private Communication comm;
	
	public WidgetCalendar(Frame frame) {
		
		comm = new Communication(frame);
		
		widget = new JButtonOwn("calendar");
		widget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				
				
				if(ConfigureApp.isTabs()) {
					MainWindow.labTab.setVisible(true);
					if (indexCal == -1) {
						
						
						
						ConfigureApp.countTab();
						ConfigureWindow.repaintMenu();
						MainWindow.currentCard = 4;
						MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
						CalendarPanelNew calendarPanel = new CalendarPanelNew();
						MainWindow.tabbedPanel.addTab(name, calendarPanel.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
						indexCal = selIndex;
						MainWindow.listPanels.add(null);
						Tab temp = new Tab(name, calendarPanel.getMainPanel(), selIndex);
						//listTab.add(temp);
						MainWindow.tabbedPanel.setForegroundAt(selIndex,ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);
						MainWindow.setWidgetBar();
						
					} else {
						
						MainWindow.tabbedPanel.setSelectedIndex(indexCal);
						if(MainWindow.currentCard==3) {
							MainWindow.currentCard = 4;
							
							MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
							MainWindow.setWidgetBar();
						}
						
					}
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
			}
		});
		
	}
	
	public JButton getWidget() {
		
		return widget;
	}

}
