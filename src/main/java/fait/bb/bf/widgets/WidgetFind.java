package fait.bb.bf.widgets;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.graphics.panels.mainpanels.SearchView;
import fait.bb.bf.logic.Communication;



public class WidgetFind implements Widget {
	
	public static final String name = "Szukaj klienta";
	private static final String code = "0001";
	public static int index = -1;
	private Dimension size;
	private JButton widget;
	
	public WidgetFind(final List<Tab> listTab, Frame frame) {
		
		final Communication comm = new Communication(frame);
		
		widget = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/findClient.png");
		widget.setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		widget.setSize(size);
		imgIc = new ImageIcon("resources/images/findClient-2.png");
		widget.setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/findClient-3.png");
		widget.setPressedIcon(imgIc);
		imgIc = new ImageIcon("resources/images/findClient-0.png");
		widget.setDisabledIcon(imgIc);
		widget.setBorderPainted(false);
		widget.setContentAreaFilled(false);
		widget.setFocusPainted(false);
		widget.setName("findClient");
		widget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				if(ConfigureApp.isTabs()) {
					MainWindow.labTab.setVisible(true);
					if (index == -1) {
						ConfigureApp.countTab();
						ConfigureWindow.repaintMenu();
						MainWindow.currentCard = 4;
						MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
						SearchView searchViewPanel = new SearchView();
						MainWindow.tabbedPanel.addTab(name, searchViewPanel.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
						index = selIndex;
						MainWindow.listPanels.add(null);
						Tab temp = new Tab(name, searchViewPanel.getMainPanel(), selIndex);
						listTab.add(temp);
						MainWindow.tabbedPanel.setForegroundAt(selIndex,ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);
						MainWindow.setWidgetBar();
						
					} else {
						
						MainWindow.tabbedPanel.setSelectedIndex(index);
						if(MainWindow.currentCard==3) {
							MainWindow.currentCard = 4;
							MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
							MainWindow.setWidgetBar();
						}
						
					}
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
			}
		});
		
	}

	@Override
	public void action() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setView() {
		// TODO Auto-generated method stub
		
	}
	
	public Dimension getSize() {
		return size;
		
	}
	
	@Override
	public JButton getWidget() {
		
		return widget;
	}
	
	
}
