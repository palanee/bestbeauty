package fait.bb.bf.widgets;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;



public class WidgetAddClient implements Widget{
	
	public static final String name = "Dodaj klienta";
	private static final String code = "0002";
	private Dimension size;
	private JButton widget;
	private JPanel panel;
	
	
	public WidgetAddClient(final Frame frame, final List<Tab> listTab) {
		
		final Communication comm = new Communication(frame);
		
		widget = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/addClient.png");
		widget.setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		widget.setSize(size);
		//czcionka 25-30 px, odstep 30-35px
		imgIc = new ImageIcon("resources/images/addClient-2.png");
		widget.setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/addClient-3.png");
		widget.setPressedIcon(imgIc);
		imgIc = new ImageIcon("resources/images/addClient-0.png");
		widget.setDisabledIcon(imgIc);
		widget.setBorderPainted(false);
		widget.setContentAreaFilled(false);
		widget.setFocusPainted(false);
		widget.setName("addClient");
		
		
		widget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
					if(ConfigureApp.isTabs()) {
						ConfigureApp.countTab();
						MainWindow.labTab.setVisible(true);
						ConfigureWindow.repaintMenu();
						MainWindow.currentCard = 4;
						MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
						
						CardPanel clientPanel = new CardPanel(1, null);
						MainWindow.listPanels.add(clientPanel);
						
						MainWindow.tabbedPanel.addTab(name, clientPanel.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
						Tab temp = new Tab(name, clientPanel.getMainPanel(),
								selIndex);

						listTab.add(temp);

						MainWindow.tabbedPanel.setForegroundAt(selIndex,
								ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);

						MainWindow.setWidgetBar();
						/*if (ConfigureWindow.bar.getText().equals(
						"------------------------")) {
							MainWindow.pane.setVisible(true);
							MainWindow.choosePanel.setVisible(true);
						}*/
					}
					else {
						comm.dont("Masz zbyt du�o otwartych zak�adek");
					}
				
			}
		});
		
	}
	
	@Override
	public void action() {
		
		
	}

	@Override
	public Dimension getSize() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JButton getWidget() {
		// TODO Auto-generated method stub
		return widget;
	}

	@Override
	public void setView() {
		// TODO Auto-generated method stub
		
	}

}
