package fait.bb.bf.graphics;

import java.sql.SQLException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.DataBase;
import fait.bb.bf.logic.ServiceData;



public class Main {
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		  System.setProperty("file.encoding" , "UTF-8");
		  ServiceData serviceData = new ServiceData();

		  //sprawdzamy czy istnieje baza danych
		  try {
		  	DataBase.testDatabase();
			  
			  String s = DataBase.getVersionData();
			  Float f = 0.0f;
			  if(s!=null)
				  f = Float.parseFloat(s);
				
			  if(f<Float.parseFloat(ConfigureApp.versionDatabase)) {
				  DataBase.updateDatabase();
			  }
				
			  ConfigureApp.listDayWork = serviceData.getDayWork();
			  ConfigureApp.createSessions(serviceData.getUsers(" AND status!='ZWOLNIONY' "));
			  ServiceData.compareDayWork(ConfigureApp.listDayWork);
			  
			  
			  
			
			  SwingUtilities.invokeLater(new Runnable(){
				 
				          public void run() {
				        	  
				        	  try {
				                  // Set System L&F
				              UIManager.setLookAndFeel(
				                  UIManager.getSystemLookAndFeelClassName());
				        	  } 
				        	  catch (UnsupportedLookAndFeelException e) {
				        		  // handle exception
				        	  }
				        	  catch (ClassNotFoundException e) {
				        		  // handle exception
				        	  }
				        	  catch (InstantiationException e) {
				        		  // handle exception
				        	  } catch (IllegalAccessException e) {
				        		  // handle exception
				        	  }
				        	  
				        	  ConfigureApp.installFont();
				        	  
				        	  MainWindow main = new MainWindow();
				
				          }
				 
				      });

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
