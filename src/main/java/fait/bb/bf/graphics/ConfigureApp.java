package fait.bb.bf.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

import fait.bb.bf.graphics.components.JHDresserBar;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.DayWork;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Session;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;

/**
 * Klasa ze zmiennymi konfiguracyjnymi, niezb�dnymi do stworzenia wygl�du aplikacji
 * i jej obs�ugi
 * TO-DO: przenie�� do pliku XML?
 * TO-DO: pokukrywa� cz�� zmiennych!
 *
 */

public class ConfigureApp {
	
	//wersja bazy danych
	public static final String versionDatabase = "1.2";
	public static final String versionApp = "0.9";
	
	//has�o master
	private static final String passwordMaster = "ala234ala";
	
	//napisy na karcie klienta
	public static final String number = "np. 000-000-000";
	public static final String natural = "kolor naturalny";
	public static final String grey = "siwe np. w %";
	public static final String condition = "np. zniszczone";
	public static final String email ="np. xxx.yyy@zzz.com";
	public static final String price = "Cena";
	public static final String time = "Czas";
	
	public static Font font;
	
	//kolor t�a aplikacji
	public static Color backgroundColor = new Color(254,247,238); 
	
	//kolor t�a karty klienta
	public static Color backgroundCardColor = new Color(253,239,199);
	
	//kolor t�a paneli formularzy
	public static Color backPanelColorForm = new Color(175,123,172);
	
	
	//kolor t�a nieobowi�zkowego i obowi�zkowego pola tekstowego w formularzu
	public static Color additionalFieldColor = new Color(254,247,238);
	//kolor t�a pola tekstowego w historii zabieg�w
	public static Color colorFieldHistory = new Color(224,132,13);
	//kolor czcionki etykiety w formularzu
	public static Color etColorFont = new Color(254,247,238);
	//kolor czcionki w tle w polach tekstowych w formularzu
	public static Color backgroundFont = new Color(195,183,194);
	//kolor czcionki przy wpisywaniu w formularzu
	public static Color foreFontColor = new Color(135,52,131);
	//ustawienie obramowania p�l tesktowych - kiedy nic nie piszemy
	public static Border notWrite = BorderFactory.createLineBorder(new Color(136,53,131));
	//ustawienie obramowania p�l tesktowych - kiedy piszmey
	public static Border setWrite = BorderFactory.createLoweredBevelBorder();
	public static Border mistakeWrite = BorderFactory.createLineBorder(Color.RED);
	//kolor czcionki na zak�adkach
	public static Color fontAtTabColor = new Color(224,132,13);
	
	public static User user;
	
	public static String logUser = JHDresserBar.getTextOnBar();
	public static String logUserCard = "DOM";
	public static final String DEFAULT_CODE = "DOM";
	
	public static String activeTab;
	
	public static Dimension sizeOfDialog = new Dimension(1000,580);
	public static Dimension sizeOfDialogBack = new Dimension(950,535);
	
	public static int startYear = 2012;
	
	public static Border panelBorder = BorderFactory.createMatteBorder(
            						   					3, 3, 3, 3, new Color(224,132,13));
	
	public static Boolean isClickToSave = false;
	
	public static DateOfDay date;
	
	//zmienna odpowiadaj�ca za list� godzin
	public static List<DayWork> listDayWork=new ArrayList<DayWork>();
	public static int minDayStart;
	public static int maxDayEnd;
	
	private static int maxTabs = 16;
	private static int tabs = 0;
	
	//zmienne odno�nie ilo�ci wizyt
	private static int maxVisit = 4;
	
	private static Visit bufforVisit = null;
	
	private static Customer customer = null;
	
	private static ArrayList<Session> listSession;
	
	public enum LabelType {
		AFTER_VISIT(new Color(216,215,213), new Color(160,160,159)), 
		NO_VISIT(new Color(136,206,250), new Color(5,93,148)), 
		MIDDLE_VISIT(new Color(232,186,55), new Color(164,39,1)), 
		FULL_VISIT(new Color(164,39,1), ConfigureApp.backgroundColor);
		
		private Color back;
		private Color font;
		
		public Color getColor() {
			return this.back;
		}
		
		public Color getColorFont() {
			return this.font;
		}
		
		LabelType(Color back, Color f) {
			this.back = back;
			this.font = f;
		}
	};
	
	public static String getMaster() {
		
		return passwordMaster;
		
	}
	
	public static void createSessions(List<User> listUser) {
		
		listSession = new ArrayList<Session>();
		
		Iterator<User> it = listUser.iterator();
		while(it.hasNext()) {
			User u = it.next();
			u.setFreeDay();
			listSession.add(new Session(u));
			
		}
		
	}
	
	
	
	public static void setBufforVisit(Visit visit) {
		
		bufforVisit = visit;
		
	}
	
	public static void setBufforCustomer(Customer c) {
		
		customer = c;
		
	}
	
	public static Customer getBufforCustomer() {
		
		return customer;
		
	}
	
	public static Visit getBufforVisit() {
		
		return bufforVisit;
		
	}
	
	public static Calendar findAndGetDate(User u) {
		
		
		Iterator<Session> it = listSession.iterator();
		while(it.hasNext()) {
			
			Session ses = it.next();
			if(u.getCode().equals(ses.getUser().getCode())) {
				
					return ses.getDate().getCalDate();
				
			}
		}
		
		return null;
		
	}
	
	
	public static void saveDateSession(User u, Calendar calendar) {
		
		Iterator<Session> it = listSession.iterator();
		while(it.hasNext()) {
			
			Session ses = it.next();
			if(u.getCode().equals(ses.getUser().getCode()))
					ses.setCalendar(calendar);
		}
		
	}
	
	public static List<Session> getSessions() {
		
		return listSession;
		
	}
	
	public static void addSession(User user) {
		
		user.setFreeDay();
		listSession.add(new Session(user));
		
	}
	
	public static void updateSession(User user) {
		
		int i = 0;
		
		for(Session s : listSession) {
			
			if(s.getUser().getCode().equals(user.getCode())) {
				System.out.println("Aktualizacja sesji...");
				s.getUser().setFreeDay();
			}
			
		}
		
	}
	
	public static LabelType getColor(int count) {
		
		LabelType lab;
		
		if(count==0) 
			lab = LabelType.NO_VISIT;
		else if(count==maxVisit)
			lab = LabelType.FULL_VISIT;
		else if(count>0 && count<maxVisit)
			lab = LabelType.MIDDLE_VISIT;
		else
			lab = LabelType.AFTER_VISIT;
		
		return lab;
		
	}
	
	public static Boolean isTabs() {
		MainWindow.labTab.setVisible(true);
		ConfigureWindow.setButtonHome();
		if(tabs+1>=maxTabs) 
			return false;
		else {
			return true;
		}
	}
	
	public static void closeCard() {
		if(tabs>1) {
			tabs--;
			MainWindow.countTab.removeAll();
			MainWindow.setCounterTab(tabs);
			MainWindow.countTab.repaint();
		}
		else {
			tabs=0;
			MainWindow.labTab.setVisible(false);
		}
	}
	
	public static int getTab() {
		return tabs;
	}
	
	public static void countTab() {
		tabs++;
		MainWindow.countTab.removeAll();
		MainWindow.setCounterTab(tabs);
		MainWindow.countTab.repaint();
	}
	
	public static void installFont() {
		
		try {
			
			font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/images/font/Sansation_regular.ttf"));
			
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
