package fait.bb.bf.graphics;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.jdesktop.animation.timing.Animator;

import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JHDresserBar;
import fait.bb.bf.graphics.panels.glasspanels.ChooseHairdresser;
import fait.bb.bf.graphics.panels.glasspanels.HelpPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.ServiceData;



public class ConfigureWindow  {
	
	//ustawienia og�lne
	private static Frame frame;
	private static  JPanel mainPanel;
	
	
	//menu
	public static JLabel menu;
	
	public static JPanel menuPanel;
	private static JButton menuHome;
	private static JButton backTab;
	private JButton displayMenu;
	
	//ustawienie pola do zmiany fryzjera
	public static JHDresserBar bar;
	
	public static JLabel mainBar;
	
	
	//menu_okna
	private static JLabel windowMenu;
	
	//zdefiniowane cardPanel
	private JPanel cardPanel;
	private CardLayout cl;
	
	//zdefiniowanie paska z widgetami
	private static JPanel widgetBarPanel;
	
	
	Animator anim = new Animator(500);
	private static Dimension maxScreen;
	
	int prevW, prevH;
	
	private static Communication comm;
	
	private void setScreen() {
		Dimension temp = Toolkit.getDefaultToolkit().getScreenSize();
		maxScreen = new Dimension(temp.width, temp.height-42);
		new Dimension(temp.width, temp.height-42-60);
	}
	
	private void setWindowMenu() {
		
		windowMenu = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/backgroundMenuWindow.png");
		windowMenu.setIcon(imgIc);
		windowMenu.setBounds(maxScreen.width-imgIc.getIconWidth(),
							 0,
							 imgIc.getIconWidth(),
							 imgIc.getIconHeight());
		
		
		
		
		//przycisk zamykajacy okno
		JButton close = new JButtonOwn("close");
		close.setLocation(windowMenu.getWidth() - close.getWidth() - 10,
						  windowMenu.getHeight()/2-close.getHeight()/2);
		close.setToolTipText("Zamknij program");
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				Boolean no = comm.youSure("Czy na pewno chcesz zamkn�� aplikacj�?", 1);
				if(no) 
					frame.dispose();
				
					
			}
		});
		
		JButton minWindow = new JButtonOwn("minimize");
		minWindow.setLocation(close.getX() - minWindow.getWidth() - 18,
							  windowMenu.getHeight()/2-minWindow.getHeight()/2);
		minWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				frame.setState(Frame.ICONIFIED);
				frame.repaint();
			}
		});     
		
		JButton helpWin = new JButtonOwn("helpWindow");
		helpWin.setLocation(minWindow.getX() - helpWin.getWidth() - 18,
							windowMenu.getHeight()/2-helpWin.getHeight()/2);
		
		helpWin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				//frame.setState(Frame.ICONIFIED);
				MainWindow.pane.setVisible(true);
				
				HelpPanel helpPanel = new HelpPanel(MainWindow.mainGlassPanel.getSize());
				helpPanel.setTextHtml();
				MainWindow.mainGlassPanel.setPanel(helpPanel.getMainPanel());
				MainWindow.mainGlassPanel.setVisible(true);
				
			}
		});
		
		windowMenu.add(close);
		windowMenu.add(minWindow);
		windowMenu.add(helpWin);
		
		mainPanel.add(windowMenu);
							
		
	}
	
	
/*	private void setMenuPanel() {
		
		menuPanel = new JPanel();
		
		menuPanel.setOpaque(false);
		menuPanel.setLayout(null);
		mainPanel.add(menuPanel);
		
		
	}*/
	
	
	private void setMainMenu() {
		
		menu = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/mainMenu.png");
		menu.setIcon(imgIc);
		
		menu.setBounds(mainPanel.getWidth() - imgIc.getIconWidth(),
					   mainPanel.getHeight() - imgIc.getIconHeight(),
					   imgIc.getIconWidth(),
					   imgIc.getIconHeight());
		
		int temp = imgIc.getIconHeight();
		int height = temp/3;
		
		
		menuHome = new JButton();
		imgIc = new ImageIcon("resources/images/menuHome.png");
		menuHome.setIcon(imgIc);
		menuHome.setToolTipText("Wr�� do strony g��wnej programu");
		
		menuHome.setBounds((menu.getWidth() - imgIc.getIconWidth())/2,
					   	   (height - imgIc.getIconHeight())/2,
					   	   imgIc.getIconWidth(),
					   	   imgIc.getIconHeight());
		
		menuHome.setBorderPainted(false);
		menuHome.setContentAreaFilled(false);
		menuHome.setFocusPainted(false);
		menu.add(menuHome);
		
		backTab = new JButton();
		imgIc = new ImageIcon("resources/images/backTab.png");
		backTab.setIcon(imgIc);
		
		backTab.setBounds((menu.getWidth() - imgIc.getIconWidth())/2,
					   	   (height - imgIc.getIconHeight())/2,
					   	   imgIc.getIconWidth(),
					   	   imgIc.getIconHeight());
		backTab.setToolTipText("Wr�� do otwartych zak�adek");
		
		backTab.setBorderPainted(false);
		backTab.setContentAreaFilled(false);
		backTab.setFocusPainted(false);
		menu.add(backTab);
		
		backTab.setVisible(false);
		
		displayMenu = new JButton();
		imgIc = new ImageIcon("resources/images/displayMenu.png");
		displayMenu.setIcon(imgIc);
		displayMenu.setToolTipText("Otw�rz menu z ustawieniami programu");
		
		displayMenu.setBounds((menu.getWidth() - imgIc.getIconWidth())/2,
					   	   	  height + (height - imgIc.getIconHeight())/2,
					   	   	  imgIc.getIconWidth(),
					   	   	  imgIc.getIconHeight());
		
		displayMenu.setBorderPainted(false);
		displayMenu.setContentAreaFilled(false);
		displayMenu.setFocusPainted(false);
		
		displayMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
			}
		});
		
		menu.add(displayMenu);
		
		JButton menuSite = new JButton();
		imgIc = new ImageIcon("resources/images/site.png");
		menuSite.setIcon(imgIc);
		
		menuSite.setToolTipText("Przejd� do strony internetowej produkt�w BestBeauty");
		
		menuSite.setBounds((menu.getWidth() - imgIc.getIconWidth())/2,
					   	   	  2*height + (height - imgIc.getIconHeight())/2,
					   	   	  imgIc.getIconWidth(),
					   	   	  imgIc.getIconHeight());
		
		menuSite.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				java.net.URI location;
				try {
					location = new java.net.URI("http://best-beauty.pl/");
					java.awt.Desktop.getDesktop().browse(location);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});

		
		menuSite.setBorderPainted(false);
		menuSite.setContentAreaFilled(false);
		menuSite.setFocusPainted(false);
		menu.add(menuSite);
		
		mainPanel.add(menu);
		
	}
	
	public static void changeButton() {
		
		if(!menuHome.isVisible()) {
			menuHome.setVisible(true);
			backTab.setVisible(false);
		}
		else {
			backTab.setVisible(true);
			menuHome.setVisible(false);
		}
			
		
	}
	
	public static void setButtonHome() {
		menuHome.setVisible(true);
		backTab.setVisible(false);
	}
	
	/*private static void changeState() {
		
		if(state==1) 
			state = 0;
		
		else if(state==0)
			state = 1;
		
	}
	*/
	
	
	
	
	public static void setBarMenu() {
		
		ChooseHairdresser chooseHair = new ChooseHairdresser(ConfigureApp.sizeOfDialog);
		
		ServiceData serviceData = new ServiceData();
		
		bar = new JHDresserBar(chooseHair);
		if(ConfigureApp.getSessions().size()==1) {
			  
			  bar.setTextOnBar(serviceData.formatUserToBar(serviceData.getUsers(" AND status!='ZWOLNIONY' ").get(0)));
			  
		 }
		
		bar.setLocation(mainBar.getWidth() - bar.getWidth() - 10,
						mainBar.getHeight()/2 - bar.getHeight()/2);
		
		//mainBar.add(bar);
		
	
		
	}
	
	
	
	private void setWidgetBarPanel() {
		
		mainBar = new JLabel();
		mainBar.setOpaque(true);
		mainBar.setLayout(null);
		mainBar.setBackground(new Color(236,199,99));
		mainBar.setBounds(0, 0,
						  maxScreen.width-windowMenu.getWidth()+5,
					      windowMenu.getHeight()-6);
		mainPanel.add(mainBar);
		
		setBarMenu();
		
		widgetBarPanel = new JPanel();
		widgetBarPanel.setOpaque(false);
		widgetBarPanel.setLayout(null);
		widgetBarPanel.setBounds(0, 0,
								 mainBar.getWidth()-bar.getWidth()-5,
								 mainBar.getHeight());
		mainBar.add(widgetBarPanel);
		
		JLabel versionApp = new JLabel("Wersja programu: " + ConfigureApp.versionApp);
		versionApp.setFont(new Font("Arial", Font.BOLD, 12));
		versionApp.setSize(160, mainBar.getHeight() - 10);
		versionApp.setForeground(ConfigureApp.backgroundColor);
		versionApp.setLocation(mainBar.getWidth() - versionApp.getWidth(), mainBar.getHeight()/2 - versionApp.getHeight()/2);
		mainBar.add(versionApp);
		
	}
	
	private void setCardPanel() {
		
		cl = new CardLayout();
		
		cardPanel = new JPanel();
		cardPanel.setLayout(cl);
		cardPanel.setBounds(18,
							windowMenu.getHeight()+12,
							maxScreen.width-18-menu.getWidth() - 20,
							maxScreen.height-2*(widgetBarPanel.getHeight()) + 10);
		
		cardPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.add(cardPanel);
		
	}
	
	private void component() {
		
		mainPanel = new JPanel();
		mainPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.setBounds(0, 0, (int) maxScreen.width, (int) maxScreen.height);
		mainPanel.setLayout(null);
		
		
		
		setMainMenu();
		setWindowMenu();
		setWidgetBarPanel();
		
		
		
		setCardPanel();
		
		//ustawienie tla panelnu
		
		frame.add(mainPanel);
		
		
	}
	
	
	
	public ConfigureWindow() {
		setScreen();
		
		frame = new Frame();
		frame.setUndecorated(true);
		frame.setVisible(true);
		//frame.setSize(SCREEN);
		frame.setSize(maxScreen.width, maxScreen.height);
	
		
		frame.setLocation(0,0);
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
			Frame frame = (Frame)we.getSource();
			frame.dispose();
			}
			  });
		
		
		
		frame.addWindowStateListener(new WindowStateListener() {
			
			@Override
			public void windowStateChanged(WindowEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("Stan okna: " + frame.getState());
				
				//MainWindow.countTab.remove(MainWindow.textCounter);
				//MainWindow.setCounterTab(ConfigureApp.getTab());
				//MainWindow.textCounter.revalidate();
				frame.repaint();
				
			}
		});
		
		comm = new Communication(frame);
		
		component();
		
		frame.repaint();
	
	}

	public static Frame getFrame() {
		return frame;
	}

	public static void setFrame(Frame frame) {
		ConfigureWindow.frame = frame;
	}

	public static Dimension getMaxScreen() {
		return maxScreen;
	}

	public static void setMaxScreen(Dimension maxScreen) {
		ConfigureWindow.maxScreen = maxScreen;
	}
	
	public  JPanel getMainPanel() {
		return mainPanel;
	}

	public JPanel getCardPanel() {
		return cardPanel;
	}

	public CardLayout getCl() {
		return cl;
	}

	public JPanel getWidgetBarPanel() {
		return widgetBarPanel;
	}
	
	public JButton getMenuHome() {
		return menuHome;
	}
	
	public JButton getDisplayMenu() {
		
		return displayMenu;
		
	}
	
	public JButton getBackTab() {
		return backTab;
	}
	

	public static void repaintMenu() {
		
		
		
	}
	
	public int getHeightBar() {
		
		return widgetBarPanel.getHeight();
		
	}
	
		



	
	
	
	

}


