package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.Calendar;
import java.util.Date;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;

public class ListVisitView extends JLabel {
	
	private JLabel nameLabel;
	private JLabel codeLabel;
	
	private Visit visit;
	private Customer customer;
	private String user;
	private String kind;
	
	private Date date;
	
	private Font font = new Font("Arial", Font.BOLD, 15);
	private Color fontColor = new Color(59,59,59);
	
	private JButton edit;
	private JButton delete;
	
	public ListVisitView(Visit visit, Customer customer, String user, String kind) {
		
		this.visit = visit;
		this.customer = customer;
		this.user = user;
		this.kind = kind;
		
		setLayout(null);
		
		components();
		
	}
	
	private void components() {
		
		
		
		
		ImageIcon imgIc = new ImageIcon("resources/images/backCustomer.png");
		setIcon(imgIc);
		setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		
		
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(visit.getDateToString());
		
		System.out.println("Data przy ListVisitView: " + cal.get(Calendar.DAY_OF_WEEK));
		
		String month = "";
		Integer tempMonth = cal.get(Calendar.MONTH) + 1;
		if(tempMonth<10)
			month = "0" + tempMonth;
		else
			month = tempMonth.toString();
		
		String name = CalendarApp.getNameOfDay(cal.get(Calendar.DAY_OF_WEEK)-1) + ", " + 
					  cal.get(Calendar.DAY_OF_MONTH) + "." + 
					  month + "." + 
					  cal.get(Calendar.YEAR);
		
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(name, null);		
		nameLabel = new JLabel();
		nameLabel.setFont(font);
		nameLabel.setText(name);
		nameLabel.setForeground(fontColor);
		nameLabel.setBounds(15,
						    5,
						   (int) bounds.getWidth(),
						   (int) bounds.getHeight());
		
		
		//add(nameLabel);
		
		name = "godz: " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
		bounds = metrics.getStringBounds(name, null);
		
		JLabel hour = new JLabel();
		hour.setFont(font);
		hour.setText(name);
		hour.setForeground(fontColor);
		hour.setSize((int) bounds.getWidth(),
					 (int) bounds.getHeight());
		hour.setLocation(nameLabel.getX() + nameLabel.getWidth() + 30,
						 nameLabel.getY());
		//add(hour);
		
		if(kind.equals("customer")) 
			name = visit.getUser().toString();
		else
			name = "Klient: " + visit.getCustomer().getName() + " " + visit.getCustomer().getSurname();
			
		bounds = metrics.getStringBounds(name, null);
		
		JLabel person = new JLabel(); 
		person.setFont(font);
		person.setText(name);
		person.setForeground(fontColor);
		person.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		person.setLocation(100,
						   21);
		add(person);
		
		name = "Czas trwania: " + visit.getDuration();
		bounds = metrics.getStringBounds(name, null);
		
		JLabel duration = new JLabel(); 
		duration.setFont(font);
		duration.setText(name);
		duration.setForeground(fontColor);
		duration.setSize((int) bounds.getWidth(),
					     (int) bounds.getHeight());
		duration.setLocation(person.getX(),
						   	 person.getY() + person.getHeight() + 2);
		add(duration);
		
		
		name = visit.getText();
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Arial", Font.PLAIN, 12));
		textArea.setText(name);
		//textArea.setOpaque(false);
		textArea.setBackground(new Color(232,232,232));
		
		textArea.setForeground(fontColor);
		textArea.setRows(1);
		textArea.setSize(400,
						 20);
		textArea.setLocation(person.getX(),
							 duration.getY() + duration.getHeight() + 2);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setBorder(new EmptyBorder(1,5,0,0));
		textArea.setEditable(false);
		
		add(textArea);
		
		JButtonWithout come = new JButtonWithout("comeToCard");
		come.setLocation(getWidth() - come.getWidth(), 0);
		add(come);
		
		come.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int size = MainWindow.tabbedPanel.getTabCount();
				int i=0;
				Boolean isThere=false;
				
				ServiceData serviceData = new ServiceData();
				
				
				Customer custTemp = serviceData.getCustomer(visit.getCustomer().getId());
				custTemp = serviceData.getCustomerMeta(custTemp);
				
				System.out.println("CustTemp: " + custTemp.getId());
				
				while(i<size) {
					if(MainWindow.tabbedPanel.getTitleAt(i).equals(custTemp.getSurname() 
							+ " " + custTemp.getName())) {
						isThere = true;
						MainWindow.tabbedPanel.setSelectedIndex(i);
						JPanel temp = ((JPanel) MainWindow.tabbedPanel.getSelectedComponent());
					}
						
					i++;
				}
				if(!isThere) {
					if(ConfigureApp.isTabs()) {
						
						ConfigureApp.countTab();
						MainWindow.currentCard=4;
						MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
						
						CardPanel cardClient = new CardPanel(0,custTemp);
						cardClient.setFormula(Integer.toString(visit.getDuration()),
											  Float.toString(visit.getAllPrice()));
						MainWindow.listPanels.add(cardClient);
						
						MainWindow.tabbedPanel.addTab(custTemp.getSurname() + " "  + custTemp.getName(), 
													  cardClient.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount()-1;

						MainWindow.tabbedPanel.setForegroundAt(selIndex, ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);

						MainWindow.setWidgetBar();
						
						/*if (ConfigureWindow.bar.getText().equals("------------------------")) {
							MainWindow.pane.setVisible(true);
							MainWindow.choosePanel.setVisible(true);
						}*/
						
					}	
					else {
						new Communication(MainWindow.getFrame()).dont("Masz zbyt du�o otwartych zak�adek");
					}
				}
				
				MainWindow.pane.setVisible(false);
				MainWindow.addDate.setVisible(false);
				MainWindow.mainGlassPanel.removePanel();
				MainWindow.mainGlassPanel.setVisible(false);
			}
		});
		
		edit = new JButtonWithout("editVisit");
		edit.setLocation(1, 7);
		add(edit);
		
		delete = new JButtonWithout("deleteVisit");
		delete.setLocation(edit.getX(), edit.getY()+edit.getHeight());
		add(delete);
	}
	
	public Visit getVisit() {
		
		return visit;
		
	}
	
	public String getUser() {
		
		return user;
		
	}
	
	public JButton getEdit() {
		
		return edit;
		
	}
	
	public JButton getDelete() {
		
		return delete;
		
	}
	

}
