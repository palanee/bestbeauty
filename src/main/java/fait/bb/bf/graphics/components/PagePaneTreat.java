package fait.bb.bf.graphics.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.PagePanel;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Treatment;

public class PagePaneTreat extends PagePanel {
	
	private JPanel addPanel;
	private AdminPanel adminPanel;
	
	
	public PagePaneTreat(JPanel addPanel, AdminPanel admin) {
		
		this.addPanel = addPanel;
		this.adminPanel = admin;
	}


	@Override
	public void setListPanel(List list) {
		// TODO Auto-generated method stub
		
		addPanel.removeAll();
		
		Iterator<Treatment> it = list.iterator();
		ListTreatView temp;
		int y = 0;
		int position = 0;
		while(it.hasNext()) {
			Treatment t = it.next();
			temp = getTreatLab(t);
			temp.setLocation(adminPanel.getMainPanel().getWidth()/2 - (temp.getWidth() - 80)/2,
							y);
			position = temp.getX();
			addPanel.add(temp);
			y = temp.getY() + temp.getHeight() + 1;
		}
		
		addPanel.repaint();
	}
	
	private ListTreatView getTreatLab(final Treatment t) {
		
		final ListTreatView lab = new ListTreatView(t);
		
		final ServiceData serviceData = new ServiceData();
		final Communication comm = new Communication(MainWindow.getFrame());
	
		JButton delete = lab.getDelete();
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				if(comm.youSure("Czy na pewno chcesz usunu�� zabieg?", 0)) {
					
					serviceData.deleteTreat(t.getNumber());
					adminPanel.removeAllTreatmentPanel();
						
					MainWindow.addDate.updateVisit();
					
					
					
				}
				
			}
		});
		
		if(t.getName().equals("15 min") || t.getName().equals("30 min") || t.getName().equals("45 min") || t.getName().equals("60 min"))
			delete.setEnabled(false);

		
		JButton edit = lab.getEdit();
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				
			}
		});
		
		if(t.getName().equals("15 min") || t.getName().equals("30 min") || t.getName().equals("45 min") || t.getName().equals("60 min"))
			edit.setEnabled(false);
		
		lab.add(delete);
		lab.add(edit);
		
		return lab;
		
	}

}
