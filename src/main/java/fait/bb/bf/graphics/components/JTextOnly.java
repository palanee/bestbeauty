package fait.bb.bf.graphics.components;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;

public class JTextOnly extends PlainDocument {
	
	JTextComponent text;
	private int limit;
	JPanel panel;
	JLabel lab;
	
	public JTextOnly(JTextComponent text, JPanel panel, int limit) {
		super();
		this.text = text;
		this.panel = panel;
		this.limit = limit;
		lab = new JLabel("Wpisuj tylko liczby");
     	lab.setSize(text.getWidth(), 12);
     	lab.setForeground(Color.red);
     	lab.setLocation(text.getX(), text.getY() + text.getHeight() + 2);
     	panel.add(lab);
     	lab.setVisible(false);
     	panel.repaint();
	}
	
	public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
		
		if (str == null) return;

		char[] chars = str.toCharArray();
        boolean ok = true;

        for (int i = 0; i < chars.length; i++ ) {
        	
        		 try {
                     Integer.parseInt(String.valueOf(chars[i]));
                 } catch ( NumberFormatException exc ) {
                	 lab.setText("Wpisuj tylko liczby");
                	 lab.setVisible(true);
                     ok = false;
                     break;
                 } 
        	 

        }
        
        if(ok && (getLength() + str.length()) > limit) {
        	lab.setText("Wpisa�a�/-e� ju� maksymaln� liczb� znak�w");
        	lab.setVisible(true);
        }
        else if (ok && (getLength() + str.length()) <= limit) {
            super.insertString( offset, new String( chars ), attr);
            lab.setVisible(false);
        }
        else {
        	lab.setVisible(true);
        }
	
	}

}
