package fait.bb.bf.graphics.components;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class JTextFieldBackground extends JTextField {
	
	private ImageIcon imgIc;
	private int leftMargin;
	
	public JTextFieldBackground(ImageIcon imgIc, int leftMargin)  {  
		this.imgIc = imgIc;
		this.leftMargin = leftMargin;
	}  
	  
	public void paint(Graphics g)  {  
		
		setOpaque(false); 
		setBorder(null);
		//setMargin(new Insets(0,10,0,0));
		setBorder(new EmptyBorder(0, leftMargin, 0, 0));
		Image image=imgIc.getImage();  
		g.drawImage(image,0,0,this);  
		super.paint(g);  
	}  
	  
	public int getWidth() {
		return imgIc.getIconWidth();
	}
	
	public int getHeight() {
		return imgIc.getIconHeight();
	}

}
