package fait.bb.bf.graphics.components;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class JButtonStates extends JButton {
	
	ImageIcon imgIcNot;
	ImageIcon imgIcYes;
	ImageIcon imgIcBet;
	
	
	Dimension size;
	
	//true - imgIcYes - wcisniety
	Boolean state;
	
	
	public JButtonStates(String name, Boolean state) {
		
		imgIcNot = new ImageIcon("resources/images/" + name + ".png");
		imgIcYes = new ImageIcon("resources/images/" + name + "-3.png");
		imgIcBet = new ImageIcon("resources/images/" + name + "-2.png");
		
		if(state)
			setIcon(imgIcYes);
		else
			setIcon(imgIcBet);
		
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + "-0.png");
		
		size = new Dimension(imgIcNot.getIconWidth(), imgIcNot.getIconHeight());
		setSize(size);
		setRolloverIcon(imgIcBet);
		setPressedIcon(imgIcYes);
		setDisabledIcon(imgIc);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setName(name);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		
	}
	
	public void changeState(Boolean pressed) {
		
		if(pressed) {
			setIcon(imgIcYes);
			setRolloverIcon(imgIcBet);
			setPressedIcon(imgIcYes);
		}
		else {
			setIcon(imgIcNot);
			setRolloverIcon(imgIcBet);
			setPressedIcon(imgIcYes);
		}
		
	}
	
}
