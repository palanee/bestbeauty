package fait.bb.bf.graphics.components.customUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

import fait.bb.bf.graphics.ConfigureApp;


public class DateComboBoxUI extends BasicComboBoxUI {
	
	
	
	DateComboBoxUI() {
		
		super();
	}
	
	public static ComponentUI createUI(JComponent c) {
	      return new DateComboBoxUI();
	}
	
	protected void installDefaults()
	{
		comboBox.setBorder(BorderFactory.createMatteBorder(
                5, 5, 5, 5, new Color(213,212,212)));
		
	}
	
	protected JButton createArrowButton()  {
		
		arrowButton = new JButton();
		
		ImageIcon imgIc = new ImageIcon("resources/images/buttons/arrowComboBox.png");
		arrowButton.setIcon(imgIc);
		arrowButton.setSize(imgIc.getIconWidth(),
							imgIc.getIconHeight());
		arrowButton.setLocation(205,
								5);
		arrowButton.setBorderPainted(false);
		arrowButton.setContentAreaFilled(false);
		
		return arrowButton;
		
	}
	
	public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
		
		if(hasFocus) {
			g.setColor(ConfigureApp.backgroundColor);
		}
		else {
			g.setColor(ConfigureApp.backgroundColor);
		}
		
	}
	
	protected LayoutManager createLayoutManager() {
		
		return null;
		
	}
	
	public void paint(Graphics g, JComponent c) {
		
		Graphics2D g2 = (Graphics2D) g;
		super.paint(g,c);
		
	}
	
}
