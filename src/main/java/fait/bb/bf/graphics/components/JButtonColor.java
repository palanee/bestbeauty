package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JButton;

public class JButtonColor extends JButton {
	
	private Color color;
	private Dimension size;
	
	public JButtonColor(Color color, Dimension size) {
		
		
		this.color = color;
		this.size = size;
		
	}
	
	public void setColor(Color c) {
		
		this.color = c;
		
	}
	
	public void paint(Graphics g) {
		
		setOpaque(false); 
		setBorder(null);
		setLayout(null);
		g.setColor(color);
		g.fillRect(0, 0, (int) size.width, (int) size.height);
		g.setColor(Color.WHITE);
		setUI(null);
		super.paint(g);  
		
	}

}
