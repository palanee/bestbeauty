package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import fait.bb.bf.graphics.ConfigureApp;


public class ButtonSite extends JButton {
	
	private JLabel num;
	private int number;
	private ImageIcon imgIc1;
	private ImageIcon imgIc2;
	private int state;
	
	public ButtonSite(int number, int state) {
		
		this.number = number;
		this.state = state;
		
		imgIc1 = new ImageIcon("resources/images/buttons/site.png");
		imgIc2 = new ImageIcon("resources/images/buttons/site-2.png");
		
		setButton();
		
	}
	
	private void setButton() {
		
		if(state==1)
			setIcon(imgIc1);
		else
			setIcon(imgIc2);
		
		setSize(imgIc1.getIconWidth()+2,
				imgIc1.getIconHeight());
		
	/*	String s = String.valueOf(number);
		Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 14);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		num = new JLabel(s);
		num.setFont(font);
		num.setForeground(Color.WHITE);
		num.setBounds((getWidth() - (int) bounds.getWidth())/2 + 1,
					  (getHeight() - (int) bounds.getHeight())/2 + 1,
					   (int) bounds.getWidth(),
					   (int) bounds.getHeight());*/
		
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		
		//add(num);
		
	}
	
	public int getNumber() {
		
		return number;
		
	}
	
	public void changeIcon(int state) {
		if(state==1) 
			setIcon(imgIc1);
		else
			setIcon(imgIc2);
	}
	

}
