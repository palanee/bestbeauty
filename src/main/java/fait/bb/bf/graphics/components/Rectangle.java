package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

public class Rectangle extends JComponent {
	
	private int x;
	private int y;
	private int width;
	private int height;
	private Color color;
	
	Rectangle(int x, int y, int width, int height, Color color) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
		
	}
	
	public void paint(Graphics g) {
		g.setColor(color);
	    g.fillRect(x, y, width, height);
	 }

}
