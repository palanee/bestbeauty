package fait.bb.bf.graphics.components.customUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.components.BasicArrowButtonOwn;
import fait.bb.bf.graphics.components.PopUpTab;
import fait.bb.bf.graphics.components.PopUpTabListener;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;


public class TabbedPaneUI extends BasicTabbedPaneUI {
	
	private Font boldFont;
	private FontMetrics boldFontMetrics;
	private Color fillColor = Color.RED;
	private Color unselected = new Color(236,199,99);
	private int max = 130;
	


	protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected)
	{
		/*g.setColor(MainWindow.cards);
		g.drawLine(x, y, x, y + h);
		g.drawLine(x, y, x + w, y);
		g.drawLine(x + w, y, x + w, y + h);

		if (isSelected) {
			g.setColor(MainWindow.cards);
			
		}
		else {
			g.setColor(unselected);
		}*/
	}
	
	
	
	protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected)
	{
		Polygon shape = new Polygon();

		shape.addPoint(x, y + h + 2);
		shape.addPoint(x, y);
		shape.addPoint(x + w, y);
		shape.addPoint(x + w, y + h + 2);
		
		if(isSelected) {
			g.setColor(ConfigureApp.backgroundCardColor);
		}
		else {
			g.setColor(unselected);
		}
		
		

		g.fillPolygon(shape);
	}
	
	protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected)
	{
		 // Do nothing
	} 
	
	protected void installDefaults()
	{
		super.installDefaults();
		tabAreaInsets.left = 0;
		maxTabWidth = 100;
		selectedTabPadInsets = new Insets(10, 5, 0, 0);
		tabInsets = selectedTabPadInsets;

		fillColor = ConfigureApp.backgroundColor;
		
		boldFont = ConfigureApp.font.deriveFont(Font.BOLD, 16);
		boldFontMetrics = tabPane.getFontMetrics(boldFont);
		
		PopUpTabListener l = new PopUpTabListener(new PopUpTab());
		tabPane.addMouseListener(l);
		
	}
	
	protected void installKeyboardActions() {
		
		super.installKeyboardActions();
		tabPane.getActionMap().put("foo", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("kasuj");
                //tabbedPanel.setSelectedIndex(1);
                //tabPane.remove(tabPane.getSelectedIndex());
                tabPane.setBackground(Color.RED);
            }
        });

    /* connect two keystrokes with the newly created "foo" action:
       - a
       - CTRL-a
    */
		InputMap inputMap = tabPane.getInputMap();
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK), "foo");
	}
	
	protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight)
	{
		int vHeight = fontHeight + 20;
		if (vHeight % 2 > 0) {
			vHeight += 1;
		}
		return vHeight;
	}
	
	protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics)
	{	
		//int[] tab = metrics.getWidths();
		
		String title = MainWindow.tabbedPanel.getTitleAt(tabIndex);
		Rectangle2D bounds = metrics.getStringBounds(title, null);
		
		int width =  (int) bounds.getWidth();
		
		return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + width + 10;
	}
	
	protected int getTabLabelShiftY(int tabPlacement, int tabIndex, boolean isSelected)
	{
		return 0;
	}
	 
	protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected)
	{
		if (isSelected) {
			int vDifference = (int) (boldFontMetrics.getStringBounds(title, g)
					.getWidth())
					- textRect.width;
			textRect.x -= (vDifference / 2);
		
			super.paintText(g, tabPlacement, boldFont, boldFontMetrics,
					tabIndex, title, textRect, isSelected);
		} else {
			int vDifference = (int) (boldFontMetrics.getStringBounds(title, g)
					.getWidth())
					- textRect.width;
			textRect.x -= (vDifference / 2);

			font = ConfigureApp.font.deriveFont(Font.BOLD,14);
			
			super.paintText(g, tabPlacement, font, metrics, tabIndex, title,
					textRect, isSelected);
		}
	}
	
	protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex)
	{
		int tw = tabPane.getBounds().width;

		g.setColor(fillColor);
		g.fillRect(0, 0, tw, rects[0].height + 3);
		
		

		super.paintTabArea(g, tabPlacement, selectedIndex);
	}
	
	
	
	protected void paintContentBorderRightEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h)
	{
		// Do nothing
	}

	protected void paintContentBorderLeftEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h)
	{
		// Do nothing
	}

	protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h)
	{
		// Do nothing
	}
	
	
	
	protected Insets getContentBorderInsets(int tabPlacement)
	{
		return new Insets(0, 0, 0, 0);
	}
	
	protected JButton createScrollButton(int direction)
	{
		return new BasicArrowButtonOwn(direction){
			public Dimension getPreferredSize() {
				return new Dimension(15, 40);
			}
		};
		
	}
	 

}
