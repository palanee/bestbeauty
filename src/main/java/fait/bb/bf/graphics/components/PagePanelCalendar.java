package fait.bb.bf.graphics.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.PagePanel;
import fait.bb.bf.graphics.calendar.CalendarDayNew;
import fait.bb.bf.graphics.calendar.CalendarHeader;
import fait.bb.bf.graphics.calendar.CalendarHourEt;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;

public class PagePanelCalendar extends PagePanel {
	
	JPanel addPanel;
	CalendarApp calendarApp;
	Calendar currentDate;
	int userWidth;
	
	public PagePanelCalendar(JPanel addPanel, CalendarApp calendarApp, Calendar currentDate) {
		
		this.addPanel = addPanel;
		this.calendarApp = calendarApp;
		this.currentDate = currentDate;
		
	}
	
	@Override
	public void setListPanel(List list) {
		// TODO Auto-generated method stub
		System.out.println("Wielko�� listy user�w: " + list.size());
		System.out.println("Lista: " + list);
		addPanel.removeAll();
		addPanel.add(new CalendarHourEt(calendarApp.changeDate(currentDate)));
		System.out.println("Po�o�enie calendar: " + addPanel.getPreferredSize());
		
		Iterator<User> it = list.iterator();
		CalendarDayNew dayTemp;
	
		
		CalendarPanelNew.countDays(CalendarDayNew.getWidthCol(), list.size());
		int width = CalendarPanelNew.getUserWidth();
		System.out.println("Liczymy...." + width);
		
		while(it.hasNext()) {
		
			User u = it.next();
			dayTemp = new CalendarDayNew(calendarApp.changeDate(currentDate), u);
			//dayTemp.setLocation(startX, 0);
			
			dayTemp.setWidth(width);
			CalendarPanelNew.addToCalendarList(dayTemp);
			addPanel.add(dayTemp);
			//calendarPanel.repaint();
			dayTemp.repaint();
			
		}
		
		System.out.println("Lista u�ytkownik�w wysy�ana do scrolla: " + list);
		CalendarPanelNew.refreshScroll(list);
		
		addPanel.revalidate();
		addPanel.repaint();
		
	}
	
	
	
}
