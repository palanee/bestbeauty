package fait.bb.bf.graphics.components;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import fait.bb.bf.graphics.ConfigureApp;

public class JButtonOwn extends JButton {
	
	private Dimension size;
	private JLabel nameLabel;
	
	private Dimension smallSize;
	
	public JButtonOwn(String name) {
		
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + ".png");
		setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		setSize(size);
		imgIc = new ImageIcon("resources/images/" + name + "-2.png");
		setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-3.png");
		setPressedIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-0.png");
		setDisabledIcon(imgIc);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setName(name);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
	}
	
	public JButtonOwn(String name, String string) {
		
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + ".png");
		setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		setSize(size);
		imgIc = new ImageIcon("resources/images/" + name + "-2.png");
		setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-3.png");
		smallSize = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		setPressedIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-0.png");
		setDisabledIcon(imgIc);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setName(name);
		setLayout(null);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		//Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 15);
		Font font = new Font("Arial", Font.PLAIN, 15);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(string, null);
		
		nameLabel = new JLabel(string);
		nameLabel.setFont(font);
		nameLabel.setForeground(ConfigureApp.backgroundColor);
		nameLabel.setSize((int) bounds.getWidth(),
					(int) bounds.getHeight());
		nameLabel.setLocation(size.width/2 - nameLabel.getWidth()/2,
						(size.height - (size.height-smallSize.height))/2 - nameLabel.getHeight()/2);
		
		add(nameLabel);
		
		
	}
	
	
	
	
	public JButtonOwn(String name, Boolean resize) {
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + ".png");
		
		int x = (int) (imgIc.getIconWidth()*0.5);
		int y = (int) (imgIc.getIconHeight()*0.5) ;
		this.size = new Dimension(x + 20, y + 20);
		
		imgIc = changeSize(imgIc);
		setIcon(imgIc);
		setSize(this.size);
		
		imgIc = changeSize(new ImageIcon("resources/images/" + name + "-2.png"));
		setRolloverIcon(imgIc);
		
		imgIc = changeSize(new ImageIcon("resources/images/" + name + "-3.png"));
		setPressedIcon(imgIc);

		imgIc = changeSize(new ImageIcon("resources/images/" + name + "-0.png"));
		
		setDisabledIcon(imgIc);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setName(name);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
	}
	
	public ImageIcon changeSize(ImageIcon imgIc) {
		
		Image img = imgIc.getImage();  
		Image newimg = img.getScaledInstance((int) (0.6 * imgIc.getIconWidth()), 
											 (int) (0.6 * imgIc.getIconHeight()),  
										 		java.awt.Image.SCALE_SMOOTH);  
		imgIc = new ImageIcon(newimg); 
		return imgIc;
		
	}
	
	
	public void changeIcon(String name) {
		
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + ".png");
		setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		setSize(size);
		imgIc = new ImageIcon("resources/images/" + name + "-2.png");
		setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-3.png");
		setPressedIcon(imgIc);
		imgIc = new ImageIcon("resources/images/" + name + "-0.png");
		setDisabledIcon(imgIc);
		
		
	}
	
	public void changeText(String string) {
		
		Font font = new Font("Arial", Font.PLAIN, 15);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(string, null);
		
		nameLabel.setText(string);
		nameLabel.setSize((int) bounds.getWidth(),
					(int) bounds.getHeight());
		nameLabel.setLocation(size.width/2 - nameLabel.getWidth()/2,
						(size.height - (size.height-smallSize.height))/2 - nameLabel.getHeight()/2);
		
		add(nameLabel);
		
		this.repaint();
	}
	
	public Dimension getSize() {
		
		return size;
		
	}

}
