package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.logic.Visit;

public class ListVisitsCard extends JLabel{
	
	private Visit visit;
	private Dimension size;
	private Color color;
	
	
	private JTextField date;
	private JTextField hour;
	private JTextField code;
	private JLabel realizeVisit;
	
	private int height = 32;
	private int counter;
	
	private CardPanel cardPanel;
	
	public ListVisitsCard(Visit visit, int width, Color color, CardPanel c, int counter) {
		
		this.visit = visit;
		this.color = color;
		this.cardPanel = c;
		this.counter = counter;
		
		if(!this.visit.getActive())
			this.color = new Color(213,213,213);
		
		setSize(width, height);
		System.out.println("Rozmiar przes�any: " + size);
		System.out.println("Rozmiar temp: " + getSize());
		
		initComponents();
		
		repaint();
	}
	
	private void initComponents() {
		
		
		
		System.out.println("Generujemy pole z wizyt�");
		
		date = new JTextField();
		date = setTextField(date, new Dimension(129,height), visit.getDateWithout());
		date.setLocation(0,0);
		add(date);
		
		hour = new JTextField();
		hour = setTextField(hour, new Dimension(71, height), visit.getTime());
		hour.setLocation(date.getX() + date.getWidth() + 1, 0);
		add(hour);
		
		code = new JTextField();
		code = setTextField(code, new Dimension(77, height), visit.getUserCode());
		code.setLocation(hour.getX() + hour.getWidth() + 1, 0);
		add(code);
		
		String s;
		
		if(visit.getActive())
			s = "Realizuj >>";
		else
			s = "Edytuj >>";
		
		realizeVisit = new JLabel(s);
		realizeVisit.setForeground(color);
		realizeVisit.setFont(new Font("Arial", Font.BOLD, 18));
		realizeVisit.setSize(117, 18);
		realizeVisit.setLocation(code.getX() + code.getWidth() + 10,
								 code.getY() + (code.getHeight()/2 - realizeVisit.getHeight()/2));
		add(realizeVisit);
		
		realizeVisit.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
				
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				realizeVisit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				realizeVisit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				setBoldBorder();
				cardPanel.setCounter(0);
				cardPanel.setPlainBorderVisit(counter);
				if(!visit.getActive()) {
					System.out.println("Id wizyty w ListVisitCard: " + visit.getId());
					System.out.println("Zabiegi w ListVisitCard: " + visit.getAllTreatments());
					cardPanel.editVisit(visit, counter);				
				}
				else {
					cardPanel.realizeVisit(visit, counter);
				}
				
			}
		});
		
	}
	
	private void setBoldBorder() {
		
		date.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(
						3, 3, 3, 3, color),
						BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		hour.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(
				3, 3, 3, 3, color),
				BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		code.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(
				3, 3, 3, 3, color),
				BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		
	}
	
	public void setPlainBorder() {
		
		date.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
	  				   BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		hour.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
	  			BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		code.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
	  			BorderFactory.createEmptyBorder(0, 3, 0, 3)));
	}
	
	private JTextField setTextField(JTextField textField, Dimension size, String s) {
		
		textField.setSize(size);
		textField.setBackground(ConfigureApp.backgroundColor);
		textField.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
				  			BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		textField.setFont(new Font("Arial", Font.PLAIN, 18));
		textField.setForeground(color);
		textField.setText(s);
		textField.setEditable(false);
		textField.setHighlighter(null);
		textField.setHorizontalAlignment(JTextField.CENTER);
		
		return textField;
		
	}
	
	public int getWidthData() {
		return date.getWidth();
	}
	
	public int getWidthHour() {
		return hour.getWidth();
	}
	
	public int getWidthCode() {
		return code.getWidth();
	}
	
	

}
