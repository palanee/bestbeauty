package fait.bb.bf.graphics.components.customUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

import fait.bb.bf.graphics.ConfigureApp;


public class CalendarComboBoxUI extends BasicComboBoxUI {
	
	
	BufferedImage helpImage;
	
	CalendarComboBoxUI() {
		
		super();
	}
	
	public static ComponentUI createUI(JComponent c) {
	      return new CalendarComboBoxUI();
	}
	
	protected void installDefaults()
	{
		comboBox.setBorder(BorderFactory.createMatteBorder(
                5, 5, 5, 5, new Color(175,123,172)));
		
	}
	
	protected JButton createArrowButton()  {
		
		arrowButton = new JButton();
		
		ImageIcon imgIc = new ImageIcon("resources/images/arrowComboBox.png");
		arrowButton.setIcon(imgIc);
		arrowButton.setSize(imgIc.getIconWidth(),
							imgIc.getIconHeight());
		arrowButton.setLocation(250,
								5);
		
		return arrowButton;
		
	}
	
	public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
		
		if(hasFocus) {
			g.setColor(ConfigureApp.backgroundColor);
		}
		else {
			g.setColor(ConfigureApp.backgroundColor);
		}
		
	}
	
	protected LayoutManager createLayoutManager() {
		
		return null;
		
	}
	
	public void paint(Graphics g, JComponent c) {
		
		Graphics2D g2 = (Graphics2D) g;
		super.paint(g,c);
		
	}

}
