package fait.bb.bf.graphics.components.customUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import fait.bb.bf.graphics.ConfigureApp;


public class ComboBoxRenderer extends JLabel implements ListCellRenderer {

	int option;
	
	public ComboBoxRenderer(int option) {
		
		this.option = option;
		
		setOpaque(true);
        //setHorizontalAlignment(CENTER);
        setVerticalAlignment(CENTER);
	}
	
	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		
		//int selectedIndex = ((Integer)value).intValue();
		if (isSelected) {
			if(option == 0) {
				setBackground(ConfigureApp.backgroundCardColor);
				setForeground(new Color(119,42,115));
			}
			else if(option==1) {
				setBackground(ConfigureApp.backgroundCardColor);
				setForeground(new Color(76,75,75));
			}
		}
		else {
			setBackground(list.getBackground());
			if(option==0)
				setForeground(new Color(119,42,115));
			else
				setForeground(new Color(76,75,75));
		}
		setText((String) value);
		if(option==0)
			setFont(ConfigureApp.font.deriveFont(Font.BOLD, 14));
		else 
			setFont(ConfigureApp.font.deriveFont(Font.BOLD, 12));
		
		setPreferredSize(new Dimension(list.getWidth(), 30));
		
		return this;
	}
	
	

}
