package fait.bb.bf.graphics.components.customUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSpinnerUI;
import java.util.logging.Handler;

public class DateSpinnerUI extends BasicSpinnerUI {
	
	
	DateSpinnerUI() {
		
		super();
	
	}
	
	
	public static DateSpinnerUI createUI(JComponent c) {
	      return new DateSpinnerUI();
	}
	
	
	
	/*protected JComponent createNextButton() {
		
		JButton but = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/buttons/arrowNext.png");
		but.setIcon(imgIc);
		but.setDisabledIcon(imgIc);
		but.setBorderPainted(false);
		but.setContentAreaFilled(false);
		but.setFocusPainted(false);
		but.setSize(imgIc.getIconWidth(),
							imgIc.getIconHeight());
		but.setLocation(205,
						5);
		spinner.add(but);
		
		return but;
		
	}*/
	
	protected void installDefaults()
	{
		spinner.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, new Color(232,186,55)));
		
	}
	
	/*protected void installListeners() {
		
	}
	
	protected void installUI() {
		
		
		
	}
	*/
	/*protected JComponent createPreviousButton() {
		
		JButton but = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/buttons/arrowPrev.png");
		but.setIcon(imgIc);
		but.setDisabledIcon(imgIc);
		but.setBorderPainted(false);
		but.setContentAreaFilled(false);
		but.setFocusPainted(false);
		but.setSize(imgIc.getIconWidth(),
							imgIc.getIconHeight());
		but.setLocation(205,
						35-imgIc.getIconHeight()-5);
		spinner.add(but);
		
		return but;
	}*/
	
	/*protected LayoutManager createLayoutManager() {
		
		return null;
		
	}*/
	
	
	

}
