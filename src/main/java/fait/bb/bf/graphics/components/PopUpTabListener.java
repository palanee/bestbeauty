package fait.bb.bf.graphics.components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import fait.bb.bf.graphics.panels.mainpanels.MainWindow;

public class PopUpTabListener extends MouseAdapter {
	
	private JPopupMenu popup;

	public PopUpTabListener(JPopupMenu popup) {
		
		this.popup = popup;
		
		
	}
	
	 public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	System.out.println("KlikniÍto!!!");
	        	
	        
	        		
	        		popup.show(e.getComponent(),
		                       e.getX(), e.getY());
		            MainWindow.cardPanel.repaint();
	        	
	        	
	            
	        }
	    }

}
