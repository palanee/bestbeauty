package fait.bb.bf.graphics.components;

import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.widgets.WidgetCalendar;
import fait.bb.bf.widgets.WidgetFind;

public class PopUpTab extends JPopupMenu {
	
	private JMenuItem menuItems;
	
	public PopUpTab() {
		
		menuItems = new JMenuItem("Zamknij zak�adk�");
		add(menuItems);
		
		menuItems.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				MainWindow.closeTabAction();
				MainWindow.tabbedPanel.repaint();
			}
		});
		//addMouseListener(new PopUpTabListener(this));
		
	}
	

}
