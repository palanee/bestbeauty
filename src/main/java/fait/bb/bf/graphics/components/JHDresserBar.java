package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.glasspanels.ChooseHairdresser;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;

public class JHDresserBar extends JPanel {
	
	private JButton arrowBut;
	private String text;
	private JLabel lab;
	
	private ChooseHairdresser chooseHair;
	
	private static String textOnBar = "Zaloguj pracownika";
	
	public JHDresserBar(ChooseHairdresser chooseHair) {
		
		setOpaque(true);
		setBackground(ConfigureApp.backgroundColor);
		setSize(283, 26);
		setLayout(null);
		
		this.chooseHair = chooseHair;
		//ImageIcon imgIc = new ImageIcon("resources/images/bar.png");
		//setIcon(imgIc);
		
		addMouseListener(new MouseAdapter() {
			
			
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				setActionBut(false);
			}
		});
		
		setTextOnBar(textOnBar);
		//setStartLabel();
	}
	
	public void setActionBut(Boolean isSaved) {
		
		MainWindow.mainGlassPanel.removePanel();
		chooseHair.setUsers(isSaved);
		chooseHair.setMainPanel();
		MainWindow.mainGlassPanel.setPanel(chooseHair.getMainPanel());
		MainWindow.pane.setVisible(true);
		MainWindow.mainGlassPanel.setVisible(true);
		
		
	}
	
	public void setBut() {
		
		arrowBut = new JButtonOwn("arrowHairDress");
		arrowBut.setLocation(getWidth() - 10 - arrowBut.getWidth(),
						    (getHeight() - arrowBut.getHeight())/2);
		arrowBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				//nie chcemy zapisywa� receptury - dlatego false
				setActionBut(false);
				
			}
		});
		add(arrowBut);
		
	}
	
	public void setStartLabel() {
		
		
		
		Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 16);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(textOnBar, null);
		
		lab = new JLabel();
		lab.setText(textOnBar);
		lab.setSize(getWidth() - 10, (int) bounds.getHeight());
		lab.setLocation(10, getHeight()/2 - lab.getHeight()/2);
		lab.setFont(font);
		lab.setForeground(new Color(159, 118, 65));
		
		add(lab);
		
		
		
	}
	
	public void setTextOnBar(String s) {
		
		ConfigureApp.logUser = s;
		removeAll();
		setBut();
		
		Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 16);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
	
		
		
		lab = new JLabel();
		lab.setText(s);
		lab.setSize(getWidth()-10, (int) bounds.getHeight());
		lab.setLocation(10, getHeight()/2 - lab.getHeight()/2);
		lab.setFont(font);
		lab.setForeground(new Color(159, 118, 65));
		
		add(lab);
		
		repaint();
	}
	
	public String getText() {
		return text;
	}
	
	public static String getTextOnBar() {
		
		return textOnBar;
		
	}
	
	public void setText(String s) {
		
		lab.setText(s);
		
		
	}
}
