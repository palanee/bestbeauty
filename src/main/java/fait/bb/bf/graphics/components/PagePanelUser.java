package fait.bb.bf.graphics.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.PagePanel;
import fait.bb.bf.graphics.Pages;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;



public class PagePanelUser extends PagePanel {
	
	private JPanel addPanel;
	private AdminPanel adminPanel;
	
	
	public PagePanelUser(JPanel addPanel, AdminPanel admin) {
		
		this.addPanel = addPanel;
		this.adminPanel = admin;
	}
	
	@Override
	public void setListPanel(List list) {
		// TODO Auto-generated method stub
		
		addPanel.removeAll();
		
		Iterator<User> it = list.iterator();
		ListWorkerView temp;
		int y = 0;
		while(it.hasNext()) {
			User u = it.next();
			temp = getWorkerLab(u.getName() + " " + u.getSurname(), u.getCode(), u);
			temp.setLocation(adminPanel.getMainPanel().getWidth()/2 - temp.getWidth()/2,
							y);
			addPanel.add(temp);
			y = temp.getY() + temp.getHeight() + 1;
		}
		
		addPanel.repaint();
		
	}
	
	private ListWorkerView getWorkerLab(String name, String code, final User u) {
		
		final ListWorkerView lab = new ListWorkerView(name, code, u);
		
		final ServiceData serviceData = new ServiceData();
		final Communication comm = new Communication(MainWindow.getFrame());
		
		User userDom = serviceData.getUserFromString("DOM");
		final String tempDom = userDom.getName() + " " + userDom.getSurname();
	
		JButton delete = new JButtonOwn("buttons/delete");
		delete.setLocation(460,
						   (lab.getHeight() - delete.getHeight())/2 - 5);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				if(comm.youSure("Czy na pewno chcesz usun�� pracownika?\n\n" + 
								"Po jego usuni�ciu zaplanowane wizyty\n zostan� " + 
								"przeniesione do terminarza\n" + tempDom, 0)) {
				
					serviceData.deleteUser(lab.getCode());
					serviceData.changeUserInVisit(u.getCode(), ConfigureApp.DEFAULT_CODE);
					
					if(CalendarPanelNew.comboBox!=null) {
						CalendarPanelNew.comboBox.removeItem(serviceData.parseToString(u));
						CalendarPanelNew.repaintCalendarPanel(CalendarPanelNew.changeStateAll());
						
					}
					
					//Pages.currentSite = 1;
					
					//CalendarPanelNew.calendarPanel.removeAll();
					//CalendarPanelNew.calendarPanel.repaint();
					adminPanel.getManagePanel().removeAll();
					adminPanel.setManagePanel();
					adminPanel.getManagePanel().repaint();
					
				}
				
			}
		});
		
		if(code.equals(ConfigureApp.DEFAULT_CODE))
				delete.setVisible(false);
		
		JButton edit = new JButtonOwn("buttons/edit");
		edit.setLocation(delete.getX() + delete.getWidth() + 15,
				   		(lab.getHeight() - edit.getHeight())/2 - 5);
		
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				User user = u;
				
				adminPanel.showUserPanel(user);
				
			}
		});
		
	
		lab.add(delete);
		lab.add(edit);
		
		return lab;
		
	}
	
	
	

}
