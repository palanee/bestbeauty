package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.logic.ServiceData;

public class WorkPlanLabel extends JPanel implements FocusListener {
	
	private String name;
	private String work = "";
	private Font etFont = new Font("Arial", Font.PLAIN, 15);
	private JTextArea textAreaTo;
	private JTextArea textAreaFrom;
	
	private JLabel error;
	private Boolean wrong = false;
	
	private Color etColor = new Color(75,75,75);
	private static int height = 30;
	
	private ServiceData serviceData;
	
	private static int countFrom = 0;
	private static int countTo = 0;
	
	private int day;
	
	
	public WorkPlanLabel(String name, int x, int y, int day) {
		
		this.name = name;	
		this.day = day;
		
		setLayout(null);
		setOpaque(false);
		setSize(245,height);
		setLocation(x,y);
		
		serviceData = new ServiceData();
		
		setDayName();
		setTextArea();
		
	}
	
	private void setDayName() {
		
		FontMetrics metrics = new FontMetrics(etFont) {};		
		Rectangle2D bounds = metrics.getStringBounds(name, null);
		
		JLabel lab = new JLabel();
		lab.setText(name);
		lab.setFont(etFont);
		lab.setForeground(etColor);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setLocation(0,getHeight()/2 - lab.getHeight()/2);
		
		add(lab);
		
	}
	
	
	
	private void setTextArea() {
		
		textAreaTo = new JTextArea();
		textAreaTo.setDocument(new JTextAreaLimit(2));
		textAreaTo.setBackground(new Color(212,212,212));
		textAreaTo.setForeground(etColor);
		textAreaTo.setFont(new Font("Arial", Font.PLAIN, 15));
		textAreaTo.setFocusable(true);
		textAreaTo.setBorder(new EmptyBorder(4,4,4,4));
		textAreaTo.setLineWrap(false);
		textAreaTo.setWrapStyleWord(true);
		textAreaTo.setSize(45,getHeight());
		textAreaTo.setLocation(getWidth() - textAreaTo.getWidth(),
							   0);
		textAreaTo.addFocusListener(this);
		textAreaTo.setName("to");
		
		add(textAreaTo);
		
		textAreaFrom = new JTextArea();
		textAreaFrom.setDocument(new JTextAreaLimit(2));
		textAreaFrom.setBackground(new Color(212,212,212));
		textAreaFrom.setForeground(etColor);
		textAreaFrom.setFont(new Font("Arial", Font.PLAIN, 15));
		textAreaFrom.setFocusable(true);
		textAreaFrom.setBorder(new EmptyBorder(4,4,4,4));
		textAreaFrom.setLineWrap(false);
		textAreaFrom.setWrapStyleWord(true);
		textAreaFrom.setSize(45,getHeight());
		textAreaFrom.setLocation(textAreaTo.getX() - 2 - textAreaTo.getWidth(),
							     0);
		textAreaFrom.addFocusListener(this);
		textAreaFrom.setName("from");
		
		
		add(textAreaFrom);
	}
	
	public JTextArea getTextAreaFrom() {
		
		return textAreaFrom;
	}
	
	public String getTextFrom() {
		
		return textAreaFrom.getText();
		
	}
	
	public String getTextTo() {
		
		return textAreaTo.getText();
		
	}
	
	public void setTextFrom(String to) {
		
		textAreaFrom.setText(to);
		
	}
	
	public void setTextTo(String from) {
		
		textAreaTo.setText(from);
		
	}
	
	public void setError(JLabel error) {
		
		this.error = error;
		
	}
	
	public void setWork(String work) {
		
		this.work = work;
		
	}
	
	public String getWork() {
		
		if(work.isEmpty()) {
			this.work = serviceData.getWork("daywork", day);
			System.out.println("Work: " + work);
			return work;
			
		}
		else
			return work;
		
	}
	
	public Boolean getWrong() {
		return wrong;
	}

	@Override
	public void focusGained(FocusEvent e) {
		
		JTextComponent textField = (JTextComponent) e.getComponent();
		
		
		textField.setBorder(BorderFactory.createCompoundBorder(ConfigureApp.setWrite,
				  											   BorderFactory.createEmptyBorder(2, 3, 0, 0)));
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
		JTextComponent textField = (JTextComponent) e.getComponent();
		
		
		String text = textField.getText().trim();
		System.out.println("WorkPlanLabel: " + text);
		
		
		if(!text.matches("[0-9]*")) {
			textField.setBorder(BorderFactory.createCompoundBorder(
									ConfigureApp.mistakeWrite,
									BorderFactory.createEmptyBorder(0, 3, 0, 0)));
			error.setText("Mo�esz wpisywa� tylko i wy��cznie liczby");
			error.setVisible(true);
			wrong = true;
		}
		else if(!text.isEmpty() && !ServiceData.inRange(text, 1, 24)) {
			System.out.println("Przecie� masz wypisaywa� error!");
			textField.setBorder(BorderFactory.createCompoundBorder(
					ConfigureApp.mistakeWrite,
					BorderFactory.createEmptyBorder(0, 3, 0, 0)));
			error.setText("Wpisano niepoprawn� godzin�");
			error.setVisible(true);
			wrong = true;
		}
		else if(textField.getName().equals("to") && 
				!textAreaFrom.getText().isEmpty() && !textAreaTo.getText().isEmpty() &&
				!ServiceData.checkProperlyHour(textAreaFrom.getText(), textAreaTo.getText())) {
			
			textField.setBorder(BorderFactory.createCompoundBorder(
					ConfigureApp.mistakeWrite,
					BorderFactory.createEmptyBorder(0, 3, 0, 0)));
			error.setText("Godzina zamkni�cia jest nieprawid�owa");
			error.setVisible(true);
			wrong = true;
		}
		else {
			textField.setBorder(new EmptyBorder(4,4,4,4));
			error.setVisible(false);
			wrong = false;
		}
		
	}
	
	public int getDay() {
		return day;
	}
	
	
	
	
}
