package fait.bb.bf.graphics.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.PagePanel;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.panels.glasspanels.AddDate;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Treatment;
import fait.bb.bf.logic.Visit;


public class PagesPanelVisit extends PagePanel {
	
	private JPanel addPanel;
	private AddDate addDate;
	private String kindCard;
	private Customer customer;
	private String userText;
	private List list;
	
	public PagesPanelVisit(JPanel addPanel, AddDate addDate, String kindCard, String userText) {
		
		this.addPanel = addPanel;
		this.addDate = addDate;
		this.kindCard = kindCard;
		this.customer = null;
		this.userText = userText;
	}
	
	@Override
	public void setListPanel(List list) {
		// TODO Auto-generated method stub
		
		this.list = list;
		addPanel.removeAll();
		
		Iterator<Visit> it = list.iterator();
		ListVisitView temp;
		int y = 0;
		int position = 0;
		while(it.hasNext()) {
			Visit t = it.next();
			temp = getWorkerLab(t, customer, userText);
			temp.setLocation(addDate.getSchedulePanel().getWidth()/2 - (temp.getWidth() - 80)/2,
							y);
			position = temp.getX();
			addPanel.add(temp);
			y = temp.getY() + temp.getHeight() + 1;
		}
		
		addPanel.repaint();
	}
	
	private ListVisitView getWorkerLab(Visit visit, Customer customer, String u) {
		
		final ListVisitView lab = new ListVisitView(visit, customer, u, kindCard);
		final Communication comm = new Communication(MainWindow.getFrame());
		final ServiceData serviceData = new ServiceData();
	
		JButton delete = lab.getDelete();
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				if(comm.youSure("Czy na pewno chcesz usunu�� wizyt�?", 0)) {
					
					serviceData.deleteVisit(lab.getVisit().getId());
					
					
					
					if(list.size() - 1 > 0) {
						
						addDate.refreshSchedulePanel();
						
					}
					else {
						
						addDate.refreshAndBackSchedule();
						
					}
					
					if(ConfigureApp.activeTab.equals(CalendarPanelNew.getNameTab())) 	
						CalendarPanelNew.repaintCalendarPanel();
					
					
					//CalendarPanelNew.calendarPanel.removeAll();
					//CalendarPanelNew.setCalendarPanel();
					//CalendarPanelNew.calendarPanel.repaint();
					
				}
				
			}
		});

		
		JButton edit = lab.getEdit();
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				addDate.editVisit(lab);
				
			}
		});
		
	
		lab.add(delete);
		lab.add(edit);
		
		return lab;
		
	}

}
