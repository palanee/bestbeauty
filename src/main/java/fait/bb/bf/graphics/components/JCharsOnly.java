package fait.bb.bf.graphics.components;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;

public class JCharsOnly extends PlainDocument {
	
	JTextComponent text;
	private JLabel lab;
	private JPanel panel;
	
	public JCharsOnly(JTextComponent text, JPanel panel) {
		super();
		
		this.text = text;
		this.panel = panel;
		
		lab = new JLabel("Mo�esz wpisywa� tylko litery");
     	lab.setSize(text.getWidth(), 12);
     	lab.setForeground(Color.red);
     	lab.setLocation(text.getX(), text.getY() + text.getHeight() + 2);
     	panel.add(lab);
     	lab.setVisible(false);
     	panel.repaint();
		
	}
	
	public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
			
		if (str == null) return;

		char[] chars = str.toCharArray();
        boolean ok = true;
        
        for (int i = 0; i < chars.length; i++ ) {
       	
        	if(Character.isLetter(chars[i])) 
       		  ok = true;
        	else {
        	  ok = false;
        	  lab.setVisible(true);
        	  break;
        		/* try {
        			 throw new Exception();  
                 } catch (Exception e) {
                	
                	 lab.setVisible(true);
                     ok = false;
                     break;
                 } */
       	 	}

       }
       
      if (ok) {
           super.insertString( offset, new String( chars ), attr);
           lab.setVisible(false);
       }
       else {
       	lab.setVisible(true);
       }
		
	}
	
	

}
