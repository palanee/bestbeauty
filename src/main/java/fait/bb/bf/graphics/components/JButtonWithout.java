package fait.bb.bf.graphics.components;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import fait.bb.bf.logic.Formula;

public class JButtonWithout extends JButton {
	
	private Dimension size;
	private Formula f;
	
	public JButtonWithout(String name) {
		
		ImageIcon imgIc = new ImageIcon("resources/images/" + name + ".png");
		setIcon(imgIc);
		size = new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight());
		setSize(size);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setName(name);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
	}
	
	public Dimension getSize() {
		
		return size;
		
	}
	
	public void setFormula(Formula f) {
		this.f = f;
	}
	
	public Formula getFormula() {
		return f;
	}
	
}
