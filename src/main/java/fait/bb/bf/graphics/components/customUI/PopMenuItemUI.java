package fait.bb.bf.graphics.components.customUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;

import fait.bb.bf.graphics.ConfigureApp;


public class PopMenuItemUI extends BasicMenuItemUI {
	
	public PopMenuItemUI() {
		
		super();
	}
	
	public static ComponentUI createUI(JComponent c) {
	      return new PopMenuItemUI();
	}
	
	protected void paintBackground(Graphics g, JMenuItem menuItem, Color bgColor)  {
		
		g.setColor(ConfigureApp.backgroundCardColor);
		g.fillRect(0,0,menuItem.getWidth(), 25);
		menuItem.setBorder(null);
		
		
	}
	
	protected void installComponents(JMenuItem menuItem) {
		
		selectionForeground = new Color(75,75,75);
		
	}
	
	
}
