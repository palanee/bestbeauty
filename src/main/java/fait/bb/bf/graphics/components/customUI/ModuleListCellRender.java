package fait.bb.bf.graphics.components.customUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;


public class ModuleListCellRender extends JPanel implements ListCellRenderer {
	
	
	private static int i = 0;

	Color foreColor = new Color(207,106,182);
	Color delModColor = new Color(255,177,42);
	Color addButtonColor = new Color(62,199,159);
	Color delButtonColor = new Color(255,22,32);
	
	JLabel lab;
	
	ImageIcon imgIcon;
	
	ModuleListCellRender() {
			
			setLayout(new FlowLayout());
			lab = new JLabel();
			lab.setBackground(new Color(255,249,254));
			lab.setFont(new Font("Arial", Font.BOLD, 20));
			lab.setForeground(foreColor);
			//lab.setSize(453,25);
			//lab.setLocation(0,0);
			add(lab);
			/*//addModule = MainView.addModule;
			f2 = new File("resources/images/GUZIK_TEMATY_MALY_dodaj.jpg");
			addModule = new JLabelBackground(f2);
			//addModule.setBackground(addButtonColor);
			addModule.setFont(new Font("Tahoma", Font.BOLD, 13));
			addModule.setForeground(Color.WHITE);
			addModule.setBorder(BorderFactory.createMatteBorder(
	                1, 1, 1, 1, Color.WHITE));
			addModule.setPreferredSize(new Dimension(72,28));
			addModule.setMinimumSize(new Dimension(72,28));
			addModule.setOpaque(true);
			
			
			
			
			add(addModule, BorderLayout.LINE_END);*/
			
		
		
		
	}
	@Override
	public Component getListCellRendererComponent(JList arg0, Object modul,
			int arg2, boolean isSelected, boolean arg4) {
		
		lab.setText("  " + (String) modul);
		if(isSelected) {
			lab.setBackground(new Color(213,213,213));
			
		}
		else {
			//lab.setForeground(foreColor);
			lab.setBackground(new Color(255,249,254));
			
		}
		
		return this;
	}

	

}
