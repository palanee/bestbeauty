package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import fait.bb.bf.logic.User;



public class ListWorkerView extends JLabel {
	
	private JLabel nameLabel;
	private JLabel codeLabel;
	
	private String name;
	private String code;
	
	private Font font = new Font("Arial", Font.PLAIN, 18);
	private Color fontColor = new Color(58,59,60);
	
	public ListWorkerView(String name, String code, User user) {
		
		
		this.name = name;
		this.code = code;
		components();
		
	}
	
	private void components() {
		
		ImageIcon imgIc = new ImageIcon("resources/images/backListWorker.png");
		setIcon(imgIc);
		setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		
		
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(name, null);		
		nameLabel = new JLabel();
		nameLabel.setFont(font);
		nameLabel.setText(name);
		nameLabel.setForeground(fontColor);
		nameLabel.setBounds(15,
						   (getHeight() - (int) bounds.getHeight())/2 - 5,
							265,
						   (int) bounds.getHeight());
		
		codeLabel = new JLabel();
		bounds = metrics.getStringBounds(code, null);		
		codeLabel = new JLabel();
		codeLabel.setFont(font);
		codeLabel.setText(code);
		codeLabel.setForeground(fontColor);
		codeLabel.setBounds(nameLabel.getX() + nameLabel.getWidth(),
						   (getHeight() - (int) bounds.getHeight())/2 - 5,
						   (int) bounds.getWidth(),
						   (int) bounds.getHeight());
		
		JButton delete = new JButtonOwn("buttons/delete");
		delete.setLocation(460,
						   (getHeight() - delete.getHeight())/2 - 5);
		
		JButton edit = new JButtonOwn("buttons/edit");
		edit.setLocation(delete.getX() + delete.getWidth() + 15,
				   		(getHeight() - edit.getHeight())/2 - 5);
		
		
		
		add(codeLabel);
		add(nameLabel);
		//add(delete);
		//add(edit);
		
		
	}
	
	public String getCode() {
		
		return this.code;
		
	}

}
