package fait.bb.bf.graphics.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.Treatment;

public class ListTreatView extends JLabel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Treatment treat;
	
	private JButton edit;
	private JButton delete;
	
	private Font font = new Font("Arial", Font.BOLD, 15);
	private Color fontColor = new Color(59,59,59);
	
	public ListTreatView(Treatment treat) {
		
		this.treat = treat;
		
		setLayout(null);
		
		components();
		
	}
	
	private void components() {
		
		ImageIcon imgIc = new ImageIcon("resources/images/backCustomer.png");
		setIcon(imgIc);
		setSize(imgIc.getIconWidth(), imgIc.getIconHeight());

		String name = treat.getNumber() + 
					  " " + treat.getName();

		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(name, null);		

		JLabel nameTreat = new JLabel(); 
		nameTreat.setFont(font);
		nameTreat.setText(name);
		nameTreat.setForeground(fontColor);
		nameTreat.setSize((int) bounds.getWidth(),
				(int) bounds.getHeight());
		nameTreat.setLocation(100,
				21);
		add(nameTreat);
		
		font = font.deriveFont(Font.PLAIN, 14);
		
		name = "Czas trwania: " + treat.getTime();
		bounds = metrics.getStringBounds(name, null);

		JLabel duration = new JLabel(); 
		duration.setFont(font);
		duration.setText(name);
		duration.setForeground(fontColor);
		duration.setSize((int) bounds.getWidth(),
				(int) bounds.getHeight());
		duration.setLocation(nameTreat.getX(),
							 nameTreat.getY() + nameTreat.getHeight() + 2);
		add(duration);


		name = "Cena: " + Float.toString(treat.getPrice()) + " z�";
		bounds = metrics.getStringBounds(name, null);

		JLabel textArea = new JLabel();
		textArea.setFont(font);
		textArea.setText(name);
		textArea.setForeground(fontColor);
		textArea.setSize((int) bounds.getWidth(),
				(int) bounds.getHeight());
		textArea.setLocation(nameTreat.getX(),
							 duration.getY() + duration.getHeight() + 2);

		add(textArea);

		edit = new JButtonWithout("editVisit");
		edit.setLocation(1, 7);
		add(edit);

		delete = new JButtonWithout("deleteVisit");
		delete.setLocation(edit.getX(), edit.getY()+edit.getHeight());
		add(delete);
		
	}
	
	public JButton getEdit() {
		
		return edit;
		
	}
	
	public JButton getDelete() {
		
		return delete;
		
	}
	
}
