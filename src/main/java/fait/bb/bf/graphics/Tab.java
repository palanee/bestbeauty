package fait.bb.bf.graphics;

import javax.swing.JComponent;

public class Tab {
	
	String name;
	JComponent comp;
	int index;
	
	
	public Tab(String name, JComponent comp, int index) {
		
		this.name = name;
		this.comp = comp;
		this.index = index;
		
	}

	public String getName() {
		return name;
	}

	public JComponent getComp() {
		return comp;
	}

	
	public int getIndex() {
		return index;
	}
	

}
