package fait.bb.bf.graphics;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractListModel;

import fait.bb.bf.logic.Customer;



public class ListModel extends AbstractListModel{
	
	Vector customer = new Vector();
	
	ListModel(List<Customer> list) {
		Iterator<Customer> it = list.iterator();
		Customer c;
		while(it.hasNext()) {
			
			c = it.next();
			System.out.println("Lista z JLIST: " + c);
			customer.add(c);
		}
	}
	@Override
	public Object getElementAt(int index) {
		Customer c = (Customer) customer.get(index);
		String text = "    " + c.getName() + " " + c.getSurname() + " tel: " + c.getNumber();
		return text;
	}

	@Override
	public int getSize() {
		Customer c = (Customer) customer.lastElement();
		return customer.lastIndexOf(c);
	}

	

}

