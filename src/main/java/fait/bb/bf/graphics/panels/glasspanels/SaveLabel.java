package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Transparency;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.DebugGraphics;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.PropertySetter;

import fait.bb.bf.graphics.ConfigureWindow;


public class SaveLabel extends JDialog {
	
	private BufferedImage helpImage;
	private ImageIcon imgIc;
	private float alpha = 1.0f;
	private Dimension size;
	
	int x;
	int y;
	
	
	public SaveLabel(Dimension sizePanel, String name) {
		
		try {
			
	        //setOpaque(false);
			
			helpImage = ImageIO.read(new File("resources/images/" + name + ".png"));
			x = ConfigureWindow.getMaxScreen().width/2 - helpImage.getWidth()/2;
	        y = ConfigureWindow.getMaxScreen().height/2 - helpImage.getHeight()/2;
			
	        setSize(helpImage.getWidth(), helpImage.getHeight());
	        this.setUndecorated(true);
	        
			this.size = sizePanel;
			this.setLocation(x,y);
			this.setBackground(null);
			this.setLayout(null);
			
			
			JLabel lab = new JLabel();
			ImageIcon img = new ImageIcon(helpImage);
			lab.setIcon(img);
			lab.setLocation(0,0);
			lab.setSize(getWidth(), getHeight());
			add(lab);
			
			
			//setIcon(imgIc);
			//repaint();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
	}
	
	public void setAnimation(float alpha) {
		
		this.alpha = alpha;
		this.setVisible(true);
		Animator animator = new Animator(2000);
		animator.addTarget(new PropertySetter(
				this, "alpha", 0.0f));
		animator.setAcceleration(0.2f);
		animator.setDeceleration(0.4f);
		animator.start();
		
	}
	
	 public void setAlpha(float alpha) {
         this.alpha = alpha;
         
         if (alpha <= 0.01f) {
        	 
        	//this.setOpaque(false);
        	
        	this.dispose();
        	 
         }
         
         
         //invalidate();
         
     }
     
     public float getAlpha() {
         return this.alpha;
     }
     
    
     
    
     

}
