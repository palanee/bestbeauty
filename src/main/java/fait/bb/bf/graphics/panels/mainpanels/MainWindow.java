package fait.bb.bf.graphics.panels.mainpanels;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.WidgetManager;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.customUI.TabbedPaneUI;
import fait.bb.bf.graphics.panels.glasspanels.AddDate;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.graphics.panels.glasspanels.ChooseHairdresser;
import fait.bb.bf.graphics.panels.glasspanels.FormulaWindow;
import fait.bb.bf.graphics.panels.glasspanels.HelpPanel;
import fait.bb.bf.graphics.panels.glasspanels.MainGlassPanel;
import fait.bb.bf.graphics.panels.glasspanels.NotePanel;
import fait.bb.bf.graphics.panels.glasspanels.SaveLabel;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.widgets.WidgetAddClient;
import fait.bb.bf.widgets.WidgetCalendar;

import fait.bb.bf.widgets.WidgetFind;

public class MainWindow  {
	
	//uwtorzenie kontenera warstw
	public static JLayeredPane layeredPane;
	public static JPanel pane;
	public static AdminPanel adminPanel;
	public static ChooseHairdresser choosePanel;
	public static FormulaWindow formulaWindow;
	public static HelpPanel helpPanel;
	public static SaveLabel save;
	public static SaveLabel todayLabel;
	public static AddDate addDate;
	public static NotePanel notePanel;
	public static MainGlassPanel mainGlassPanel;
	
	
	//pola glowne klasy
	private static Frame frame;
	private static Dimension maxScreen;
	private static JPanel mainPanel;
	private static ConfigureWindow conf;
	
	
	
	
	//public static Color background = new Color(255,249,254);
	//public static Color cards = new Color(148,91,134);
	
	public static CardLayout cl;
	public static JPanel cardPanel;
	public static int currentCard = 1;
	
	public static JTabbedPane tabbedPanel;
	public static List<CardPanel> listPanels = new ArrayList<CardPanel>();
	
	private Communication comm;
	public static JLabel labTab;
	public static JButtonWithout note;
	public static JLabel textCounter;
	public static JLabel countTab;
	//widgety
	static List<JButton> listWidget = new ArrayList<JButton>();
	public static List<Tab> listTabs = new ArrayList<Tab>();
	static JPanel widgetPanel;
	
	public static JPanel widgetBarPanel;
	
	//pobranie wielkosci monitora
	
	private static ServiceData serviceData;
	
	
	
	
	public static void addWidgets() {
		
		widgetPanel.removeAll();
		listWidget.clear();
		//dodawania buttonow-widgetow do listy
		WidgetAddClient widgetAC = new WidgetAddClient(frame, listTabs);
		listWidget.add(widgetAC.getWidget());
		
		fait.bb.bf.widgets.WidgetFind widgetF= new WidgetFind(listTabs, frame);
		listWidget.add(widgetF.getWidget());
		
		WidgetCalendar widgetCal = new WidgetCalendar(frame);
		listWidget.add(widgetCal.getWidget());
		
		//ustawienie pozycji buttonow - widgetow
		WidgetManager widgetMan = new WidgetManager(maxScreen, listWidget);
		
		
		for(int i=0; i<listWidget.size(); i++) {
			
			widgetPanel.add(listWidget.get(i));
			frame.repaint();
		}
		
		widgetPanel.repaint();
		
		
		
	}
	
	public static Dimension changeIcon(String name, JButton b) {
		
		
		ImageIcon imgIc = new ImageIcon("resources/images/icons/" + name + "-1.png");
		b.setIcon(serviceData.resizeImage(imgIc, widgetBarPanel.getSize()));
		imgIc = new ImageIcon("resources/images/icons/" + name + "-2.png");
		b.setRolloverIcon(imgIc);
		b.setIcon(serviceData.resizeImage(imgIc, widgetBarPanel.getSize()));
		imgIc = new ImageIcon("resources/images/icons/" + name + "-3.png");
		b.setPressedIcon(imgIc);
		b.setIcon(serviceData.resizeImage(imgIc, widgetBarPanel.getSize()));
		
		ImageIcon temp = serviceData.resizeImage(imgIc, widgetBarPanel.getSize());
		Dimension dim = new Dimension(temp.getIconWidth(), temp.getIconHeight());
		
		return dim; 
		
	}
	
	
	
	
	public static void setWidgetBar() {
		
		widgetBarPanel.removeAll();
		widgetPanel.removeAll();
		Iterator<JButton> it= listWidget.iterator();
		
		
		
		int x = 10;
		int length = 10;
		
		while(it.hasNext()) {
			
			JButton w = it.next();
			Dimension dim = changeIcon(w.getName(), w);
			w.setBounds(x, 
						widgetBarPanel.getHeight()/2 - (int) dim.getHeight()/2, 
						(int) dim.getWidth(), 
						(int) dim.getHeight());
			widgetBarPanel.add(w);
			x = w.getX() + w.getWidth() + length;
		}
		
		widgetBarPanel.repaint();
	}
		
	
	
	private void setCardPanel() {
		
		cl = conf.getCl();
		cardPanel = conf.getCardPanel();
		
		//panel niepotrzebny, tylko po to, by wy�wietli� si� ten z widgetami
		JPanel panel = new JPanel();
		panel.setBackground(Color.GREEN);
		cardPanel.add("1", panel);
		
		JPanel loggingpanel = new LoggingPanel();
		loggingpanel.setName("loggingPanel");
		cardPanel.add("2", loggingpanel);
		
		widgetPanel = new JPanel();
		widgetPanel.setLayout(null);
		widgetPanel.setName("widgetPanel");
		widgetPanel.setBackground(ConfigureApp.backgroundColor);
		addWidgets();
		cardPanel.add("3", widgetPanel);
		
		//tabPanel = new TabbedPanel(cardPanel.getSize());
		//tabbedPanel = tabPanel.getTabbedPanel();
		
		int insetLeft = 150;
		int insetTop = 100;
		
		tabbedPanel = new JTabbedPane();
		//tabbedPanel.addMouseListener(new PopUpTabListener(new PopUpTab()));
		tabbedPanel.setUI(new TabbedPaneUI());
		tabbedPanel.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPanel.setBounds(insetLeft, 
				  			  insetTop,
				  			  cardPanel.getWidth() - insetLeft/2,
				  			  cardPanel.getHeight() - insetTop/2);
		tabbedPanel.setName("tabbedPanel");
		setTabbedPane();
		listPanels = new ArrayList<CardPanel>();
		cardPanel.add("4", tabbedPanel);
		
		
		
	}
	
	public static void setCounterTab(Integer tabs) {
		
		
		
		textCounter = new JLabel();
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 17);
		textCounter.setFont(font);
		textCounter.setForeground(new Color(5,93,148));
		textCounter.setText(tabs.toString());
		
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(tabs.toString(), null);
		
		textCounter.setSize((int) bounds.getWidth(),
							(int) bounds.getHeight());
		textCounter.setLocation(countTab.getWidth()/2 - textCounter.getWidth()/2,
								countTab.getHeight()/2 - textCounter.getHeight()/2);

		countTab.add(textCounter);
		
		
		
	}
	
	private void setTabbedPane() {
		
		labTab = new JLabel();
		ImageIcon img = new ImageIcon("resources/images/menuTab.png");
		labTab.setIcon(img);
		labTab.setSize(img.getIconWidth(), img.getIconHeight());
		labTab.setLocation(tabbedPanel.getX() + tabbedPanel.getWidth() - 58,
						tabbedPanel.getY() - 7);
		labTab.setVisible(false);
		
		countTab = new JLabel();
		img = new ImageIcon("resources/images/buttons/backTabMenu.png");
		countTab.setIcon(img);
		countTab.setSize(img.getIconWidth(), img.getIconHeight());
		int y = (labTab.getHeight() - 2*countTab.getHeight()-3)/2;
		countTab.setLocation(labTab.getWidth()/2 - countTab.getWidth()/2,
							 y);
		labTab.add(countTab);
		
		
		JButtonWithout closeTab = new JButtonWithout("buttons/backTabMenu");
		closeTab.setLocation(countTab.getX(),	
							 countTab.getY() + countTab.getHeight() + 3);
		closeTab.setLayout(null);
		closeTab.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				closeTabAction();
			   
			}
		});


		labTab.add(closeTab);
		
		JLabel closeIcon = new JLabel();
		img = new ImageIcon("resources/images/buttons/cl.png");
		closeIcon.setIcon(img);
		closeIcon.setSize(img.getIconWidth(), img.getIconHeight());
		closeIcon.setLocation(closeTab.getWidth()/2 - closeIcon.getWidth()/2,
							  closeTab.getHeight()/2 - closeIcon.getHeight()/2 - 1);
		closeTab.add(closeIcon);
		
		mainPanel.add(labTab);
		
		
		
	}
	
	public static void closeTabAction() {
		
		int i = MainWindow.tabbedPanel.getSelectedIndex();
	 	int number = MainWindow.tabbedPanel.getTabCount();
	 	if(MainWindow.tabbedPanel.getTitleAt(i).equals(WidgetFind.name))
	 		WidgetFind.index=-1;
	 		WidgetCalendar.indexCal = -1;
	    if (number > 1 ) {
	    	
	    		ConfigureApp.closeCard();
	    		MainWindow.tabbedPanel.remove(i);
	    		listPanels.remove(i);
	    	
	    }
	    else {
	    	Communication comm = new Communication(frame);
	    	Boolean yes = comm.youSure("Czy na pewno chcesz wy��czy� kart�?", 0);
	    	if(yes) {
	    		ConfigureApp.closeCard();
	    		MainWindow.tabbedPanel.remove(i);
	    		listPanels.remove(i);
	    		listPanels.clear();
	    		widgetBarPanel.removeAll();
				addWidgets();
				//mainPanel.repaint();
				currentCard = 3;
				cl.show(cardPanel, "" + currentCard);
				widgetBarPanel.repaint();
	    	}
	    	
	    	
	    }
		
	}

	/**
	 * Wy�wietla poszczeg�lne plansze ekranu w zale�no�ci od akcji, jakie podj�� uzytkownik
	 */
	private void component() {
		
		mainPanel = conf.getMainPanel();
		mainPanel.setIgnoreRepaint(true);
		widgetBarPanel = conf.getWidgetBarPanel();
		
		setCardPanel();
		
		
		
		
		cl.show(cardPanel, "" + (currentCard));
		
		InputMap inputMap = new JPanel().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK), "find");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK), "closeTab");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK), "choose");
		inputMap.put(KeyStroke.getKeyStroke("F10"), "home");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK), "newClient");
		
		mainPanel.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
		
		//ustawienie tla panelnu
		mainPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.setBounds(0, 0, (int) maxScreen.width, (int) maxScreen.height);
		mainPanel.setLayout(null);
		mainPanel.getActionMap().put("find", new AbstractAction() {
             public void actionPerformed(ActionEvent e) {
            	 if(ConfigureApp.isTabs()) {
 					MainWindow.labTab.setVisible(true);
 					if (WidgetFind.index == -1) {
 						ConfigureApp.countTab();
 						ConfigureWindow.repaintMenu();
 						MainWindow.currentCard = 4;
 						MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
 						SearchView searchViewPanel = new SearchView();
 						MainWindow.tabbedPanel.addTab(WidgetFind.name, searchViewPanel.getMainPanel());

 						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
 						WidgetFind.index = selIndex;
 						MainWindow.listPanels.add(null);
 						Tab temp = new Tab(WidgetFind.name, searchViewPanel.getMainPanel(), selIndex);
 						
 						MainWindow.tabbedPanel.setForegroundAt(selIndex,ConfigureApp.fontAtTabColor);
 						MainWindow.tabbedPanel.setSelectedIndex(selIndex);
 						MainWindow.setWidgetBar();
 						
 					} else {
 						
 						MainWindow.tabbedPanel.setSelectedIndex(WidgetFind.index);
 						if(MainWindow.currentCard==3) {
							MainWindow.currentCard = 4;
							MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
							MainWindow.setWidgetBar();
						}
 						
 					}
 				}
 				else {
 					comm.dont("Masz zbyt du�o otwartych zak�adek");
 				}
             }
         });
		
		mainPanel.getActionMap().put("closeTab", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
            	int i = MainWindow.tabbedPanel.getSelectedIndex();
			 	int number = MainWindow.tabbedPanel.getTabCount();
			 	if(MainWindow.tabbedPanel.getTitleAt(i).equals(WidgetFind.name))
			 		WidgetFind.index=-1;
			    if (number > 1 ) {
			    	
			    		ConfigureApp.closeCard();
			    		MainWindow.tabbedPanel.remove(i);
			    		listPanels.remove(i);
			    	
			    }
			    else {
			    	
			    	Boolean yes = comm.youSure("Czy na pewno chcesz wy��czy� kart�?", 0);
			    	if(yes) {
			    		ConfigureApp.closeCard();
			    		MainWindow.tabbedPanel.remove(i);
			    		listPanels.remove(i);
			    		listPanels.clear();
			    		widgetBarPanel.removeAll();
						addWidgets();
						//mainPanel.repaint();
						currentCard = 3;
						cl.show(cardPanel, "" + currentCard);
						widgetBarPanel.repaint();
			    	}
			    	
			    	
			    }
            }
        });
		
		mainPanel.getActionMap().put("choose", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
            	ConfigureWindow.bar.setActionBut(false);
            }
        });
		
		mainPanel.getActionMap().put("home", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
            	setActionHome();
            }
        });
		
		mainPanel.getActionMap().put("newClient", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
            	
            	if(ConfigureApp.isTabs()) {
					ConfigureApp.countTab();
					MainWindow.labTab.setVisible(true);
					ConfigureWindow.repaintMenu();
					MainWindow.currentCard = 4;
					MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
					
					CardPanel clientPanel = new CardPanel(1, null);
					MainWindow.listPanels.add(clientPanel);
					
					MainWindow.tabbedPanel.addTab(WidgetAddClient.name, clientPanel.getMainPanel());

					int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
					Tab temp = new Tab(WidgetAddClient.name, clientPanel.getMainPanel(),
							selIndex);

					

					MainWindow.tabbedPanel.setForegroundAt(selIndex,
							ConfigureApp.fontAtTabColor);
					MainWindow.tabbedPanel.setSelectedIndex(selIndex);

					MainWindow.setWidgetBar();
					/*if (ConfigureWindow.bar.getText().equals(
					"------------------------")) {
						MainWindow.pane.setVisible(true);
						MainWindow.choosePanel.setVisible(true);
					}*/
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
            	
            }
        });

     /* connect two keystrokes with the newly created "foo" action:
        - a
        - CTRL-a
     */
		
		
		//inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK), "new");
		
		
		pane = new JPanel() {
			public void paintComponent(Graphics g)
		    {
		        super.paintComponent(g);
		        Color ppColor = new Color(0, 0, 0, 150); //r,g,b,alpha
		        g.setColor(ppColor);
		        g.fillRect(0,0,maxScreen.width,maxScreen.height); //x,y,width,height 
		        
		    } 
			
			
		};
		
		pane.setIgnoreRepaint(true);
		pane.setBackground(Color.BLACK);
		pane.setBounds(0, 0, (int) maxScreen.width, (int) maxScreen.height); 
		pane.setOpaque(false);
		pane.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		/*pane.addMouseListener(new MouseAdapter() 
		{
			public void mousePressed(MouseEvent evt)
			{	
				pane.setVisible(false);
				
				if(mainGlassPanel.isVisible()) {
					mainGlassPanel.removePanel();
					mainGlassPanel.setVisible(false);
				}
				else if(addDate.isVisible()) 	
					addDate.exit();
				else if(notePanel.isVisible()) {
					
					notePanel.setVisible(false);
					notePanel.saveText();
				}
				
				
				if(adminPanel.isVisible()) {
					adminPanel.removeAll();
					adminPanel.setVisible(false);
					adminPanel.components();
					adminPanel.repaint();
				}
				else if(formulaWindow.isVisible()) {
					formulaWindow.removeAll();
					formulaWindow.setVisible(false);
					formulaWindow.setMainPanel();
					formulaWindow.repaint();
				}
				else if(choosePanel.isVisible())
					choosePanel.setVisible(false);
				else if(helpPanel.isVisible()) {
					helpPanel.setVisible(false);
				}
				else if(addDate.isVisible()) {
					addDate.setVisible(false);
					
				}
				else if(notePanel.isVisible()) {
					notePanel.setVisible(false);
					notePanel.saveText();
				}
			}
			
			
		});		*/
		
		
		
	/*	save = new SaveLabel(new Dimension(300,400));
		//layeredPane.add(save, 100, 5);
		
		adminPanel = new AdminPanel();
		layeredPane.add(adminPanel,90,1);
		
		choosePanel = new ChooseHairdresser();
		layeredPane.add(choosePanel,90,2);
		
		formulaWindow = new FormulaWindow();
		layeredPane.add(formulaWindow,90,3);
		
		helpPanel = new HelpPanel();
		layeredPane.add(helpPanel, 90,4);
		
		addDate = new AddDate();
		layeredPane.add(addDate,90,5);*/
		
		mainGlassPanel = new MainGlassPanel();
		layeredPane.add(mainGlassPanel,90,1);
		
		addDate = new AddDate();
		layeredPane.add(addDate,90,2);
		
		System.out.println("AddDate: " + addDate);
		
		notePanel = new NotePanel();
		layeredPane.add(notePanel,90,3);
		
		
		save = new SaveLabel(new Dimension(300,400), "save");
		save.setAlwaysOnTop(true);
		//layeredPane.add(mainGlassPanel,100,0);
		
		todayLabel = new SaveLabel(new Dimension(300,400), "todayInfo");
		//layeredPane.add(mainGlassPanel,100,1);
		
		layeredPane.add(pane,60,7);
		
		layeredPane.add(mainPanel,30,2);
		layeredPane.setBounds(0,0,(int) maxScreen.width, (int) maxScreen.height);
				
		frame.add(layeredPane);

		pane.setVisible(false);
		addDate.setVisible(false);
		save.setVisible(false);
		notePanel.setVisible(false);
		/*adminPanel.setVisible(false);
		choosePanel.setVisible(false);
		formulaWindow.setVisible(false);
		helpPanel.setVisible(false);
		
		addDate.setVisible(false);
		*/
		mainGlassPanel.setVisible(false);
		
	}
	
	private void setActionHome() {
		MainWindow.labTab.setVisible(false);
		MainWindow.widgetBarPanel.removeAll();
		addWidgets();
		//mainPanel.repaint();
		currentCard = 3;
		cl.show(cardPanel, "" + currentCard);
		widgetBarPanel.repaint();
		if(ConfigureApp.getTab()>0)
			ConfigureWindow.changeButton();
	}
	
	
	private void action() {
		
		conf.getMenuHome().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				setActionHome();
				
			}
		});
		
		conf.getDisplayMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {

				pane.setVisible(true);
				//adminPanel.setVisible(true);
				mainGlassPanel.setPanel(new AdminPanel(mainGlassPanel.getSize()).getMainPanel());
				mainGlassPanel.setVisible(true);
			}
		});
		
		conf.getBackTab().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				if(ConfigureApp.isTabs()) {
					MainWindow.labTab.setVisible(true);
					ConfigureWindow.repaintMenu();
					MainWindow.currentCard = 4;
					MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
					

					MainWindow.setWidgetBar();
					
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
				
			}
		});
	}
	
	
	public void setKeyTab() {
			
		
		pane.getInputMap().put(KeyStroke.getKeyStroke("control w"), "closeTab");
		pane.getActionMap().put("closeTab",
			    new AbstractAction("closeTab") {
			        public void actionPerformed(ActionEvent evt) {
			            System.out.println("UDA�O SI�!!!!!!!!!!!!!!!!");
			        }
			    }
			);
		
		/* mainPanel.getActionMap().put("closeTab", new AbstractAction() {
          public void actionPerformed(ActionEvent e) {
              System.out.println("Zamykamy zak�adk�");
              int i = MainWindow.tabbedPanel.getSelectedIndex();
				 	int number = MainWindow.tabbedPanel.getTabCount();
				    if (number > 1 ) {
				    	Boolean yes = comm.youSure("Czy na pewno chcesz wy��czy� kart�?", 0);
				    	if(yes) {
				    		MainWindow.tabbedPanel.remove(i);
				    	}
				    }
				    else {
			    		comm.dont("Nie mo�esz zamkn�� ostatniej zak�adki");
		
			    	}
				    
				    
          }
		 });
		 
		 InputMap inputMap = mainPanel.getInputMap();
		 inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK), "closeTab");*/
		
	}
	
	//konstruktor glownego okna
	public MainWindow() {
		
		layeredPane = new JLayeredPane();
		
		conf = new ConfigureWindow();
		maxScreen = conf.getMaxScreen();
		
		
		frame = conf.getFrame();
		frame.setLayout(null);

		//Wy��czamy komponenty, je�li nie ma �adnego u�ytkownika, ustawiamy plansz� niezb�dn� do stworzenia u�ytkownika
		if(!ServiceData.isAnyUser()) {
			ConfigureWindow.bar.setVisible(false);
			ConfigureWindow.menu.setVisible(false);
			currentCard = 2;
		}
		else {
			ConfigureWindow.bar.setVisible(true);
			ConfigureWindow.menu.setVisible(true);
			
			currentCard=3;
		}
		comm = new Communication(frame);
		serviceData = new ServiceData();
		
		component();
		//setKeyTab();
		action();
		
		
		
		//frame.repaint();
		
	}
	
	public static JPanel getMainPanel() {
		
		return mainPanel;
		
	}
	
	public static Frame getFrame() {
		return frame;
	}

	
	
}
