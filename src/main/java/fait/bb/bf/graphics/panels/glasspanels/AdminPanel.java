package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.AWTException;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.SpinnerUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;

import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.interpolation.PropertySetter;
import org.omg.CORBA.FREE_MEM;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Pages;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.ButtonSite;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.ListTreatView;
import fait.bb.bf.graphics.components.ListVisitView;
import fait.bb.bf.graphics.components.ListWorkerView;
import fait.bb.bf.graphics.components.PagePaneTreat;
import fait.bb.bf.graphics.components.PagePanelUser;
import fait.bb.bf.graphics.components.WorkPlanLabel;
import fait.bb.bf.graphics.components.customUI.ComboBoxRenderer;
import fait.bb.bf.graphics.components.customUI.DateComboBoxUI;
import fait.bb.bf.graphics.components.customUI.DateSpinnerUI;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.graphics.panels.mainpanels.SearchView;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.DataBase;
import fait.bb.bf.logic.DayWork;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Treatment;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;
import fait.standard.Numbers;


public class AdminPanel implements FocusListener, KeyListener {
	
	private JPanel mainPanel;
	private CardLayout cl;
	
	//panele nale��ce do cardPanel
	private JPanel buttonPanel;
	private JPanel managePanel;
	private JPanel logPanel;
	private JPanel helpPanel;
	private JPanel addPanel;
	private JPanel changePassPanel;
	private JPanel settingsPanel;
	
	private int position;
	
	
	//elementy nale��ce do logPanel
	private JPasswordField pass;
	private JTextField login;
	
	//elementy nale��ce do helpPanel
	private JButtonOwn acceptButton;
	private JButtonOwn backButton;
	
	//elementy nale��ce do changePassPanel
	private JPasswordField passNow;
	private JPasswordField passRepeat;
	
	//elementy panelu buttonPanel
	private JButton manageButton;
	private JButton changePass;
	private JButton settings;
	private JLabel title;
	
	//elementy panelu managePanel
	private JLabel etName;
	private JButton addNew;
	private JButton back;
	private JPanel topPanel;
	private JPanel middlePanel;
	private JPanel bottomPanel;
	private List<ButtonSite> buttonsList;
	private static int currentSite = 1;
	private static int currentSiteTreat = 1;
	final int numberOfUser = 6;
	final int numberOfTreat = 4;
	
	
	//elementy panelu addPanel
	private JTextField name;
	private JTextField surname;
	private JTextField code;
	private ImageIcon imgAvatar;
	private File selectedFile;
	private JButton addNewWorker;
	private JButton backToList;
	private JSpinner fromSpinner;
	private JTextField fromText;
	private JTextField toText;
	private JLabel etFree;
	private JLabel from;
	private JLabel to;
	private Boolean isSelectedFree;
	private JCheckBox freeDay;
	
	//panele nalezace do settingsPanel
	private JPanel salonSettings;
	private JLabel errorDay;
	
	private JButton backTreat;
	private JButton newTreat;
	
	private Font titleFont = ConfigureApp.font.deriveFont(Font.BOLD, 22);
	private Font etFont = new Font("Arial", Font.BOLD, 14);
	private Font etFontGrey = ConfigureApp.font.deriveFont(Font.BOLD, 15);
	private Font etFontLog = ConfigureApp.font.deriveFont(Font.PLAIN, 18);
	private Font fontLog = new Font("Arial", Font.BOLD, 22);
	
	private Color colorFont = new Color(29,81,113);
	private Color etColorFont = new Color(58,59,60);
	private Color fontLabelGrey = new Color(75,75,75);
	
	Border notWrite = BorderFactory.createLineBorder(new Color(211,211,211));
	Color backField = new Color(247,246,244);
	Color fontFieldColor = new Color(83,84,84);	
	Color etFontLogColor = new Color(140,141,142);
	
	private ServiceData serviceData;
	private Communication comm;
	private List<User> listUsers = new ArrayList<User>();
	private List<Treatment> listTreat = new ArrayList<Treatment>();
	
	
	private static String loginName;
	
	Boolean isChange = false;
	public static Boolean isLog = false;
	public static Boolean isDeleted = false;
	
	public User userCreatePanel;
	private SaveLabel save;
	
	private Dimension size;
	private JPanel settingsMainPanel;
	private JPanel treatmentListPanel;
	private ArrayList<ButtonSite> buttonsTreatList;
	private JPanel treatNewPanel;
	private JTextField nameTreat;
	private JTextField number;
	private JTextField price;
	private JTextField time;
	
	private Boolean isPressedKey = false;
	private int countKey;
	
	private static String DEFAULT_CODE = "DOM";
	
	public AdminPanel(Dimension size) {
		
		
		//setSize(size);
	/*	setSize(ConfigureApp.sizeOfDialog);
		setBackground(new Color(48,134,188));*/
		//setLayout(null);
		this.size = size;
		
		serviceData = new ServiceData();
		comm = new Communication(MainWindow.getFrame());
		
		components();
		
	}
	
	public void components() {
		
		setMainPanel();
		setLogPanel() ;
		setRequestWindow(); 
		//setButtons();
		
	}
	
	private void setButtons() {
		
		
		
		String s = "Co chcesz zrobi�?";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		title.setLocation(mainPanel.getWidth()/2 - (int) bounds.getWidth()/2,
						  15);
		

		manageButton = new JButtonOwn("buttons/manageWorker");
		manageButton.setLocation(mainPanel.getWidth()/2 - 20 - manageButton.getWidth(),
								 title.getY() + title.getHeight() + 20);
		manageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				managePanel.removeAll();
				cl.show(mainPanel, "managePanel");
				setManagePanel();
				managePanel.repaint();
			}
		});

		changePass = new JButtonOwn("buttons/changePass");
		changePass.setLocation(mainPanel.getWidth()/2 + 20,
							   manageButton.getY());
		changePass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				changePassPanel.removeAll();
				cl.show(mainPanel, "changePassPanel");
				setChangePass();
				changePassPanel.repaint();
				
			}
		});

		settings = new JButtonOwn("buttons/settings");
		settings.setLocation(manageButton.getX(),
							 manageButton.getY() + manageButton.getHeight() + 40);
		settings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				settingsMainPanel.removeAll();
				cl.show(mainPanel, settingsMainPanel.getName());
				setSettingsMainPanel();
				settingsMainPanel.repaint();
				
			}
		});
		
		changePass.setEnabled(true);
		settings.setEnabled(true);
		
		buttonPanel.add(title);
		buttonPanel.add(manageButton);
		buttonPanel.add(changePass);
		buttonPanel.add(settings);
		
	}
	
	private void setChangePass() {
		
		String s = "Zmie� has�o administratora";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((changePassPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		
		

		passNow = new JPasswordField();
		setPasswordField(passNow, new Dimension(378,48));
		passNow.setLocation(changePassPanel.getWidth()/2 - 90,
						  	title.getY() + title.getHeight() + 40);
		passNow.setFocusable(true);
		
		
		JLabel etPassNow = new JLabel();
		setLabelField(etPassNow, "Wpisz obecne has�o");
		etPassNow.setLocation(passNow.getX() - etPassNow.getWidth() - 10,
							  passNow.getY() + passNow.getHeight()/2 - etPassNow.getHeight()/2);
		
		pass = new JPasswordField();
		setPasswordField(pass, passNow.getSize());
		pass.setLocation(passNow.getX(),
						 passNow.getY() + passNow.getHeight() + 30);
		pass.setFocusable(true);
		
		JLabel etPass = new JLabel();
		setLabelField(etPass, "Wpisz nowe has�o");
		etPass.setLocation(pass.getX() - etPass.getWidth() - 10,
						   pass.getY() + (pass.getHeight() - etPass.getHeight())/2);
		
		passRepeat = new JPasswordField();
		setPasswordField(passRepeat, passNow.getSize());
		passRepeat.setLocation(passNow.getX(),
				 	     pass.getY() + pass.getHeight() + 30);
		passRepeat.setFocusable(true);
		
		JLabel etPassRepeat = new JLabel();
		setLabelField(etPassRepeat, "Powt�rz nowe has�o");
		etPassRepeat.setLocation(passRepeat.getX() - etPassRepeat.getWidth() - 10,
						   		 passRepeat.getY() + (passRepeat.getHeight() - etPassRepeat.getHeight())/2);
		
		final JLabel error = new JLabel();
		error.setForeground(Color.RED);
		error.setFont(new Font("Arial", Font.BOLD, 12));
		error.setSize(pass.getWidth(), 15);
		error.setLocation(passNow.getX(),
						  passNow.getY() + passNow.getHeight() + 5);
		error.setVisible(false);
		
		final JLabel errorTwo = new JLabel();
		errorTwo.setForeground(Color.RED);
		errorTwo.setFont(new Font("Arial", Font.BOLD, 12));
		errorTwo.setSize(passRepeat.getWidth(), 15);
		errorTwo.setLocation(passRepeat.getX(),
						     passRepeat.getY() + passRepeat.getHeight() + 5);
		errorTwo.setVisible(false);
		
		JButton savePass = new JButtonOwn("but/green", "Zmie� has�o");
		savePass.setLocation(passRepeat.getX() + passRepeat.getWidth() - savePass.getWidth(),
						     passRepeat.getY() + passRepeat.getHeight() + 25);
		savePass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				changePass(error, errorTwo);
					
				
			}
		});
		
		savePass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					changePass(error, errorTwo);
					
				}
				
			}			
		});
		
		passNow.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					changePass(error, errorTwo);
					
				}
				
			}			
		});
		
		pass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					changePass(error, errorTwo);
					
				}
				
			}			
		});
		
		passRepeat.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					changePass(error, errorTwo);
					
				}
				
			}			
		});
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5,
						 changePassPanel.getHeight() - 5 - back.getHeight());
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				
				buttonPanel.removeAll();
				setButtons();
				buttonPanel.repaint();
				cl.show(mainPanel, "buttonPanel");
				
			}
		});
		
		changePassPanel.add(title);
		changePassPanel.add(passNow);
		changePassPanel.add(etPassNow);
		changePassPanel.add(pass);
		changePassPanel.add(etPass);
		changePassPanel.add(passRepeat);
		changePassPanel.add(etPassRepeat);
		changePassPanel.add(savePass);
		//changePassPanel.add(logIn);
		changePassPanel.add(error);
		changePassPanel.add(errorTwo);
		changePassPanel.add(back);
		
	}
	
	private void changePass(JLabel error, JLabel errorTwo) {
		Boolean isCorrected = true;
		
		
		
		
			if(!serviceData.logInAdmin(loginName, passNow.getText())) {
				System.out.println("Login: " + loginName);
				System.out.println("Niepoprawne obecne has�o");
				error.setText("Has�o niepoprawne");
				error.setVisible(true);
				isCorrected = false;


			}
			if(!pass.getText().equals(passRepeat.getText())) {
				System.out.println("Has�a nie zgadzaj� si�");
				isCorrected = false;
				errorTwo.setVisible(true);
				errorTwo.setText("Has�a nie s� identyczne");
				error.setVisible(false);
			}

			if(isCorrected) {
				serviceData.changePass(loginName, pass.getText());
				MainWindow.save.setAnimation(1.0f);
				
				
				pass.setText("");
				passRepeat.setText("");
				passNow.setText("");
			
				
				errorTwo.setVisible(false);
				error.setVisible(false);
				/*buttonPanel.removeAll();
				setButtons();
				buttonPanel.repaint();
				cl.show(mainPanel, buttonPanel.getName());*/
			}
		
		
	}
	
	private void setPasswordField(JPasswordField passField, Dimension size) {
		
		passField.setBackground(backField);	
		passField.setForeground(fontFieldColor);
		passField.setBorder(notWrite);
		passField.setSize(size);
		passField.setEchoChar('*');
		passField.addFocusListener(this);
		
	}
	
	
	private void setListWorker() {
		
		//ilo�� u�ytkownik�w na stron�
		
		listUsers.clear();
		listUsers = serviceData.getUsers(" AND status!='ZWOLNIONY' ");
		middlePanel.removeAll();
		
		Pages<User> pages = new Pages<User>(listUsers, numberOfUser, bottomPanel);
		pages.setPagePanel(new PagePanelUser(middlePanel, this));
		pages.setButtons(back, addNew);
		pages.setPages();
		
		
		
	}
	
	private void setBottomPanel() {
		
		currentSite = 1;
		back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5, 
						 5);
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				buttonPanel.removeAll();
				setButtons();
				buttonPanel.repaint();
				cl.show(mainPanel, "buttonPanel");
			}
		});
		
		addNew = new JButtonOwn("but/green", "Dodaj nowego");
		addNew.setLocation(managePanel.getWidth() - addNew.getWidth() - 5,
						   back.getY());
		addNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				addPanel.removeAll();
				cl.show(mainPanel, "addPanel");
				setAddPanel(null);
				addPanel.repaint();
				System.out.println("Klikni�to w Dodaj nowy");
			}
		});
		
		bottomPanel.add(back);
		bottomPanel.add(addNew);
		
		setListWorker();
		
	}
	
	public JPanel getManagePanel() {
		
		return managePanel;
		
	}
	
	public void showUserPanel(User user) {
		
		addPanel.removeAll();
		cl.show(mainPanel, "addPanel");
		setAddPanel(user);
		addPanel.repaint();
		
	}
	                           
	
	public void setManagePanel() {
		
		topPanel = new JPanel();
		topPanel.setOpaque(false);
		topPanel.setLayout(null);
		
		middlePanel = new JPanel();
		middlePanel.setOpaque(false);
		middlePanel.setLayout(null);
		
		bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		bottomPanel.setLayout(null);
		
		
		String s = "Zarz�dzaj pracownikami";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((managePanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		
		
		s = "Imi� i nazwisko";
		metrics = new FontMetrics(etFont) {};
		bounds = metrics.getStringBounds(s, null);
		etName = new JLabel();
		etName.setForeground(etColorFont);
		etName.setFont(etFont);
		etName.setText(s);
		etName.setBounds(145,
						 title.getY() + title.getHeight() + 20,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		
		s = "Kod";
		bounds = metrics.getStringBounds(s, null);
		JLabel etCode = new JLabel();
		etCode.setForeground(etColorFont);
		etCode.setFont(etFont);
		etCode.setText(s);
		etCode.setBounds(etName.getX() + etName.getWidth() + 162,
						 title.getY() + title.getHeight() + 20,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		s = "Profil";
		bounds = metrics.getStringBounds(s, null);
		JLabel etProfil= new JLabel();
		etProfil.setForeground(etColorFont);
		etProfil.setText(s);
		etProfil.setFont(etFont);
		etProfil.setBounds(etCode.getX() + etCode.getWidth() + 150,
						 title.getY() + title.getHeight() + 20,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		
		setBottomPanel();
	
		
		
		topPanel.setBounds(0,0,
						   managePanel.getWidth(),
						   etName.getY() + etName.getHeight()+7);
		
		
		middlePanel.setLocation(0, topPanel.getHeight());
		
	
		bottomPanel.setSize(managePanel.getWidth(),
							back.getHeight() + 10);
		
		middlePanel.setSize(managePanel.getWidth(),
							managePanel.getHeight() - topPanel.getHeight() - bottomPanel.getHeight());
		bottomPanel.setLocation(0, middlePanel.getY() + middlePanel.getHeight());
		
		topPanel.add(title);
		topPanel.add(etName);
		topPanel.add(etCode);
		topPanel.add(etProfil);
		
		
		managePanel.add(topPanel);
		
		managePanel.add(middlePanel);
		managePanel.add(bottomPanel);
		
		
		
	}
	
	private void setTextField(JTextField pole, Dimension size) {
		
		pole.setBackground(backField);	
		pole.setForeground(fontFieldColor);
		pole.setFont(fontLog);
		pole.setBorder(notWrite);
		pole.setSize((int) size.getWidth(), (int) size.getHeight());
		pole.setFocusable(true);
		pole.addFocusListener(this);
		
	}
	
	private void setLabelField(JLabel lab, String s) {
		
		
		FontMetrics metrics = new FontMetrics(etFontLog) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		lab.setText(s);
		lab.setFont(etFontLog);
		lab.setForeground(etFontLogColor);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		
	}
	
	public void setRequestWindow() {
		//login.requestFocusInWindow();
		
	}
	
	private void setLogPanel() {
		
		String s = "Zaloguj si� jako administrator";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((logPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());

		login = new JTextField();
		setTextField(login, new Dimension(378,48));
		login.setLocation((logPanel.getWidth() - login.getWidth())/2,
						   120);
		login.setFocusable(true);
		login.setText("");
		login.selectAll();
		
		
		
		//login.addNotify();
		
		JLabel etLogin = new JLabel();
		setLabelField(etLogin, "Login");
		etLogin.setLocation(login.getX() - 20 - etLogin.getWidth(),
						  	login.getY() + (login.getHeight() - etLogin.getHeight())/2);
		
		pass = new JPasswordField();
		pass.setBackground(backField);	
		pass.setForeground(fontFieldColor);
		pass.setBorder(notWrite);
		pass.setSize(378,48);
		pass.setEchoChar('*');
		pass.setLocation((logPanel.getWidth() - login.getWidth())/2,
						  login.getY() + login.getHeight() + 25);
		pass.setFocusable(true);
		pass.addFocusListener(this);
		
		JLabel etPass = new JLabel();
		setLabelField(etPass, "Has�o");
		etPass.setLocation(pass.getX() - 20 - etPass.getWidth(),
						   pass.getY() + (pass.getHeight() - etPass.getHeight())/2);
		
		final JLabel error = new JLabel();
		error.setForeground(Color.RED);
		error.setFont(new Font("Arial", Font.BOLD, 12));
		error.setSize(pass.getWidth(), 15);
		error.setLocation(pass.getX(),
							pass.getY() + pass.getHeight() + 5);
		error.setVisible(false);
		
		JButton logIn = new JButtonOwn("but/green", "Zaloguj");
		logIn.setLocation(pass.getX() + pass.getWidth() - logIn.getWidth(),
						  pass.getY() + pass.getHeight() + 45);
		logIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				logInIn(error);
			}
		});
		logIn.addFocusListener(null);
		
		pass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					logInIn(error);
				}
				
			}			
		});
		
		logIn.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					logInIn(error);
				}
				
			}			
		});
		
		
		final JLabel forgot = new JLabel("Zapomnia�am/-em has�a");
		forgot.setFont(new Font("Arial", Font.BOLD, 12));
		forgot.setForeground(etFontLogColor);
		forgot.setSize(140, 15);
		forgot.setLocation(pass.getX() + pass.getWidth() - forgot.getWidth(),
						   error.getY() + error.getHeight() + 5);
		forgot.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				forgot.setForeground(etFontLogColor);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				forgot.setForeground(new Color(37,149,19));
				forgot.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				helpPanel.removeAll();
				cl.show(mainPanel, helpPanel.getName());
				setHelpPanel();
				
			}
		});
		
		logPanel.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				if(login!=null)
					login.requestFocusInWindow();
				
			}
		});
		
		logPanel.requestFocusInWindow();
		logPanel.add(title);
		logPanel.add(login);
		logPanel.add(pass);
		logPanel.add(etLogin);
		logPanel.add(etPass);
		logPanel.add(logIn);
		logPanel.add(error);
		logPanel.add(forgot);
	}
	
	private void setHelpPanel(){
		
		String s = "Odzyskaj has�o";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((helpPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		helpPanel.add(title);
		
		String message = "Skontaktuj si� z pomoc� na adres e-mail: \npomoc@best-beauty.com \nlub wpisz poni�ej klucz resetuj�cy:";
		StyleContext context = new StyleContext();
	    StyledDocument document = new DefaultStyledDocument(context);
	    
	    Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
	    StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);
	    StyleConstants.setFontSize(style, 20);
	    StyleConstants.setFontFamily(style, "Arial");
	    StyleConstants.setForeground(style, etFontLogColor);
	    StyleConstants.setSpaceBelow(style, 4);
	    
	    
	    try {
			document.insertString(document.getLength(), message, style);
			
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JTextPane textPane = new JTextPane(document);
		textPane.setEditable(false);
		textPane.setBorder(null);
		textPane.setBackground(Color.white);
		textPane.setSize(500,80);
		textPane.setLocation(helpPanel.getWidth()/2 - textPane.getWidth()/2,
							 title.getY() + title.getHeight() + 30);
		
		final JTextField keyPass = new JTextField();
		setTextField(keyPass, new Dimension(378,48));
		keyPass.setLocation(helpPanel.getWidth()/2 - keyPass.getWidth()/2,
						    textPane.getY() + textPane.getHeight() + 20);
		helpPanel.add(keyPass);
		
		acceptButton = new JButtonOwn("but/green", "Zatwierd� klucz");
		acceptButton.setLocation(keyPass.getX() + keyPass.getWidth() - acceptButton.getWidth(),
							     keyPass.getY() + keyPass.getHeight() + 10);
		helpPanel.add(acceptButton);
		
		backButton = new JButtonOwn("but/green", "Powr�t");
		backButton.setLocation(keyPass.getX(), acceptButton.getY());
		helpPanel.add(backButton);
		
		acceptButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(serviceData.checkKeyMaster(keyPass.getText())) {
					backButton.setVisible(false);
					acceptButton.setVisible(false);
					setResetPass(keyPass);
					keyPass.setBorder(notWrite);
					helpPanel.repaint();
				}
				else {
					keyPass.setBorder(ConfigureApp.mistakeWrite);
				}
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				
				cl.show(mainPanel, logPanel.getName());
				
				
			}
		});
		
		
		
		
		helpPanel.add(textPane);
		
	}
	
	private void setResetPass(JComponent last) {
		
		JLabel etPass = new JLabel();
		setLabelField(etPass, "Wpisz nowe has�o");
		etPass.setLocation(last.getX(),
						   last.getY() + last.getHeight() + 10);
		
		helpPanel.add(etPass);
		
		final JPasswordField pass = new JPasswordField();
		setPasswordField(pass, new Dimension(378,48));
		pass.setLocation(last.getX(),
						 etPass.getY() + etPass.getHeight() + 5);
		
		helpPanel.add(pass);
		
		JLabel etPassRepeat = new JLabel();
		setLabelField(etPassRepeat, "Powt�rz nowe has�o");
		etPassRepeat.setLocation(pass.getX(),
						   		 pass.getY() + pass.getHeight() + 10);
		
		helpPanel.add(etPassRepeat);
		
		final JPasswordField passRepeat = new JPasswordField();
		setPasswordField(passRepeat, pass.getSize());
		passRepeat.setLocation(pass.getX(),
				 	     	   etPassRepeat.getY() + etPassRepeat.getHeight() + 5);
		
		helpPanel.add(passRepeat);
		
		backButton.setLocation(passRepeat.getX(), passRepeat.getY() + passRepeat.getHeight() + 15);
		backButton.setVisible(true);
		
		final JLabel errorTwo = new JLabel();
		errorTwo.setForeground(Color.RED);
		errorTwo.setFont(new Font("Arial", Font.BOLD, 12));
		errorTwo.setSize(passRepeat.getWidth(), 15);
		errorTwo.setLocation(passRepeat.getX(),
						     passRepeat.getY() + passRepeat.getHeight());
		errorTwo.setVisible(false);
		
		helpPanel.add(errorTwo);
		
		acceptButton.setLocation(passRepeat.getX() + passRepeat.getWidth() - acceptButton.getWidth(),
								 backButton.getY());
		acceptButton.setVisible(true);
		acceptButton.changeText("Zatwierd� has�o");
		
		acceptButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				if(!serviceData.checkPass(pass.getText(), passRepeat.getText())) {
					
					errorTwo.setVisible(true);
					errorTwo.setText("Has�a nie s� identyczne");
					passRepeat.setBorder(ConfigureApp.mistakeWrite);
					pass.setBorder(ConfigureApp.mistakeWrite);
					
				}
				else {
					errorTwo.setVisible(false);
					passRepeat.setBorder(notWrite);
					pass.setBorder(notWrite);
					serviceData.changePass(serviceData.getAdmin(), pass.getText());
					logPanel.removeAll();
					setLogPanel();
					logPanel.repaint();
					cl.show(mainPanel, logPanel.getName());
				}
				
			}
		});
		
		/*final JLabel error = new JLabel();
		error.setForeground(Color.RED);
		error.setFont(new Font("Arial", Font.BOLD, 12));
		error.setSize(pass.getWidth(), 15);
		error.setLocation(passNow.getX(),
						  passNow.getY() + passNow.getHeight() + 5);
		error.setVisible(false);*/
		
		
		
	}
	
	private void logInIn(JLabel error) {
		isLog = false;
		if(serviceData.logInAdmin(login.getText(), pass.getText())) {
			isLog = true;
			if(!isDeleted) {
				loginName = login.getText();
				error.setVisible(false);
				cl.show(mainPanel, "buttonPanel");
				buttonPanel.removeAll();
				setButtons();
				buttonPanel.repaint();
				
			}
			else {
				Customer tempC = SearchView.person;
				serviceData.deleteCustomer(tempC.getId());
				
				int size = MainWindow.tabbedPanel.getTabCount();
				int i=0;
				
				
				while(i<size) {
					if(MainWindow.tabbedPanel.getTitleAt(i).equals(tempC.getSurname() + " " + tempC.getName())) {
						MainWindow.tabbedPanel.remove(i);
						break;
					}
						
					i++;
				}
				
				SearchView.setActionSearch();
				MainWindow.pane.setVisible(false);
				MainWindow.mainGlassPanel.removePanel();
				MainWindow.mainGlassPanel.setVisible(false);
				//MainWindow.adminPanel.components();
				MainWindow.mainGlassPanel.repaint();
			}
			isDeleted = false;
			
		}
		else {
			error.setVisible(true);
			error.setText("Poda�e� z�e has�o b�d� login. Spr�buj jeszcze raz");
			
			
		}
		
	}
	
	private void setSpinner(JSpinner spinner) {
		
		SimpleDateFormat format = new SimpleDateFormat("E, d MMM yyyy", Locale.getDefault());
		Calendar cal = Calendar.getInstance();
		spinner.setValue(cal.getTime());
		
		
		//spinner = new JSpinner(model);
		//spinner.setUI((SpinnerUI) DateSpinnerUI.createUI(spinner));
		JComponent editor = new JSpinner.DateEditor(spinner, format.toPattern());
		spinner.setEditor(editor);
		spinner.setSize(150,35);
		spinner.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, new Color(209,209,209)));
		//spinner.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 1, new Color(232,186,55)));
		spinner.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 13));
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setHighlighter(null);
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setForeground(new Color(76,75,75));
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setBackground(ConfigureApp.backgroundColor);
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setHorizontalAlignment(JTextField.LEFT);
		((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setBorder(new EmptyBorder(0, 10, 0, 0));
		spinner.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				
				
			}
		});
		
		
	}
	
	private void setAddPanel(final User u) {
		
		int y = 15;
		int x = 20;
		
		isChange = false;
		userCreatePanel = u;
		String s="";
		
		if(u==null) 
			 s = "Dodaj nowego pracownika";
		else
			 s = "Edytuj dane pracownika";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		save = new SaveLabel(addPanel.getSize(), "save");
		save.setVisible(false);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((addPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		name = new JTextField();
		setTextField(name, new Dimension(210,40));
		name.setLocation(addPanel.getWidth()/2 - 15 - name.getWidth(),
						 title.getY() + title.getHeight() + 50);
		
		JLabel etName = new JLabel();
		setLabelField(etName, "Imi�");
		etName.setLocation(name.getX() - x - etName.getWidth(),
						   name.getY() + (name.getHeight()-etName.getHeight())/2);
		
		surname = new JTextField();
		setTextField(surname, new Dimension(210,40));
		surname.setLocation(name.getX(),
						 name.getY() + name.getHeight() + y);
		
		surname.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
				if(u==null) {
					String cod = "";
					if(e.getKeyCode() != KeyEvent.VK_BACK_SPACE)
						countKey++;
					else if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE && countKey>0)
						countKey--;
					
					if(countKey>=1) {
						cod = serviceData.checkNumberUser(surname.getText(), name.getText());
						if(cod!=null)
							code.setText(cod);
					}
				}
				
				
				
				
				
			}
		});
		
		JLabel etSurname = new JLabel();
		setLabelField(etSurname, "Nazwisko");
		etSurname.setLocation(surname.getX() - x - etSurname.getWidth(),
							  surname.getY() + (surname.getHeight()-etSurname.getHeight())/2);
		
		code = new JTextField();
		setTextField(code, new Dimension(210,40));
		code.setLocation(surname.getX(),
						 surname.getY() + surname.getHeight() + y);
		
		if(u!=null) 
			code.setEnabled(false);
		
		JLabel etCode = new JLabel();
		setLabelField(etCode, "Kod");
		etCode.setLocation(code.getX() - x - etCode.getWidth(),
						   code.getY() + (surname.getHeight()-etCode.getHeight())/2);
		
		
		freeDay = new JCheckBox("Ustawi� dni wolne? ");
		freeDay.setSelected(false);
		freeDay.setSize(200,25);
		freeDay.setLocation(code.getX(), code.getY() + code.getHeight() + y + 10);
		freeDay.setFont(etFontLog);
		freeDay.setForeground(etFontLogColor);
		freeDay.setBackground(null);
		
		
		
		freeDay.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if(e.getStateChange() == ItemEvent.DESELECTED) {
					
					from.setVisible(false);
					fromText.setVisible(false);
					toText.setVisible(false);
					to.setVisible(false);
					isSelectedFree = false;
					
				}
				else {
					
					from.setVisible(true);
					fromText.setVisible(true);
					toText.setVisible(true);
					to.setVisible(true);
					isSelectedFree = true;
					
				}
			}
		});
		
	      
		etFree = new JLabel();
		setLabelField(etFree, "Urlop");
		etFree.setLocation(freeDay.getX() - x - etFree.getWidth(),
						   freeDay.getY() + (freeDay.getHeight()-etFree.getHeight())/2);
		
		etFree.setVisible(false);
		
		
		
		MaskFormatter mask = null;
	        try {
	            //
	            // Create a MaskFormatter for accepting phone number, the # symbol accept
	            // only a number. We can also set the empty value with a place holder
	            // character.
	            //
	            mask = new MaskFormatter("##-##-####");
	            mask.setPlaceholderCharacter('_');
	           
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
		
		fromText = new JFormattedTextField(mask);
		fromText.setBackground(ConfigureApp.backgroundColor);
		fromText.setFont(new Font("Arial", Font.BOLD, 16));
		fromText.setSize(90,35);
		fromText.setLocation(freeDay.getX(), freeDay.getY() + freeDay.getHeight() + y + 10);
		fromText.setBackground(backField);	
		fromText.setForeground(fontFieldColor);
		fromText.addFocusListener(this);
		fromText.setFocusable(true);
		fromText.setVisible(false);
		
		from = new JLabel();
		setLabelField(from, "Od");
		from.setLocation(fromText.getX() - x - from.getWidth(), fromText.getY() + (fromText.getHeight()- from.getHeight())/2);
		from.setVisible(false);
		
		to = new JLabel();
		setLabelField(to, "Do");
		to.setLocation(fromText.getX() + fromText.getWidth() + 15, from.getY());
		to.setVisible(false);
		
		toText = new JFormattedTextField(mask);
		toText.setBackground(ConfigureApp.backgroundColor);
		toText.setFont(new Font("Arial", Font.BOLD, 16));
		toText.setSize(90,35);
		toText.setLocation(to.getX() + to.getWidth() + 10, fromText.getY());
		toText.setBackground(backField);	
		toText.setForeground(fontFieldColor);
		toText.addFocusListener(this);
		toText.setFocusable(true);
		toText.setVisible(false);
		
		if(u!=null) {
			SimpleDateFormat df = new SimpleDateFormat("d-MM-yyyy");
			u.setFreeDay();
			
			if(u.getStartDate()!=null) {
				fromText.setText(df.format(u.getStartDate()));
				freeDay.setSelected(true);
				to.setVisible(true);
				from.setVisible(true);
				toText.setVisible(true);
				fromText.setVisible(true);
			}
			else {
				freeDay.setSelected(false);
				to.setVisible(false);
				from.setVisible(false);
				toText.setVisible(false);
				fromText.setVisible(false);
			}
			
			if(u.getEndDate()!=null)
				toText.setText(df.format(u.getEndDate()));
			
			
			
			
		}
		
		
		final JLabel avatar = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/avatar.png");
		avatar.setIcon(imgIc);
		avatar.setBounds(addPanel.getWidth()/2 + 15,
						 name.getY(),
						 imgIc.getIconWidth(),
						 imgIc.getIconHeight());
		
		JButton changeAvatar = new JButtonOwn("buttons/changeAvatar");
		changeAvatar.setLocation(avatar.getX() + avatar.getWidth() + 2, 
								 avatar.getY() + avatar.getHeight()/2 - changeAvatar.getHeight()/2);
		changeAvatar.setEnabled(true);
		changeAvatar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(MainWindow.getFrame());
				selectedFile = null;
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					selectedFile = fc.getSelectedFile();
					System.out.println("Selected File: " + selectedFile.getName());
					imgAvatar = new ImageIcon(selectedFile.getPath());
					Image img = imgAvatar.getImage();  
					Image newimg = img.getScaledInstance(avatar.getWidth(), 
														 avatar.getHeight(),  
														 java.awt.Image.SCALE_SMOOTH);  
					imgAvatar = new ImageIcon(newimg); 
					avatar.setIcon(imgAvatar);
					addPanel.repaint();
					isChange = true;
				}
				else {
					selectedFile = null;
				}
				
			}
		});
		
		changeAvatar.setFocusable(false);

		
		if(u==null)
			addNewWorker = new JButtonOwn("but/green", "Zapisz");
		else
			addNewWorker = new JButtonOwn("but/green", "Zapisz zmiany");
		addNewWorker.setLocation(addPanel.getWidth() - addNew.getWidth() - 5,
						   addPanel.getHeight() - addNew.getHeight() - 5);		
		addNewWorker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				Boolean isAnimation = true;
				
				//sprawdzamy, czy u�ytkownik dodaje nowego pracownika czy te� edytuje jego dane
				if(u==null) {
					System.out.println("Kod pracownika: " + code.getText());
					User user = serviceData.addUser(name.getText(), surname.getText(), code.getText(), selectedFile);
					
					name.setText(serviceData.getName());
					surname.setText(serviceData.getSurname());
					
					Boolean isGood = true;
					
					if(user==null) {
						
						if(name.getText().equals(""))
							setMistake(name);
						else
							setGood(name);
						if(surname.getText().equals("")) 
							setMistake(surname);
						else
							setGood(surname);
						
						
						
						isGood = false;	
						isAnimation = false;
						code.setText("");
					}
					else {
						setGood(name);
						setGood(surname);
						setGood(code);
						isGood = true;
						isAnimation = true;
					}
					
					userCreatePanel = user;
					//System.out.println("User u: " + u);
					if(user!=null) {
						
						ConfigureApp.addSession(user);
						userCreatePanel.setFreeDay(freeDay.isSelected());
						listUsers.add(u);
						managePanel.removeAll();
						cl.show(mainPanel, "managePanel");
						setManagePanel();
						middlePanel.removeAll();
						bottomPanel.removeAll();
						setBottomPanel();
						middlePanel.repaint();
						bottomPanel.repaint();
						managePanel.repaint();
						
						System.out.println("CalendarPanel: " + CalendarPanelNew.comboBox);
						
						if(CalendarPanelNew.comboBox!=null) {
							CalendarPanelNew.comboBox.addItem(serviceData.parseToString(user));
							//CalendarPanelNew.setEnabledAll();
							CalendarPanelNew.repaintCalendarPanel();
							CalendarPanelNew.changeStateAll();
						}
						
						isAnimation = setFreeDays();
						
						if(isAnimation)
							MainWindow.save.setAnimation(1.0f);
					
						//bottomPanel.repaint();
					}
					else if((user==null && isGood) || code.getText().equals("")){
						
						setMistake(code);
						
						comm.dont("Wpisany kod pracownika jest ju� w bazie. Wpisz inny kod");
						code.setText("");
						user = null;
						System.out.println("Powinno tutaj wyskoczy�....");
					}
				}
				else {
					
					
					
					//Boolean isAnimation = true;
					//sprawdzamy, czy u�ytkownik zmieni� avatar
					if(isChange) {
						userCreatePanel = serviceData.updateAvatar(name.getText(), surname.getText(), code.getText(), selectedFile);
						
					}
					else {
						User userNew = new User(name.getText(), surname.getText(), code.getText(), u.getStatus());
						userCreatePanel = userNew;
						serviceData.updateUser(u, userNew);
						//MainWindow.save.setAnimation(1.0f);
						
						
						
					}
					
					if(userCreatePanel!=null) {
						
						ConfigureApp.updateSession(userCreatePanel);
						isAnimation = setFreeDays();
						if(CalendarPanelNew.calendarPanel!=null) 
							CalendarPanelNew.repaintCalendarPanel(listUsers.size());
						
					}
					
							
					
				}
				
				
				
				if(isAnimation) {
					MainWindow.save.setAnimation(1.0f);
					
				}
			}	
		});
		
		backToList = new JButtonOwn("but/green", "Powr�t");
		backToList.setLocation(5, 
						 	   addNewWorker.getY());
		backToList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				if(isEmptyAll()) {
					System.out.println("Peszek, nic nie wpisa�e� :P");
					repaintManagePanel();
				}
				else if(!isEmptyAll() && userCreatePanel==null) {
					Boolean yes = comm.youSure("Nie zapisa�e� zmian. Czy na pewno chcesz wyj��?", 1);
					if(yes)
						repaintManagePanel();
					
				}
				
				else {
					byte [] bytes = null;
					if(selectedFile!=null)
						bytes = serviceData.changeFileToByte(selectedFile);
					else 
						bytes = userCreatePanel.getAvatarByte();
					System.out.println("Nazwisko: '" + surname.getText() + "'");
					
					User userNew = new User(name.getText(), surname.getText(), code.getText(), bytes, u.getStatus());
					System.out.println("AdminPanel: Linijka 899: UserNew : " + userNew);
					System.out.println("AdminPanel: Linijka 900: UserOld : " + userCreatePanel);
					Boolean yes = false;
					if(userCreatePanel.compareTo(userNew)==1) {
						yes = comm.youSure("Nie zapisa�e� zmian. Czy na pewno chcesz wyj��?", 1);
					}
					else {
						repaintManagePanel();
					}
					
					if((yes==true) || (u.compareTo(userNew)==0)) {
						repaintManagePanel();
					}
				}
				
				
				
			}
		});
		

		if(u!=null) {
			name.setText(u.getName());
			surname.setText(u.getSurname());
			code.setText(u.getCode());
			
			if(u.getAvatar()==null) {
				imgIc = new ImageIcon("resources/images/avatar.png");
				avatar.setIcon(imgIc);
			}
			else {
			//wyskalowanie zdj�cia
				Image img = u.getAvatar().getImage();  
				Image newimg = img.getScaledInstance(avatar.getWidth(), 
												 avatar.getHeight(),  
												 java.awt.Image.SCALE_SMOOTH);  
				imgIc = new ImageIcon(newimg); 
				avatar.setIcon(imgIc);
			}
		}
		
		//save.setVisible(true);
		//addPanel.add(save);
		addPanel.add(title);
		addPanel.add(name);
		addPanel.add(surname);
		addPanel.add(code);
		addPanel.add(from);
		addPanel.add(fromText);
		addPanel.add(to);
		addPanel.add(toText);
		addPanel.add(avatar);
		addPanel.add(changeAvatar);
		addPanel.add(etName);
		addPanel.add(etSurname);
		addPanel.add(etCode);
		addPanel.add(addNewWorker);
		addPanel.add(backToList);
		addPanel.add(etFree);
		addPanel.add(freeDay);
		
	}
	
	private Boolean setFreeDays() {
		
		Boolean isAnimation = true;
		
		System.out.println("Sprawdzamy, czy uruchamia si� funkcja...");
		System.out.println("FromText - " + fromText.getText());
		System.out.println("ToText - " + toText.getText());
		
		if(serviceData.checkFree(fromText.getText(), toText.getText())) {
			
			System.out.println("Daty wprowadzone s� poprawnie...");
			
			Date dateFrom = serviceData.checkDateFromString(fromText.getText(), "-");
			
			System.out.println("Data od: " + dateFrom);
			
			Date dateTo = serviceData.checkDateFromString(toText.getText(), "-");
			
			if(dateFrom!=null && dateTo!=null) {
				
				String date = null;
				
				if(dateFrom.compareTo(dateTo)==-1) {
					DateFormat df = DateFormat.getDateInstance();
				
					date = df.format(dateFrom) + ";;" + df.format(dateTo);
					
					userCreatePanel.setFreeDay(freeDay.isSelected());
					if(!freeDay.isSelected() && !isSelectedFree) {
						isAnimation = true;
						isSelectedFree = null;
						serviceData.updateUserMeta(userCreatePanel.getCode());
						ConfigureApp.updateSession(userCreatePanel);
					}
					else if(userCreatePanel.getFreeDate()==null) {
						serviceData.setUserMeta(userCreatePanel.getCode(), date, "urlop");
						ConfigureApp.updateSession(userCreatePanel);
					}
					else if(!userCreatePanel.getFreeDate().equals(date)) {
						serviceData.updateUserMeta(userCreatePanel.getCode(), date, "urlop");
						ConfigureApp.updateSession(userCreatePanel);
					}
					
					else
						isAnimation = false;
					
				}
				else {
					isAnimation = false;
					toText.setBorder(ConfigureApp.mistakeWrite);
				}
					
				
			}
			else if(!freeDay.isSelected() && !isSelectedFree) {
				System.out.println("Aktualizacja - dezaktywacja urlopu... - linia 1664" );
				isAnimation = true;
				isSelectedFree = null;
				serviceData.updateUserMeta(userCreatePanel.getCode());
				ConfigureApp.updateSession(userCreatePanel);
			}
	 		else {
				if(dateFrom==null)
					fromText.setBorder(ConfigureApp.mistakeWrite);
				if(dateTo==null)
					toText.setBorder(ConfigureApp.mistakeWrite);
					
				isAnimation = false;
				
				
			}
			
			
		}
		
		return isAnimation;
		
	}
	
	private void setSettingsPanel() {
		
		salonSettings = new JPanel();
		salonSettings.setBackground(Color.WHITE);
		salonSettings.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		salonSettings.setName("salonSettings");
		salonSettings.setLayout(null);
		settingsPanel.add(salonSettings, salonSettings.getName());
		
		String s = "Godziny otwarcia salonu";
		
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((addPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		salonSettings.add(title);
		
		JLabel etOpen = new JLabel();
		setLabelFont(etOpen, "Godziny pracy salonu");
		etOpen.setLocation(200, title.getY() + title.getHeight() + 40);
		
		salonSettings.add(etOpen);
		
		final List<WorkPlanLabel> listDays = new ArrayList<WorkPlanLabel>();
		int x = etOpen.getX();
		int y = etOpen.getY() + etOpen.getHeight() + 30;
		
		listDays.add(new WorkPlanLabel("poniedzia�ek", x, y, 2));
		
		
		
		JButton fillAll = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/but/fillAll.png");
		fillAll.setIcon(imgIc);
		fillAll.setBorderPainted(false);
		fillAll.setContentAreaFilled(false);
		fillAll.setFocusPainted(false);
		fillAll.setSize(imgIc.getIconWidth(),
						imgIc.getIconHeight());
		fillAll.setLocation(x + listDays.get(0).getWidth() + 5,
							y);
		fillAll.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(listDays.get(0).getWrong()) {
					errorDay.setText("Pola s� niepoprawnie wype�nione. Popraw i sp�buj jeszcze raz");
					errorDay.setVisible(true);
				}
				else {
					String to = listDays.get(0).getTextTo();
					String from = listDays.get(0).getTextFrom();
					
					Iterator<WorkPlanLabel> it = listDays.iterator();
					while(it.hasNext()) {
						WorkPlanLabel plan = it.next();
						plan.setTextFrom(from);
						plan.setTextTo(to);
					}
					errorDay.setVisible(false);
				}
				
				
				
			}
		});
		
		salonSettings.add(fillAll);
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("wtorek", x, y, 3));
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("�roda", x, y, 4));
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("czwartek", x, y, 5));
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("pi�tek", x, y, 6));
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("sobota", x, y, 7));
		
		y = y + listDays.get(0).getHeight() + 2;
		listDays.add(new WorkPlanLabel("niedziela", x, y, 1));
		
		Iterator<WorkPlanLabel> it = listDays.iterator();
		
		errorDay = new JLabel();
		errorDay.setForeground(Color.red);
		errorDay.setLocation(x, 
							 listDays.get(listDays.size()-1).getY() + 5 + listDays.get(listDays.size()-1).getHeight());
		errorDay.setSize(300, 15);
		
		while(it.hasNext()) {
			
			WorkPlanLabel l = it.next();
			l.setError(errorDay);
			salonSettings.add(l);
			
		}
		
		salonSettings.add(errorDay);
		
		JButton save = new JButtonOwn("but/green", "Zapisz");
		save.setLocation(salonSettings.getWidth() - save.getWidth() - 10,
						 salonSettings.getHeight() - save.getHeight() - 10);
		save.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Iterator<WorkPlanLabel> it = listDays.iterator();
				Numbers num = new Numbers();
				
				Boolean isAnimation = true;
				Boolean isGood = true;
				
				if(!isHoursEmpty(listDays)) {
					errorDay.setVisible(false);
					if(ConfigureApp.listDayWork.size()<1) {
						
						int i = 2;
						while(it.hasNext()) {
							
							WorkPlanLabel plan = it.next();
							if(i==8)
								i=1;
							if(plan.getTextFrom().isEmpty() && plan.getTextTo().isEmpty()) {
								
							}
							else if(plan.getTextFrom().isEmpty() || plan.getTextTo().isEmpty()) {
								errorDay.setText("Nie wype�niono poprawnie wszystkich p�l");
								errorDay.setVisible(true);
								isAnimation = false;
							}
							else if(!num.isInteger(plan.getTextFrom()) || !num.isInteger(plan.getTextTo())) {
								errorDay.setText("Nie wype�niono poprawnie wszystkich p�l");
								errorDay.setVisible(true);
								isAnimation = false;
							}
							else {
								
								serviceData.setVars(i, plan.getTextFrom(), plan.getTextTo());
								
									
							}
							i++;
						}
					}
					else {
						
						Iterator<DayWork> it2 = ConfigureApp.listDayWork.iterator();
						it = listDays.iterator();
						
						
						
						while(it2.hasNext()) {
							
							DayWork day = it2.next();
							
							while(it.hasNext() && isGood) {
								
								WorkPlanLabel plan = it.next();
								if(day.getDay()==plan.getDay()) {
									isGood = serviceData.updateVars(day.getId(), day.getDay(), 
														   plan.getTextFrom(), plan.getTextTo());
									System.out.println("Update: " + day.getDay());
								}
								
								
							}
							it = listDays.iterator();
						}
						
					}
					
					ConfigureApp.listDayWork = serviceData.getDayWork();
					/*CalendarPanelNew.calendarPanel.removeAll();
					CalendarPanelNew.setCalendarPanel();
					CalendarPanelNew.calendarPanel.repaint();*/
					if(CalendarPanelNew.calendarPanel!=null)
						CalendarPanelNew.refreshCalendarPanel();
					if(isAnimation && isGood)
						MainWindow.save.setAnimation(1.0f);
				}
				else {
					errorDay.setText("Nie wype�ni�a�/-e� ani jednego dnia poprawnie!");
					errorDay.setVisible(true);
				}
				
				
				
			}
		});
		salonSettings.add(save);
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(10,
						 salonSettings.getHeight() - back.getHeight() - 10);
		salonSettings.add(back);
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//buttonPanel.removeAll();
				//setButtons();
				//buttonPanel.repaint();
				cl.show(mainPanel, settingsMainPanel.getName());
				
			}
		});
		
		if(ConfigureApp.listDayWork.size()>0) {
			
			Iterator<DayWork> it2 = ConfigureApp.listDayWork.iterator();
			it = listDays.iterator();
			
			while(it2.hasNext()) {
				
				DayWork day = it2.next();
				while(it.hasNext()) {
					
					WorkPlanLabel plan = it.next();
					if(day.getDay()==plan.getDay()) {
						if(plan.getWork().equals("p")) {
							plan.setTextFrom(Integer.toString(day.getStart()));
							plan.setTextTo(Integer.toString(day.getEnd()));
						}
						else {
							plan.setTextFrom("");
							plan.setTextTo("");
						}
					}
					
				}
				it = listDays.iterator();
			}
			
		}
		
	}
	
	private Boolean isHoursEmpty(List<WorkPlanLabel> list) {
		
		Boolean isEmpty = true;
		
		for(WorkPlanLabel l : list) {
			if(!l.getTextTo().isEmpty() && !l.getTextFrom().isEmpty())
				isEmpty = false;
		}
		
		return isEmpty;
		
	}
	
	private void setTreatmentListPanel() {
		
		String s = "Lista zdefiniowanych zabieg�w";
		
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((addPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		treatmentListPanel.add(title);
		
		middlePanel = new JPanel();
		middlePanel.setOpaque(false);
		middlePanel.setLayout(null);
		middlePanel.setLocation(0, title.getY() + title.getHeight() + 10);
		middlePanel.setSize(treatmentListPanel.getWidth(), 
				            treatmentListPanel.getHeight()- title.getY() - title.getHeight() - 10 - 80);
		
		treatmentListPanel.add(middlePanel);
		
		bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		bottomPanel.setLayout(null);
		
		backTreat = new JButtonOwn("but/green", "Powr�t");
		backTreat.setLocation(5, 5);
		bottomPanel.add(backTreat);
		backTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cl.show(mainPanel, settingsMainPanel.getName());
			}
		});
		
		newTreat = new JButtonOwn("but/green", "Dodaj zabieg");
		newTreat.setLocation(treatmentListPanel.getWidth() - newTreat.getWidth() - 5, 
							 backTreat.getY());
		bottomPanel.add(newTreat);
		newTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				treatNewPanel.removeAll();
				setTreatNewPanel();
				treatNewPanel.repaint();
				cl.show(mainPanel, treatNewPanel.getName());
			}
		});
		
		bottomPanel.setSize(treatmentListPanel.getWidth(),
							backTreat.getHeight() + 10);

		bottomPanel.setLocation(0, treatmentListPanel.getHeight() - backTreat.getHeight() -10);
		
		treatmentListPanel.add(bottomPanel);
		
		setListTreat();
		
	}
	
	private void setListTreat() {
		
		//ilo�� u�ytkownik�w na stron�
		
		//listVisit.clear();
		
		listTreat = new ArrayList<Treatment>();
		listTreat = serviceData.getTreatments("WHERE order_by!=0");
		Pages pages = new Pages(listTreat, numberOfTreat, bottomPanel);
		pages.setPagePanel(new PagePaneTreat(middlePanel, this));
		pages.setButtons(backTreat, newTreat);
		pages.setPages();
		
		
		
		
		
		
		
	}
	
	
	public void removeAllTreatmentPanel() {
		
		treatmentListPanel.removeAll();
		setTreatmentListPanel();
		cl.show(mainPanel, treatmentListPanel.getName());
		treatmentListPanel.repaint();
		
	}
	

	
	private void setTreatNewPanel() {
		
		String s = "Dodaj nowy zabieg";
		
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((treatNewPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		treatNewPanel.add(title);
		
		int y = 15;
		int x = 20;
		
		
		nameTreat = new JTextField();
		setTextField(nameTreat, new Dimension(210,40));
		nameTreat.setName("nameTreat");
		nameTreat.setLocation(treatNewPanel.getWidth()/2 - 15 - nameTreat.getWidth(),
						 title.getY() + title.getHeight() + 50);
		
		treatNewPanel.add(nameTreat);
		
		
		
		JLabel etName = new JLabel();
		setLabelField(etName, "Nazwa zabiegu");
		etName.setLocation(nameTreat.getX() - x - etName.getWidth(),
						   nameTreat.getY() + (nameTreat.getHeight()-etName.getHeight())/2);
		
		treatNewPanel.add(etName);
		
		number = new JTextField();
		setTextField(number, new Dimension(210,40));
		number.setName("number");
		number.setLocation(nameTreat.getX(),
						   nameTreat.getY() + nameTreat.getHeight() + y);
		
		treatNewPanel.add(number);
		
		JLabel etNumber = new JLabel();
		setLabelField(etNumber, "Numer kasowy");
		etNumber.setLocation(number.getX() - x - etNumber.getWidth(),
							 number.getY() + (number.getHeight()-etNumber.getHeight())/2);
		
		treatNewPanel.add(etNumber);
		
		price = new JTextField();
		price.setName("price");
		setTextField(price, new Dimension(210,40));
		price.setLocation(number.getX(),
						  number.getY() + number.getHeight() + y);
		
		treatNewPanel.add(price);
		
		JLabel etPrice = new JLabel();
		setLabelField(etPrice, "Cena");
		etPrice.setLocation(price.getX() - x - etPrice.getWidth(),
							price.getY() + (price.getHeight() - etPrice.getHeight())/2);
		
		treatNewPanel.add(etPrice);
		
		time = new JTextField();
		time.setName("time");
		setTextField(time, new Dimension(210,40));
		time.setLocation(price.getX(),
						 price.getY() + price.getHeight() + y);
		
		treatNewPanel.add(time);
		
		JLabel etTime = new JLabel();
		setLabelField(etTime, "Czas trwania");
		etTime.setLocation(time.getX() - x - etTime.getWidth(),
						    time.getY() + (etTime.getHeight()-etTime.getHeight())/2);
		
		treatNewPanel.add(etTime);
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5, treatmentListPanel.getHeight() - back.getHeight() - 5);
		treatNewPanel.add(back);
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cl.show(mainPanel, treatmentListPanel.getName());
			}
		});
		
		JButton saveTreat = new JButtonOwn("but/green", "Zapisz zabieg");
		saveTreat.setLocation(treatmentListPanel.getWidth() - saveTreat.getWidth() - 5, 
							 treatmentListPanel.getHeight() - saveTreat.getHeight() - 5);
		treatNewPanel.add(saveTreat);
		saveTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				//je�li true - prawid�owy; false - nieprawid�owy
				Boolean goodPrice = false;
				Boolean goodTime = false;
				Boolean goodName = false;
				
				Numbers numb = new Numbers();
				
				if(!nameTreat.getText().isEmpty()) {
					goodName = true;
					setGood(nameTreat);
				}
				else
					setMistake(nameTreat);
				
				if(!price.getText().isEmpty())
					goodPrice = numb.isFloatOrInteger(price.getText());
				else
					goodPrice = true;
				if(!time.getText().isEmpty())
					goodTime = numb.isInteger(time.getText());
				else
					goodTime = true;
				
				if(goodPrice) 
					setGood(price);
				else
					setMistake(price);
				
				if(goodTime)
					setGood(time);
				else
					setMistake(time);
				
				if(goodName && goodTime && goodPrice) {
					MainWindow.save.setEnabled(true);
					MainWindow.save.setAnimation(1.0f);
					
					serviceData.addTreat(new Treatment(number.getText(), nameTreat.getText(), time.getText(), price.getText()));
					nameTreat.setText("");
					number.setText("");
					time.setText("");
					price.setText("");
					
					treatmentListPanel.removeAll();
					setTreatmentListPanel();
					cl.show(mainPanel, treatmentListPanel.getName());
					treatmentListPanel.repaint();
					
					MainWindow.addDate.updateVisit();
					
				}
			
				
			}
		});
		
		
	}
	
	private void setTreatNewPanel(final Treatment treat) {
		
		
		
		String s = "Dodaj nowy zabieg";
		
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((treatNewPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		treatNewPanel.add(title);
		
		int y = 15;
		int x = 20;
		
		
		nameTreat = new JTextField();
		setTextField(nameTreat, new Dimension(210,40));
		nameTreat.setName("nameTreat");
		nameTreat.setText(treat.getName());
		nameTreat.setLocation(treatNewPanel.getWidth()/2 - 15 - nameTreat.getWidth(),
						 title.getY() + title.getHeight() + 50);
		nameTreat.addKeyListener(this);
		
		treatNewPanel.add(nameTreat);
		
		
		
		JLabel etName = new JLabel();
		setLabelField(etName, "Nazwa zabiegu");
		etName.setLocation(nameTreat.getX() - x - etName.getWidth(),
						   nameTreat.getY() + (nameTreat.getHeight()-etName.getHeight())/2);
		
		treatNewPanel.add(etName);
		
		number = new JTextField();
		setTextField(number, new Dimension(210,40));
		number.setName("number");
		if(number!=null)
			number.setText(treat.getNumber());
		else
			number.setText("");
		number.setLocation(nameTreat.getX(),
						   nameTreat.getY() + nameTreat.getHeight() + y);
		number.addKeyListener(this);
		
		treatNewPanel.add(number);
		
		JLabel etNumber = new JLabel();
		setLabelField(etNumber, "Numer kasowy");
		etNumber.setLocation(number.getX() - x - etNumber.getWidth(),
							 number.getY() + (number.getHeight()-etNumber.getHeight())/2);
		
		treatNewPanel.add(etNumber);
		
		price = new JTextField();
		price.setName("price");
		price.setText(Float.toString(treat.getPrice()));
		setTextField(price, new Dimension(210,40));
		price.setLocation(number.getX(),
						  number.getY() + number.getHeight() + y);
		price.addKeyListener(this);
		treatNewPanel.add(price);
		
		JLabel etPrice = new JLabel();
		setLabelField(etPrice, "Cena");
		etPrice.setLocation(price.getX() - x - etPrice.getWidth(),
							price.getY() + (price.getHeight() - etPrice.getHeight())/2);
		
		treatNewPanel.add(etPrice);
		
		time = new JTextField();
		time.setName("time");
		time.setText(Integer.toString(treat.getTime()));
		setTextField(time, new Dimension(210,40));
		time.setLocation(price.getX(),
						 price.getY() + price.getHeight() + y);
		time.addKeyListener(this);
		treatNewPanel.add(time);
		
		JLabel etTime = new JLabel();
		setLabelField(etTime, "Czas trwania");
		etTime.setLocation(time.getX() - x - etTime.getWidth(),
						    time.getY() + (etTime.getHeight()-etTime.getHeight())/2);
		
		treatNewPanel.add(etTime);
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5, treatmentListPanel.getHeight() - back.getHeight() - 5);
		treatNewPanel.add(back);
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cl.show(mainPanel, treatmentListPanel.getName());
			}
		});
		
		JButton saveTreat = new JButtonOwn("but/green", "Zapisz zabieg");
		saveTreat.setLocation(treatmentListPanel.getWidth() - saveTreat.getWidth() - 5, 
							 treatmentListPanel.getHeight() - saveTreat.getHeight() - 5);
		treatNewPanel.add(saveTreat);
		saveTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				//je�li true - prawid�owy; false - nieprawid�owy
				Boolean goodPrice = false;
				Boolean goodTime = false;
				Boolean goodName = false;
				
				Numbers numb = new Numbers();
				
				if(!nameTreat.getText().isEmpty()) {
					goodName = true;
					setGood(nameTreat);
				}
				else
					setMistake(nameTreat);
				
				if(!price.getText().isEmpty())
					goodPrice = numb.isFloatOrInteger(price.getText());
				else
					goodPrice = true;
				if(!time.getText().isEmpty())
					goodTime = numb.isInteger(time.getText());
				else
					goodTime = true;
				
				if(goodPrice) 
					setGood(price);
				else
					setMistake(price);
				
				if(goodTime)
					setGood(time);
				else
					setMistake(time);
				
				
				System.out.println("ID zabiegu: " + treat.getId());
				
				if(goodName && goodTime && goodPrice && isPressedKey) {
					currentSiteTreat = 1;
					MainWindow.save.setEnabled(true);
					MainWindow.save.setAnimation(1.0f);
					serviceData.updateTreat(new Treatment(number.getText(), nameTreat.getText(), time.getText(), price.getText(), treat.getId()));
					nameTreat.setText("");
					number.setText("");
					time.setText("");
					price.setText("");
					treatmentListPanel.removeAll();
					setTreatmentListPanel();
					treatmentListPanel.repaint();
					cl.show(mainPanel, treatmentListPanel.getName());
					
				}
				
			
				isPressedKey = false;
			}
		});
		
		
	}
	
	private void setMistake(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.mistakeWrite,
	   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(new Color(249,203,201));
		
	}
	
	private void setGood(JTextField textField) {
		
		textField.setBorder(notWrite);
		textField.setBackground(ConfigureApp.additionalFieldColor);
		
	}
	
	
	
	
	private void setSettingsMainPanel() {
		
		
		
		String s = "Ustawienia programu";
		
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((addPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		settingsMainPanel.add(title);
		
		JButton salonHours = new JButtonOwn("buttons/hourOpen");
		//setSettingsButton(salonHours, "Godziny otwarcia salonu");
		salonHours.setLocation(mainPanel.getWidth()/2 - 20 - salonHours.getWidth(),
				 			   (settingsMainPanel.getHeight() - title.getY() - title.getHeight())/2 - salonHours.getHeight()/2);
		settingsMainPanel.add(salonHours);
		salonHours.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				setSettingsPanel();
				cl.show(mainPanel, settingsPanel.getName());
				
				
			}
		});
		
		salonHours.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				setSettingsPanel();
				cl.show(mainPanel, settingsPanel.getName());
				
			}
			
		});
		
		JButton salonTreat = new JButtonOwn("buttons/treatment");
		//setSettingsButton(salonTreat, "Zabiegi");
		salonTreat.setLocation(mainPanel.getWidth()/2 + 20, salonHours.getY());
		settingsMainPanel.add(salonTreat);
		salonTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				treatmentListPanel.removeAll();
				setTreatmentListPanel();
				cl.show(mainPanel, treatmentListPanel.getName());
				treatmentListPanel.repaint();
			}
		});
		
		salonTreat.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				treatmentListPanel.removeAll();
				setTreatmentListPanel();
				cl.show(mainPanel, treatmentListPanel.getName());
				treatmentListPanel.repaint();
				
			}
			
		});
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5, settingsMainPanel.getHeight() - 5 - back.getHeight());
		settingsMainPanel.add(back);
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cl.show(mainPanel, buttonPanel.getName());
			}
		});
		
	}
	
	
	private void setSettingsButton(JButton button, String string) {
		
		button.setLayout(null);
		
		int size = 19;
		
		Font font = new Font("Arial", Font.PLAIN, size);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(string, null);
		
		
		
		StyledDocument document = new DefaultStyledDocument();
		Style defaultStyle = document.getStyle(StyleContext.DEFAULT_STYLE);
		StyleConstants.setAlignment(defaultStyle, StyleConstants.ALIGN_CENTER);
		
		
		
		JTextPane textArea = new JTextPane(document);
		textArea.setFont(font);
		textArea.setForeground(Color.white);
		//textArea.setBackground(Color.red);
		textArea.setOpaque(false);
		textArea.setBorder(null);
		//textArea.setLineWrap(true);
		//textArea.setWrapStyleWord(true);
		//textArea.setRows(4);
		textArea.setText(string);
		
		
		int rest = (int) bounds.getWidth()%(button.getWidth()-10);
		int height = (int) bounds.getWidth()/(button.getWidth()-10);
		if(rest!=0)
			height++;
		
		textArea.setSize(button.getWidth() - 10, (size+4)*height);
		textArea.setLocation(5, (button.getHeight()-textArea.getHeight())/2);
		textArea.setEditable(false);
		
		
		button.add(textArea);
		
		
	}
	
	private void setLabelFont(JLabel lab, String s) {
		
		FontMetrics metrics = new FontMetrics(etFontGrey) {};
		//Font font = new Font();
		
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setText(s);
		lab.setForeground(fontLabelGrey);
		lab.setFont(etFontGrey);
		lab.setSize((int) bounds.getWidth(),
					(int) bounds.getHeight());
		
	}
	
	private Boolean isEmptyAll() {
		
		Boolean isEmpty = true;
		
		if(!name.getText().isEmpty())
			return false;
		if(!surname.getText().isEmpty())
			return false;
		if(!code.getText().isEmpty())
			return false;
		return isEmpty;
		
	}
	
	private void repaintManagePanel() {
		
		managePanel.removeAll();
		cl.show(mainPanel, "managePanel");
		setManagePanel();
		middlePanel.removeAll();
		bottomPanel.removeAll();
		setBottomPanel();
		middlePanel.repaint();
		bottomPanel.repaint();
		managePanel.repaint();
		
	}
	
	private void setMainPanel() {
		
		cl = new CardLayout();
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setSize(size);
		mainPanel.setLocation(((int) size.getWidth() - mainPanel.getWidth())/2,
							  ((int) size.getHeight() - mainPanel.getHeight())/2);
		mainPanel.setLayout(cl);
		
		logPanel = new JPanel();
		logPanel.setBackground(Color.WHITE);
		logPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		logPanel.setName("logPanel");
		logPanel.setLayout(null);
		mainPanel.add(logPanel, logPanel.getName());
		
		helpPanel = new JPanel();
		helpPanel.setBackground(Color.WHITE);
		helpPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		helpPanel.setName("helpPanel");
		helpPanel.setLayout(null);
		mainPanel.add(helpPanel, helpPanel.getName());
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(null);
		buttonPanel.setBackground(Color.WHITE);
		buttonPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		buttonPanel.setName("buttonPanel");
		mainPanel.add(buttonPanel, buttonPanel.getName());
		
		
		managePanel = new JPanel();
		managePanel.setLayout(null);
		managePanel.setBackground(Color.WHITE);
		managePanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		managePanel.setName("managePanel");
		mainPanel.add(managePanel, managePanel.getName());
		
		addPanel = new JPanel();
		addPanel.setBackground(Color.WHITE);
		addPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		addPanel.setName("addPanel");
		addPanel.setLayout(null);
		mainPanel.add(addPanel, addPanel.getName());
		
		changePassPanel = new JPanel();
		changePassPanel.setBackground(Color.WHITE);
		changePassPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		changePassPanel.setName("changePassPanel");
		changePassPanel.setLayout(null);
		mainPanel.add(changePassPanel, changePassPanel.getName());
		
		settingsPanel = new JPanel();
		settingsPanel.setBackground(Color.WHITE);
		settingsPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		settingsPanel.setName("settingsPanel");
		settingsPanel.setLayout(new CardLayout());
		mainPanel.add(settingsPanel, settingsPanel.getName());
		
		treatmentListPanel = new JPanel();
		treatmentListPanel.setBackground(Color.WHITE);
		treatmentListPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		treatmentListPanel.setName("treatmentListPanel");
		treatmentListPanel.setLayout(null);
		mainPanel.add(treatmentListPanel, treatmentListPanel.getName());
		
		treatNewPanel = new JPanel();
		treatNewPanel.setBackground(Color.WHITE);
		treatNewPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		treatNewPanel.setName("treatNewPanel");
		treatNewPanel.setLayout(null);
		mainPanel.add(treatNewPanel, treatNewPanel.getName());
		
		settingsMainPanel = new JPanel();
		settingsMainPanel.setBackground(Color.WHITE);
		settingsMainPanel.setBounds(0,0,mainPanel.getWidth(), mainPanel.getHeight());
		settingsMainPanel.setName("settingsMainPanel");
		settingsMainPanel.setLayout(null);
		mainPanel.add(settingsMainPanel, settingsMainPanel.getName());
		
	}
	

	public JPanel getMainPanel() {
		
		return mainPanel;
		
	}

	
	
	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent textField = (JTextComponent) e.getComponent();
		textField.setBorder(ConfigureApp.setWrite);
		
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent tempField = (JTextComponent) e.getComponent();
		tempField.setBorder(notWrite);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
		isPressedKey = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
