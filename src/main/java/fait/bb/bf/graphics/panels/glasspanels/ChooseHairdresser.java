package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;


public class ChooseHairdresser  implements MouseListener {
	
	private Color background = new Color(227,227,227);
	private JPanel mainPanel;
	private JPanel cardPanel;
	
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JButton leftButton;
	private JButton rightButton;
	
	private int hairdresser = 4;
	private static int currentSite = 1;
	private int sites = 1;
	private List<User> listUsers = new ArrayList<User>();
	private List<JButton> listButton;
	
	private ServiceData serviceData;
	
	private int wButton = 50;
	private int hButton = 5;
	private int x;
	private int y;
	
	//czy trzeba zapisywa� receptur�
	private Boolean isSaved = false;
	
	private Dimension size;
	
	public ChooseHairdresser(Dimension size) {
		
		serviceData = new ServiceData();
		
		this.size = size;
		
		setMainPanel();
		
	}
	
	public void setUsers(Boolean isSaved) {
		
		listUsers.clear();
		listUsers = serviceData.getUsers(" AND status = 'AKTYWNY' ");
		
		this.isSaved = isSaved;
		
	}
	
	public void setMainPanel() {
		
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setSize(ConfigureApp.sizeOfDialogBack);
		mainPanel.setLocation((int) size.getWidth()/2 - mainPanel.getWidth()/2,
							  (int) size.getHeight()/2 - mainPanel.getHeight()/2);	
		
		
		setNavigation();
		
		cardPanel = new JPanel();
		cardPanel.setLayout(null);
		cardPanel.setOpaque(false);
		cardPanel.setSize(mainPanel.getWidth() - leftPanel.getWidth()*2,
						  mainPanel.getHeight());
		cardPanel.setLocation(leftPanel.getX() + leftPanel.getWidth(),
							  0);
		
		listUsers.clear();
		listUsers = serviceData.getUsers(" AND status = 'AKTYWNY' ");
		countXY();
		setCardPanel();
		
		mainPanel.add(cardPanel);
		
		
	}
	
	private void countXY() {
		
		x = 2;
		y = 2;
		
	}
	
	private void paintUser() {
		
		if((listUsers.size() - currentSite*hairdresser)> 0)
			setListButtons(listUsers.subList((currentSite-1)*hairdresser, currentSite*hairdresser));
		else
			setListButtons(listUsers.subList((currentSite-1)*hairdresser, listUsers.size()));
	}
	
	private void setCardPanel() {
		
		System.out.println("Liczba u�ytkownik�w: " + listUsers.size());
		
		if(listUsers.size()!=0) {
			
			
			currentSite=1;
			
			if(currentSite==1) 
				leftButton.setEnabled(false);
			else 
				leftButton.setEnabled(true);
				
			
		
			sites = listUsers.size()/hairdresser;
			if(listUsers.size()%hairdresser!=0)
				sites++;
			if(sites==1) 
				rightButton.setEnabled(false);
			else
				rightButton.setEnabled(true);
		
			paintUser(); 
			
		}
		
	}
	
	private JButton setUserButton(final User u) {
		
		final JButton temp = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/backgroundCardH.png");
		temp.setIcon(imgIc);
		imgIc = new ImageIcon("resources/images/backgroundCardH-2.png");
		temp.setRolloverIcon(imgIc);
		//imgIc = new ImageIcon("resources/images/addClient-3.png");
		temp.setPressedIcon(imgIc);
		//imgIc = new ImageIcon("resources/images/addClient-0.png");
		//temp.setDisabledIcon(imgIc);
		temp.setBorderPainted(false);
		temp.setContentAreaFilled(false);
		temp.setFocusPainted(false);
		temp.setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		temp.setLayout(null);
		
		JLabel backPhoto = new JLabel();
		imgIc = new ImageIcon("resources/images/backgroundPhoto.png");
		backPhoto.setIcon(imgIc);
		backPhoto.setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		backPhoto.setLocation((temp.getWidth()-3)/2 - (backPhoto.getWidth()-3)/2,
							   5);
		
		temp.add(backPhoto);
		
		JLabel photo = new JLabel();
		if(u.getAvatar()==null) {
			imgIc = new ImageIcon("resources/images/avatar.png");
		}
		else {
			imgIc = u.getAvatar();
		}
		Image img = imgIc.getImage();  
		Image newimg = img.getScaledInstance((int) (backPhoto.getWidth()*0.8), 
										 (int) (backPhoto.getHeight() * 0.8),  
										 java.awt.Image.SCALE_SMOOTH);  
		imgIc = new ImageIcon(newimg); 
		photo.setIcon(imgIc);
		photo.setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		photo.setLocation((backPhoto.getWidth()-5)/2 - (photo.getWidth())/2,
						  (backPhoto.getHeight()-10)/2 - (photo.getHeight())/2);
		
		
		backPhoto.add(photo);
		
		Font font = new Font("Arial", Font.PLAIN, 20);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds;
		
		String s = "";
		
		s = "[" + u.getCode() + "]";
		bounds = metrics.getStringBounds(s, null);
		JLabel code = new JLabel();
		code.setText(s);
		code.setFont(font);
		code.setForeground(new Color(101,101,101));
		code.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		code.setLocation((temp.getWidth()-3)/2 - code.getWidth()/2,
				  backPhoto.getY() + backPhoto.getHeight() + 5);
		
		temp.add(code);
		
		s = u.getName() + " " + u.getSurname();
		bounds = metrics.getStringBounds(s, null);
		JLabel name = new JLabel();
		name.setText(s);
		name.setFont(font);
		name.setForeground(new Color(101,101,101));
		name.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		name.setLocation((temp.getWidth()-3)/2 - name.getWidth()/2,
				  code.getY() + code.getHeight() + 1);
		temp.add(name);
		
		
		
		temp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
					
					String s = "[" +  u.getCode() + "] " + u.getName() + " " + u.getSurname();
					
					//ConfigureWindow.bar.setText(s);
					ConfigureApp.logUserCard = u.getCode();
					ConfigureApp.user = u;
					
					int i = MainWindow.tabbedPanel.getSelectedIndex();
					System.out.println("Indeks zak�adki: " + i + " oraz rozmiar listy: " + MainWindow.listPanels.size());
					if((i>=0) && (MainWindow.listPanels.size()>0) && (MainWindow.listPanels.get(i)!=null)) {
						
						MainWindow.listPanels.get(i).setHairdresser(s);
						/*if(isSaved)
							MainWindow.listPanels.get(i).setActionAddData();*/
							
						
					
					}
					
					MainWindow.mainGlassPanel.removePanel();
					MainWindow.mainGlassPanel.setVisible(false);
					MainWindow.pane.setVisible(false);
					/*if(isSaved)
						MainWindow.save.setAnimation(1.0f);*/
					
					/*int j = 0;
					
					if(CalendarPanelNew.tabUser!=null) {
						while(j<CalendarPanelNew.tabUser.length) {
							System.out.println("String tabuser: " + CalendarPanelNew.tabUser[j]);
							System.out.println("String S: " + s);
							if(CalendarPanelNew.tabUser[j].equals(s)) {
								CalendarPanelNew.comboBox.setSelectedIndex(j);
							}
							j++;
						}
					}*/
					
					
					
			}
		});
		
		return temp;
		
	}
	
	private void setListButtons(final List<User> tempList) {
		
		Iterator<User> it = tempList.iterator();
		User u;
		
		listButton = new ArrayList<JButton>();
		//int width_x = 100;
		//int height_y = 60;
		int i = 0;
		
		while(it.hasNext()) {
			
			u = it.next();
			JButton temp = setUserButton(u);

			if(listButton.isEmpty()) {
				temp.setLocation((cardPanel.getWidth() - temp.getWidth()*x - (x-1)*wButton)/2,
								  (cardPanel.getHeight() - temp.getHeight()*y - (y-1)*hButton)/2);	
			}
			else {
				int x_temp = listButton.get(i-1).getX() + listButton.get(i-1).getWidth() + 
						     wButton + temp.getWidth();
				if(x_temp>cardPanel.getWidth()) 
				   temp.setLocation(listButton.get(0).getX(),
							 		listButton.get(i-1).getY() + listButton.get(i-1).getHeight() + hButton);
				
				else
					temp.setLocation(x_temp - temp.getWidth(),
							 		 listButton.get(i-1).getY());
			}
			
			System.out.println("Dodano do cardPanel");
			System.out.println("Po�o�enie: " + temp.getBounds());
			cardPanel.add(temp);
			listButton.add(temp);
			i++;
			
		}
		
		
		
	}
	
	
	
	private void setNavigation() {
		
		leftPanel = new JPanel();
		leftPanel.setOpaque(false);
		leftPanel.setLayout(null);
		
		rightPanel = new JPanel();
		rightPanel.setOpaque(false);
		rightPanel.setLayout(null);
		
		leftButton = new JButtonOwn("buttons/left");
		leftButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent zdarz) {
				cardPanel.removeAll();
				
				currentSite--;
				if(currentSite==1)
					leftButton.setEnabled(false);
				else
					leftButton.setEnabled(true);
				if(currentSite==sites)
					rightButton.setEnabled(false);
				else
					rightButton.setEnabled(true);
				paintUser(); 
				cardPanel.repaint();
			}
		});
		rightButton = new JButtonOwn("buttons/right");
		rightButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent zdarz) {
				cardPanel.removeAll();
				
				currentSite++;
				if(currentSite==sites)
					rightButton.setEnabled(false);
				else
					rightButton.setEnabled(true);
				if(currentSite==1)
					leftButton.setEnabled(false);
				else
					leftButton.setEnabled(true);
				paintUser(); 
				cardPanel.repaint();
			}
		});
		
		leftPanel.setSize(leftButton.getWidth() + 30,
						  mainPanel.getHeight());
		leftPanel.setLocation(0,0);
		
		rightPanel.setSize(leftPanel.getSize());
		rightPanel.setLocation(mainPanel.getWidth() - rightPanel.getWidth(),
							   0);
		
		leftButton.setLocation(leftPanel.getWidth()/2 - leftButton.getWidth()/2,
							   leftPanel.getHeight()/2 - leftButton.getHeight()/2);
		rightButton.setLocation(rightPanel.getWidth()/2 - rightButton.getWidth()/2,
								rightPanel.getHeight()/2 - rightButton.getHeight()/2);
		
		leftPanel.add(leftButton);
		rightPanel.add(rightButton);
		mainPanel.add(leftPanel);
		mainPanel.add(rightPanel);
		
		
	}
	
	public JPanel getMainPanel() {
		
		return mainPanel;
		
	}
	

	@Override
	public void mouseClicked(final MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(final MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(final MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	
}
