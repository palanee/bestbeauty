package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Pages;
import fait.bb.bf.graphics.calendar.CalendarHourLabel;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.ButtonSite;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.JTextFieldBackground;
import fait.bb.bf.graphics.components.ListVisitView;
import fait.bb.bf.graphics.components.PagePanelUser;
import fait.bb.bf.graphics.components.PagesPanelVisit;
import fait.bb.bf.graphics.components.customUI.CalendarComboBoxUI;
import fait.bb.bf.graphics.components.customUI.ComboBoxRenderer;
import fait.bb.bf.graphics.panels.mainpanels.CardPanel;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Treatment;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;


public class AddDate extends JPanel implements MouseListener, FocusListener  {
	
	private ServiceData serviceData;
	public static int sizeTab;
	
	private JPanel mainPanel;
	private JLabel title;
	
	private Color background = new Color(227,227,227);
	private Color fontLabelColor = new Color(224,132,13);
	private Color fontLabelGrey = new Color(75,75,75);
	
	private CardLayout cl;
	
	private JPanel welcomePanel;
	private JButton lookVisit;
	private JButton addVisits;
	
	private JPanel calendarPanel;
	private JPanel schedulePanel;
	
	private JPanel choosePanel;
	private JTextFieldBackground findCustomerText;
	public static JComboBox comboBox;
	private String[] tabUser;
	private JLabel etFind;
	private JButtonOwn findButton;
	
	private JPanel hourPanel;
	private int rows = 4;
	private int columns = 3;
	private JLabel etHour;
	private int y_hour = 5;
	
	private JPanel additionalInfoPanel;
	private JLabel etBottom;
	private JTextArea textArea;
	
	private JPanel datePanel;
	private JComboBox comboTreat;
	private JLabel labCombo;
	private JLabel dayText;
	private JLabel dateText;
	private JLabel hourText;
	private JLabel userText;
	
	private JPanel bottomPanel;
	private JButton save; 
	
	private String kindCard = "";
	
	//private JSpinner spinnerDate;
	private JSpinner spinner;
	private Font font = ConfigureApp.font.deriveFont(Font.BOLD, 22);
	private Font etFont = ConfigureApp.font.deriveFont(Font.BOLD, 15);
	
	Color backField = new Color(247,246,244);
	Color fontFieldColor = new Color(83,84,84);	
	
	private Font etFontLog = ConfigureApp.font.deriveFont(Font.PLAIN, 18);
	private Font fontLog = new Font("Arial", Font.BOLD, 22);
	Color etFontLogColor = new Color(140,141,142);
	
	private Border notWrite = BorderFactory.createLineBorder(new Color(211,211,211));
	
	
	//Obiekty potrzebne do dodania terminu do bazy
	private Customer customer;
	public static Float HOUR;
	public static String hourString;
	public static List<CalendarHourLabel> listHours;
	public static List<Visit> listVisit = new ArrayList<Visit>();
	private DateOfDay dateOfday;
	
	//Elementy potrzebne do wyswietlenia listy wizyt
	private int numberOfVisit = 4;
	private JLabel subTitle;
	private List<ButtonSite> buttonsList;
	private static int currentSite = 1;
	private JPanel middlePanel;
	private int position;
	
	private Boolean isEdit=false;
	
	private Communication comm;
	
	private Visit visit;
	
	private JButton copyVisit;
	private JButton pasteVisit;
	private Boolean isPaste;
	
	private JPanel customerPanel;
	private JTextField name;
	private JTextField surname;
	private JTextField number;
	
	private String[] tabTreatment;
	private List<Treatment> listTreat;
	
	private List<Treatment> treats;
	private List<Treatment> indexes = new ArrayList<Treatment>();
	
	//listy potrzebne do przechowywania komponent�w zwi�zanych z dodawaniem zabieg�w
	private List<JComboBox> comboList;
	private List<JButton> plusButtonList;
	
	private JButton cancel;
	private int countKey = 0;
	private Date dateDate;
	
	private JButton addNewCustomer;
	
	
	public AddDate() {
		
		
		serviceData = new ServiceData();
		comm = new Communication(MainWindow.getFrame());
		tabUser = serviceData.parseToString(serviceData.getUsers(" AND status!='ZWOLNIONY' ")); 
		listTreat = serviceData.getTreatments();
		
		comboList = new ArrayList<JComboBox>();
		plusButtonList = new ArrayList<JButton>();
		treats = new ArrayList<Treatment>();
		
		tabTreatment = serviceData.getStringTreat();
		sizeTab = tabUser.length;
		visit = null;
		isPaste = false;
		
		setBackground(background);
		setLayout(null);
		setSize(ConfigureApp.sizeOfDialog);
		setLocation((ConfigureWindow.getMaxScreen().width - this.getWidth())/2,
				  	(ConfigureWindow.getMaxScreen().height - this.getHeight())/2);
		setDoubleBuffered(true);
		
		JButton close = new JButtonWithout("closeGlass");
		close.setLocation(getWidth() - close.getWidth() - 2, 2);
		add(close);
		close.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				MainWindow.pane.setVisible(false);
				MainWindow.addDate.setVisible(false);
			}
		});
		
		addMouseListener(this);
		
		setMainPanel();
		components();
		
	}
	
	public void updateVisit() {
		
		tabTreatment = serviceData.getStringTreat();
		listTreat = serviceData.getTreatments();
		datePanel.removeAll();
		setDatePanel();
		datePanel.repaint();
		
	}
	
	public void components() {
		
		
		welcomePanel();
		
		setPanels();
		setChoosePanel();
		setAdditionalInfoPanel();
		setDatePanel();
		//setHourPanel();
		setBottomPanel();
		setCustomerPanel();
		
	}
	
	private void welcomePanel() {
		
		welcomePanel = new JPanel();
		welcomePanel.setLayout(null);
		welcomePanel.setOpaque(false);
		welcomePanel.setSize(mainPanel.getSize());
		welcomePanel.setLocation(0,
							  	 0);	
		welcomePanel.setName("welcomePanel");
		mainPanel.add(welcomePanel, welcomePanel.getName());
		
		setTitle(welcomePanel, "Co chcesz zrobi�?");
		
		int width = 35;
		
		addVisits = new JButtonOwn("buttons/addDate");
		addVisits.setLocation(welcomePanel.getWidth()/2 - width - addVisits.getWidth(),
							 welcomePanel.getHeight()/2 - addVisits.getHeight()/2);
		
		addVisits.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
					cl.show(mainPanel, calendarPanel.getName());		
				
			}
		});
		
		
		welcomePanel.add(addVisits);
		
		lookVisit = new JButtonOwn("buttons/lookVisit");
		lookVisit.setLocation(addVisits.getX() + addVisits.getWidth() + width*2,
							  addVisits.getY());
		
		lookVisit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
					
					
					setSchedulePanel();
					cl.show(mainPanel, schedulePanel.getName());	
					
				
			}
		});
		
		welcomePanel.add(lookVisit);
		
		
		
	}
	
	
	private void setCustomerPanel() {
		
		int y = 15;
		int x = 20;
		
		customerPanel = new JPanel();
		customerPanel.setLayout(null);
		customerPanel.setOpaque(false);
		customerPanel.setSize(mainPanel.getSize());
		customerPanel.setLocation(0,
							  	 0);	
		customerPanel.setName("customerPanel");
		mainPanel.add(customerPanel, customerPanel.getName());
		
		
		
		setTitle(customerPanel, "Dodaj nowego klienta");
		
		
		name = new JTextField();
		setTextField(name, new Dimension(210,40));
		name.setName("name");
		name.setLocation(customerPanel.getWidth()/2 - 15 - name.getWidth(),
						 title.getY() + title.getHeight() + 50);
		
		JLabel etName = new JLabel();
		setLabelField(etName, "Imi�");
		etName.setLocation(name.getX() - x - etName.getWidth(),
						   name.getY() + (name.getHeight()-etName.getHeight())/2);
		
		surname = new JTextField();
		setTextField(surname, new Dimension(210,40));
		surname.setName("surname");
		surname.setLocation(name.getX(),
						 name.getY() + name.getHeight() + y);
		
		JLabel etSurname = new JLabel();
		setLabelField(etSurname, "Nazwisko");
		etSurname.setLocation(surname.getX() - x - etSurname.getWidth(),
							  surname.getY() + (surname.getHeight()-etSurname.getHeight())/2);
		
		number = new JTextField();
		number.setName("number");
		setTextField(number, new Dimension(210,40));
		number.setLocation(surname.getX(),
						 surname.getY() + surname.getHeight() + y);
		
		JLabel etCode = new JLabel();
		setLabelField(etCode, "Numer telefonu");
		etCode.setLocation(number.getX() - x - etCode.getWidth(),
						   number.getY() + (surname.getHeight()-etCode.getHeight())/2);
		
		JButton cancel = new JButtonOwn("but/red", "Anuluj dodawanie");
		cancel.setLocation(findButton.getX() + findButton.getWidth() - cancel.getWidth(),
						   customerPanel.getHeight() - 5 - cancel.getHeight());
		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
					name.setText("");
					surname.setText("");
					number.setText("");
					cl.show(mainPanel, calendarPanel.getName());
					//clear();
				
			}
		});
		
		
		
		JButton saveCustomer = new JButtonOwn("but/blue", "Zapisz");
		saveCustomer.setLocation(cancel.getX() - 15 - saveCustomer.getWidth(),
						 		  cancel.getY());
		saveCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				//je�li true - prawid�owy; false - nieprawid�owy
					Boolean goodNumber = checkNumber(number.getText());
					Boolean goodName = false;
					Boolean goodSurname = false;
					
					if(!name.getText().isEmpty()) {
						goodName = true;
						setGood(name);
					}
					else
						setMistake(name);
					
					if(!surname.getText().isEmpty()) {
						goodSurname = true;
						setGood(surname);
					}
					else
						setMistake(surname);
					
				
					if(goodName && goodSurname && goodNumber) {
						MainWindow.save.setEnabled(true);
						MainWindow.save.setAnimation(1.0f);
						customer = serviceData.addCustomer(name.getText(), surname.getText(), number.getText(), "", "", "");
						findCustomerText.setText(customer.getCustomerMenu());
						name.setText("");
						surname.setText("");
						number.setText("");
						
						cl.show(mainPanel, calendarPanel.getName());
						save.setEnabled(true);
					}
					
				
				
			}
		});
		
		
		
		
		
		customerPanel.add(name);
		customerPanel.add(surname);
		customerPanel.add(number);
		customerPanel.add(etName);
		customerPanel.add(etSurname);
		customerPanel.add(etCode);
		customerPanel.add(cancel);
		customerPanel.add(saveCustomer);
		
		
	}
	
	private void setMistake(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.mistakeWrite,
	   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(new Color(249,203,201));
		
	}
	
	private void setGood(JTextField textField) {
		
		textField.setBorder(notWrite);
		textField.setBackground(ConfigureApp.additionalFieldColor);
		
	}
	
	private Boolean checkNumber(String text) {
		
		Boolean incorrect = serviceData.isGoodNumber(text);
		
		if(!incorrect)
			setMistake(number);
		else 
			setGood(number);
		return incorrect;
	}
	
	private void setTextField(JTextField pole, Dimension size) {
		
		pole.setBackground(backField);	
		pole.setForeground(fontFieldColor);
		pole.setFont(fontLog);
		pole.setBorder(notWrite);
		pole.setSize((int) size.getWidth(), (int) size.getHeight());
		pole.setFocusable(true);
		pole.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				
				JTextComponent tempField = (JTextComponent) e.getComponent();
				tempField.setBorder(notWrite);
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				
				JTextComponent textField = (JTextComponent) e.getComponent();
				textField.setBorder(ConfigureApp.setWrite);
				
				
			
			
			
			}
				
		});
		
	}
	
	private void setLabelField(JLabel lab, String s) {
		
		
		FontMetrics metrics = new FontMetrics(etFontLog) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		lab.setText(s);
		lab.setFont(etFontLog);
		lab.setForeground(etFontLogColor);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		
	}
	
	private void setPanels() {
				
	
		
		calendarPanel = new JPanel();
		calendarPanel.setLayout(null);
		calendarPanel.setOpaque(false);
		calendarPanel.setSize(mainPanel.getSize());
		calendarPanel.setLocation(0,
							  	 0);	
		calendarPanel.setName("calendarPanel");
		
		mainPanel.add(calendarPanel, calendarPanel.getName());
		
		setTitle(calendarPanel, "Dodaj termin do terminarza");
		
		choosePanel = new JPanel();
		choosePanel.setLayout(null);
		choosePanel.setOpaque(false);
		//choosePanel.setBackground(Color.RED);
		choosePanel.setSize(mainPanel.getWidth(),
							70);
		choosePanel.setLocation(0,
								title.getY() + title.getHeight() + 30);
		calendarPanel.add(choosePanel);
		
		additionalInfoPanel = new JPanel();
		additionalInfoPanel.setLayout(null);
		additionalInfoPanel.setOpaque(false);
		additionalInfoPanel.setSize(mainPanel.getWidth(),200);
		additionalInfoPanel.setDoubleBuffered(true);
		additionalInfoPanel.setLocation(0,
										choosePanel.getY() + choosePanel.getHeight());
		calendarPanel.add(additionalInfoPanel);
		
		datePanel = new JPanel();
		datePanel.setLayout(null);
		//datePanel.setBackground(Color.yellow);
		datePanel.setOpaque(false);
		datePanel.setSize(mainPanel.getWidth(),100);
		datePanel.setLocation(0,
							  additionalInfoPanel.getY() + additionalInfoPanel.getHeight());
		calendarPanel.add(datePanel);
		
		bottomPanel = new JPanel();
		bottomPanel.setLayout(null);
		bottomPanel.setOpaque(false);
		//bottomPanel.setBackground(Color.BLUE);
		bottomPanel.setSize(mainPanel.getWidth(),
							mainPanel.getHeight() - datePanel.getY() - datePanel.getHeight());
		bottomPanel.setLocation(0,
								datePanel.getY() + datePanel.getHeight());
		calendarPanel.add(bottomPanel);
		
		
	}
	
	private void setMainPanel() {
		
		cl = new CardLayout();
		
		mainPanel = new JPanel();
		mainPanel.setLayout(cl);
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setSize(950,535);
		mainPanel.setLocation(getWidth()/2 - mainPanel.getWidth()/2,
							  getHeight()/2 - mainPanel.getHeight()/2);	
		add(mainPanel);
	}
	
	private void setTitle(JComponent panel, String string) {
		
		FontMetrics metrics = new FontMetrics(font) {};
		
		String s = string;
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		title = new JLabel(s);
		title.setForeground(fontLabelColor);
		title.setFont(font);
		title.setSize((int) bounds.getWidth(),
					  (int) bounds.getHeight());
		title.setLocation(mainPanel.getWidth()/2 - title.getWidth()/2,
						  20);
		
		panel.add(title);
		
	}
	
	private void setSchedulePanel() {
		
		schedulePanel = new JPanel();
		schedulePanel.setLayout(null);
		schedulePanel.setOpaque(false);
		schedulePanel.setSize(mainPanel.getSize());
		schedulePanel.setLocation(0,
							  	 0);	
		schedulePanel.setName("schedulePanel");
		mainPanel.add(schedulePanel, schedulePanel.getName());
		
		String s="";
		
		if(kindCard.equals("customer"))
			s = "Zaplanowane wizyty dla klientki: ";
		else
			s = "Zaplanowane wizyty dla fryzjera: ";
		
		setTitle(schedulePanel, s);
		
		
		FontMetrics metrics = new FontMetrics(font) {};	
		
		if(kindCard.equals("customer"))
			s = customer.getName() + " " + customer.getSurname();
		else
			s = userText.getText();
		
		
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		subTitle = new JLabel(s);
		subTitle.setForeground(fontLabelColor);
		subTitle.setFont(font);
		subTitle.setSize((int) bounds.getWidth(),
					     (int) bounds.getHeight());
		subTitle.setLocation(mainPanel.getWidth()/2 - subTitle.getWidth()/2,
						     title.getY() + title.getHeight() + 2);
		
		
		schedulePanel.add(subTitle);
		
		middlePanel = new JPanel();
		middlePanel.setOpaque(false);
		middlePanel.setLayout(null);
		middlePanel.setSize(mainPanel.getWidth(),
				            schedulePanel.getHeight() - subTitle.getY() - subTitle.getHeight() - 10 - 55);
		middlePanel.setLocation(0,
								subTitle.getY() + subTitle.getHeight() + 10);
		schedulePanel.add(middlePanel);
		
		JButton back = new JButtonOwn("but/green", "Powr�t");
		back.setLocation(5, 5);
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent evt) {
				
				 schedulePanel.removeAll();
				 cl.show(mainPanel, welcomePanel.getName());
				
			}
		});
		
		bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		bottomPanel.setLayout(null);
		bottomPanel.setSize(mainPanel.getWidth(),
				            back.getHeight() + 10);
		bottomPanel.setLocation(0,
								middlePanel.getY() + middlePanel.getHeight());
		schedulePanel.add(bottomPanel);
		
		setListVisit();
		
		
		
		bottomPanel.add(back);
		
	}
	

	
	public JPanel getSchedulePanel() {
		return schedulePanel;
	}
	
	public void refreshSchedulePanel() {
		
		listVisit = serviceData.getListVisit(userText.getText(), dateDate);
		
		schedulePanel.removeAll();
		setSchedulePanel();
		schedulePanel.repaint();
		cl.show(mainPanel, schedulePanel.getName());
		
	}
	
	public void editVisit(ListVisitView lab) {
		
		schedulePanel.removeAll();
		isEdit = true;
		setFields(lab.getVisit(), userText.getText());
		//setFields(findUser(lab.getVisit().getUser()), lab.getVisit().getCustomer(), lab.getVisit().getDateToString(), lab.getVisit().getHour());
		cl.show(mainPanel, calendarPanel.getName());
		
	}
	
	public void refreshAndBackSchedule() {
		
		schedulePanel.removeAll();
		cl.show(mainPanel, welcomePanel.getName());
		lookVisit.setEnabled(false);
	}
	
	
	
	
	
	
	
	private void setListVisit() {
		
		//ilo�� u�ytkownik�w na stron�
		
		//listVisit.clear();
		if(kindCard.equals("customer")) {
			listVisit.clear();
			listVisit = serviceData.getListVisit(customer.getId());
		}
		/*else
			listVisit = serviceData.getListVisit(userText.getText(), dateOfday.getDateDate(), hourText.getText()); */
		
		
		
		if(listVisit.size()==0) {
			
			cl.show(mainPanel, welcomePanel.getName());
		}
		else {
			
			Pages<Visit> pages = new Pages<Visit>(listVisit, numberOfVisit, bottomPanel);
			pages.setPagePanel(new PagesPanelVisit(middlePanel, this, kindCard, userText.getText()));
			pages.setPages();
			
		}
		
		
	}
	
	
	
	private void setActionSearch() {
		
		
		
		List<Customer> listCustomer = new ArrayList<Customer>();
		listCustomer = serviceData.searchCustomer(findCustomerText.getText(), 10);
		
		if(listCustomer.size()>0) {
			final JPopupMenu popMenu = new JPopupMenu();
			popMenu.setBorderPainted(false);
			popMenu.setBorder(null);
			
			
			//JMenuItem menuItem;
			
			Iterator<Customer> it = listCustomer.iterator();
			popMenu.setPopupSize(findCustomerText.getWidth(), 25 * listCustomer.size());
			
			while(it.hasNext()) {
				
				final Customer c = it.next();
				JMenuItem menuItem = new JMenuItem(c.getCustomerMenu());
				//menuItem.setUI(new PopMenuItemUI());
				menuItem.setFont(new Font("Arial", Font.BOLD, 14));
				menuItem.setForeground(new Color(224,132,13));
				menuItem.setBackground(ConfigureApp.backgroundCardColor);
				menuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						
						Boolean isGood = true;
						
						for(Visit v : listVisit) {
							if(c.getId()==v.getCustomerID()) {
								comm.dont("Ju� istnieje wizyta dla tego klienta o tej godzinie");
								isGood = false;
							}
						}
						
						if(isGood) {
							customer = c;
							findCustomerText.setText(c.getSurname() + " " + c.getName() + ", " + c.getNumber());
							//findCustomerText.setEnabled(false);
							System.out.println(customer);
							save.setEnabled(true);
							//popMenu.hide();
							additionalInfoPanel.repaint();
							datePanel.repaint();
							bottomPanel.repaint();
						}
						
						
						
					}
				});

				popMenu.add(menuItem);
				
				
			}
			
			popMenu.show(choosePanel, findCustomerText.getX(), findCustomerText.getY() + findCustomerText.getHeight());
		}
		
		else {
			
			findCustomerText.setText("Brak wynik�w wyszukiwania");
			
		}
		
		
		
	}
	
	private void setChoosePanel() {
		
		FontMetrics metrics = new FontMetrics(etFont) {};
		int space = 10;
		int space_y = 5;
		int width = space;
		
		String s = "Wybierz klienta";
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		etFind = new JLabel(s);
		etFind.setForeground(fontLabelGrey);
		etFind.setFont(etFont);
		etFind.setSize((int) bounds.getWidth(),
					  (int) bounds.getHeight());
		
		ImageIcon imgIc = new ImageIcon("resources/images/backJTextField.png");
		findCustomerText = new JTextFieldBackground(imgIc,10);
		findCustomerText.setFocusable(true);
		findCustomerText.setLayout(null);
		findCustomerText.setFont(new Font("Arial", Font.PLAIN, 14));
		findCustomerText.setForeground(fontLabelGrey);
		findCustomerText.setSize(imgIc.getIconWidth(),
								 imgIc.getIconHeight());
		
		
		
		
		findButton = new JButtonOwn("but/blue", "Szukaj");
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				setActionSearch();			
				
			}
		});
		
		findButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionSearch();
				}
				
			}			
		});
		
		findCustomerText.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				
				if(e.getKeyCode()==KeyEvent.VK_BACK_SPACE && countKey>0)
					countKey--;
				else
					countKey++;
				if(countKey>=3) {
					setActionSearch();
				}
				
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionSearch();
					countKey = 0;
				}
				
			}			
		});


		width += findCustomerText.getWidth() + findButton.getWidth();
		
		etFind.setLocation(choosePanel.getWidth()/2 - width/2,
						   3);

		findCustomerText.setLocation(etFind.getX(),
									 etFind.getY() + etFind.getHeight() + space_y);
		findButton.setLocation(findCustomerText.getX() + findCustomerText.getWidth() + space,
							   findCustomerText.getY() + findCustomerText.getHeight()/2 - (findButton.getHeight()-4)/2);
		
		addNewCustomer = new JButtonWithout("addCustomer");
		addNewCustomer.setLocation(findCustomerText.getX() - 5 - addNewCustomer.getWidth(),
								   findCustomerText.getY());
		addNewCustomer.setToolTipText("Dodaj nowego klienta do bazy");
		addNewCustomer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				
				cl.show(mainPanel, "customerPanel");
				
			}
		});
		
		
		choosePanel.add(etFind);
		choosePanel.add(findCustomerText);
		choosePanel.add(findButton);
		choosePanel.add(addNewCustomer);
		
		
		
		
	}
	
	private void setAdditionalInfoPanel() {
		
		FontMetrics metrics = new FontMetrics(etFont) {};
		
		String s = "Informacje dotycz�ce wizyty:";
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		etBottom = new JLabel(s);
		etBottom.setForeground(fontLabelGrey);
		etBottom.setFont(etFont);
		etBottom.setSize((int) bounds.getWidth(),
					     (int) bounds.getHeight());
		etBottom.setLocation(etFind.getX(),
						     10);
		
		additionalInfoPanel.add(etBottom);
		
		textArea = new JTextArea();
		textArea.setBackground(new Color(212,212,212));
		textArea.setForeground(fontLabelGrey);
		textArea.setFont(new Font("Arial", Font.PLAIN, 13));
		textArea.setFocusable(true);
		textArea.setBorder(new EmptyBorder(4,4,4,4));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSize(263,99);
		textArea.setDoubleBuffered(true);
		textArea.setLocation(etBottom.getX(),
							 etBottom.getY() + etBottom.getHeight() + 10);
		
		additionalInfoPanel.add(textArea);
		
		Font font = new Font("Arial", Font.PLAIN, 20);
		
		userText = new JLabel("");
		userText.setForeground(fontLabelGrey);
		userText.setFont(font);
		userText.setSize(additionalInfoPanel.getWidth() - textArea.getWidth() - 10,
						 etBottom.getHeight() + 12);
		userText.setLocation(textArea.getX() + textArea.getWidth() + 10,
							 textArea.getY());
		additionalInfoPanel.add(userText);
		
		dateText = new JLabel("");
		dateText.setForeground(fontLabelGrey);
		dateText.setFont(font);
		dateText.setSize(userText.getWidth(),
						 userText.getHeight());
		dateText.setLocation(userText.getX(),
							 userText.getY() + userText.getHeight() + 15);
		additionalInfoPanel.add(dateText);
		
		dayText = new JLabel("");
		dayText.setForeground(fontLabelGrey);
		dayText.setFont(font);
		dayText.setSize(userText.getWidth()/2,
					    userText.getHeight());
		dayText.setLocation(userText.getX(),
						  	dateText.getY() + dateText.getHeight());
		additionalInfoPanel.add(dayText);
		
		hourText = new JLabel("");
		hourText.setForeground(fontLabelGrey);
		hourText.setFont(font);
		hourText.setSize(userText.getWidth()/2,
						 userText.getHeight());
		hourText.setLocation(dayText.getX() + dayText.getWidth(),
							 dayText.getY());
		additionalInfoPanel.add(hourText);
		
		copyVisit = new JButtonWithout("copy");
		copyVisit.setLocation(textArea.getX(), 
						      textArea.getY() + textArea.getHeight() + 10);
		copyVisit.setToolTipText("Skopiuj wizyt�");
		copyVisit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				comm.dont("Wizyt� skopiowano do schowka");
				ConfigureApp.setBufforVisit(visit);
				System.out.println("!!!!!!!!!!Zabiegi: " + visit.getNumber());
				bottomPanel.remove(cancel);
				copyVisit.setVisible(false);
				setCancel("but/green", "Zamknij");
				save.setEnabled(false);
				
				
			}
		});
		
		additionalInfoPanel.add(copyVisit);
		
		if(this.visit==null)
			copyVisit.setVisible(false);
		
		pasteVisit = new JButtonWithout("paste");
		pasteVisit.setLocation(copyVisit.getX() + copyVisit.getWidth() + 5, 
							   copyVisit.getY());
		pasteVisit.setToolTipText("Wklej wizyt�");
		pasteVisit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				System.out.println("!!!!!!!Zabiegi: " + ConfigureApp.getBufforVisit().getNumber());
				setTreat(ConfigureApp.getBufforVisit());
				findCustomerText.setText(ConfigureApp.getBufforVisit().getCustomer().getCustomerMenu());
				textArea.setText(ConfigureApp.getBufforVisit().getText());
				save.setEnabled(true);
				pasteVisit.setVisible(false);
				isPaste = true;
				
				
			}
		});
		
		additionalInfoPanel.add(pasteVisit);
		
		if(ConfigureApp.getBufforVisit()==null)
			pasteVisit.setVisible(false);
		
	}
	
	private void setComboTreat() {
		
		comboList.clear();
		
		comboTreat = new JComboBox(tabTreatment);
		//comboBox.setUI((ComboBoxUI) CalendarComboBoxUI.createUI(comboBox));
		comboTreat.setRenderer(new ComboBoxRenderer(0));
		comboTreat.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 12));
		comboTreat.setForeground(new Color(119,42,115));
		comboTreat.setBackground(ConfigureApp.backgroundColor);
		if(tabTreatment.length>0)
			comboTreat.setSelectedIndex(0);
		comboTreat.setSize(200, 25);
		comboTreat.setLocation(textArea.getX(), labCombo.getY() + labCombo.getHeight() + 5);
		
		comboList.add(comboTreat);
		
		treats.add(listTreat.get(0));
		
		comboTreat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				//System.out.println("comboList: " + comboList.indexOf(comboTreat));
				//System.out.println("listTreat: " + listTreat.get(comboTreat.getSelectedIndex()));
				
				if(treats.size()>0)
					treats.set(0, listTreat.get(comboTreat.getSelectedIndex()));
				
				//treats = listTreat.get(comboTreat.getSelectedIndex());
				
				addPlus();
				
			}
		});
		
	}
	
	private void setDatePanel() {
		
		FontMetrics metrics = new FontMetrics(etFont) {};
		
		String s = "Przewidywany czas trwania wizyty (mo�esz zostawi� puste)";
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		labCombo = new JLabel(s);
		labCombo.setForeground(fontLabelGrey);
		labCombo.setFont(etFont);
		labCombo.setSize((int) bounds.getWidth(),
					(int) bounds.getHeight());
		labCombo.setLocation(etBottom.getX(),
						     0);
		
		datePanel.add(labCombo);
		
		
		
		setComboTreat();
		
		
		datePanel.add(comboTreat);
		
		
	}
	
	private void addComboTreat() {
		
		JButton last = plusButtonList.get(plusButtonList.size()-1);
		
		final JComboBox combo = new JComboBox(tabTreatment);
		//combo.setUI((ComboBoxUI) CalendarComboBoxUI.createUI(combo));
		combo.setRenderer(new ComboBoxRenderer(0));
		combo.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 12));
		combo.setForeground(new Color(119,42,115));
		combo.setBackground(ConfigureApp.backgroundColor);
		if(tabTreatment.length>0)
			combo.setSelectedIndex(0);
		combo.setSize(200, 25);
		
		int up = comboTreat.getY();
		int down = comboTreat.getY() + comboTreat.getHeight() + 10;
		
		if(comboList.size()==3)
			combo.setLocation(comboTreat.getX(), down);	
		else if(comboList.size()<3)
			combo.setLocation(last.getX() + last.getWidth() + 10, up);
		else
			combo.setLocation(last.getX() + last.getWidth() + 10, down);
		
		combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				setActionCombo(combo);
			}
		});
		
		
		
		datePanel.add(combo);
		datePanel.repaint();
		//datePanel.revalidate();
		
		comboList.add(combo);
	}
	
	private void addComboTreat(int i) {
		
		JButton last = plusButtonList.get(plusButtonList.size()-1);
		
		final JComboBox combo = new JComboBox(tabTreatment);
		//comboBox.setUI((ComboBoxUI) CalendarComboBoxUI.createUI(comboBox));
		combo.setRenderer(new ComboBoxRenderer(0));
		combo.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 12));
		combo.setForeground(new Color(119,42,115));
		combo.setBackground(ConfigureApp.backgroundColor);
		if(tabTreatment.length>0)
			combo.setSelectedIndex(i);
		combo.setSize(200, 25);
		
		int up = comboTreat.getY();
		int down = comboTreat.getY() + comboTreat.getHeight() + 10;
		
		if(comboList.size()==3)
			combo.setLocation(comboTreat.getX(), down);	
		else if(comboList.size()<3)
			combo.setLocation(last.getX() + last.getWidth() + 10, up);
		else
			combo.setLocation(last.getX() + last.getWidth() + 10, down);
		
		
		combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				setActionCombo(combo);
			}
		});
		
		
		
		datePanel.add(combo);
		datePanel.repaint();
		
		comboList.add(combo);
	}
	
	private void setActionCombo(JComboBox combo) {
		
		
		if(combo.getSelectedIndex()==0 && comboList.size()>1) {
			 
			
			indexes.add(treats.get(comboList.indexOf(combo)));	
		}
		
		else {
			if(treats.size()==comboList.size()) {					
				treats.set(comboList.indexOf(combo), listTreat.get(combo.getSelectedIndex()));	
			}
			else {
				treats.add(listTreat.get(combo.getSelectedIndex()));
			}
		}
		
		
		
		if(plusButtonList.size()<5)
			addPlus();
		datePanel.repaint();
		
	}
	
	private void addPlus() {
		
		JComboBox last = comboList.get(comboList.size()-1);
		
		JButton but = new JButtonWithout("addTreat");
		but.setLocation(last.getX() + last.getWidth() + 10, last.getY() + (last.getHeight() - but.getHeight())/2);
		but.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				addComboTreat();
				
			}
		});
		
		datePanel.add(but);
		
		plusButtonList.add(but);
		
		datePanel.repaint();
		
	}
	
	private void setHourPanel() {
		
		FontMetrics metrics = new FontMetrics(etFont) {};
		
		String s = "Wybierz godzin�:";
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		etHour = new JLabel(s);
		etHour.setForeground(fontLabelGrey);
		etHour.setFont(etFont);
		etHour.setSize((int) bounds.getWidth(),
					  (int) bounds.getHeight());
		etHour.setLocation(etFind.getX(),
						   5);
		
		hourPanel.add(etHour);
		
		
		
		
	}
	
	private void setRows(float start) {
		
		float temp = start;
		
		for(int col = 0; col<columns; col++) {
			temp = start + col*rows;
			setColumn(col, temp);
		}
		
	}
	
	private int getCount(float labH) {
		
		Iterator<Visit> it = listVisit.iterator();
		int count = 0;
		
		
		
		while(it.hasNext()) {
			
			
			Visit vis = it.next();
			
			
			if(vis.getHour()==labH) {
				count++;

			}
			
		}
		
		return count;
	}
	
	
	private void setColumn(int col, float start) {
		
		y_hour = etHour.getY() + etHour.getHeight() + 8;
		float h, labH;
		int count = 0;
		int size = 0;
		
		if(listVisit!=null)
			size = listVisit.size();
		else
			size = 0;
		
		for(int j = 0; j<rows; j++) {
			
			labH = start + j;
			
			if(size>0) 
				count = getCount(labH);
			else
				count = 0;
			
			
			CalendarHourLabel lab = new CalendarHourLabel(labH, ConfigureApp.getColor(count), count, hourPanel);
			
			lab.setLocation(etHour.getX() + col*lab.getWidth(),
							y_hour + j*lab.getHeight());
			listHours.add(lab);
			///System.out.println("Szeroko�� labHour: " + lab.getWidth());
			hourPanel.add(lab);
			
			
		}
		
	}
	
	private void setCancel(String name, String lab) {
		
		
		cancel = new JButtonOwn(name, lab);
		cancel.setLocation(findButton.getX() + findButton.getWidth() - cancel.getWidth(),
				   bottomPanel.getHeight() - 5 - cancel.getHeight());
		cancel.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent arg0) {
		
				MainWindow.pane.setVisible(false);
				exit();
				
				treats.clear();
				
				treats.add(listTreat.get(0));
				plusButtonList.clear();
				comboList.clear();
				comboList.add(comboTreat);
				//clear();
		
			}
		});

		bottomPanel.add(cancel);
		bottomPanel.repaint();
	}
	
	private void setBottomPanel() {
		
		setCancel("but/red", "Anuluj");
		
		
		
		
		save = new JButtonOwn("but/blue", "Zapisz");
		save.setLocation(cancel.getX() - 15 - save.getWidth(),
						 cancel.getY());
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				Date date = dateOfday.getDateDate();
				//String date = ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().getText();
				
				//Iterator<Treatment> it = indexes.iterator();
				
				int i = 0;
				
				while(i<indexes.size()) {
					
					
					Treatment t = indexes.get(i);
					System.out.println("Usuwane z treats: " + t);
					
					treats.remove(t);
					
					i++;
				}
				
				System.out.println("Treats: " + treats);
				
				indexes.clear();
				
				if(ConfigureApp.getBufforVisit()!=null && isPaste) {
					
					isPaste = false;
					visit = serviceData.updateVisit(ConfigureApp.getBufforVisit(), ConfigureApp.getBufforVisit().getCustomerID(), 
											hourText.getText(), date, 
											textArea.getText(), userText.getText(), treats);
					ConfigureApp.setBufforVisit(null);
					
				}
				else if(visit==null && !isPaste) {
					
					System.out.println("Dodajemy wizyt� gdy Visit = null oraz isPaste = false");
					System.out.println("Customer: " + customer);
					System.out.println("HourText: " + hourText.getText());
					System.out.println("User: " + userText.getText());
					System.out.println("Treats: " + treats);
					visit = serviceData.addDate(customer, hourText.getText(), date, textArea.getText(), userText.getText(), treats);
					
				}
				else if(visit!=null && !isPaste) {
					
					visit = serviceData.updateVisit(visit, customer.getId(), hourText.getText(), date, 
											textArea.getText(), userText.getText(), treats);
					
				}
				
				
				if(ConfigureApp.getBufforCustomer()!=null)
					ConfigureApp.setBufforCustomer(null);
				
				if(ConfigureApp.activeTab.equals(CalendarPanelNew.getNameTab())) {
					
					
					CalendarPanelNew.repaintCalendarPanel();
					
					updateClientCard(customer, visit);
					

				}
				
				
				
				/*if(calUser.equals(user)) {
					
					
				}*/
				
				MainWindow.pane.setVisible(false);
				exit();
				MainWindow.save.setAnimation(1.0f);
				
				treats.clear();
				customer = null;
				visit = null;
				
				treats.add(listTreat.get(0));
				plusButtonList.clear();
				comboList.clear();
				comboList.add(comboTreat);
				
				//clear();
				
			}
		});
		
		if(!isTextFull())
			save.setEnabled(false);
		
		bottomPanel.add(save);
		
		
	}
	
	private void updateClientCard(Customer c, Visit visit) {
		
		int size = MainWindow.tabbedPanel.getTabCount();
		int i=0;
		
		while(i<size) {
			if(MainWindow.tabbedPanel.getTitleAt(i).equals(c.getSurname() + " " + c.getName())) {
				//isThere = true;
				//MainWindow.tabbedPanel.setSelectedIndex(i);
				System.out.println("Znaleziona karta klienta...");
				MainWindow.listPanels.get(i).updateCard(visit);
				
				break;
				
			}
				
			i++;
		}
		
		
	}
	
	public Boolean isTextFull() {
		
		
		if(findCustomerText.getText().equals("Brak wynik�w wyszukiwania") || findCustomerText.getText().isEmpty())
			return false;
		else
			return true;
		
	}
	
	/*private void clear() {
		
		//MainWindow.addDate.removeAll();
		//MainWindow.addDate.components();
		//MainWindow.addDate.repaint();
		Iterator<CalendarHourLabel> it = listHours.iterator();
		int i = 0;v
		while(it.hasNext()) {
			CalendarHourLabel temp = it.next();
			if(temp.getIsClicked())
				listHours.get(i).setNotClicked();
			
			i++;
		}
		
	}
	*/
	public void setHour(float hour) {
		
		Iterator<CalendarHourLabel> it = listHours.iterator();
		while(it.hasNext()) {
			CalendarHourLabel temp = it.next();
			if(temp.getHour().equals(hour)) {
				int index = listHours.indexOf(temp);
				System.out.println("Sprawd�my index: " + index);
				listHours.get(index).setClicked(true, isEdit);
				isEdit=false;
			}
		}
		
		
		
	}
	
	public void setHourFromString(String hour) {
		
		String[] tempHour = hour.split(":");
		float h = Float.parseFloat(tempHour[0]);
		
		Iterator<CalendarHourLabel> it = listHours.iterator();
		while(it.hasNext()) {
			CalendarHourLabel temp = it.next();
			if(temp.getHour().equals(h)) {
				int index = listHours.indexOf(temp);
				System.out.println("Sprawd�my index: " + index);
				listHours.get(index).setClicked(true, isEdit);
				isEdit = false;
			}
		}
		
		
		
	}
	
	public void setFields(Date dateDate, String hour, String user, List<Visit> visits) {
		
		System.out.println("Generujemy AddDate");
		visit = null;
		
		if(ConfigureApp.getBufforVisit()!=null)
			pasteVisit.setVisible(true);
		
		copyVisit.setVisible(false);
		
		if(ConfigureApp.getBufforCustomer()!=null) {
			save.setEnabled(true);
			findCustomerText.setText(ConfigureApp.getBufforCustomer().getCustomerMenu());
			this.customer = ConfigureApp.getBufforCustomer();
		}
		
		//listVisit.clear();
		listVisit = visits;
		
		System.out.println("Visits: " + listVisit);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateDate);
		
		CalendarApp calendarApp = new CalendarApp();
		
		
		
		//resetowanie ustawie�
		//findCustomerText.setText("");
		textArea.setText("");
		dayText.setText("");
		dateText.setText("");
		userText.setText("");
		hourText.setText("");
		
		dateOfday = calendarApp.changeDate(calendar);
		this.dateDate = dateDate;
		
		String s = calendarApp.getFullName(dateOfday.getNameOfDay());
		
		Font font = new Font("Arial", Font.PLAIN, 20);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		dayText.setText(s);
		dayText.setSize((int) bounds.getWidth(),
						dayText.getHeight());
		additionalInfoPanel.repaint();
		
		dateText.setText(dateOfday.getDateWithoutDay());
		hourText.setLocation(dayText.getX() + dayText.getWidth() + 5, 
							 hourText.getY());
		
		userText.setText(user);
		hourText.setText(hour);
		
		
		
		
	}
	
	public void setFields(Visit visit, String user) {
		
		System.out.println("Generujemy AddDate 2");
		treats.clear();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(visit.getDateToString());
		
		CalendarApp calendarApp = new CalendarApp();
		
		this.visit = visit;
		copyVisit.setVisible(true);
		pasteVisit.setVisible(false);
		
		//resetowanie ustawie�
		findCustomerText.setText("");
		textArea.setText("");
		
		
		dateOfday = calendarApp.changeDate(calendar);
		
		
		
		this.customer = visit.getCustomer();
		findButton.setEnabled(false);
		findCustomerText.setText(this.customer.getCustomerMenu());
		findCustomerText.setEditable(false);
		save.setEnabled(true);
		
		textArea.setText(visit.getText());
		
		setTreat(visit);
		
		if(isEdit) {
			addNewCustomer.setVisible(false);
		}
		else
			addNewCustomer.setVisible(true);
		
	}
	
	public void setTreat(Visit visit) {
		
		
		List<Treatment> list = visit.getAllTreatments();
		System.out.println("Wielko�� list: " + list.size());
		
		Boolean isFind = false;
		int i = 0;
		
		
		System.out.println("Treatment size: " + tabTreatment.length);
		treats.clear();
		System.out.println("Treats size: " + treats.size());
		
		if(list.size()>0) {
			
			while(i<tabTreatment.length && !isFind) {
				//System.out.println("Zabieg z listy: " + list.get(0).getName());
				//System.out.println("Zabieg z tabTreat: " + tabTreatment[i]);
				if(tabTreatment[i].contains(list.get(0).getName())) 
					isFind = true;
				i++;
			}
			
			treats.add(list.get(0));
			
		}
		else 
			treats.add(new Treatment());
		
		
		//ustawiamy pierwszy combobox
		
		if(i>0)
			comboTreat.setSelectedIndex(i-1);
		else
			comboTreat.setSelectedIndex(i);
		
		
		
		
		//je�li jest wi�cej zabieg�w, rysujemy pozosta�e comboboxy
		if(list.size()>1) {
			for(int j=1; j<list.size(); j++) {
				
				
				i = 0;
				isFind = false;
				while(i<tabTreatment.length && !isFind) {
					if(tabTreatment[i].contains(list.get(j).getName())) 
						isFind = true;
					i++;
				}
				
				addComboTreat(i-1);
				addPlus();
				treats.add(list.get(j));
			}
			
		}
		
	}
	
	
	public int getVisits() {
		
		return listVisit.size();
		
	}
	
	private void setListVisit(String user, Date date) {
		
		listVisit = serviceData.getListVisit(user, date);
		System.out.println("ListVisit: " + listVisit);
	}
	
	public void exit() {
		
		setVisible(false);
		mainPanel.removeAll();
		hourString = null;
		components();
		mainPanel.repaint();
		
		
		
	}
	
	public void setCard(String name, String kind, Boolean isActive) {
		
		if(name.equals("main"))  {
			cl.show(mainPanel, welcomePanel.getName());
			addVisits.setEnabled(isActive);
		}
		else if(name.equals("add")) 
			cl.show(mainPanel, calendarPanel.getName());
		
		else if(name.equals("look"))
			cl.show(mainPanel, schedulePanel.getName());
		
		this.kindCard = kind;
		
		
		
		MainWindow.pane.setVisible(true);
		MainWindow.addDate.setVisible(true);
	}
	
	public JButton getSave() {
		
		return save;
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
		
		
		
		
		
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

	

}
