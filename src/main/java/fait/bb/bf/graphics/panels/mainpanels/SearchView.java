package fait.bb.bf.graphics.panels.mainpanels;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.JTextFieldBackground;
import fait.bb.bf.graphics.panels.glasspanels.AdminPanel;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.InputDataService;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.widgets.WidgetAddClient;


public class SearchView implements FocusListener {
	
	private JPanel mainPanel;
	private JPanel whitePanel;
	
	
	private JPanel searchPanel;
	private JLabel searchFieldPanel;
	private static JTextField searchField;
	private JButton searchButton;
	
	private JLabel errorLabel;
	
	private static JPanel cardPanel;
	private JPanel customersPanel;
	private JButton seeMore;
	
	private JPanel navPanelLeft;
	private JPanel navPanelRight;
	private static JButton left;
	private static JButton right;
	private static int currentSite = 1;
	private static int sites = 1;
	private static int numberOfCustomer = 4;
	
	private JPanel moreResultPanel;
	private JList list;
	private JList list_2;
	private JScrollPane scrollPanel;
	private JScrollPane scrollPanel_2;
	
	private static List<JButton> listButton;
	private static List<Customer> listCustomer;
	
	private Font fontCustomer;
	
	public static Border notWrite = BorderFactory.createLineBorder(new Color(207,106,182));
	public static Border setWrite = BorderFactory.createLoweredBevelBorder();
	
	private static ServiceData serviceData;
	private static int currentCard = 2;
	
	private static int statXbutton = 30;
	private static int statYbutton = 10;
	private static int wButton = 10;
	private static int hButton = 10;
	private static int pages = 4;
	private static int customer;
	private static int x;
	private static int y;
	
	private static Communication comm;
	public static int custom;
	public static Customer person;
	
	private int countKey = 0;
	private static String phrase;
	
	public SearchView() {
		
		mainPanel = new JPanel();
		mainPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.setLayout(null);
		
		serviceData = new ServiceData();
		listCustomer = new ArrayList<Customer>();
		
		setWhitePanel();
		setNavigation();
		setCardPanel();
		setBorder();
		countCustomer();
		
		comm = new Communication(MainWindow.getFrame());
	}
	
	private void setBorder() {
		
		JLabel lab = new JLabel();
		lab.setBackground(ConfigureApp.backgroundCardColor);
		lab.setSize(MainWindow.cardPanel.getSize().width, 10);
		lab.setLocation(0,0);
		lab.setOpaque(true);
		mainPanel.add(lab);
		
	}
	
	
	private static void paintCustomer() {
		
		if((listCustomer.size() - currentSite*numberOfCustomer)> 0)
			setListButtons(listCustomer.subList((currentSite-1)*numberOfCustomer, currentSite*numberOfCustomer));
		else
			setListButtons(listCustomer.subList((currentSite-1)*numberOfCustomer, listCustomer.size()));
	}
	
	private static void countCustomer() {
		
		int szer = cardPanel.getWidth() - wButton*2;
		ImageIcon imgIc = new ImageIcon("resources/images/clientButton.png");
		x = szer/imgIc.getIconWidth();
		
		Boolean split = true;
		while(split){
			if(x*imgIc.getIconWidth()+(x-1)*statXbutton>szer)
				x--;
			else
				split=false;
		}
		
		System.out.println("Wysoko�� obrazka: " + imgIc.getIconHeight());
		int height = cardPanel.getHeight() - hButton*2;
		y = height/imgIc.getIconHeight();
		
		split = true;
		while(split){
			if(y*imgIc.getIconHeight()+(y-1)*statYbutton>height)
				y--;
			else
				split=false;
		}
		System.out.println("X: " + x);
		System.out.println("Y: " + y);
		customer = pages * x * y;
		numberOfCustomer = x * y;
		
	}
	
	
	
	public static void setActionSearch() {
		//najpierw sprawdzam poprawno�� wprowadzonego �a�cucha. Mo�e sk�ada� si� z nast�puj�cych
		//znak�w: [A-Z][a-z][0-9]-+
		
		//int code = serviceData.checkSearchString(searchField.getText());
			
			
			cardPanel.removeAll();
			
			
			
			listCustomer = new ArrayList<Customer>();
			
			System.out.println("Wyszukiwana fraza: " + phrase);
			
			
			listCustomer = serviceData.searchCustomer(phrase);
			
			System.out.println("Znalezieni klienci: " + listCustomer);
			
			//setCustomerPanels();
			if(listCustomer.size()!=0) {
				
				left.setVisible(true);
				right.setVisible(true);
				
				currentSite=1;
				if(currentSite==1) {
					
					left.setEnabled(false);
				}
				else {
					left.setEnabled(true);
					
				}
			
				sites = listCustomer.size()/numberOfCustomer;
				if(listCustomer.size()%numberOfCustomer!=0)
					sites++;
				if(sites==1) 
					right.setEnabled(false);
				else
					right.setEnabled(true);
			
				paintCustomer(); 
				/*if(listCustomer.size()<=numberOfCustomer) 
					setListButtons(listCustomer, 0);
				else {
				setListButtons(listCustomer.subList(0, 4),1);*/
			
				//}
				
				//customersPanel.repaint();
			}
			else {
				String s = "Brak wynik�w w bazie dla wprowadzonego zapytania.";
				Font font = new Font("Arial", Font.BOLD, 25);
				FontMetrics metrics = new FontMetrics(font) {};
				Rectangle2D bounds = metrics.getStringBounds(s, null);
				
				JTextField one= new JTextField(s);
				one.setForeground(new Color(119,26,114));
				one.setFont(font);
				one.setBounds(cardPanel.getWidth()/2 - (int) bounds.getWidth()/2,
							   20,
							  (int) bounds.getWidth(),
							  (int) bounds.getHeight());
				one.setOpaque(false);
				one.setBorder(null);
				cardPanel.add(one);
				
				s = "Pami�taj, �e wyszukujemy po nazwiskach lub numerach telefonu";
				bounds = metrics.getStringBounds(s, null);
				
				JTextField two= new JTextField(s);
				two.setForeground(new Color(119,26,114));
				two.setFont(font);
				two.setBounds(cardPanel.getWidth()/2 - (int) bounds.getWidth()/2,
							   one.getY() + one.getHeight() + 5,
							  (int) bounds.getWidth(),
							  (int) bounds.getHeight());
				two.setOpaque(false);
				two.setBorder(null);
				cardPanel.add(two);
				
				right.setVisible(false);
				left.setVisible(false);
				
				JButtonWithout button = new JButtonWithout("addCustomer_2");
				button.setLocation(cardPanel.getWidth()/2 - button.getWidth()/2,
								   two.getY() + two.getHeight() + 20);
				button.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						if(ConfigureApp.isTabs()) {
							ConfigureApp.countTab();
							MainWindow.labTab.setVisible(true);
							ConfigureWindow.repaintMenu();
							MainWindow.currentCard = 4;
							MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
							
							CardPanel clientPanel = new CardPanel(1, null);
							MainWindow.listPanels.add(clientPanel);
							
							MainWindow.tabbedPanel.addTab(WidgetAddClient.name, clientPanel.getMainPanel());

							int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
							Tab temp = new Tab(WidgetAddClient.name, clientPanel.getMainPanel(),
									selIndex);

							//listTab.add(temp);

							MainWindow.tabbedPanel.setForegroundAt(selIndex,
									ConfigureApp.fontAtTabColor);
							MainWindow.tabbedPanel.setSelectedIndex(selIndex);

							MainWindow.setWidgetBar();
							/*if (ConfigureWindow.bar.getText().equals(
							"------------------------")) {
								MainWindow.pane.setVisible(true);
								MainWindow.choosePanel.setVisible(true);
							}*/
						}
						else {
							comm.dont("Masz zbyt du�o otwartych zak�adek");
						}
						
					}
				});
				cardPanel.add(button);
				
				
				
			}
			//searchField.setText("");
			//searchField.selectAll();
			cardPanel.repaint();
			
			
	}
	
	private void setWhitePanel() {
		
		
		whitePanel = new JPanel();
		whitePanel.setBackground(ConfigureApp.backgroundColor);
		//whitePanel.setBorder(ConfigureApp.panelBorder);
		whitePanel.setLayout(null);
		whitePanel.setBounds(10, 10,
							 MainWindow.tabbedPanel.getWidth() - 20,
							 MainWindow.tabbedPanel.getHeight()- 60);
		
		System.out.println("Wysoko�� whitePanel: " + whitePanel.getHeight());
		
		searchPanel = new JPanel();
		searchPanel.setOpaque(false);
		searchPanel.setLayout(null);
		searchPanel.setBounds(0,0,
							  whitePanel.getWidth(),
							  80);
		
		whitePanel.add(searchPanel);
		
		int heightField = 40;
		
		fontCustomer = new Font("Arial", Font.BOLD, 22);
		
		searchFieldPanel = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/findLabel.png");
		searchFieldPanel.setIcon(imgIc);
		searchFieldPanel.setBounds(20, 
				  				   searchPanel.getHeight()/2 - imgIc.getIconHeight()/2, 
				  				   imgIc.getIconWidth(), 
				  				   imgIc.getIconHeight());
		
		searchPanel.add(searchFieldPanel);
		
		imgIc = new ImageIcon("resources/images/findText.png");
		JLabel searchFieldBack = new JLabel(imgIc);
		searchFieldBack.setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		searchFieldBack.setLocation(searchFieldPanel.getWidth()/2 - searchFieldBack.getWidth()/2, 
							  		searchFieldPanel.getHeight()/2 - searchFieldBack.getHeight()/2-5);
		searchFieldPanel.add(searchFieldBack);
		
		searchField = new JTextField();
		//searchField.setBorder(new EmptyBorder(0, 5, 0, 0));
		searchField.setBorder(null);
		searchField.setOpaque(false);
		//searchField.setBorder(notWrite);
		//searchField.setBackground(new Color(247,247,247));
		searchField.setForeground(new Color(221,217,217));
		searchField.setText("  Wpisz s�owo...");
		searchField.setFont(fontCustomer);
		searchField.setFocusable(true);
		searchField.selectAll();
		
		searchField.setBounds(4, 
							  4, 
							  searchFieldBack.getWidth() - 8, 
							  searchFieldBack.getHeight() - 8);
		searchField.setFocusable(true);
		searchField.addFocusListener(this);
		
		
		searchFieldBack.add(searchField);
		
		errorLabel = new JLabel();
		errorLabel.setForeground(Color.RED);
		errorLabel.setBounds(searchField.getX(),
							 searchField.getY() + searchPanel.getHeight() + 5,
							 searchField.getWidth(),
							 15);
		
		searchPanel.add(errorLabel);
		
		
		
		searchButton = new JButtonOwn("findButton");
		
		searchPanel.add(searchButton);
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				setActionSearch();
			}
		});
		
		searchButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionSearch();
				}
				
			}			
		});
		
		searchField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				
				phrase = searchField.getText() + e.getKeyChar();
				
				setActionSearch();
				
				/*if(e.getKeyCode()==KeyEvent.VK_BACK_SPACE && countKey>0) {
					countKey--;
					
				}
				else
					countKey++;*/
				/*if(countKey>=3) {
					setActionSearch();
				}*/
				
			/*	if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionSearch();
					
				}*/
				
				
				
			}			
		});
		
		
		
		searchFieldPanel.setLocation(searchPanel.getWidth()/2 - (searchFieldPanel.getWidth() + searchButton.getWidth()+3)/2,
									 searchPanel.getHeight()/2 - searchFieldPanel.getHeight()/2);
		searchButton.setLocation(searchFieldPanel.getX() + searchFieldPanel.getWidth() + 3,
			   	 				 searchFieldPanel.getY());
		
		mainPanel.add(whitePanel);
		
	}
	
	private void setNavigation() {
		
		navPanelLeft = new JPanel();
		navPanelLeft.setLayout(null);
		navPanelLeft.setOpaque(false);
		
		navPanelRight = new JPanel();
		navPanelRight.setLayout(null);
		navPanelRight.setOpaque(false);
		
		left = new JButtonOwn("/buttons/left");
		navPanelLeft.setSize(left.getWidth() + 40,
							 whitePanel.getHeight() - searchPanel.getHeight());
		whitePanel.add(navPanelLeft);
		
		left.setLocation((navPanelLeft.getWidth() - left.getWidth())/2,
						 (navPanelLeft.getHeight() - left.getHeight())/2);
		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				cardPanel.removeAll();
				
				currentSite--;
				if(currentSite==1)
					left.setEnabled(false);
				else
					left.setEnabled(true);
				if(currentSite==sites)
					right.setEnabled(false);
				else
					right.setEnabled(true);
				paintCustomer(); 
				cardPanel.repaint();
			}
		});

		
		navPanelLeft.add(left);
		navPanelLeft.setLocation(0, 
							     searchPanel.getHeight());
		
		
		right = new JButtonOwn("/buttons/right");
		navPanelRight.setSize(navPanelLeft.getWidth(),
							  navPanelLeft.getHeight());
		whitePanel.add(navPanelRight);
		
		right.setLocation((navPanelRight.getWidth() - right.getWidth())/2,
						  (navPanelRight.getHeight() - right.getHeight())/2);
		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				cardPanel.removeAll();
				
				currentSite++;
				if(currentSite==sites)
					right.setEnabled(false);
				else
					right.setEnabled(true);
				if(currentSite==1)
					left.setEnabled(false);
				else
					left.setEnabled(true);
				paintCustomer(); 
				cardPanel.repaint();
			}
		});
		
		navPanelRight.add(right);
		navPanelRight.setLocation(whitePanel.getWidth() - navPanelRight.getWidth(), 
							      searchPanel.getHeight());
		
		if(sites==1)
			right.setEnabled(false);
		
		right.setVisible(false);
		left.setVisible(false);
	}
	
	private void setCardPanel() {
		
		cardPanel = new JPanel();
		//CardLayout cl = new CardLayout();
		cardPanel.setOpaque(false);
		cardPanel.setLayout(null);
		cardPanel.setBounds(navPanelLeft.getWidth(),
							navPanelLeft.getY(),
							whitePanel.getWidth() - 2 * navPanelLeft.getWidth(),
							navPanelLeft.getHeight());
		
		whitePanel.add(cardPanel);
		
		
		
		
		
	}
	
	private void setCustomerPanels() {
		
		
		customersPanel = new JPanel();
		customersPanel.setLayout(null);
		//customersPanel.setBackground(Color.RED);
		customersPanel.setBounds(1,1,cardPanel.getWidth() - 2, cardPanel.getHeight() - 2);
		customersPanel.setOpaque(false);
		cardPanel.add("2", customersPanel);
		
		/*seeMore = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/seeMoreResult.png");
		seeMore.setIcon(imgIc);
		seeMore.setSize(new Dimension(imgIc.getIconWidth(), imgIc.getIconHeight()));
		seeMore.setVisible(false);
		seeMore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				currentCard=3;
				setMoreResultPanel();
				((CardLayout) cardPanel.getLayout()).show(cardPanel, "" + currentCard);
			}
		});*/
		//customersPanel.add(seeMore);
		
	}
	
	
	
	
	
	private static JButton setButtonClient(ImageIcon img, final Customer c) {
		
		JButton temp = new JButton();
		ImageIcon imgIc = new ImageIcon("resources/images/clientButton.png");
		temp.setIcon(imgIc);
		//czcionka 25-30 px, odstep 30-35px
		imgIc = new ImageIcon("resources/images/clientButton-2.png");
		temp.setRolloverIcon(imgIc);
		//imgIc = new ImageIcon("resources/images/addClient-3.png");
		//widget.setPressedIcon(imgIc);
		//imgIc = new ImageIcon("resources/images/addClient-0.png");
		//temp.setDisabledIcon(imgIc);
		temp.setBorderPainted(false);
		temp.setContentAreaFilled(false);
		temp.setFocusPainted(false);
		temp.setSize(imgIc.getIconWidth(), imgIc.getIconHeight());
		temp.setLayout(null);
		
		Font font = new Font("Arial", Font.BOLD, 23);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds;
		
		String s = c.getSurname();
		bounds = metrics.getStringBounds(s, null);
		JLabel surname = new JLabel();
		surname.setText(s);
		surname.setFont(font);
		surname.setForeground(new Color(119,26,114));
		surname.setBounds(temp.getWidth()/2 - (int) bounds.getWidth()/2,
						  10,
						  (int) bounds.getWidth(),
						  (int) bounds.getHeight());
		temp.add(surname);
		
		
		font = new Font("Arial", Font.PLAIN, 18);
		metrics = new FontMetrics(font) {};
		s = c.getName();
		bounds = metrics.getStringBounds(s, null);
		JLabel name = new JLabel();
		name.setText(s);
		name.setFont(font);
		name.setForeground(new Color(175,123,172));
		name.setBounds(temp.getWidth()/2 - (int) bounds.getWidth()/2,
					   surname.getY() + surname.getHeight(),
					   (int) bounds.getWidth(),
					   (int) bounds.getHeight());
		temp.add(name);

		JLabel lab = new JLabel(img);
		lab.setBounds(20,
					  name.getY() + name.getHeight() + 10,
					  img.getIconWidth(), 
					  img.getIconHeight());
		lab.setBorder(BorderFactory.createLineBorder(new Color(134,129,134)));
		temp.add(lab);

		
		
		s = "Nr telefonu";
		font = new Font("Arial", Font.PLAIN, 16);
		metrics = new FontMetrics(font) {};
		bounds = metrics.getStringBounds(s, null);
		JLabel numberEt = new JLabel();
		numberEt.setText(s);
		numberEt.setFont(font);
		numberEt.setForeground(new Color(176,175,175));
		int width = temp.getWidth() - lab.getWidth() - lab.getX();
		numberEt.setBounds(lab.getX() + lab.getWidth() + (width - (int) bounds.getWidth())/2,
					   	 	lab.getY() + lab.getHeight()/2 - (int) bounds.getHeight() - 10,
					   	 	(int) bounds.getWidth(),
					   	 	(int) bounds.getHeight());
		
		temp.add(numberEt);
		
		s = c.getNumber();
		System.out.println("Numer telefonu: " + s);
		font = new Font("Arial", Font.PLAIN, 16);
		metrics = new FontMetrics(font) {};
		bounds = metrics.getStringBounds(s, null);
		JLabel number = new JLabel();
		number.setText(s);
		number.setFont(font);
		number.setForeground(new Color(176,175,175));
		number.setBounds(lab.getX() + lab.getWidth() + (width - (int) bounds.getWidth())/2,
						 numberEt.getY() + numberEt.getHeight() + 2,
					   	 (int) bounds.getWidth(),
					   	 (int) bounds.getHeight());
		temp.add(number);
		
		s = "Nr karty: " + InputDataService.formatCustomerID(c.getId());
		font = new Font("Arial", Font.BOLD, 12);
		metrics = new FontMetrics(font) {};
		bounds = metrics.getStringBounds(s, null);
		JLabel numberCard = new JLabel();
		numberCard.setText(s);
		numberCard.setFont(font);
		numberCard.setForeground(new Color(119,26,114));
		width = temp.getWidth() - lab.getWidth() - lab.getX();
		numberCard.setBounds(lab.getX() + lab.getWidth() + (width - (int) bounds.getWidth())/2 - 30,
					   	 	 number.getY() +  number.getHeight() + 20,
					   	 	(int) bounds.getWidth(),
					   	 	(int) bounds.getHeight());
		
		temp.add(numberCard);
		
		JButton delete = new JButtonOwn("buttons/delete", true);
		delete.setLocation(numberCard.getX() + numberCard.getWidth(),
						   numberCard.getY() - delete.getHeight()/2 + 5);
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				if(comm.youSure("Czy na pewno chcesz usun�� klienta?", 0)) {
					custom = c.getId();
					person = c;
					AdminPanel.isDeleted = true;
					MainWindow.pane.setVisible(true);
					
					MainWindow.mainGlassPanel.setPanel(new AdminPanel(MainWindow.mainGlassPanel.getSize())
													       .getMainPanel());
					MainWindow.mainGlassPanel.setVisible(true);
					
					//serviceData.deleteCustomer(c.getId());
					//setActionSearch();
					
				}
				
			}
		});
		
		temp.add(delete);
		
		temp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				int size = MainWindow.tabbedPanel.getTabCount();
				int i=0;
				Boolean isThere=false;
				while(i<size) {
					if(MainWindow.tabbedPanel.getTitleAt(i).equals(c.getSurname() + " " + c.getName())) {
						isThere = true;
						MainWindow.tabbedPanel.setSelectedIndex(i);
						break;
					}
						
					i++;
				}
				if(!isThere) {
					if(ConfigureApp.isTabs()) {
						
						Customer cust = serviceData.getCustomerMeta(c);
						
						ConfigureApp.countTab();
						MainWindow.currentCard=4;
						MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
						
						CardPanel cardClient = new CardPanel(0,cust);
						MainWindow.listPanels.add(cardClient);
						
						MainWindow.tabbedPanel.addTab(cust.getSurname() + " "  + cust.getName(), 
													  cardClient.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount()-1;

						MainWindow.tabbedPanel.setForegroundAt(selIndex, ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);

						MainWindow.setWidgetBar();
						
						/*if (ConfigureWindow.bar.getText().equals("------------------------")) {
							MainWindow.pane.setVisible(true);
							MainWindow.choosePanel.setVisible(true);
						}*/
						
					}	
					else {
						comm.dont("Masz zbyt du�o otwartych zak�adek");
					}
				}
				
				
			}
		});
		
		
		return temp;
		
	}
	
	private static void setListButtons(List<Customer> tempList) {
		
		Iterator<Customer> it = tempList.iterator();
		Customer c;
		ImageIcon imgIc = new ImageIcon("resources/images/avatar.png");
		
		
		listButton = new ArrayList<JButton>();
		//int width_x = 100;
		//int height_y = 60;
		int i = 0;
		
		while(it.hasNext()) {
			
			c = it.next();
			JButton temp = setButtonClient(imgIc, c);

			if(listButton.isEmpty()) {
				temp.setLocation((cardPanel.getWidth() - temp.getWidth()*x - (x-1)*wButton)/2,
								  (cardPanel.getHeight() - temp.getHeight()*y - (y-1)*hButton)/2);	
			}
			else {
				int x_temp = listButton.get(i-1).getX() + listButton.get(i-1).getWidth() + 
						     wButton + temp.getWidth();
				if(x_temp>cardPanel.getWidth()) 
				   temp.setLocation(listButton.get(0).getX(),
							 		listButton.get(i-1).getY() + listButton.get(i-1).getHeight() + hButton);
				
				else
					temp.setLocation(x_temp - temp.getWidth(),
							 		 listButton.get(i-1).getY());
			}
			
			
			cardPanel.add(temp);
			listButton.add(temp);
			i++;
			
		}
		
		/*if(more==1) {
			seeMore.setVisible(true);
			seeMore.setLocation(listButton.get(0).getX(),
								listButton.get(listButton.size()-1).getY() + listButton.get(listButton.size()-1).getHeight() + 20);
		}
		else {
			seeMore.setVisible(false);
		}*/
		
	}
	
	public static JPanel getCardPanel() {
		return cardPanel;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	@Override
	public void focusGained(FocusEvent evt) {
		
		JTextComponent textField = (JTextComponent) evt.getComponent();
		//textField.setBorder(setWrite);
		textField.setForeground(new Color(89,56,81));
		//textField.setText("");
		//textField.selectAll();
		textField.setText("");
	}

	@Override
	public void focusLost(FocusEvent evt) {
		//((JComponent) evt.getComponent()).setBorder(notWrite);		
	}

}
