package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.ServiceData;

public class MainGlassPanel extends JPanel implements MouseListener {
	
	private Color background = new Color(227,227,227);
	private JPanel mainPanel;
	
	public MainGlassPanel() {
		
		setDoubleBuffered(true);
		setBackground(background);
		setLayout(null);
		setSize(ConfigureApp.sizeOfDialog);
		setLocation((ConfigureWindow.getMaxScreen().width - this.getWidth())/2,
				  	(ConfigureWindow.getMaxScreen().height - this.getHeight())/2);
		addMouseListener(this);
		
		JButton close = new JButtonWithout("closeGlass");
		close.setLocation(getWidth() - close.getWidth() - 2, 2);
		add(close);
		close.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				removePanel();
				MainWindow.pane.setVisible(false);
				setVisible(false);
			}
		});
		
		components();
		
	}
	
	public void components() {
		
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setSize(950,535);
		mainPanel.setLocation(getWidth()/2 - mainPanel.getWidth()/2,
							  getHeight()/2 - mainPanel.getHeight()/2);	
		add(mainPanel);
		
		
		
	}
	
	public void setPanel(JPanel panel) {
		
		panel.setLocation(0,0);
		mainPanel.add(panel);
		//mainPanel.repaint();
	}
	
	public void removePanel() {
		
		mainPanel.removeAll();
	
	}
	
	public Dimension getSize() {
		
		return mainPanel.getSize();
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
