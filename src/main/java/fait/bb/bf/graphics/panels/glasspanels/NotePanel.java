package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.JTextAreaLimit;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.Note;
import fait.bb.bf.logic.ServiceData;


public class NotePanel extends JPanel implements MouseListener {
	
	private ServiceData serviceData;
	private Color background = new Color(246,215,68);
	
	private JTextArea textArea;
	private Note note;
	
	private Customer customer;
	
	public NotePanel() {
		
		setSize(330, ConfigureWindow.getMaxScreen().height - 20);
		setBackground(background);
		setLocation(ConfigureWindow.getMaxScreen().width - getWidth(),
					ConfigureWindow.getMaxScreen().height/2 - getHeight()/2);
		addMouseListener(this);
		setLayout(null);
		setDoubleBuffered(true);
		
		customer = null;
		serviceData = new ServiceData();
		
		components();
		
	}
	
	public void components() {
		
		JButtonWithout close = new JButtonWithout("closeGlass");
		close.setLocation(getWidth() - close.getWidth()-2, 2);
		add(close);
		close.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				MainWindow.pane.setVisible(false);
				setVisible(false);
				saveText();
			}
		});
		
		setTextArea();
		
	}
	
	private void setTextArea() {
		
		textArea = new JTextArea();
		textArea.setBorder(null);
		textArea.setDocument(new JTextAreaLimit(1000));
		textArea.setBackground(background);
		textArea.setFont(ConfigureApp.font.deriveFont(Font.PLAIN, 15));
		textArea.setSize(getWidth() - 20,
						 getHeight() - 40);
		textArea.setLocation(getWidth()/2 - textArea.getWidth()/2,
							 getHeight()/2 - textArea.getHeight()/2);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		
		add(textArea);
		
	}
	
	public void saveText() {
		
		if(note!=null) {
			System.out.println("Tekst notatki aktualizacyjnej: " + textArea.getText());
			serviceData.updateNote(note, textArea.getText());
		}
		else
			serviceData.setCustomerMeta("note", textArea.getText(), customer);
		
	}
	
	public void paintNote(Customer cust) {
		
		String text = "";
		
		if(customer==null && cust!=null) {
			note = serviceData.getNote(cust.getId());
			if(note!=null)
				text = note.getText();
			
		}
		else if(customer!=null && cust!=null) {
			if(customer.getId()!=cust.getId()) 
				customer = cust;
				
			
			note = serviceData.getNote(customer.getId());
			if(note!=null)
				text = note.getText();
				
		}
		
		customer = cust;
		textArea.setText(text);
		repaint();
		MainWindow.pane.setVisible(true);
		MainWindow.notePanel.setVisible(true);
		
		
		
	}
	
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
