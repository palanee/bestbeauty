package fait.bb.bf.graphics.panels.mainpanels;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.ImagePanel;
import fait.bb.bf.graphics.panels.glasspanels.AddDate;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;


public class LoggingPanel extends JPanel implements FocusListener {
	
	Color stepColor = new Color(53,53,53);
	Color textColor = new Color(84,50,76);
	Color etFontLogColor = new Color(140,141,142);
	
	Font stepFont = new Font("Arial", Font.BOLD, 25);
	Font textFont = new Font("Arial", Font.PLAIN, 30);
	Font labelFont = new Font("Arial", Font.PLAIN, 23);
	Font comFont = new Font("Arial", Font.BOLD, 14);
	
	FontMetrics fontmetrics;
	ServiceData servData = new ServiceData();
	
	private static int currentCard;
	
	private JLabel createAccount;
	
	
	//panel z cardLayout oraz nawigacj�
	private JPanel mainPanel;
	private JPanel logPanel;
	
	private JButton next;
	
	private JPanel cardLogin;
	private JLabel step;
	private JTextField login;
	private JPasswordField pass;
	private JPasswordField repeatPass;
	private JLabel etLogin;
	private JLabel etPass;
	private JLabel etRepeat;
	private JLabel komunikat;
	
	//panel step two - klucz
	private JPanel keyPanel;
	private JTextField key;
	private JTextField number;
	
	//panel step three - zako�czenie
	private JPanel endPanel;
	private JLabel one;
	private JLabel two;
	private JLabel three;
	private JLabel logo;
	private JButton endButton;
	
	private JPanel createUserPanel;
	private JTextField name;
	private JTextField surname;
	private JTextField code;
	
	private ServiceData serviceData;
	
	
	private Border notWrite = BorderFactory.createLineBorder(new Color(186,185,186));
	private Border setWrite = BorderFactory.createLoweredBevelBorder();
	
	public LoggingPanel() {
		
		currentCard = 1;
		serviceData = new ServiceData();
		setBackground(ConfigureApp.backgroundColor);
		setLayout(null);
		setPanelLog();
		setStepOne();
		
		
		
	}
	
	private void setTextField(JComponent comp) {
		
		comp.setBackground(ConfigureApp.backgroundColor);
		comp.setForeground(textColor);
		comp.setFont(textFont);
		comp.setFocusable(true);
		comp.setBorder(notWrite);
		comp.addFocusListener(this);
		comp.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					setNextAction();
					
				}
				
			}			
		});
		
	}
	
	private void addAndCheckAdmin() {
		
		Boolean isGood = servData.checkPass(pass.getText(), 
				   							repeatPass.getText());
		
		Boolean isNext = true;
		
		if(isGood) {
			
			
			if(login.getText().isEmpty()) {
				komunikat.setText("Nie uzupe�niono pola login");
				komunikat.setVisible(true);
			}
				
			else  {
				komunikat.setVisible(false);
				currentCard++;
			}
			
		}
		else {
			komunikat.setText("Has�a nie s� identyczne");
			komunikat.setVisible(true);
			
		}
		
		
		
	}
	
	private void addAndCheckUser() {
		
		User u = serviceData.addUser(name.getText(), surname.getText(), code.getText(), null);
		if(u!=null) {
			currentCard++;
		}
	}
	
	private void checkKey() {
		
		Boolean isGood = servData.checkKey(key.getText(), number.getText());
		if(isGood) {
			komunikat.setVisible(false);
			currentCard++;
			currentCard++;
		}
		else {
			komunikat.setText("Niepoprawny klucz aktywacyjny");
			komunikat.setVisible(true);
			keyPanel.repaint();
		}
		
	}
	
	private void setNextAction() {
		
		if(currentCard==1) {
			 addAndCheckAdmin();
		}
		else if(currentCard==2) {
			checkKey();
		}
		/*else if(currentCard==3) {
			addAndCheckUser();
			System.out.println("Tworz� konto u�ytkownika");
		}*/
		
		switch(currentCard) {
		
			case 2:
				logPanel.removeAll();
				setStepTwo();
				keyPanel.repaint();
				number.requestFocusInWindow();
				break;
			/*case 3:
				setCreatePanel();
				createUserPanel.repaint();
				break;*/
			case 4:
				endButton.setVisible(true);
				next.setVisible(false);
				setStepThree();
				endButton.requestFocusInWindow();
				
				break;
		}
		
		((CardLayout) logPanel.getLayout()).show(logPanel, "" + currentCard);
		
	}
	
	private void setPanelLog() {
		
		
		/*Font font = null;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/images/DrukarniaPolska.ttf"));
			font = font.deriveFont(Font.PLAIN, 50);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		fontmetrics = new FontMetrics(stepFont) {};
		Dimension size = MainWindow.cardPanel.getSize();
		
		ImageIcon imgIc = new ImageIcon("resources/images/logPanel.png");
		mainPanel = new ImagePanel(imgIc.getImage());
		mainPanel.setLayout(null);
		mainPanel.setBounds(size.width/2 - imgIc.getIconWidth()/2,
							size.height/2 - imgIc.getIconHeight()/2,
							imgIc.getIconWidth(),
							imgIc.getIconHeight());
		
		add(mainPanel);
		
		createAccount = new JLabel();
		imgIc = new ImageIcon("resources/images/createAccount.png");
		createAccount.setIcon(imgIc);
		createAccount.setBounds(mainPanel.getX(),
								mainPanel.getY() - imgIc.getIconHeight() + 5,
								imgIc.getIconWidth(),
								imgIc.getIconHeight());
		add(createAccount);
		
		final CardLayout cl = new CardLayout();
		logPanel = new JPanel();
		logPanel.setOpaque(false);
		logPanel.setLayout(cl);
		logPanel.setBounds(0,0,
						   mainPanel.getWidth(),
						   mainPanel.getHeight() - 100);
		
		mainPanel.add(logPanel);
		
		next = new JButton();
		imgIc = new ImageIcon("resources/images/nextButton-1.png");
		next.setIcon(imgIc);
		next.setBounds(mainPanel.getWidth() - 20 - imgIc.getIconWidth(),
					   logPanel.getHeight() + 10,
					   imgIc.getIconWidth(),
					   imgIc.getIconHeight());
		imgIc = new ImageIcon("resources/images/nextButton-2.png");
		next.setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/nextButton-3.png");
		next.setPressedIcon(imgIc);
		next.setBorderPainted(false);
		next.setContentAreaFilled(false);
	
		
		next.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					
					setNextAction();
					
				}
				
			}			
		});
		
		
		
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				setNextAction();
			}
		});
		
		
		mainPanel.add(next);
		
		endButton = new JButton();
		imgIc = new ImageIcon("resources/images/endButton-1.png");
		endButton.setIcon(imgIc);
		endButton.setBounds(mainPanel.getWidth() - 20 - imgIc.getIconWidth(),
					   logPanel.getHeight() + 10,
					   imgIc.getIconWidth(),
					   imgIc.getIconHeight());
		imgIc = new ImageIcon("resources/images/endButton-2.png");
		endButton.setRolloverIcon(imgIc);
		imgIc = new ImageIcon("resources/images/endButton-3.png");
		endButton.setPressedIcon(imgIc);
		endButton.setBorderPainted(false);
		endButton.setContentAreaFilled(false);
		endButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				servData.addUser(login.getText(), pass.getText(), 1);
				
				//g��wny panel z widgetami
				MainWindow.currentCard = 3;
				
				MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
				ConfigureWindow.bar.setVisible(true);
				ConfigureWindow.menu.setVisible(true);
				
			}
		});
		
		endButton.addKeyListener(new KeyAdapter() {
			
			
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					
					servData.addUser(login.getText(), pass.getText(), 1);
					
					//g��wny panel z widgetami
					MainWindow.currentCard = 3;
					
					MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
					ConfigureWindow.bar.setVisible(true);
					ConfigureWindow.menu.setVisible(true);
				}
			}
		});
		
		
		endButton.setVisible(false);
		mainPanel.add(endButton);
		
		
		
	}
	
	public void setStepOne() {
		
		//krok 1
		cardLogin = new JPanel();
		cardLogin.setOpaque(false);
		cardLogin.setLayout(null);
		logPanel.add("1", cardLogin);
		
		String s = "Krok 1. Podaj swoje imi�/login i has�o";
		Rectangle2D bounds = fontmetrics.getStringBounds(s, null);  
		
		step = new JLabel(s);
		step.setFont(stepFont);
		step.setForeground(stepColor);
		step.setBounds(10, 
					   15,
					   logPanel.getWidth(),
					   (int) bounds.getHeight());
		
		cardLogin.add(step);
		
		Dimension sizeField = new Dimension(380,40);
		
		login = new JTextField();
		setTextField(login);
		login.setBounds(mainPanel.getWidth() - sizeField.width - 20,
						step.getY() + step.getHeight() + 30,
						sizeField.width,
						sizeField.height);
		
		cardLogin.add(login);
		
		fontmetrics = new FontMetrics(labelFont) {};
		
		s = "Login";
		bounds = fontmetrics.getStringBounds(s, null);  
		etLogin = new JLabel(s);
		etLogin.setFont(labelFont);
		etLogin.setForeground(stepColor);
		etLogin.setBounds(login.getX() - (int) bounds.getWidth() - 50,
						  login.getY() + (login.getHeight()/2 - (int) bounds.getHeight()/2),
						  (int) bounds.getWidth(),
						  (int) bounds.getHeight());
		
		cardLogin.add(etLogin);
		
		
		pass = new JPasswordField();
		setTextField(pass);
		pass.setBounds(login.getX(),
					   login.getY() + login.getHeight() + 15,
					   sizeField.width,
					   sizeField.height);
		pass.setEchoChar('*');
		cardLogin.add(pass);
		
		
		s = "Has�o";
		bounds = fontmetrics.getStringBounds(s, null);  
		etPass = new JLabel(s);
		etPass.setFont(labelFont);
		etPass.setForeground(stepColor);
		etPass.setBounds(pass.getX() - (int) bounds.getWidth() - 50,
						 pass.getY() + (pass.getHeight()/2 - (int) bounds.getHeight()/2),
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		cardLogin.add(etPass);
		
		repeatPass = new JPasswordField();
		setTextField(repeatPass);
		repeatPass.setBounds(pass.getX(),
					   		 pass.getY() + pass.getHeight() + 15,
					   		 sizeField.width,
					   		 sizeField.height);
		repeatPass.setEchoChar('*');
		cardLogin.add(repeatPass);
		
		
		s = "Powt�rz has�o";
		bounds = fontmetrics.getStringBounds(s, null);  
		etRepeat = new JLabel(s);
		etRepeat.setFont(labelFont);
		etRepeat.setForeground(stepColor);
		etRepeat.setBounds(repeatPass.getX() - (int) bounds.getWidth() - 50,
						   repeatPass.getY() + (repeatPass.getHeight()/2 - (int) bounds.getHeight()/2),
						   (int) bounds.getWidth(),
						   (int) bounds.getHeight());
		
		cardLogin.add(etRepeat);
		
		komunikat = new JLabel();
		komunikat.setForeground(Color.RED);
		komunikat.setFont(comFont);
		komunikat.setBounds(repeatPass.getX(),
							repeatPass.getY() + repeatPass.getHeight() + 10,
							repeatPass.getWidth(),
							20);
		komunikat.setVisible(false);
		cardLogin.add(komunikat);
		
		
		
	}
	
	public void setStepTwo() {
		
		
		String s = "";
		Rectangle2D bounds;
		
		keyPanel = new JPanel();
		keyPanel.setOpaque(false);
		keyPanel.setLayout(null);
		keyPanel.setBounds(0,0,logPanel.getWidth(), logPanel.getHeight());
		
		logPanel.add("2", keyPanel);
		
		s = "Krok 2. Podaj numer klienta i klucz aktywacyjny produktu";
		step.setText(s);
		
		Dimension sizeField = new Dimension(450, 45);
		
		keyPanel.add(step);
		
		number = new JTextField();
		setTextField(number);
		number.setBounds(mainPanel.getWidth() - sizeField.width - 20,
						step.getY() + step.getHeight() + 30,
						sizeField.width,
						sizeField.height);
		number.grabFocus();
		
		keyPanel.add(number);
		
		fontmetrics = new FontMetrics(labelFont) {};
		
		s = "Numer klienta";
		bounds = fontmetrics.getStringBounds(s, null);  
		etLogin = new JLabel(s);
		etLogin.setFont(labelFont);
		etLogin.setForeground(stepColor);
		etLogin.setBounds(number.getX() - (int) bounds.getWidth() - 50,
						  number.getY() + (number.getHeight()/2 - (int) bounds.getHeight()/2),
						  (int) bounds.getWidth(),
						  (int) bounds.getHeight());
		
		keyPanel.add(etLogin);
		
		 MaskFormatter mask = null;
	        try {
	            //
	            // Create a MaskFormatter for accepting phone number, the # symbol accept
	            // only a number. We can also set the empty value with a place holder
	            // character.
	            //
	            mask = new MaskFormatter("AAAA-AAAA-AAAA-AAAA");
	            mask.setPlaceholderCharacter('_');
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
		
		key = new JFormattedTextField(mask);
		setTextField(key);
		key.setSize(number.getSize());
		key.setLocation(number.getX(), number.getY() + number.getHeight() + 15);
		keyPanel.add(key);
		
		
		
		s = "Klucz";
		bounds = fontmetrics.getStringBounds(s, null);  
		etPass = new JLabel(s);
		etPass.setFont(labelFont);
		etPass.setForeground(stepColor);
		etPass.setBounds(key.getX() - (int) bounds.getWidth() - 50,
						 key.getY() + (key.getHeight()/2 - (int) bounds.getHeight()/2),
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		keyPanel.add(etPass);
		
		
		
		final JLabel link = new JLabel("Nie masz jeszcze numeru? Utw�rz go, klikaj�c tutaj!");
		link.setForeground(Color.blue);
		link.setSize(key.getWidth(), 15);
		link.setLocation(key.getX(), key.getY() + key.getHeight() + 10);
		
		keyPanel.add(link);
		
		link.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				link.setForeground(etFontLogColor);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				link.setForeground(new Color(37,149,19));
				link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				java.net.URI location;
				try {
					location = new java.net.URI("http://best-beauty.pl/");
					java.awt.Desktop.getDesktop().browse(location);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		
		komunikat.setBounds(key.getX(),
							key.getY() + key.getHeight() + 10,
							key.getWidth(),
							20);
		
		keyPanel.add(komunikat);
		
		
	}
	
	private void setStepThree() {
		
		endPanel = new JPanel();
		endPanel.setOpaque(false);
		endPanel.setLayout(null);
		endPanel.setBounds(0,0,logPanel.getWidth(), logPanel.getHeight());
		
		logPanel.add("4", endPanel);
		
		fontmetrics = new FontMetrics(textFont) {};
		
		String s = "GRATULACJE!";
		Rectangle2D bounds = fontmetrics.getStringBounds(s, null);  
		
		one = new JLabel(s);
		one.setFont(textFont);
		one.setForeground(stepColor);
		one.setBounds((endPanel.getWidth() - (int) bounds.getWidth())/2 -5,
					   30,
					   (int) bounds.getWidth(),
					   (int) bounds.getHeight());
		
		endPanel.add(one);
		
		s = "Konto zosta�o za�o�one";
		bounds = fontmetrics.getStringBounds(s, null); 
		two = new JLabel(s);
		two.setFont(textFont);
		two.setForeground(stepColor);
		two.setBounds((endPanel.getWidth() - (int) bounds.getWidth())/2,
				   	   one.getY() + one.getHeight() + 35,
				   	   (int) bounds.getWidth(),
				   	   (int) bounds.getHeight());
		
		endPanel.add(two);
		
		s = "Mo�esz korzysta� z programu";
		bounds = fontmetrics.getStringBounds(s, null); 
		three = new JLabel(s);
		three.setFont(textFont);
		three.setForeground(stepColor);
		three.setBounds((endPanel.getWidth() - (int) bounds.getWidth())/2,
				   	   two.getY() + two.getHeight() + 10,
				   	   (int) bounds.getWidth(),
				   	   (int) bounds.getHeight());
		
		endPanel.add(three);
		
		logo = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/bestFryzjer.png");
		logo.setIcon(imgIc);
		logo.setBounds((endPanel.getWidth() - imgIc.getIconWidth())/2,
						three.getY() + three.getHeight() + 20,
						imgIc.getIconWidth(),
						imgIc.getIconHeight());
		
		endPanel.add(logo);
		
	}
	
	public void setCreatePanel() {
		
		
		createUserPanel = new JPanel();
		createUserPanel.setOpaque(false);
		createUserPanel.setLayout(null);
		createUserPanel.setBounds(0,0,logPanel.getWidth(), logPanel.getHeight());
		
		logPanel.add("3", createUserPanel);
		
		String s = "Krok 3. Utw�rz konto pierwszego pracownika";
		step.setText(s);
		
		Dimension sizeField = new Dimension(670, 60);
		
		createUserPanel.add(step);
		
		Dimension sizeFields = new Dimension(400, 40);
		fontmetrics = new FontMetrics(labelFont) {};
		s = "Imi�";
		Rectangle2D bounds = fontmetrics.getStringBounds(s, null);  

		
		name = new JTextField();
		setTextField(name);
		name.setSize(sizeFields.width, sizeFields.height);
		name.setLocation(createUserPanel.getWidth() - 20 - name.getWidth(), 
						 step.getY() + step.getHeight() + 30);
		
		JLabel etName = new JLabel(s);
		etName.setFont(labelFont);
		etName.setForeground(stepColor);
		etName.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		etName.setLocation(name.getX() - 50 - etName.getWidth(),
						   name.getY() + name.getHeight()/2 - etName.getHeight()/2);
		
		createUserPanel.add(name);
		createUserPanel.add(etName);
		
		surname = new JTextField();
		setTextField(surname);
		surname.setSize(sizeFields.width, sizeFields.height);
		surname.setLocation(name.getX(), name.getY() + name.getHeight() + 20);
		
		s = "Nazwisko";
		bounds = fontmetrics.getStringBounds(s, null);  
		
		JLabel etSurname = new JLabel(s);
		etSurname.setFont(labelFont);
		etSurname.setForeground(stepColor);
		etSurname.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		etSurname.setLocation(surname.getX() - 50 - etSurname.getWidth(),
							  surname.getY() + surname.getHeight()/2 - etSurname.getHeight()/2);
		
		createUserPanel.add(surname);
		createUserPanel.add(etSurname);
		
		code = new JTextField();
		setTextField(code);
		code.setSize(sizeFields.width, sizeFields.height);
		code.setLocation(name.getX(), surname.getY() + surname.getHeight() + 20);
		
		s = "Kod pracownika";
		bounds = fontmetrics.getStringBounds(s, null);  
		
		JLabel etCode = new JLabel(s);
		etCode.setFont(labelFont);
		etCode.setForeground(stepColor);
		etCode.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		etCode.setLocation(code.getX() - 50 - etCode.getWidth(),
						   code.getY() + code.getHeight()/2 - etCode.getHeight()/2);
		
		createUserPanel.add(code);
		createUserPanel.add(etCode);
		
		
		
	}
	
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		((JTextComponent) e.getComponent()).setBorder(setWrite);
		
		
		
	}



	@Override
	public void focusLost(FocusEvent e) {
		
		((JTextComponent) e.getComponent()).setBorder(notWrite);
		
	}

}
