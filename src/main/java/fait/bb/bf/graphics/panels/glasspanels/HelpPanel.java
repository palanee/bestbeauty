package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.ServiceData;


public class HelpPanel  {
	
	private JPanel mainPanel;
	
	private ServiceData serviceData;
	private JEditorPane editorPane;
	private JScrollPane editorScrollPane;
	
	private Dimension size;
	
	public HelpPanel(Dimension size) {
		
		serviceData = new ServiceData();
		
		this.size = size;
		
		components();
		//setMainPanel();
		//setEditorPanel();
		
	}
	
	private void components() {
		setMainPanel();
		setEditorPanel();
	}
	
	private void setMainPanel() {

		System.out.println("CurrentCard: " + MainWindow.currentCard);
		
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.RED);
		mainPanel.setSize(ConfigureApp.sizeOfDialogBack);
		
		mainPanel.setLayout(null);
		

	}
	
	public void setTextHtml() {
		
		String url = serviceData.getTextHtml(MainWindow.currentCard);
		editorPane.setText(url);
		System.out.println("URL: " + url);
		
	}
	
	private void setEditorPanel() {
		
		
		
		editorPane = new JEditorPane();
		editorPane.setEditable(false);
		editorPane.setContentType("text/html");
		HTMLEditorKit kit = new HTMLEditorKit();
		editorPane.setEditorKit(kit);
		editorPane.setSize(mainPanel.getWidth(),
						   mainPanel.getHeight()*3);
		
		//editorPane.setSize(mainPanel.getWidth()-2, mainPanel.getHeight()*2);
		
		editorScrollPane = new JScrollPane(editorPane);
		editorScrollPane.setVerticalScrollBarPolicy(
		                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setSize(mainPanel.getSize());
		editorScrollPane.setLocation(0,0);
		editorScrollPane.setBorder(null);
		
		mainPanel.add(editorScrollPane);
	}
	
	public JPanel getMainPanel() {
		
		return mainPanel;
		
	}
	


}
