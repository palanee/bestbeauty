package fait.bb.bf.graphics.panels.glasspanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Formula;
import fait.bb.bf.logic.ServiceData;
import fait.standard.Numbers;


public class FormulaWindow implements FocusListener {
	
	private JPanel mainPanel;
	private JLabel title;
	private JTextArea formulation;
	private JTextField price;
	private JTextField time;
	
	private JButton save;
	private JButton end;
	
	private JTextField form;
	private JTextField pr;
	private JTextField tim;
	
	private Font titleFont = ConfigureApp.font.deriveFont(Font.BOLD, 22);
	private Font labelFont = ConfigureApp.font.deriveFont(Font.BOLD, 16);
	private Font textFont = new Font("Arial", Font.PLAIN, 18);
	
	private Color colorFont = new Color(29,81,113);
	private Color colorLabelFont = new Color(140,141,142);
	private Color colorBackText = new Color(247,246,244);
	private Color colorForeText = colorFont;;
	
	private  Formula formula;
	private  ServiceData service;
	private Communication comm;
	
	private Formula formulaTemp;
	
	private Border notWrite = BorderFactory.createLineBorder(new Color(211,211,211));
	
	private Dimension size;
	
	public FormulaWindow(Dimension size) {
		
		this.size = size;
		
		formula = null;
		formulaTemp = null;
		service = new ServiceData();
		comm = new Communication(ConfigureWindow.getFrame());
		
		setMainPanel();
		
	}
	
	private void setLabel(JLabel lab, String s) {
		
		FontMetrics metrics = new FontMetrics(labelFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		lab.setText(s);
		lab.setFont(labelFont);
		lab.setForeground(colorLabelFont);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		
	}
	
	public void setMainPanel() {
		
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setSize(ConfigureApp.sizeOfDialogBack);
		mainPanel.setLocation(((int) size.getWidth() - mainPanel.getWidth())/2,
							  ((int) size.getHeight() - mainPanel.getHeight())/2);
		mainPanel.setLayout(null);
		
		setPanel();
		
	
	}
	
	private void setPanel() {
		
		String s = "Okno edycji receptury";
		FontMetrics metrics = new FontMetrics(titleFont) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		title = new JLabel();
		title.setText(s);
		title.setFont(titleFont);
		title.setForeground(colorFont);
		title.setBounds((mainPanel.getWidth() - (int) bounds.getWidth())/2,
						 15,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		JLabel formulaEt = new JLabel();
		setLabel(formulaEt, "Receptura");
		
		formulaEt.setLocation(20,
				  title.getY() + title.getHeight() + 10);
		
		formulation = new JTextArea();
		formulation.setBackground(colorBackText);
		formulation.setForeground(colorForeText);
		formulation.setFont(textFont);
		formulation.setFocusable(true);
		formulation.setBorder(BorderFactory.createCompoundBorder(notWrite,
							  BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		formulation.setSize(635,250);
		formulation.setLocation(mainPanel.getWidth()/2 - formulation.getWidth()/2,
								formulaEt.getY() + formulaEt.getHeight() + 3);
		formulation.addFocusListener(this);
		
		formulaEt.setLocation(formulation.getX(), formulaEt.getY());
		
		price = new JTextField();
		price.setBackground(colorBackText);
		price.setForeground(colorForeText);
		price.setFont(textFont);
		price.setFocusable(true);
		price.setBorder(BorderFactory.createCompoundBorder(notWrite,
						BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		price.setSize(140,40);
		price.setLocation(formulation.getX(),
						  formulation.getY() + formulation.getHeight() + 10);
		price.addFocusListener(this);
		
		JLabel priceEt = new JLabel();
		setLabel(priceEt, "z�");
		priceEt.setLocation(price.getX() + price.getWidth() + 5,
							price.getY() + price.getHeight()/2 - priceEt.getHeight()/2);
		
		time = new JTextField();
		time.setBackground(colorBackText);
		time.setForeground(colorForeText);
		time.setFont(textFont);
		time.setFocusable(true);
		time.setBorder(BorderFactory.createCompoundBorder(notWrite,
					   BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		time.setSize(140,40);
		time.setLocation(priceEt.getX() + priceEt.getWidth() + 7,
						 price.getY());
		time.addFocusListener(this);
		
		JLabel timeEt = new JLabel();
		setLabel(timeEt, "min");
		timeEt.setLocation(time.getX() + time.getWidth() + 5,
							time.getY() + time.getHeight()/2 - timeEt.getHeight()/2);
		
		end = new JButtonOwn("but/green", "Anuluj");
		end.setLocation(formulation.getX(),
						mainPanel.getHeight() - 10 - end.getHeight());
		
		save = new JButtonOwn("but/green", "Zapisz i wyjd�");
		save.setLocation(formulation.getX() + formulation.getWidth() - save.getWidth(),
					     end.getY());
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				Numbers numb = new Numbers();
				Boolean goodPrice = true;
				Boolean goodTime = true; 
				
				if(!price.getText().isEmpty() || !price.getText().equals("Cena"))
					goodPrice = numb.isFloatOrInteger(price.getText());
				if(!time.getText().isEmpty() || !time.getText().equals("Czas"))
					goodTime = numb.isInteger(time.getText());
				
				
				if(goodPrice) 
					setGood(price);
				else
					setMistake(price);
				
				if(goodTime)
					setGood(time);
				else
					setMistake(time);
				
				int idTreats = 0;
				
				formulaTemp = new Formula(formulation.getText(), idTreats);
				if(formulaTemp!=null && goodTime && goodPrice && !formulaTemp.equals(formula) ) {
					MainWindow.save.setAnimation(1.0f);
					service.checkFormulaAndSave(formula, formulation.getText());
					formula.setContent(formulation.getText());
					
					tim.setText(time.getText());
					tim.repaint();
					form.setText(formulation.getText());
					form.repaint();
					pr.setText(price.getText());
					pr.repaint();
					
					
				}
				
				if(goodTime && goodPrice) {
					MainWindow.pane.setVisible(false);
					MainWindow.mainGlassPanel.removePanel();
					MainWindow.mainGlassPanel.setVisible(false);
				}
				
			}
		});

		
		
		end.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				Numbers numb = new Numbers();
				Boolean goodPrice = true;
				Boolean goodTime = true; 
				
				if(!price.getText().isEmpty() || !price.getText().equals("Cena"))
					goodPrice = numb.isFloatOrInteger(price.getText());
				if(!time.getText().isEmpty() || !time.getText().equals("Czas"))
					goodTime = numb.isInteger(time.getText());
				
				
				if(goodPrice) 
					setGood(price);
				else
					setMistake(price);
				
				if(goodTime)
					setGood(time);
				else
					setMistake(time);
				
				int idTreats = 0;
				formulaTemp = new Formula(formulation.getText(), idTreats);
				
				if(formulaTemp!=null && goodPrice && goodTime && !formulaTemp.equals(formula)) {
					Boolean yes = comm.youSure("Czy przed wyj�ciem chcesz zapisa� receptur�?", 0);
					if(yes) {
						service.checkFormulaAndSave(formula, formulation.getText());
						formula.setContent(formulation.getText());
						tim.setText(time.getText());
						tim.repaint();
						form.setText(formulation.getText());
						form.repaint();
						pr.setText(price.getText());
						pr.repaint();
						
					}
					MainWindow.pane.setVisible(false);
					MainWindow.mainGlassPanel.removePanel();
					MainWindow.mainGlassPanel.setVisible(false);
					
				}
				else if(goodPrice && goodTime){
					MainWindow.pane.setVisible(false);
					MainWindow.mainGlassPanel.removePanel();
					MainWindow.mainGlassPanel.setVisible(false);
				}
				
				
			}
		});
		
		
		mainPanel.add(title);
		mainPanel.add(formulaEt);
		mainPanel.add(formulation);
		mainPanel.add(price);
		mainPanel.add(priceEt);
		mainPanel.add(time);
		mainPanel.add(timeEt);
		mainPanel.add(save);
		mainPanel.add(end);
		
	}
	
	private void setMistake(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.mistakeWrite,
	   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(new Color(249,203,201));
		
	}
	
	private void setGood(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				notWrite,
				BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(ConfigureApp.additionalFieldColor);
		
	}
	
	public void setFormula(Formula f, JTextField form, JTextField pr, JTextField tim) {
		
		formula = f;
		formulation.setText(formula.getContent());
		
		this.form = form;
		this.pr = pr;
		this.tim = tim;
		
	}
	
	public Formula getFormula() {
		return formula;
	}

	public JPanel getMainPanel() {
		
		return mainPanel;
		
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent textField = (JTextComponent) e.getComponent();
		textField.setBorder(ConfigureApp.setWrite);
		
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent tempField = (JTextComponent) e.getComponent();
		tempField.setBorder(BorderFactory.createCompoundBorder(notWrite,
							BorderFactory.createEmptyBorder(0, 3, 0, 0)));
	}

}
