package fait.bb.bf.graphics.panels.mainpanels;



import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Main;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonStates;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.JCharsOnly;
import fait.bb.bf.graphics.components.JTextOnly;
import fait.bb.bf.graphics.components.ListVisitsCard;
import fait.bb.bf.graphics.components.customUI.ComboBoxRenderer;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.Formula;
import fait.bb.bf.logic.InputDataService;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Treatment;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;
import fait.bb.bf.widgets.WidgetCalendar;


public class CardPanel implements FocusListener {
	
	private JPanel mainPanel;
	private JPanel whitePanel;
	private JButton saveButton;
	
	
	//panel do zarz�dzania podstawowymi danymi klienta
	private JPanel customerPanel;
	
	private JPanel navigatePanel;
	private JButton note;
	private JButton addVisit;
	private JButtonStates informButton;
	private JButtonStates visitButton;
	
	private JPanel panelCustomer;
	private JLabel etNumberCard;
	private JLabel numberCard;
	private JTextComponent name;
	private JTextComponent surname;
	private JTextComponent number;
	
	private JPanel panelAdditional;
	private JTextComponent email;
	private JFormattedTextField birthday;
	private JTextComponent city;
	
	private JPanel panelBody;
	private JTextComponent natural;
	private JTextComponent grey;
	private JTextComponent condition;
	private JPanel panelTodayVisit;
	private JLabel realizeVisit;
	private JButton planVisit;
	private JLabel etDate;
	private JLabel date;
	private JLabel etDuration;
	private JLabel duration;
	private JLabel etTreat;
	private JTextArea treatText;
	
	private JPanel visitPanel;
	private JPanel visitHelpPanel;
	private JPanel visitContentPanel;
	private JComboBox comboUser;
	private JComboBox comboTreat;
	private JLabel labDes;
	private JTextField price;
	private JTextField time;
	private JTextArea formula;
	private String[] tabUser;
	private String[] tabTreat;
	private JPanel historyPanel;
	private JPanel descriptionPanel;
	private JLabel newTreat;
	private JLabel prevTreat;
	private JPanel tabsPanel;
	
	private List<Visit> visits;
	private Visit visit;
	private ServiceData serviceData;
	private String currentCard;
	
	private JLabel worker;
	private static final int widthMainBar = 411;
	private static final int heightMainBar = 38;
	private int startPoint;
	
	private int kindCard;
	private Customer customer;
	
	private List<Treatment> listTreats;
	private List<Treatment> comboListTreat;
	private List<ListVisitsCard> listVisitCards;
	private Border notWrite;
	private Boolean isSave;
	private int countKey;
	private int countTreats;
	private int countVCard;
	
	private Color contentColor = new Color(174,161,145);
	private Color topColor = new Color(136,53,131);
	private Color contentColors = new Color(149,138,124);

	private Boolean isQuestion;
	private Communication comm;
	
	private Boolean isGoodNumber;
	
	public CardPanel(int kindCard, Customer c) {
		
		
		serviceData = new ServiceData();
		comm = new Communication(MainWindow.getFrame());
		
		this.kindCard = kindCard;
		this.customer = c;
		
		
		isSave = false;
		isQuestion = false;
		isGoodNumber = true;
		countKey = 0;
		countVCard = 0;
		new ArrayList<Visit>();
		//lista do przechowywania zabieg�w zdefiniowanych przez u�ytkownika/zmodyfikowanych
		listTreats = new ArrayList<Treatment>();
		new ArrayList<Treatment>();
		listVisitCards = new ArrayList<ListVisitsCard>();
		
		if(c!=null) {
			//visits = serviceData.getListVisit(c.getId(), 9);
			visit = serviceData.getNextVisit(c.getId());
			
		}
		else
			visits = new ArrayList<Visit>();
		
		setMainPanel();
		setAllPanels();
		checkComponents();
		
	}

	public CardPanel() {
		
	}


	private void setMainPanel() {
		
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.setIgnoreRepaint(true);
		
		setBorder();
		setStartPoint();
		
		InputMap inputMap = new JPanel().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), "saveCustomer");
		
		//inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK), "closeTab");
		
		mainPanel.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
		mainPanel.getActionMap().put("saveCustomer", new AbstractAction() {
             public void actionPerformed(ActionEvent e) {
            	 addSaveAction();
             }
         });
		
		navigatePanel = new JPanel();
		navigatePanel.setLayout(null);
		navigatePanel.setOpaque(false);
		navigatePanel.setSize(MainWindow.cardPanel.getSize().width,
							  60);
		navigatePanel.setLocation(0, 10);
		mainPanel.add(navigatePanel);
		
		
		
		whitePanel = new JPanel();
		whitePanel.setLayout(new CardLayout());
		whitePanel.setBackground(ConfigureApp.backgroundColor);
		whitePanel.setIgnoreRepaint(true);
		whitePanel.setSize(navigatePanel.getWidth(), 
						   MainWindow.cardPanel.getSize().height - 10 - navigatePanel.getHeight() - 100);
		whitePanel.setLocation(0,navigatePanel.getY() + navigatePanel.getHeight());
		mainPanel.add(whitePanel);
		
	}
	
	//ustawienie obramowania karty
	private void setBorder() {
		
		JLabel lab = new JLabel();
		lab.setName("lab");
		lab.setBackground(ConfigureApp.backgroundCardColor);
		lab.setSize(MainWindow.cardPanel.getSize().width, 10);
		lab.setLocation(0,0);
		lab.setOpaque(true);
		mainPanel.add(lab);
		
	}
	
	private void setStartPoint() {
		
		if(MainWindow.cardPanel.getWidth()<920)
			startPoint = 0;
		else 
			startPoint = (MainWindow.cardPanel.getWidth() - 80)/2 - widthMainBar;
		
		
	}
	
	private void setAllPanels() {
		
		
		
		customerPanel = new JPanel();
		customerPanel.setLayout(null);
		customerPanel.setOpaque(false);
		customerPanel.setName("customerPanel");
		customerPanel.setSize(whitePanel.getWidth(), whitePanel.getHeight());
		customerPanel.setLocation(0,0);
		currentCard = customerPanel.getName();
		whitePanel.add(customerPanel, customerPanel.getName());
		setCustomerPanel();
		setSaveButton();
		
		visitPanel = new JPanel();
		visitPanel.setLayout(null);
		visitPanel.setOpaque(false);
		visitPanel.setName("visitPanel");
		visitPanel.setSize(whitePanel.getWidth(), whitePanel.getHeight());
		visitPanel.setLocation(0,0);
		whitePanel.add(visitPanel, visitPanel.getName());
		
		
	}
	
	//ustawienie panel do zarz�dzania podstawowymi danymi klienta i jego wizytami
	private void setCustomerPanel() {
		
		Color topColor = new Color(136,53,131);
		Color contentColor = new Color(149,138,124);
		
		setDataCustomer(topColor, contentColor);
		setAddData(topColor, contentColor);
		setPanelBody(topColor, contentColor);
		//setVisitsPanel(topColor, contentColor);
		setTodayVisit(topColor, contentColor);
		setTextField();
		setNavigatePanel();
	}
	
	private void setActiveField(Boolean isActive) {
		
		number.setEnabled(isActive);
		city.setEnabled(isActive);
		birthday.setEnabled(isActive);
		email.setEnabled(isActive);
		condition.setEnabled(isActive);
		natural.setEnabled(isActive);
		grey.setEnabled(isActive);
		
	}
	
	private void checkComponents() {
		
		if(customer==null) {
			
			note.setEnabled(false);
			visitButton.setEnabled(false);
			addVisit.setEnabled(false);
			numberCard.setVisible(false);
			etNumberCard.setVisible(false);
			planVisit.setEnabled(false);
			//saveButton.setEnabled(false);
			//setActiveField(false);
		}
		else {
			
			setVisitPanel();
		}
		
		if(customer==null || visit==null) {
			realizeVisit.setVisible(false);
			etDate.setVisible(false);
			date.setVisible(false);
			etDuration.setVisible(false);
			duration.setVisible(false);
			etTreat.setVisible(false);
			treatText.setVisible(false);
			planVisit.setVisible(true);
			
		}
		else if(visit!=null) {
			
			realizeVisit.setVisible(true);
			setNextVisit();
			
			
			
		}
		

		
	}
	
	private void setNextVisit() {
		
		etDate.setVisible(true);
		date.setVisible(true);
		date = setLabelContent(date, visit.getDateText());
		etDuration.setVisible(true);
		duration.setVisible(true);
		duration = setLabelContent(duration, Integer.toString(visit.getDuration()));
		etTreat.setVisible(true);
		treatText.setVisible(true);
		planVisit.setVisible(false);
		
		List<Treatment> treats =  visit.getListTreatment();
		if(!treats.isEmpty()) {
			treatText.setText(treats.toString());
		}
		
	}
	
	private void setNavigatePanel() {
		
		note = new JButtonOwn("note");
		note.setLocation(navigatePanel.getWidth() - note.getWidth() - 20,
						 10);
		navigatePanel.add(note);
		
		note.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				MainWindow.notePanel.paintNote(customer);
			}
		});
		
		addVisit = new JButtonOwn("addNewVisit");
		addVisit.setLocation(note.getX() - 5 - addVisit.getWidth(),
							 note.getY());
		navigatePanel.add(addVisit);
		
		addVisit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				countTreats = 0;
				listTreats = new ArrayList<Treatment>();
				visit = null;
				if(currentCard.equals(customerPanel.getName())) {
					
					informButton.changeState(false);
					visitButton.changeState(true);
					saveButton.setEnabled(true);
					currentCard = visitPanel.getName();
					((CardLayout) whitePanel.getLayout()).show(whitePanel, visitPanel.getName());
				}
				if(visitContentPanel!=null) {
					visitContentPanel.setVisible(true);
					setUserVisit();
					setContentVisit();
					setTreatTabs();
				}
				else
					setVisitPanel();
				
			}
		});
		
		informButton = new JButtonStates("but/information", true);
		informButton.setLocation(panelCustomer.getX(), note.getY());
		navigatePanel.add(informButton);
		
		informButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				informButton.changeState(true);
				visitButton.changeState(false);
				currentCard = customerPanel.getName();
				((CardLayout) whitePanel.getLayout()).show(whitePanel, customerPanel.getName());
			}
		});
		
		visitButton = new JButtonStates("but/visits", false);
		visitButton.setLocation(informButton.getX() + informButton.getWidth(),
								informButton.getY());
		navigatePanel.add(visitButton);
		
		visitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				informButton.changeState(false);
				visitButton.changeState(true);
				saveButton.setEnabled(true);
				currentCard = visitPanel.getName();
				((CardLayout) whitePanel.getLayout()).show(whitePanel, visitPanel.getName());
			}
		});
		
	}
	
	private void setTextField() {
		
		if(kindCard==0) {
			System.out.println("Uzupe�niamy wszystkie dane");
			name.setText(customer.getName());
			surname.setText(customer.getSurname());
			number.setText(customer.getNumber());
			email.setText(customer.getEmail());
			
			if(customer.getBirthday()!=null && !customer.getBirthday().isEmpty()) {
				birthday.setText(customer.getBirthday());
				birthday.setEditable(false);
			}
			
			city.setText(customer.getCity());
			condition.setText(customer.getCondition());
			grey.setText(customer.getGrey());
			natural.setText(customer.getNatural());
			isSave = true;
		}
			
		
	}
	
	private void setDataCustomer(Color color, Color contentColor) {
		
		int height = 0;
		int spaceUp = 28;
		int spaceDown = 35;
		int space = 16;
		int spaceBefore = 22;
		
		notWrite = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(contentColor),
				   BorderFactory.createEmptyBorder(0, 3, 0, 0));
		
		panelCustomer = new JPanel();
		panelCustomer.setLayout(null);
		panelCustomer.setOpaque(false);
		panelCustomer.setLocation(startPoint, 2);
		
		customerPanel.add(panelCustomer);
		
		JLabel labCustomer = new JLabel();	
		labCustomer = setBar(labCustomer, color, "Informacje podstawowe");
		labCustomer.setLocation(0, 0);
		panelCustomer.add(labCustomer);
		
		height += spaceUp + labCustomer.getHeight() + spaceDown;
		
		numberCard = new JLabel();
		String s = "";
		if(customer!=null) 
			s = InputDataService.formatCustomerID(customer.getId());
		numberCard = setLabelTitle(numberCard, s, contentColor);
		numberCard.setSize(261, numberCard.getHeight());
		numberCard.setLocation(labCustomer.getX() + labCustomer.getWidth() - numberCard.getWidth(),
							   labCustomer.getY() + labCustomer.getHeight() + spaceUp);
		panelCustomer.add(numberCard);
		
		etNumberCard = new JLabel();
		etNumberCard = setLabelTitle(etNumberCard, "nr klienta", contentColor);
		etNumberCard.setLocation(numberCard.getX() - spaceBefore - etNumberCard.getWidth(),
				   				 numberCard.getY());
		panelCustomer.add(etNumberCard);
		
		height += numberCard.getHeight() + space;
		
		name = new JTextField();
		//pole obowi�zkowe, wi�c ramka pogrubiona
		name = setTextField(name, contentColor, true);
		name.setLocation(numberCard.getX(), 
			 			 numberCard.getY() + numberCard.getHeight() + space);
		name.setDocument(new JCharsOnly(name, panelCustomer));
		name.setName("name");
		panelCustomer.add(name);
		
		JLabel etName = new JLabel();
		etName = setLabelTitle(etName, "imi� *", contentColor);
		etName.setLocation(name.getX() - spaceBefore - etName.getWidth(),
						   name.getY() + name.getHeight()/2 - etName.getHeight()/2);
		panelCustomer.add(etName);
		
		height += name.getHeight() + space;
		
		surname = new JTextField();
		surname = setTextField(surname, contentColor, true);
		surname.setLocation(name.getX(), 
						 	name.getY() + name.getHeight() + space);
		surname.setDocument(new JCharsOnly(surname, panelCustomer));
		surname.setName("surname");
		surname.addKeyListener(new KeyAdapter() {
			
			public void keyTyped(KeyEvent e) {
				
				countKey++;
				if(countKey==3) {
					saveButton.setEnabled(true);
					//setActiveField(true);
				}
				
			}
			
		});
		panelCustomer.add(surname);
		
		JLabel etSurname = new JLabel();
		etSurname = setLabelTitle(etSurname, "nazwisko *", contentColor);
		etSurname.setLocation(surname.getX() - spaceBefore - etSurname.getWidth(),
						   	  surname.getY() + surname.getHeight()/2 - etSurname.getHeight()/2);
		panelCustomer.add(etSurname);
		
		height += surname.getHeight() + space;
		
		number = new JTextField();
		//pole opcjonalne, wi�c false
		number = setTextField(number, contentColor, false);
		number.setLocation(surname.getX(), 
						   surname.getY() + surname.getHeight() + space);
		number.setDocument(new JTextOnly(number, panelCustomer, 15));
		number.setName("number");
		panelCustomer.add(number);
		
		JLabel etNumber = new JLabel();
		etNumber = setLabelTitle(etNumber, "telefon", contentColor);
		etNumber.setLocation(number.getX() - spaceBefore - etNumber.getWidth(),
						     number.getY() + number.getHeight()/2 - etNumber.getHeight()/2);
		panelCustomer.add(etNumber);
		
		height += number.getHeight();
		
		panelCustomer.setSize(widthMainBar, height);
		
	}
	
	private void setAddData(Color color, Color contentColor) {
		
		int height = 0;
		int spaceUp = 28;
		int spaceDown = 35;
		int space = 16;
		int spaceBefore = 22;
		
		panelAdditional = new JPanel();
		panelAdditional.setLayout(null);
		panelAdditional.setOpaque(false);
		panelAdditional.setLocation(panelCustomer.getX(), 
									panelCustomer.getY() + panelCustomer.getHeight() + 10);
		
		customerPanel.add(panelAdditional);
		
		JLabel labAdditional = new JLabel();	
		labAdditional = setBar(labAdditional, color, "Informacje dodatkowe");
		labAdditional.setLocation(0, 0);
		panelAdditional.add(labAdditional);
		
		height += spaceUp + labAdditional.getHeight() + spaceDown;
		
		MaskFormatter mask = null;
        try {
            //
            // Create a MaskFormatter for accepting phone number, the # symbol accept
            // only a number. We can also set the empty value with a place holder
            // character.
            //
            mask = new MaskFormatter("##-##-####");
            mask.setPlaceholderCharacter('_');
           
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		
		email = new JTextField();
		email = setTextField(email, contentColor, false);
		email.setLocation(labAdditional.getX() + labAdditional.getWidth() - email.getWidth(), 
				 		  labAdditional.getY() + labAdditional.getHeight() + spaceUp);
		email.setName("email");
		panelAdditional.add(email);
		
		JLabel etEmail = new JLabel();
		etEmail = setLabelTitle(etEmail, "e-mail", contentColor);
		etEmail.setLocation(email.getX() - spaceBefore - etEmail.getWidth(),
						    email.getY() + email.getHeight()/2 - etEmail.getHeight()/2);
		panelAdditional.add(etEmail);
		
		height += email.getHeight() + space;
		
		city = new JTextField();
		city = setTextField(city, contentColor, false);
		city.setLocation(email.getX(), 
						 email.getY() + email.getHeight() + space);
		city.setDocument(new JCharsOnly(city, panelAdditional));
		city.setName("city");
		panelAdditional.add(city);
		
		JLabel etCity = new JLabel();
		etCity = setLabelTitle(etCity, "miejscowo��", contentColor);
		etCity.setLocation(city.getX() - spaceBefore - etCity.getWidth(),
						   city.getY() + city.getHeight()/2 - etCity.getHeight()/2);
		panelAdditional.add(etCity);
		
		height += city.getHeight();
		
		birthday = new JFormattedTextField(mask);
		birthday.setBackground(ConfigureApp.backgroundColor);
		birthday.setSize(110,32);
		birthday.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(contentColor),
				  BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		birthday.setFont(new Font("Arial", Font.PLAIN, 20));
		birthday.setForeground(contentColor);
		birthday.addFocusListener(this);
		birthday.setLocation(city.getX(), 
							 city.getY() + city.getHeight() + space);
		//birthday.setDocument(new JTextOnly());
		birthday.setName("birthday");
		birthday.setFocusable(true);
		panelAdditional.add(birthday);
		
		JLabel etBirthday = new JLabel();
		etBirthday = setLabelTitle(etBirthday, "data urodzin", contentColor);
		etBirthday.setLocation(birthday.getX() - spaceBefore - etBirthday.getWidth(),
							   birthday.getY() + birthday.getHeight()/2 - etBirthday.getHeight()/2);
		panelAdditional.add(etBirthday);
		
		height += birthday.getHeight() + space;
		
		panelAdditional.setSize(widthMainBar, height);
	}
	
	private void setPanelBody(Color color, Color contentColor) {
		
		int height = 0;
		int spaceUp = 28;
		int spaceDown = 35;
		int space = 16;
		int spaceBefore = 22;
		
		panelBody = new JPanel();
		panelBody.setLayout(null);
		panelBody.setOpaque(false);
		panelBody.setLocation(panelCustomer.getX() + panelCustomer.getWidth() + 80, 
			    			  panelCustomer.getY());
		
		customerPanel.add(panelBody);
		
		JLabel labBody = new JLabel();	
		labBody = setBar(labBody, color, "Stan w�os�w");
		labBody.setLocation(0, 0);
		panelBody.add(labBody);
		
		height += spaceUp + labBody.getHeight() + spaceDown;
		
		natural = new JTextField();
		natural = setTextField(natural, contentColor, false);
		natural.setLocation(labBody.getX() + labBody.getWidth() - natural.getWidth(), 
							 labBody.getY() + labBody.getHeight() + spaceUp);
		natural.setName("natural");
		panelBody.add(natural);
		
		JLabel etNatural = new JLabel();
		etNatural = setLabelTitle(etNatural, "naturalne", contentColor);
		etNatural.setLocation(natural.getX() - spaceBefore - etNatural.getWidth(),
							   natural.getY() + natural.getHeight()/2 - etNatural.getHeight()/2);
		panelBody.add(etNatural);
		
		height += natural.getHeight() + space;
		
		grey = new JTextField();
		grey = setTextField(grey, contentColor, false);
		grey.setLocation(natural.getX(), 
						 natural.getY() + natural.getHeight() + space);
		grey.setName("grey");
		panelBody.add(grey);
		
		JLabel etGrey = new JLabel();
		etGrey = setLabelTitle(etGrey, "siwe", contentColor);
		etGrey.setLocation(grey.getX() - spaceBefore - etGrey.getWidth(),
							grey.getY() + grey.getHeight()/2 - etGrey.getHeight()/2);
		panelBody.add(etGrey);
		
		height += grey.getHeight() + space;
		
		condition = new JTextField();
		condition = setTextField(condition, contentColor, false);
		condition.setLocation(grey.getX(), 
						      grey.getY() + grey.getHeight() + space);
		condition.setName("condition");
		panelBody.add(condition);
		
		JLabel etCondition = new JLabel();
		etCondition = setLabelTitle(etCondition, "kondycja", contentColor);
		etCondition.setLocation(condition.getX() - spaceBefore - etCondition.getWidth(),
								condition.getY() + condition.getHeight()/2 - etCondition.getHeight()/2);
		panelBody.add(etCondition);
		
		height += condition.getHeight();
		
		panelBody.setSize(widthMainBar, height);
	}
	
	
	
	private JTextComponent setTextField(JTextComponent textField, Color color, Boolean isBold) {
		
		textField.setBackground(ConfigureApp.backgroundColor);
		textField.setSize(261,32);
		if(!isBold)
				  textField.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
				  BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		else 
			textField.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(
								2, 2, 2, 2, color),
								BorderFactory.createEmptyBorder(0, 3, 0, 3)));
		textField.setFont(new Font("Arial", Font.PLAIN, 20));
		textField.setForeground(color);
		textField.addFocusListener(this);
		textField.setFocusable(true);
		textField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(isSave)
					saveButton.setEnabled(true);
			}
		});
		
		return textField;
		
	}
	
	private JLabel setLabelTitle(JLabel lab, String s, Color color) {
		
		//Font font = ConfigureApp.font.deriveFont(Font.BOLD, 20);
		Font font = new Font("Arial", Font.PLAIN, 18);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setForeground(color);
		lab.setFont(font);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setText(s);
		
		
		return lab;
		
	}
	
	private JLabel setLabelContent(JLabel lab, String s, Color color) {
		
		Font font = new Font("Arial", Font.BOLD, 17);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setForeground(color);
		lab.setFont(font);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setText(s);
		
		
		return lab;
		
	}
	
	private JLabel setLabelContent(JLabel lab, String s) {
		
		Font font = new Font("Arial", Font.BOLD, 17);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setFont(font);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setText(s);
		
		lab.revalidate();
		lab.repaint();
		
		return lab;
		
	}
	
	private void setVisitPanel() {
		
		Color topColor = new Color(136,53,131);
		Color contentColor = new Color(149,138,124);
		
		setHistoryPanel(topColor, contentColor);
		setDescriptionPanel(topColor, contentColor);
	}
	
	private void setHistoryPanel(Color topColor, Color contentColor) {
		
		int height = 0;
		int spaceUp = 28;
		int spaceDown = 35;
		
		historyPanel = new JPanel();
		historyPanel.setLayout(null);
		historyPanel.setOpaque(false);
		historyPanel.setLocation(startPoint, 2);
		
		visitPanel.add(historyPanel);
		
		JLabel labHistory = new JLabel();	
		labHistory = setBar(labHistory, topColor, "Wizyty");
		labHistory.setLocation(0, 0);
		historyPanel.add(labHistory);
		
		height += spaceUp + labHistory.getHeight() + spaceDown;
		

		visits = serviceData.getListVisit(customer.getId(), 9);
		int y = labHistory.getY() + labHistory.getHeight() + 35;
		
		if(visits.size()>0) {
			int i = 0;
			//setLabelsVisits(contentColor);
			for(Visit v : visits) {
				ListVisitsCard temp = new ListVisitsCard(v, labHistory.getWidth(), contentColor, this, i);
				listVisitCards.add(temp);
				temp.setLocation(0, y);
				System.out.println("Temp: " + temp);
				historyPanel.add(temp);
				y = y + temp.getHeight() + 1;
				height += temp.getHeight() + 1;
				
				if(i==0) {
					setBarDescription(temp.getWidthData(), temp.getWidthHour(), 
									  temp.getWidthCode(), contentColor, 
									  labHistory.getY() + labHistory.getHeight() + 20);
				}
				
				i++;
			}
			
			
		}
		
		historyPanel.setSize(labHistory.getWidth(), height);
		historyPanel.repaint();
	}
	
	public void setPlainBorderVisit(int i) {
		
		int j = 0;
		while(j<listVisitCards.size()) {
			if(j!=i) 
				listVisitCards.get(j).setPlainBorder();
			
			j++;
		}
		
	}
	
	private void setBarDescription(int widthData, int widthHour, int widthCode, Color color, int y) {
		
		String s = "Data";
		
		Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 14);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		JLabel labDate = new JLabel(s);
		labDate.setFont(font);
		labDate.setForeground(color);
		labDate.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		labDate.setLocation(widthData/2 - labDate.getWidth()/2, y);
		historyPanel.add(labDate);
		
		s = "Godzina";
		bounds = metrics.getStringBounds(s, null);
			
		JLabel labHour = new JLabel(s);
		labHour.setFont(font);
		labHour.setForeground(color);
		labHour.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		labHour.setLocation(widthData + 1 + (widthHour/2 - labHour.getWidth()/2), y);
		historyPanel.add(labHour);
		
		s = "Kod";
		bounds = metrics.getStringBounds(s, null);
		
		JLabel labCode = new JLabel(s);
		labCode.setFont(font);
		labCode.setForeground(color);
		labCode.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		labCode.setLocation(widthData + widthHour + 2 + (widthCode/2 - labCode.getWidth()/2), y);
		historyPanel.add(labCode);
		
	}
	 
	private void setDescriptionPanel(Color topColor, final Color contentColor) {
		
		int height = 0;
		int spaceUp = 28;
		int spaceDown = 35;
		int space = 10;
		int spaceBefore = 22;
		int spacee = 10;
		
		descriptionPanel = new JPanel();
		descriptionPanel.setLayout(null);
		descriptionPanel.setOpaque(false);
		descriptionPanel.setLocation(historyPanel.getX() + historyPanel.getWidth() + 80, 
			    			  		 historyPanel.getY());
		
		visitPanel.add(descriptionPanel);
		
		labDes = new JLabel();	
		labDes = setBar(labDes, topColor, "Opis (z)realizowanej wizyty");
		labDes.setLocation(0, 0);
		descriptionPanel.add(labDes);
		
		height += spaceUp + labDes.getHeight() + spaceDown;
		
		visitHelpPanel = new JPanel();
		visitHelpPanel.setLayout(null);
		visitHelpPanel.setOpaque(false);
		visitHelpPanel.setLocation(0, labDes.getY() + labDes.getHeight());
		visitHelpPanel.setSize(labDes.getWidth(), 1);
		
		height += visitHelpPanel.getHeight();
		
		descriptionPanel.add(visitHelpPanel);
		
		tabsPanel = new JPanel();
		tabsPanel.setLayout(null);
		tabsPanel.setOpaque(false);
		tabsPanel.setSize(labDes.getWidth(), 20);
		
		height += tabsPanel.getHeight();
		
		descriptionPanel.add(tabsPanel);
		
		visitContentPanel = new JPanel();
		visitContentPanel.setLayout(null);
		visitContentPanel.setBackground(new Color(241,232,220));
		
		visitContentPanel.setSize(labDes.getWidth(), 1);
		
		height += visitContentPanel.getHeight() + 2;
		
		descriptionPanel.add(visitContentPanel);
		
		descriptionPanel.setSize(labDes.getWidth(), height);
		
		
		
	}
	
	private void setUserVisit() {
		
		int height = 0;
		int spaceUp = 24;
		int space = 10;
		
		if(visitHelpPanel!=null)
			visitHelpPanel.removeAll();
		
		JLabel etUser = new JLabel();
		etUser = setLabelToday(etUser, "Wybierz pracownika:", contentColor);
		etUser.setLocation(1, spaceUp);
		visitHelpPanel.add(etUser);
		
		height += etUser.getHeight() + spaceUp;
		
		tabUser = serviceData.parseToString(serviceData.getUsers(" AND status!='ZWOLNIONY' "));
		
		comboUser = new JComboBox(tabUser);
		comboUser = setComboBox(comboUser, contentColor);
		comboUser.setLocation(etUser.getX(), etUser.getY() + etUser.getHeight() + space);
		visitHelpPanel.add(comboUser);
		
		height += comboUser.getHeight() + space;
		
		visitHelpPanel.setSize(labDes.getWidth(), height);
		descriptionPanel.setSize(labDes.getWidth(), descriptionPanel.getHeight() + visitHelpPanel.getHeight());
		visitHelpPanel.revalidate();
		descriptionPanel.revalidate();
		
	}
	
	private void setTreatTabs() {
		
		if(listTreats.size()>1) {
			
			JLabel line = new JLabel();
			line.setOpaque(true);
			line.setBackground(contentColor);
			line.setSize(labDes.getWidth(), 1);
			line.setLocation(0, tabsPanel.getHeight()-line.getHeight());
			tabsPanel.add(line);
			
			int i = 0;
			int x = -1;
			while(i<listTreats.size()) {
				
				JButtonWithout button = new JButtonWithout("rect");
				button.setLocation(x, line.getY() - button.getHeight());
				button.setName("" + i);
				tabsPanel.add(button);
				x += button.getWidth()+1;
				i++;
			}
			
		}
		
		tabsPanel.setLocation(visitHelpPanel.getX(), visitHelpPanel.getHeight() + visitHelpPanel.getY() + 5);
		
	}
	
	private void setContentVisit() {
		
		int height = 25;
		int spacee = 10;
		int space = 10;
		
		visitContentPanel.removeAll();
		
		visitContentPanel.setLocation(0,visitHelpPanel.getY() + visitHelpPanel.getHeight() + height);
		
		JLabel etTreat = new JLabel();
		etTreat = setLabelToday(etTreat, "Wybierz zabieg:", contentColor);
		etTreat.setLocation(comboUser.getX(), spacee);
		visitContentPanel.add(etTreat);
		
		height += etTreat.getHeight() + spacee;
		
		tabTreat = serviceData.getStringTreat();
		comboListTreat = serviceData.getTreatments();
		
		comboTreat = new JComboBox(tabTreat);
		comboTreat = setComboBox(comboTreat, contentColor);
		comboTreat.setLocation(etTreat.getX(), etTreat.getY() + etTreat.getHeight() + space);
		comboTreat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Treatment t = comboListTreat.get(comboTreat.getSelectedIndex());
				price.setText(Float.toString(t.getPrice()));
				time.setText(Integer.toString(t.getTime()));
			}
		});
		
		visitContentPanel.add(comboTreat);
		
		height += comboTreat.getHeight() + space;
		
		JLabel etPrice = new JLabel();
		etPrice = setLabelToday(etPrice, "Cena:", contentColor);
		etPrice.setLocation(comboTreat.getX(),
							comboTreat.getY() + comboTreat.getHeight() + spacee);
		visitContentPanel.add(etPrice);
		
		height += etPrice.getHeight() + spacee;
		
		price = new JTextField();
		price = (JTextField) setTextField(price, contentColor, false);
		price.setSize(60, comboTreat.getHeight());
		price.setLocation(etPrice.getX(), etPrice.getY() + etPrice.getHeight() + space);
		price.setForeground(contentColor);
		price.setName("price");
		price.setFocusable(true);
		
		visitContentPanel.add(price);
		
		height += price.getHeight() + space;
		
		JLabel etLabelPrice = new JLabel();
		etLabelPrice = setLabelToday(etLabelPrice, "z�", contentColor);
		etLabelPrice.setLocation(price.getX() + price.getWidth() + 10,
							price.getY() + (price.getHeight()/2 - etLabelPrice.getHeight()/2));
		visitContentPanel.add(etLabelPrice);
		
		JLabel etTime = new JLabel();
		etTime = setLabelToday(etTime, "Czas:", contentColor);
		etTime.setLocation(price.getX() + price.getWidth() + 44,
							etPrice.getY());
		visitContentPanel.add(etTime);
		
		time = new JTextField();
		time = (JTextField) setTextField(time, contentColor, false);
		time.setSize(60, comboTreat.getHeight());
		time.setLocation(etTime.getX(), price.getY());
		time.setForeground(contentColor);
		time.setName("time");
		time.setFocusable(true);
		visitContentPanel.add(time);
		
		JLabel etLabelTime = new JLabel();
		etLabelTime = setLabelToday(etLabelTime, "min", contentColor);
		etLabelTime.setLocation(time.getX() + time.getWidth() + 10,
								time.getY() + (time.getHeight()/2 - etLabelTime.getHeight()/2));
		visitContentPanel.add(etLabelTime);
		
		JLabel etFormula = new JLabel();
		etFormula = setLabelToday(etFormula, "Wpisz receptur�/uwagi:", contentColor);
		etFormula.setLocation(price.getX(),
							  price.getY() + price.getHeight() + spacee);
		visitContentPanel.add(etFormula);
		
		height += etFormula.getHeight() + spacee;
		
		formula = new JTextArea();
		formula.setSize(labDes.getWidth()-2, 55);
		formula.setForeground(contentColor);
		formula.setBackground(ConfigureApp.backgroundColor);
		formula.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(contentColor),
				  		  BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		formula.setWrapStyleWord(true);
		formula.setLineWrap(true);
		formula.setFont(new Font("Arial", Font.BOLD, 18));
		formula.setLocation(etFormula.getX(), etFormula.getY() + etFormula.getHeight() + space);
		formula.setName("formula");
		formula.setFocusable(true);
		formula.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
            	
            	if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    if (e.getModifiers() > 0) {
                    	formula.transferFocusBackward();
                    } else {
                    	formula.transferFocus();
                    }
                    e.consume();
                }
                
                
                
            }
        });
		visitContentPanel.add(formula);
		
		height += formula.getHeight() + space;
		
		newTreat = new JLabel();
		String s = "";
		
		if(listTreats.size()<=1)
			s = "Dodaj kolejny zabieg >>";
		else
			s = "Zobacz kolejny zabieg >>";
		
		setLabelToday(newTreat, s, contentColor);
		newTreat.setLocation(labDes.getWidth() - newTreat.getWidth(),
							 formula.getY() + formula.getHeight() + space);
		visitContentPanel.add(newTreat);
		
		height += newTreat.getHeight() + space;
		
		prevTreat = new JLabel();
		setLabelToday(prevTreat, "<< Poprzedni zabieg", contentColor);
		prevTreat.setLocation(0,
							  newTreat.getY());
		prevTreat.setVisible(false);
		visitContentPanel.add(prevTreat);
		
		
		
		newTreat.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				newTreat.setForeground(contentColor);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				newTreat.setForeground(new Color(155,88,2));
				newTreat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				//listVisit.add(new Visit());
				if(comboTreat.getSelectedIndex()!=0) {
					setListTreats();
					
					countTreats++;
					setContentVisit();
					setVisit();
				
					System.out.println("Wielko�� listy: " + listTreats.size() + "; wielko�� count: " + countTreats);
					if(countTreats<listTreats.size()-1) {
						newTreat.setText("Zobacz kolejny zabieg >>");
						System.out.println("Zobacz kolejny zabieg");
					}
					else {
						newTreat.setText("Dodaj kolejny zabieg >>");
						System.out.println("Dodaj kolejny zabieg");
					}
					
					if(countTreats==0)
						prevTreat.setVisible(false);
					else
						prevTreat.setVisible(true);
				}
				
			}
		});
		
		prevTreat.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				prevTreat.setForeground(contentColor);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				prevTreat.setForeground(new Color(155,88,2));
				prevTreat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				//listVisit.add(new Visit());
				setListTreats();
				if(countTreats>0)
					countTreats--;
				setContentVisit();
				setVisit();
				if(countTreats>=listTreats.size()) {
					newTreat.setText("Dodaj kolejny zabieg");
				}
				else {
					newTreat.setText("Zobacz kolejny zabieg");
				}
				if(countTreats==0)
					prevTreat.setVisible(false);
				else
					prevTreat.setVisible(true);
					
				
			}
		});
		
		visitContentPanel.setSize(labDes.getWidth(), height);
		descriptionPanel.setSize(labDes.getWidth(), descriptionPanel.getHeight() + visitContentPanel.getHeight());
		visitContentPanel.revalidate();
		descriptionPanel.revalidate();
		
	}
	
	public void setVisit() {
		
		if(countTreats<listTreats.size()) {
			
			fillContentPanel(listTreats.get(countTreats));
			
		}
		
	}
	
	public void realizeVisit(Visit v) {
		
		visitContentPanel.setVisible(true);
		
		countTreats = 0;
		
		saveButton.setEnabled(true);
		setUserCombo(v.getUser());
		setTreatCombo(v.getTreatment());
	
		listTreats = v.getAllTreatments();
		Formula f = serviceData.getFormula(listTreats.get(countTreats).getId());
		
		price.setText(Float.toString(f.getPrice()));
		time.setText(Integer.toString(f.getTime()));
		
	}
	
	public void realizeVisit(Visit v, int counter) {

		visit = v;
		this.countVCard = counter;
		listTreats = v.getAllTreatments();
		visitHelpPanel.removeAll();
		visitContentPanel.removeAll();
		
		countTreats = 0;
		
		saveButton.setEnabled(true);
		
		setUserVisit();
		setUserCombo(v.getUser());
		
		setContentVisit();
		setTreatCombo(v.getTreatment());
	
		listTreats = v.getAllTreatments();
		Treatment t = listTreats.get(countTreats);
		Formula f = serviceData.getFormula(t.getRelation());
		
		if(f==null) {
			f = new Formula(t.getPrice(), t.getTime());
		}
		
		if(f.getPrice()!=null && f.getPrice()!=0) {
			price.setText(Float.toString(f.getPrice()));
			price.setEnabled(false);
		}
		
		if(f.getTime()!=null && f.getTime()!=0) {
			time.setText(Integer.toString(f.getTime()));
			time.setEnabled(false);
		}
		
		visitHelpPanel.repaint();
		visitContentPanel.repaint();
		
	}
	
	public void fillContentPanel(Treatment t) {
		
		
		
		
		saveButton.setEnabled(true);
		
		setContentVisit();
		setTreatCombo(t);
	
		Formula f = serviceData.getFormula(t.getRelation());
		
		if(f==null) {
			f = new Formula(t.getPrice(), t.getTime());
		}
		
		if(f.getPrice()!=null && f.getPrice()!=0) {
			price.setText(Float.toString(f.getPrice()));
		}
		
		if(f.getTime()!=null && f.getTime()!=0) {
			time.setText(Integer.toString(f.getTime()));
		}
		
		if(!f.getContent().isEmpty())
			formula.setText(f.getContent());
		
		visitHelpPanel.repaint();
		visitContentPanel.repaint();
		
	}
	
	public void editVisit(Visit v, int countCard) {
		
	
		
		visit = v;
		this.countVCard = countCard;
		listTreats.clear();
		listTreats = v.getAllTreatments();
		visitHelpPanel.removeAll();
		visitContentPanel.removeAll();
		visitContentPanel.setVisible(true);
		
		setUserVisit();
		setUserCombo(v.getUser());
		
		setContentVisit();
		
		System.out.println("Wizyta edytowana: " + v.getId());
		System.out.println("Lista zabieg�w: " + listTreats);
		
		Treatment t = listTreats.get(countTreats);
		
		setTreatCombo(t);
		comboTreat.setEnabled(false);
		
		Formula f = t.getFormula();
		if(f==null) {
			f = new Formula(t.getPrice(), t.getTime());
		}
		
		if(f.getPrice()!=null && f.getPrice()!=0) {
			price.setText(Float.toString(f.getPrice()));
			price.setEnabled(false);
		}
		
		if(f.getTime()!=null && f.getTime()!=0) {
			time.setText(Integer.toString(f.getTime()));
			time.setEnabled(false);
		}
		
		
		formula.setText(f.getContent());
		
		tabsPanel.removeAll();
		setTreatTabs();
		
		visitHelpPanel.repaint();
		visitContentPanel.repaint();
	}
	
	private void setUserCombo(User u) {
		
		int i = 0;
		
		for(String s : tabUser) {
			System.out.println("S: " + s + " v: " + u.toString());
			if(s.equals(u.toString())) {
				comboUser.setSelectedIndex(i);
				System.out.println("Zgadza si�!!!");
			}
			i++;
		}
		
	}
	
	private void setTreatCombo(Treatment treat) {
		
		int i = 0;
		
		for(String t : tabTreat) {
			
			if(t.equals(treat.toString())) {
				comboTreat.setSelectedIndex(i);
				System.out.println("Zgadza si�!!!");
			}
			i++;
			
		}
		
	}
	
	private void setTodayVisit(Color color, Color contentColor) {
		
		int height = 0;
		int spaceUp = 19;
		int spaceDown = 19;
		int space = 12;
		int spacee = 20;
		int width = 34;
		
		
		panelTodayVisit = new JPanel();
		panelTodayVisit.setLayout(null);
		panelTodayVisit.setOpaque(false);
		panelTodayVisit.setLocation(panelBody.getX(), 
							    	panelBody.getY() + panelBody.getHeight() + 10);
		
		customerPanel.add(panelTodayVisit);
		
		height += spaceUp + spaceDown;

		JLabel labToday = new JLabel();	
		labToday = setBar(labToday, color, "Najbli�sza wizyta");
		labToday.setLocation(0, 0);
		panelTodayVisit.add(labToday);
		
		height += labToday.getHeight();
		
		realizeVisit = new JLabel();
		realizeVisit = linkLabel(realizeVisit, color, "Realizuj wizyt� >>");
		realizeVisit.setLocation(labToday.getWidth() - realizeVisit.getWidth()-1, 
									 12);
		labToday.add(realizeVisit);
		
		realizeVisit.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				realizeVisit.setForeground(new Color(155,88,2));
				realizeVisit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
				
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				currentCard = visitPanel.getName();
				((CardLayout) whitePanel.getLayout()).show(whitePanel, visitPanel.getName());
				setUserVisit();
				setContentVisit();
				setTreatTabs();
				realizeVisit(visit);
			}
		});
			
		etDate = new JLabel();
		etDate = setLabelTitle(etDate, "data: ", contentColor);
		etDate.setLocation(2, labToday.getY() + labToday.getHeight() + spaceUp);
		panelTodayVisit.add(etDate);
				
		date = new JLabel();
		date = setLabelContent(date, "", contentColor);
		date.setLocation(178, etDate.getY());
		panelTodayVisit.add(date);
		
		planVisit = new JButtonOwn("but/planVisit");
		planVisit.setLocation(labToday.getX(), labToday.getY() + labToday.getHeight() + space);
		panelTodayVisit.add(planVisit);
		if(date.isVisible())
			planVisit.setVisible(false);
		else
			planVisit.setVisible(true);
		
		planVisit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				ConfigureApp.setBufforCustomer(customer);
				if(ConfigureApp.isTabs()) {
					MainWindow.labTab.setVisible(true);
					if (WidgetCalendar.indexCal == -1) {
						ConfigureApp.countTab();
						ConfigureWindow.repaintMenu();
						MainWindow.currentCard = 4;
						MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
						CalendarPanelNew calendarPanel = new CalendarPanelNew();
						MainWindow.tabbedPanel.addTab(WidgetCalendar.name, calendarPanel.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
						WidgetCalendar.indexCal = selIndex;
						MainWindow.listPanels.add(null);
						//Tab temp = new Tab(name, calendarPanel.getMainPanel(), selIndex);
						//listTab.add(temp);
						MainWindow.tabbedPanel.setForegroundAt(selIndex,ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);
						MainWindow.setWidgetBar();
						
					} else {
						
						MainWindow.tabbedPanel.setSelectedIndex(WidgetCalendar.indexCal);
						if(MainWindow.currentCard==3) {
							MainWindow.currentCard = 4;
							MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
							MainWindow.setWidgetBar();
						}
						
					}
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
			}
		});
				
		height += etDate.getHeight() + space;
				
		etDuration = new JLabel();
		etDuration = setLabelTitle(etDuration, "czas trwania: ", contentColor);
		etDuration.setLocation(etDate.getX(), etDate.getY() + etDate.getHeight() + space);
		panelTodayVisit.add(etDuration);
				
		duration = new JLabel();
		//duration = setLabelContent(duration, Integer.toString(visit.getDuration()), contentColor);
		duration = setLabelContent(duration, "", contentColor);
		duration.setLocation(date.getX(), etDuration.getY());
		panelTodayVisit.add(duration);
				
		height += etDuration.getHeight() + space;	
			
		etTreat = new JLabel();
		etTreat = setLabelTitle(etTreat, "zabiegi : ", contentColor);
		etTreat.setLocation(etDate.getX(), etDuration.getY() + etDuration.getHeight() + space);
		panelTodayVisit.add(etTreat);
			
		Font font = new Font("Arial", Font.BOLD, 17);
			
		treatText = new JTextArea();
		treatText.setBorder(null);
		treatText.setBackground(ConfigureApp.backgroundColor);
		treatText.setWrapStyleWord(true);
		treatText.setEditable(false);
		treatText.setLineWrap(true);
		treatText.setFont(font);
		treatText.setForeground(contentColor);
		treatText.setSize(240,116);
		treatText.setLocation(duration.getX(), etTreat.getY());
		//treatText.setText(visit.getListTreatment().toString());
		panelTodayVisit.add(treatText);
				
		height += etDuration.getHeight();
			
			
		
	
		
		
		
		
		
		/*JLabel etUser = new JLabel();
		etUser = setLabelToday(etUser, "Wybierz pracownika:", color);
		etUser.setLocation(1, labToday.getY() + labToday.getHeight() + spaceUp);
		panelTodayVisit.add(etUser);
		
		height += etUser.getHeight();
		
		String[] tabUser = serviceData.parseToString(serviceData.getUsers(" AND status!='ZWOLNIONY' "));
		
		JComboBox comboUser = new JComboBox(tabUser);
		comboUser = setComboBox(comboUser, color);
		comboUser.setLocation(etUser.getX(), etUser.getY() + etUser.getHeight() + space);
		panelTodayVisit.add(comboUser);
		
		height += comboUser.getHeight() + space;
		
		JLabel etTreat = new JLabel();
		etTreat = setLabelToday(etTreat, "Wybierz zabieg:", color);
		etTreat.setLocation(comboUser.getX(), comboUser.getY() + comboUser.getHeight() + spacee);
		panelTodayVisit.add(etTreat);
		
		height += etTreat.getHeight() + spacee;
		
		String[] tabTreat = serviceData.getStringTreat();
		
		JComboBox comboTreat = new JComboBox(tabTreat);
		comboTreat = setComboBox(comboTreat, color);
		comboTreat.setLocation(etTreat.getX(), etTreat.getY() + etTreat.getHeight() + space);
		panelTodayVisit.add(comboTreat);
		
		height += comboTreat.getHeight() + space;
		
		JLabel etPrice = new JLabel();
		etPrice = setLabelToday(etPrice, "Cena:", color);
		etPrice.setLocation(comboTreat.getX() + comboTreat.getWidth() + 34,
							etTreat.getY());
		panelTodayVisit.add(etPrice);
		
		JTextField price = new JTextField();
		price = setTextField(price, color);
		price.setSize(60, comboTreat.getHeight());
		price.setLocation(etPrice.getX(), comboTreat.getY());
		price.setForeground(color);
		panelTodayVisit.add(price);
		
		JLabel etTime = new JLabel();
		etTime = setLabelToday(etTime, "Czas:", color);
		etTime.setLocation(price.getX() + price.getWidth() + 34,
							etTreat.getY());
		panelTodayVisit.add(etTime);
		
		JTextField time = new JTextField();
		time = setTextField(time, color);
		time.setSize(60, comboTreat.getHeight());
		time.setLocation(etTime.getX(), comboTreat.getY());
		time.setForeground(color);
		panelTodayVisit.add(time);
		
		JLabel etFormula = new JLabel();
		etFormula = setLabelToday(etFormula, "Wpisz receptur�/uwagi:", color);
		etFormula.setLocation(comboTreat.getX(),
							  comboTreat.getY() + comboTreat.getHeight() + spacee);
		panelTodayVisit.add(etFormula);
		
		height += etFormula.getHeight() + spacee;
		
		JTextArea formula = new JTextArea();
		formula.setSize(labToday.getWidth()-2, 55);
		formula.setForeground(color);
		formula.setBackground(ConfigureApp.backgroundColor);
		formula.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color),
				  		  BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		formula.setWrapStyleWord(true);
		formula.setLineWrap(true);
		formula.setFont(new Font("Arial", Font.BOLD, 18));
		formula.setLocation(etFormula.getX(), etFormula.getY() + etFormula.getHeight() + space);
		panelTodayVisit.add(formula);
		
		height += formula.getHeight() + space;
		
		newTreat = new JLabel();
		newTreat = setLabelToday(newTreat, "Kolejny zabieg >>", color);
		newTreat.setLocation(labToday.getWidth() - newTreat.getWidth(),
							 formula.getY() + formula.getHeight() + space);
		panelTodayVisit.add(newTreat);
		
		height += newTreat.getHeight() + space;
		
		newTreat.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				newTreat.setForeground(color);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				newTreat.setForeground(new Color(155,88,2));
				newTreat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				//listVisit.add(new Visit());
			}
		});*/
		
		panelTodayVisit.setSize(labToday.getWidth(), height);
		
	}
	
	private void setSaveButton() {
		
		saveButton = new JButtonOwn("but/save");
		saveButton.setFocusable(false);
		saveButton.addFocusListener(null);
		saveButton.setLocation(whitePanel.getWidth() - saveButton.getWidth() - 10,
							   whitePanel.getY() + whitePanel.getHeight() + 5);
		saveButton.setEnabled(false);
		saveButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				addSaveAction();
			}
		});
		
		
		mainPanel.add(saveButton);
		
	}
	
	private void addSaveAction() {
		
		if(currentCard.equals(customerPanel.getName()))
			setSaveAction();
		else
			setSaveVisit();
		
	}
	
	public void repaintViewSearch() {
		
		if(SearchView.getCardPanel()!=null) {
			SearchView.setActionSearch();
			MainWindow.pane.setVisible(false);
			MainWindow.mainGlassPanel.removePanel();
			MainWindow.mainGlassPanel.setVisible(false);
			//MainWindow.adminPanel.components();
			MainWindow.mainGlassPanel.repaint();
		}
		
		
	}
	
	private void setListTreats() {
		
		if(comboTreat.getSelectedIndex()!=0) {
			
			if((listTreats.size()==countTreats || listTreats.size()==0)) {
				Treatment t = comboListTreat.get(comboTreat.getSelectedIndex());
				Boolean isSame = true;
				if(!formula.getText().isEmpty())
					isSame = false;
				else if(!price.getText().equals(Float.toString(t.getPrice())))
					isSame = false;
				else if(!time.getText().equals(Integer.toString(t.getTime())))
					isSame = false;
				if(!isSame) {
					System.out.println("Tworzymy formu��, by doda� j� do tabeli receptury");
					Formula f = new Formula(Float.parseFloat(price.getText()), 
									    Integer.parseInt(time.getText()),
									    formula.getText());
					t.setFormula(f);
					listTreats.add(t);
					System.out.println("Wybieramy listTreats: " + listTreats.size());
					System.out.println("A o to dodana formula: " + listTreats.get(countTreats).getFormula());
				}
			}
			else {
				Treatment t = listTreats.get(countTreats);
				Boolean isSame = true;
				
				if(comboListTreat.get(comboTreat.getSelectedIndex()).getId()!=t.getId()) {
					int idRelation = t.getRelation();
					t = comboListTreat.get(comboTreat.getSelectedIndex());
					t.setRelation(idRelation);
					listTreats.set(countTreats, t);
				}
				
				if(!formula.getText().isEmpty())
					isSame = false;
				else if(!price.getText().equals(Float.toString(t.getPrice())))
					isSame = false;
				else if(!time.getText().equals(Integer.toString(t.getTime())))
					isSame = false;
				
			
				
				if(!isSame) {
					System.out.println("Tworzymy formu��, by doda� j� do tabeli receptury");
					Formula f = new Formula(Float.parseFloat(price.getText()), 
									    Integer.parseInt(time.getText()),
									    formula.getText());
					
					if(t.getFormula()!=null)
						f.setIdTreats(t.getFormula().getIdTreats());
					t.setFormula(f);
					listTreats.set(countTreats, t);
					System.out.println("A o to dodana formula: " + listTreats.get(countTreats).getFormula());
				}
			}
			
			
		}

	}
	
	
	private void setSaveVisit() {

		
		Boolean isAnimation = false;
		//saveButton.setEnabled(false);
		if(listVisitCards.size()>0)
			listVisitCards.get(countVCard).setPlainBorder();
		
		System.out.println("ZAPISUJEMY WIZYT�!!!!");
		
		if(visit!=null) {
			
			
			setListTreats();
			
			int size = visit.getAllTreatments().size();
			System.out.println("Poprzedni size treatmentu: " + size);
			
			serviceData.updateVisitFormula(visit, (String) comboUser.getSelectedItem(), 
										listTreats, size);
			isAnimation = true;
		}
		else {
			
			System.out.println("Tworzymy now� wizyt� ");
			setListTreats();
			serviceData.addVisitFormula((String) comboUser.getSelectedItem(), customer, listTreats);
			isAnimation = true;
		}
		
		if(isAnimation) {
			
			listVisitCards.clear();
			MainWindow.save.setAnimation(1.0f);
			
			visitHelpPanel.removeAll();
			visitContentPanel.removeAll();
			historyPanel.removeAll();
			
			visitContentPanel.setVisible(false);
			
			tabsPanel.removeAll();
			updateCalendar();
			
			visits.clear();
			
			
			setHistoryPanel(topColor, contentColors);
			
			historyPanel.repaint();
			visitHelpPanel.repaint();
			visitContentPanel.repaint();
			
			
		}
		
	}
	
	
	private void addCustomerToDataBase() {
		
		customer = serviceData.addCustomer(name.getText(), surname.getText(), number.getText(), 
				   email.getText(), birthday.getText(), city.getText(),
				   natural.getText(), grey.getText(), condition.getText());
		visitButton.setEnabled(true);
		addVisit.setEnabled(true);
		planVisit.setEnabled(true);
		numberCard.setText(InputDataService.formatCustomerID(customer.getId()));
		numberCard.setVisible(true);
		etNumberCard.setVisible(true);
	
		
	}
	
	private void setSaveAction() {
		
		Boolean isAnimation = false;
		
		saveButton.setEnabled(false);

		if(kindCard==1) {
			
			Customer c = isCustomerExist();
			isGoodNumber = serviceData.isGoodNumber(number.getText());
			if(c!=null && customer==null && !isQuestion) {
				isQuestion = true;
				
				Boolean yes = comm.youSure("W bazie znajduje si� klient o podanych danych\n" +
						   				   "Co chcesz zrobi�?", 
						   				   0, "Poka� kart�", "Zapisz klienta do bazy");
				
				if(yes) {
					showCard(c);
					isAnimation = false;
					saveButton.setEnabled(true);
				}
				else {
					
					
					if(isGoodNumber) {
						addCustomerToDataBase();
						isAnimation = true;
						isSave = true;
						setGoodBorder(number);
					}
					else {
						setMistakeBorder(number);
						isAnimation = false;
						isSave = false;
					}
					
				}
				
			}
			
			else {
				
				if(isGoodNumber) {
					addCustomerToDataBase();
					isAnimation = true;
					isSave = true;
					setGoodBorder(number);
				}
				else {
					isAnimation = false;
					isSave = false;
					setMistakeBorder(number);
				}
				
			}
			
			
			
			
		}
		else {
			
			customer = serviceData.checkAndchange(customer, name.getText(), 
			   		   surname.getText(), number.getText(), email.getText(),
			   		   birthday.getText(), city.getText(),
			   		   natural.getText(), grey.getText(),
			   		   condition.getText());	
			System.out.println("");
			isAnimation = true;
			repaintViewSearch();
		}
		
		if(customer!=null) {
			
			int i = MainWindow.tabbedPanel.getSelectedIndex();
			MainWindow.tabbedPanel.setTitleAt(i, surname.getText() + " " + name.getText());
			MainWindow.tabbedPanel.repaint();
			note.setEnabled(true);
			
		}
		
		if(isAnimation) 
				MainWindow.save.setAnimation(1.0f);
		
			
		
	}
	
	private void updateCalendar() {
		
		if(WidgetCalendar.indexCal!=-1)
			CalendarPanelNew.repaintCalendarPanel();
		
		
		
	}
	
	public void updateCard(Visit visit) {
		
		System.out.println("Update Card... Co z tego b�dzie?");
		
		visits.clear();
		historyPanel.removeAll();
		setHistoryPanel(topColor, contentColors);
		historyPanel.repaint();
		
		this.visit = visit;
		setNextVisit();
		
	}
	
	private void showCard(Customer c) {
		
		int size = MainWindow.tabbedPanel.getTabCount();
		int i=0;
		Boolean isThere=false;
		while(i<size) {
			if(MainWindow.tabbedPanel.getTitleAt(i).equals(c.getSurname() + " " + c.getName())) {
				isThere = true;
				MainWindow.tabbedPanel.setSelectedIndex(i);
				break;
			}
				
			i++;
		}
		if(!isThere) {
			if(ConfigureApp.isTabs()) {
				
				ConfigureApp.countTab();
				MainWindow.currentCard=4;
				MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
				
				CardPanel cardClient = new CardPanel(0,c);
				MainWindow.listPanels.add(cardClient);
				
				MainWindow.tabbedPanel.addTab(c.getSurname() + " "  + c.getName(), 
											  cardClient.getMainPanel());

				int selIndex = MainWindow.tabbedPanel.getTabCount()-1;

				MainWindow.tabbedPanel.setForegroundAt(selIndex, ConfigureApp.fontAtTabColor);
				MainWindow.tabbedPanel.setSelectedIndex(selIndex);

				MainWindow.setWidgetBar();
				
				
				
			}	
			else {
				comm.dont("Masz zbyt du�o otwartych zak�adek");
			}
		}
		
	}
	
	private JLabel linkLabel(JLabel lab, Color color, String s) {
		
		Font font = new Font("Arial", Font.BOLD, 16);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setForeground(color);
		lab.setFont(font);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setText(s);
		
		return lab;
	}
	
	private JComboBox setComboBox(JComboBox combo, Color color) {
		
	
		combo.setRenderer(new ComboBoxRenderer(0));
		combo.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 14));
		combo.setForeground(color);
		combo.setBackground(ConfigureApp.backgroundColor);
		combo.setSize(256, 28);
		combo.setBorder(null);
		
		return combo;
		
	}
	

	
	private JLabel setLabelToday(JLabel lab, String s, Color color) {
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 16);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		lab.setForeground(color);
		lab.setFont(font);
		lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		lab.setText(s);
		
		
		return lab;
		
	}
	
	private JLabel setBar(JLabel lab, Color color, String title) {
		
		lab.setSize(widthMainBar, heightMainBar);
		
		
		JLabel labTitle = new JLabel(title);
		labTitle.setForeground(color);
		labTitle.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 25));
		labTitle.setSize(lab.getWidth(), 28);
		labTitle.setLocation(2, 4);
		lab.add(labTitle);
		
		JLabel labBar = new JLabel();
		labBar.setOpaque(true);
		labBar.setBackground(color);
		labBar.setSize(lab.getWidth(), 2);
		labBar.setLocation(0, lab.getHeight() - labBar.getHeight());
		lab.add(labBar);
		
		
		return lab;
		
	}
	
	public int getCounter() {
		return countTreats;
	}
	
	public void setCounter(int i) {
		countTreats = i;
	}
	
	public void setMistakeBorder(JTextComponent text) {
		
		text.setBorder(BorderFactory.createCompoundBorder(
					   ConfigureApp.mistakeWrite,
				       BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		text.setBackground(new Color(217,144,144));
		
	}
	
	public void setGoodBorder(JTextComponent text) {
		
		if(text.getName().equals("name") || text.getName().equals("surname")) {
			
			text.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(
					2, 2, 2, 2, contentColors),
					BorderFactory.createEmptyBorder(0, 3, 0, 3)));
			
		}
		else {
			text.setBorder(BorderFactory.createCompoundBorder(
					notWrite,
					BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		}
		
		text.setBackground(ConfigureApp.backgroundColor);
	}
	
	
	
	
	//do uzupe�nienia
	
	public void setFormula(String time, String price) {
		

	}
	
	//do ewentualnej zmiany
	
	public  void setHairdresser(String s) {
		
		
		worker.setText(s);
		worker.repaint();
		
		
	}
	
	private Customer isCustomerExist() {
				
		return serviceData.checkClient(number.getText(), surname.getText(), name.getText());

	}	
	
	
	public JPanel getMainPanel() {
		return mainPanel;
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent textField = (JTextComponent) e.getComponent();
		textField.setBorder(BorderFactory.createCompoundBorder(ConfigureApp.setWrite,
				  											   BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent tempField = (JTextComponent) e.getComponent();
		
		Boolean isGood = true;
		
		if((tempField.getName().equals("name") || tempField.getName().equals("surname")) 
			&& tempField.getText().isEmpty()) {
			
			saveButton.setEnabled(false);
			
			isGood = false;
			
		}
		else if(tempField.getName().equals("number")) {
			
			isGood = serviceData.isGoodNumber(tempField.getText());;
			isGoodNumber = isGood;
			
		}
		else {
			isGood = true;
		}
		
		if(isGood) {
			setGoodBorder(tempField);
			
		}
		else {
			setMistakeBorder(tempField);
		}
		
		
		
		
	}
	
	

}

	class TabRect extends JComponent {
	  
	
	  
	  /**
		 * 
		 */
	private static final long serialVersionUID = 1L;
		
	public Dimension getPreferredSize() {
		        
		return new Dimension(13, 17);
	}
	
	public Dimension getSize() {
        
		return new Dimension(13, 17);
	}
	
	public Dimension getMinimumSize() {
        
		return new Dimension(13, 17);
	}
	
	public Dimension getMaximumSize() {
        
		return new Dimension(13, 17);
	}
	
	public void paintComponent(Graphics g) {
		System.out.println("No i malujemy");
		super.paintComponent(g);
		g.drawRect(0, 50, 13, 17);
	    g.setColor(Color.RED);
	    //g.fillRect(0, 50, 13, 17);
	  }
	}
