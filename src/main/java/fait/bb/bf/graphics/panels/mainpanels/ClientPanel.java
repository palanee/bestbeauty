package fait.bb.bf.graphics.panels.mainpanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureWindow;
import fait.bb.bf.graphics.Tab;
import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.components.JHDresserBar;
import fait.bb.bf.graphics.components.JTextAreaLimit;
import fait.bb.bf.graphics.panels.glasspanels.FormulaWindow;
import fait.bb.bf.logic.Communication;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.Formula;
import fait.bb.bf.logic.Hair;
import fait.bb.bf.logic.InputDataService;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;
import fait.bb.bf.widgets.WidgetCalendar;
import fait.bb.bf.widgets.WidgetFind;
import fait.standard.Numbers;


public class ClientPanel extends CardPanel implements FocusListener, MouseListener {
	
	private JPanel mainPanel;
	private Point startPoint;
	private static Dimension sizeField;

	//private Color backgroundFont = new Color(234,233,234);
	//private Color obligatoryFieldColor = new Color(204,202,204);
	//private Color additionalFieldColor = new Color(223,221,223);
	//private Color backgroudAdditionalColor = new Color(188,186,188);
	//public static Color foreFontColor = new Color(86,64,81);
	//public static Color addForeFontColor = new Color(127,95,120);
	
	private Font fontCustomer = ConfigureApp.font.deriveFont(Font.BOLD, 16);
	private static Font fontEtCustomer = ConfigureApp.font.deriveFont(Font.PLAIN, 15);
	private Color backNotActive = new Color(222,222,222);
	
	private JPanel customerPanel;
	private JLabel customerBar;
	private JTextField name;
	private JTextField surname;
	private JTextField number;
	private JTextField naturalColor;
	private JTextField greyColor;
	private JLabel etName;
	private JLabel etSurname;
	private JLabel etNumber;
	private JLabel etNaturalColor;
	private JLabel etGreyColor;
	
	private JPanel formulationPanel;
	private JLabel formulationBar;
	private JLabel etDescription;
	private JTextArea description;
	private JTextField price;
	private JTextField time;
	private JLabel hairdresser;
	private JLabel worker;
	private JLabel changeWorker;
	
	private Formula deleteFormula;
	
	private JLabel historyLabel;
	private JPanel historyBar;
	private JPanel historyPanel;
	private JLabel etDate;
	private JLabel etTreatment;
	private JLabel etPriceHis;
	private JLabel etTimeHis;
	
	private JButton note;
	
	private JLabel numberCard;
	
	private Boolean isQuestion = false;
	
	private int countKey;
	
	private int countWord = 0;
	
	//public static Border notWrite = BorderFactory.createLineBorder(new Color(186,185,186));
	//public static Border setWrite = BorderFactory.createLoweredBevelBorder();
	
	private List<JTextComponent> obligatoryFieldList;
	private List<JTextComponent> additionalFieldList;
	
	Communication comm;
	private ServiceData serviceData;
	
	private JButton addData;
	private JButton newFormula;
	private JButton addVisit;
	
	private int h;
	private JLabel etHairdresser;
	private JPanel hairPanel;
	
	public static List<User> listUsers;
	
	private Integer prevIndex = null;
	private List<JTextField> dateList;
	private List<JTextField> formulaList;
	private List<JTextField> priceList;
	private List<JTextField> timeList;
	private List<JTextField> hairList;
	private List<Formula> formulasList;
	private List<JButtonWithout> deleteList;
	
	private Boolean isAddNewCustomer = false;
	
	private final static String nameTab = "ClientPanel";
	
	private List<String> fieldString;
	
	private int kindCard;
	private Customer customer;
	private Formula formula;
	
	private int widthOther = 85;
	private int widthDate = 100;
	//private int widthFormula = 538;
	private int widthFormula = 477;
	
	private Hair hair;
	private static Boolean isFill = false; 
	private int heightH = 45;
	
	private Boolean incorretNumber = false;

	
	//kindCard oznacza rodzaj karty - dodawanie klienta, b�d� otworzenie z danymi klienta
	//0 - karta klienta
	//1 - dodawania klienta

	public ClientPanel(Dimension size, int kindCard, Customer c) {
		super();

		//ustawienia dotycz�ce g��wnego panelu, czyli karty klienta
		
		this.kindCard = kindCard;
		this.countKey = 0;
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(ConfigureApp.backgroundColor);
		mainPanel.setIgnoreRepaint(true);
		
		InputMap inputMap = new JPanel().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), "saveCustomer");
		
		//inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK), "closeTab");
		
		mainPanel.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
		mainPanel.getActionMap().put("saveCustomer", new AbstractAction() {
             public void actionPerformed(ActionEvent e) {
            	 setActionAddData();
             }
         });
		
		additionalFieldList = new ArrayList<JTextComponent>();
		obligatoryFieldList = new ArrayList<JTextComponent>();
		
		comm = new Communication(ConfigureWindow.getFrame());
		serviceData = new ServiceData();
		
		setStartPoint();
		setCustomerPanel();
		setFormulationPanel();
		setAddButton();
		setHistory();
		
		
		fieldString = new ArrayList<String>();
		
		
		dateList = new ArrayList<JTextField>();
		formulaList = new ArrayList<JTextField>();
		priceList = new ArrayList<JTextField>();
		timeList = new ArrayList<JTextField>();
		hairList = new ArrayList<JTextField>();
		formulasList = new ArrayList<Formula>();
		deleteList = new ArrayList<JButtonWithout>();
		
		fieldString.add("Obowi�zkowe");
		fieldString.add(ConfigureApp.number);
		fieldString.add(ConfigureApp.natural);
		fieldString.add(ConfigureApp.grey);
		fieldString.add(ConfigureApp.price);
		fieldString.add(ConfigureApp.time);
		fieldString.add("");
		
		
		
		if(kindCard==1) {
			this.customer = c;
			
		}
		else if(kindCard==0) {
			formulasList = serviceData.getFormulasList(c.getId());
			this.hair = serviceData.getHair(c.getId());
			this.customer = new Customer(c, hair);
			if(this.customer!=null) {
				description.setEnabled(true);
				price.setEnabled(true);
				time.setEnabled(true);
				addData.setEnabled(true);
				description.setBackground(ConfigureApp.additionalFieldColor);
				price.setBackground(ConfigureApp.additionalFieldColor);
				time.setBackground(ConfigureApp.additionalFieldColor);
				note.setEnabled(true);
				setNumberCard();
			}
			Collections.reverse(formulasList);
			isFill = true;
		}
		
		
	
		
		initComponents();
		setBorder();
		
		
		
	}
	
	public void setFormula(String time, String price) {
		
	
		this.time.setText(time);
		this.price.setText(price);
	}
	
	private void setBorder() {
		
		JLabel lab = new JLabel();
		lab.setName("lab");
		lab.setBackground(ConfigureApp.backgroundCardColor);
		lab.setSize(MainWindow.cardPanel.getSize().width, 10);
		lab.setLocation(0,0);
		lab.setOpaque(true);
		mainPanel.add(lab);
	}
	
	private void resizeHistory() {
		
		int height = 0;
		
		if(formulasList.size()!=0)
			height = formulasList.size() * heightH + (formulasList.size()-1);
		
		historyPanel.setSize(historyPanel.getWidth(),
							 height + formulasList.size() + historyBar.getHeight()*2-5);
		
	}
	
	private void createList() {
		
		int j = formulasList.size() - 1;
		
		
		resizeHistory();
		
		
		while(j>=0) {
			
			Formula formula = formulasList.get(j);
			System.out.println("Do wpisania: " + formula);
			if(dateList.size() == 0) {
				int y = 0;
				JTextField field = getHistoryField(0, 0, widthDate, formula.getDate(true),0);
				dateList.add(field);
					
				addToList(y, field, 0, formula);
				setHistoryBarLab(dateList.get(0), formulaList.get(0), 
									timeList.get(0), priceList.get(0), hairList.get(0));	
			}
			else {
				 
				int y = dateList.get(dateList.size() - 1).getY() + 
				 		dateList.get(dateList.size() - 1).getHeight();
				 
			 
				JTextField field = getHistoryField(0, y, 
						 							   widthDate, 
						 							   formula.getDate(true), 1);
				
				dateList.add(field);
				addToList(y, field, 1, formula);
				
			}
			j--;
		}
		
		
		
	}
	
	public void setText(JTextComponent comp, String back) {
		
		comp.setText(back);
		comp.addFocusListener(this);
		if(kindCard==0) 
			comp.setForeground(ConfigureApp.foreFontColor);

	}
	
	private void updateFormulaList() {
		
		Iterator<Formula> it = formulasList.iterator();
		//index dotycz�cy list price, time oraz formula
		int index;
		//index dotycz�cy listy obiekt�w Formula
		int i=0;
		while(it.hasNext()) {
			Formula f = it.next();
			if(f.getId()==formula.getId()) {
				formulasList.set(i, formula);
				index = formulasList.size() - 1 - i;
				formulaList.get(index).setText(formula.getContent());
				priceList.get(index).setText(formula.getPrice().toString());
				timeList.get(index).setText(formula.getTime().toString());
				//hairList.get(index).setText(formula.getUser());
			}
			i++;
		}
		
	}
	
	
	
	public void changeList() {
		
		int i = 0;
		int j = formulasList.size() - 1;
		
		while(i<dateList.size()) {
			dateList.get(i).setText(formulasList.get(j).getDate(true));
			formulaList.get(i).setText(formulasList.get(j).getContent());
			priceList.get(i).setText(formulasList.get(j).getPrice().toString());
			timeList.get(i).setText(formulasList.get(j).getTime().toString());
			//hairList.get(i).setText(formulasList.get(j).getUser());
			deleteList.get(i).setFormula(formulasList.get(j));
			i++;
			j--;
		}
		
	}
	
	public void addToList(int y, JTextField field, int number, final Formula formula) {
		
		field.addMouseListener(this);
		
		field = getHistoryField(field.getX() + field.getWidth() + 1,
				   				y, 
				   				widthFormula,
				   				formula.getContent(),number);
		field.addMouseListener(this);
		formulaList.add(field);
		
		String s = formula.getTime().toString();
		if(!s.isEmpty())
			s += " min";
		else
			s = "-";
		field = getHistoryField(field.getX() + field.getWidth() + 1,
								y, 
								widthOther,
								s, number);
		timeList.add(field);
		field.addMouseListener(this);
		
		s = formula.getPrice().toString();
		if(!s.isEmpty())
			s += " z�";
		else
			s = "-";
		field = getHistoryField(field.getX() + field.getWidth() + 1,
   				   				y, 
   				   				widthOther,
   				   				s, number);
		field.addMouseListener(this);
		priceList.add(field);
		
		
//		field = getHistoryField(field.getX() + field.getWidth() + 1,
//  								y,
//  								widthOther,
//  								formula.getUser(),number);
		//CHECK: sprawdzi� dlaczego nie pobiera User
		field = getHistoryField(field.getX() + field.getWidth() + 1,
				y,
				widthOther,
				"formula.getUser()",number);
		field.addMouseListener(this);
		hairList.add(field);
		
		final JButtonWithout delete = new JButtonWithout("deleteHistory");
		int y_bounds = y + 1;
		if(number==0) 
			y_bounds = y + historyBar.getY() + historyBar.getHeight() + 1;
		delete.setLocation(field.getX() + field.getWidth() + 1, y_bounds);
		delete.setFormula(formula);
		delete.addMouseListener(this);
		delete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				System.out.println("Usuwamy.... " + deleteFormula);
				if(deleteFormula!=null) {
					
					Boolean yes = comm.youSure("Czy na pewno chcesz usun�� receptur� z historii?", 1);
					if(yes) {
						serviceData.deleteFormula(deleteFormula.getId());
						formulasList = new ArrayList<Formula>();
						dateList = new ArrayList<JTextField>();
						formulaList = new ArrayList<JTextField>();
						priceList = new ArrayList<JTextField>();
						timeList = new ArrayList<JTextField>();
						deleteList = new ArrayList<JButtonWithout>();
						hairList = new ArrayList<JTextField>(); 
						formulasList = serviceData.getFormulasList(customer.getId());
						Collections.reverse(formulasList);
						mainPanel.remove(historyPanel);
						mainPanel.repaint();
						//historyPanel.removeAll();
						setHistory();
						createList();
						
						historyPanel.repaint();
					}
				}
				
				
				
			}
		});
		
		
		historyPanel.add(delete);
		deleteList.add(delete);
		
		resizeHistory();
		
		
	}
	
	/*public void addToList(int y, JTextPane field, int number) {
		
		field.addMouseListener(this);
		
		field = getHistoryField(field.getX() + field.getWidth() + 1,
								y, 
								widthFormula,
								formula.getContent(),number);
		field.addMouseListener(this);
		formulaList.add(field);
		
		field = getHistoryField(field.getX() + field.getWidth() + 1,
								y, 
								widthOther,
								formula.getTime(),number);
		timeList.add(field);
		field.addMouseListener(this);
		
		field = getHistoryField(field.getX() + field.getWidth() + 1,
   				   				y, 
   				   				widthOther,
   				   				formula.getPrice(),number);
		field.addMouseListener(this);
		priceList.add(field);
		
		field = getHistoryField(field.getX() + field.getWidth() + 1,
  			    				y, 
  			    				widthOther,
  			    				formula.getUser(),number);
		field.addMouseListener(this);
		hairList.add(field);
		
	}*/
	
	public Boolean saveClient() {
		
		Boolean isAnimation = false;
		
		if(customer==null) {
			
			
			
			customer = serviceData.addCustomer(name.getText(), surname.getText(), 
					   number.getText(), naturalColor.getText(), 
					   greyColor.getText());
			
			
			if(customer!=null) {
				setNumberCard();
				
			}
		}
		else {
			customer = serviceData.checkAndchange(customer, name.getText(), 
			   		  surname.getText(), number.getText(), 
			   		  naturalColor.getText(), greyColor.getText());	
			
		}
		
		surname.setText(serviceData.getSurname());
		name.setText(serviceData.getName());
		
		if(customer!=null) {
			
			isAnimation = true;
			setGood(surname);
			setGood(name);
			
			
			
			int i = MainWindow.tabbedPanel.getSelectedIndex();
			MainWindow.tabbedPanel.setTitleAt(i, surname.getText() + " " + name.getText());
			MainWindow.tabbedPanel.repaint();
			
			addVisit.setEnabled(true);
			note.setEnabled(true);
		}
		else {
			
			if(surname.getText().equals(""))
				setMistake(surname);
			if(name.getText().equals(""))
				setMistake(name);
			
			isAnimation = false;
		}
		
		return isAnimation;
		
	}
	
	public void repaintViewSearch() {
		
		if(SearchView.getCardPanel()!=null) {
			SearchView.setActionSearch();
			MainWindow.pane.setVisible(false);
			MainWindow.mainGlassPanel.removePanel();
			MainWindow.mainGlassPanel.setVisible(false);
			//MainWindow.adminPanel.components();
			MainWindow.mainGlassPanel.repaint();
		}
		
		
	}
	
	
	public void setActionAddData() {
		
		Boolean isAnimation = true;
		Boolean isCheck = false;
		Boolean yes = false;
		
		
		//Boolean isQuestion = false;
		
		Boolean incorrect = checkNumber(number.getText());
		
		if(!incorrect)
			comm.dont("Niepoprawny numer telefonu");
		else {
			//sprawdzamy, czy w bazie istnieje kto� o podanym numerze telefonu lub imieniu i nazwisku
			Customer c = isCustomerExist();
			
			if(c!=null && customer==null && !isQuestion) {
				
				isQuestion = true;
				
				yes = comm.youSure("W bazie znajduje si� klient o podanych danych\n" +
						   				   "Co chcesz zrobi�?", 
						   				   0, "Poka� kart�", "Zapisz klienta do bazy");
				
				if(yes) {
					showCard(c);
					isAnimation = false;
				}
				else {
					isAnimation = saveClient();
					repaintViewSearch();
				}
				
				
				
			}
			else {				
				isAnimation = saveClient();
				repaintViewSearch();
			}
			
			System.out.println("Animacja: isAnimation: " + isAnimation);
			
			isCheck = true;
			
		}	
		
		if(isCheck) {
			
			Numbers numb = new Numbers();
			Boolean goodPrice = true;
			Boolean goodTime = true; 
			
			if(!price.getText().isEmpty() && !price.getText().equals(ConfigureApp.price))
				goodPrice = numb.isFloatOrInteger(price.getText());
			if(!time.getText().isEmpty() && !time.getText().equals(ConfigureApp.time))
				goodTime = numb.isInteger(time.getText());
			
			
			if(goodPrice) 
				setGood(price);
			else
				setMistake(price);
			
			if(goodTime)
				setGood(time);
			else
				setMistake(time);
			
			if(formula!=null && goodPrice && goodTime) {
				
				//CHECK
				/*formula = serviceData.checkFormulaAndSave(formula, description.getText(),
														  price.getText(), time.getText());*/

				formula = serviceData.checkFormulaAndSave(formula, description.getText());
				updateFormulaList();
				//MainWindow.save.setAnimation(1.0f);
				
			}
			else if(formula==null && goodPrice && goodTime) {
				
				System.out.println("ClientPanel: Zapisywanie: " + ConfigureApp.logUser);
				System.out.println("ClientPanel: isEmpty: " + isEmpty());
				
				/*if(ConfigureApp.logUser.equals(JHDresserBar.getTextOnBar()) && !isEmpty()) {
					//przesy�am informacj�, czy ma by�
					
					System.out.println("Pasek fryzjera: " + ConfigureWindow.bar.getText());
					
					ConfigureWindow.bar.setActionBut(true);
					
				}*/
				if(isEmpty() && !yes && isAnimation) {
					
					System.out.println("Dodaj� tylko dane klienta");
					isAnimation = true;
					
				}
				else if(isEmpty() && yes) {
					System.out.println("Przechodz� na kart� klienta");
					isAnimation = false;
				}
				else {
					formula = null;
					formula = serviceData.addFormula(description.getText(), time.getText(), 
							price.getText(), customer, worker.getText());
					if(formula!=null) {
						formulasList.add(formula);
				
						if(dateList.size()==0) {
							int y = 0;
							JTextField field = getHistoryField(0, 0, widthDate, formula.getDate(true),0);
							dateList.add(field);

							addToList(y, field, 0, formula);
							setHistoryBarLab(dateList.get(0), formulaList.get(0), 
									         timeList.get(0), priceList.get(0), hairList.get(0));	

						}
					
						else if((dateList.size() <3) && (dateList.size() > 0)) {

							int y = dateList.get(dateList.size() - 1).getY() + 
							dateList.get(dateList.size() - 1).getHeight();


							JTextField field = getHistoryField(0, y, 
											   widthDate, 
											   formula.getDate(true), 1);

							dateList.add(field);
							addToList(y, field, 1, formula);
							changeList();
						}
					
						else {

							formulasList.remove(0);
							changeList();			 
						}
						newFormula.setEnabled(true);
						clearFormula();
						
						
					}
					
					
					if(isAnimation)
						MainWindow.save.setAnimation(1.0f);
			 }
			 
			}
			
			
			if(isAnimation && goodPrice && goodTime)
				MainWindow.save.setAnimation(1.0f);
			
		}
		
		/*if(!isAnimation)
			MainWindow.save.setAnimation(1.0f);*/
		
		getHistoryPanel().repaint();
		
		
	}
	
	private void setMistake(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.mistakeWrite,
	   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(new Color(249,203,201));
		
	}
	
	private void setGood(JTextField textField) {
		
		textField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.notWrite,
				BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		textField.setBackground(ConfigureApp.additionalFieldColor);
		
	}
	
	private Boolean isEmpty() {
		
		
		if(!description.getText().isEmpty())
			return false;
		else if(!time.getText().isEmpty() && !time.getText().equals(ConfigureApp.time))
			return false;
		else if(!price.getText().isEmpty() && !price.getText().equals(ConfigureApp.price))
			return false;
		else
			return true;
	}
	
	public void clearFormula() {
		
		clearBorder();
		description.setText("");
		price.setText("");
		time.setText("");
		formula = null;
	}
	
	public void initComponents() {
		
		if(kindCard==1) {
			setText(name, "Obowi�zkowe");
			setText(surname, "Obowi�zkowe");
			setText(number, ConfigureApp.number);
			setText(naturalColor, ConfigureApp.natural);
			setText(greyColor, ConfigureApp.grey);
			setText(price, ConfigureApp.price);
			setText(time, ConfigureApp.time);
			setText(description, "");
		}
		else if(kindCard==0) {
			setText(name, customer.getName());
			setText(surname, customer.getSurname());
			setText(number, customer.getNumber());
			setText(naturalColor, hair.getNatural());
			setText(greyColor, hair.getGrey());
			setText(description, "");
			setText(price, "");
			setText(time, "");
			createList();
			newFormula.setEnabled(true);
			addVisit.setEnabled(true);
			
		}
		
		//mistake = new JLabel();
		
		
		number.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});
		
		naturalColor.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});
		
		greyColor.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});
		
		/*description.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});*/
		
		/*price.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});*/
		
		time.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					//logInIn(error);
					setActionAddData();
				}
				
			}			
		});
		
		addData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				
				
				setActionAddData();
				
					
				
			}
		});
		
		newFormula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
					
					Boolean yes = comm.youSure("Czy na pewno chcesz doda� now� receptur�?\n" +
								  "(wszystkie pola receptury zostan� wyczyszczone)", 0);
					if(yes) {
						clearFormula();
					}
			}
		});
		
		addVisit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent zdarz) {
				
				ConfigureApp.setBufforCustomer(customer);
				if(ConfigureApp.isTabs()) {
					MainWindow.labTab.setVisible(true);
					if (WidgetCalendar.indexCal == -1) {
						ConfigureApp.countTab();
						ConfigureWindow.repaintMenu();
						MainWindow.currentCard = 4;
						MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
						CalendarPanelNew calendarPanel = new CalendarPanelNew();
						MainWindow.tabbedPanel.addTab(WidgetCalendar.name, calendarPanel.getMainPanel());

						int selIndex = MainWindow.tabbedPanel.getTabCount() - 1;
						WidgetCalendar.indexCal = selIndex;
						MainWindow.listPanels.add(null);
						//Tab temp = new Tab(name, calendarPanel.getMainPanel(), selIndex);
						//listTab.add(temp);
						MainWindow.tabbedPanel.setForegroundAt(selIndex,ConfigureApp.fontAtTabColor);
						MainWindow.tabbedPanel.setSelectedIndex(selIndex);
						MainWindow.setWidgetBar();
						
					} else {
						
						MainWindow.tabbedPanel.setSelectedIndex(WidgetCalendar.indexCal);
						if(MainWindow.currentCard==3) {
							MainWindow.currentCard = 4;
							MainWindow.cl.show(MainWindow.cardPanel, ""+(MainWindow.currentCard));
							MainWindow.setWidgetBar();
						}
						
					}
				}
				else {
					comm.dont("Masz zbyt du�o otwartych zak�adek");
				}
			}
			
		});
		
		
		
	}
	
	public void setObligatoryField(JTextComponent text, JComponent comp) {
		
		text.setBackground(ConfigureApp.additionalFieldColor);
		text.setForeground(ConfigureApp.backgroundFont);
		text.setFont(fontCustomer);
		text.setFocusable(true);
		text.setBorder(BorderFactory.createCompoundBorder(ConfigureApp.notWrite,
                										  BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		comp.add(text);
		obligatoryFieldList.add(text);
		
		
	}
	
	public void setAdditionalField(JTextComponent text, JComponent comp) {
		
		text.setBackground(ConfigureApp.additionalFieldColor);
		text.setForeground(ConfigureApp.backgroundFont);
		text.setFont(fontCustomer);
		text.setFocusable(true);
		text.setBorder(BorderFactory.createCompoundBorder(ConfigureApp.notWrite,
				  										  BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		comp.add(text);
		additionalFieldList.add(text);
		//fieldString.add(back);
		
	}
	
	public void setEtLabel(JLabel lab, String text) {
		
		lab.setForeground(ConfigureApp.etColorFont);
		lab.setFont(fontEtCustomer);
		lab.setText(text);
		customerPanel.add(lab);
		
	}
	
	
	
	public void setNumberCard() {
		
		Font font = new Font("Arial", Font.PLAIN, 19);
		FontMetrics metrics = new FontMetrics(font) {};

		
		String s = InputDataService.formatCustomerID(customer.getId());
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		
		numberCard = new JLabel(s);
		numberCard.setFont(font);
		numberCard.setForeground(ConfigureApp.backgroundColor);
		numberCard.setSize((int) bounds.getWidth(),
						   (int) bounds.getHeight());
		numberCard.setLocation(83,
							   2);
		customerBar.add(numberCard);
		
	}
	
	public void setCustomerPanel() {
		
		sizeField = new Dimension(167,31);
		
		int height = 270;
		int y = 20;
		int xFont = 20;
		
		
		
		FontMetrics metrics = new FontMetrics(fontEtCustomer) {};
		Rectangle2D bounds; 
		
		
		customerBar = new JLabel();
		ImageIcon img = new ImageIcon("resources/images/customerBar.png");
		customerBar.setIcon(img);
		customerBar.setBounds(startPoint.x,
							  30,
							  img.getIconWidth(),
							  img.getIconHeight());
		h = startPoint.y + customerBar.getHeight();
		customerBar.setName("customerBar");
	
		
		
		customerPanel = new JPanel();
		customerPanel.setLayout(null);
		customerPanel.setBackground(ConfigureApp.backPanelColorForm);
		customerPanel.setBounds(customerBar.getX(),
								customerBar.getY() + customerBar.getHeight() - 3,
								customerBar.getWidth(),
								height);
		
		h = h + customerPanel.getHeight();
		customerPanel.setName("customerPanel");
		
		mainPanel.add(customerPanel);
		mainPanel.add(customerBar);
		
		
		name = new JTextField();
		setObligatoryField(name, customerPanel); 
		name.setBounds(customerPanel.getWidth() - sizeField.width - 10,
				   15,
				   sizeField.width,
				   sizeField.height
				   );
		name.setName("name");
		
		etName = new JLabel();
		String temp = "Imi�";
		setEtLabel(etName, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etName.setBounds(name.getX()-xFont - (int) bounds.getWidth(),
						 name.getY() + (name.getHeight()-(int)bounds.getHeight())/2,
						 (int)bounds.getWidth(),
						 (int)bounds.getHeight());
		
		
		surname = new JTextField();
		setObligatoryField(surname, customerPanel);
		surname.setBounds(name.getX(),
						  name.getY() + name.getHeight() + y,
						  sizeField.width,
						  sizeField.height);
		surname.setName("surname");
		surname.addKeyListener(new KeyAdapter() {
			
			public void keyTyped(KeyEvent e) {
				
				countKey++;
				if(countKey==3) {
					description.setEnabled(true);
					description.setBackground(ConfigureApp.additionalFieldColor);
					price.setEnabled(true);
					price.setBackground(ConfigureApp.additionalFieldColor);
					time.setEnabled(true);
					time.setBackground(ConfigureApp.additionalFieldColor);
					addData.setEnabled(true);
				}
				
			}
			
		});
		
		etSurname = new JLabel();
		temp = "Nazwisko";
		setEtLabel(etSurname, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etSurname.setBounds(surname.getX()-xFont - (int) bounds.getWidth(),
						 	surname.getY() + (surname.getHeight()-(int)bounds.getHeight())/2,
						 	(int)bounds.getWidth(),
						 	(int)bounds.getHeight());
		
		number = new JTextField();
		number.setDocument(new JTextAreaLimit(14));
		setAdditionalField(number, customerPanel);
		number.setBounds(surname.getX(),
						 surname.getY() + surname.getHeight() + y,
						 sizeField.width,
						 sizeField.height);
		number.setName("number");
		
		etNumber = new JLabel();
		temp = "Telefon";
		setEtLabel(etNumber, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etNumber.setBounds(number.getX()-xFont - (int) bounds.getWidth(),
						   number.getY() + (number.getHeight()-(int)bounds.getHeight())/2,
						   (int)bounds.getWidth(),
						   (int)bounds.getHeight());
		
		naturalColor = new JTextField();
		setAdditionalField(naturalColor, customerPanel);
		naturalColor.setBounds(number.getX(),
							   number.getY() + number.getHeight() + y,
							   sizeField.width,
							   sizeField.height);
		naturalColor.setName("naturalColor");
		
		etNaturalColor = new JLabel();
		temp = "Kolor naturalny";
		setEtLabel(etNaturalColor, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etNaturalColor.setBounds(naturalColor.getX() - xFont - (int) bounds.getWidth(),
								 naturalColor.getY() + (naturalColor.getHeight()-(int)bounds.getHeight())/2,
						 		 (int)bounds.getWidth(),
						 		 (int)bounds.getHeight());
		
		greyColor = new JTextField();
		setAdditionalField(greyColor, customerPanel);
		greyColor.setBounds(naturalColor.getX(),
							   naturalColor.getY() + naturalColor.getHeight() + y,
							   sizeField.width,
							   sizeField.height);
		greyColor.setName("greyColor");
		
		etGreyColor = new JLabel();
		temp = "Siwe";
		setEtLabel(etGreyColor, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etGreyColor.setBounds(greyColor.getX() - xFont - (int) bounds.getWidth(),
							  greyColor.getY() + (greyColor.getHeight()-(int)bounds.getHeight())/2,
						      (int)bounds.getWidth(),
						 	  (int)bounds.getHeight());
		
		
	}
	
	public void setFormulationPanel() {
		
		final FontMetrics metrics = new FontMetrics(fontEtCustomer) {};
		Rectangle2D bounds; 
		int y = 10;
		
		sizeField = new Dimension(100,31);
		
		formulationBar = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/formulationBar.png");
		formulationBar.setIcon(imgIc);
		formulationBar.setBounds(customerBar.getX() + customerBar.getWidth() + 30,
								 customerBar.getY(),
								 imgIc.getIconWidth(),
								 imgIc.getIconHeight());
		
		formulationBar.setName("formulationBar");
		
		
		formulationPanel = new JPanel();
		formulationPanel.setLayout(null);
		formulationPanel.setBackground(ConfigureApp.backPanelColorForm);
		formulationPanel.setBounds(formulationBar.getX(),
								   formulationBar.getY() + formulationBar.getHeight() - 5,
								   formulationBar.getWidth(),
								   customerPanel.getHeight());
		
		formulationPanel.setName("formulation");
		
		mainPanel.add(formulationPanel);
		mainPanel.add(formulationBar);
		
		etDescription = new JLabel();
		etDescription.setName("etDescription");
		String temp = "Receptura";
		setEtLabel(etDescription, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etDescription.setBounds(10,
								5,
								(int) bounds.getWidth(),
								(int) bounds.getHeight());
		formulationPanel.add(etDescription);
		
		description = new JTextArea();
		setAdditionalField(description, formulationPanel);
		description.setWrapStyleWord(true);
		description.setLineWrap(true);
		description.setDocument(new JTextAreaLimit(250));
		description.setName("description");
		description.setBounds(etDescription.getX(),
							  etDescription.getY() + etDescription.getHeight() + 5,
							  formulationPanel.getWidth() - 2*etDescription.getX(),
							  149);
		description.setName("description");
		
		description.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
            	
            	if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    if (e.getModifiers() > 0) {
                    	description.transferFocusBackward();
                    } else {
                    	description.transferFocus();
                    }
                    e.consume();
                }
                
                countWord++;
                System.out.println("Jest ju�... " + countWord);
               
                
                
            }
        });
		
		description.setEnabled(false);
		formulationPanel.add(description);
		
		price = new JTextField();
		price.setDocument(new JTextAreaLimit(7));
		price.setName("price");
		setAdditionalField(price, formulationPanel);
		price.setBounds(description.getX(),
						description.getY() + description.getHeight() + y,
						sizeField.width,
						sizeField.height);
		price.setName("price");
		price.setEnabled(false);
		
		temp = "z�";
		bounds = metrics.getStringBounds(temp, null);  
		JLabel etPrice = new JLabel();
		etPrice.setName("etPrice");
		setEtLabel(etPrice, temp);
		etPrice.setBounds(price.getX() + price.getWidth() + 5,
						  price.getY() + (price.getHeight() - (int) bounds.getHeight())/2,
						  (int) bounds.getWidth(),
						  (int) bounds.getHeight());
		formulationPanel.add(etPrice);
		
		
		time = new JTextField();
		time.setDocument(new JTextAreaLimit(4));
		time.setName("time");
		setAdditionalField(time, formulationPanel);
		time.setBounds(etPrice.getX() + etPrice.getWidth() + 20,
					   price.getY(),
					   sizeField.width,
					   sizeField.height);
		time.setName("time");
		time.setEnabled(false);
		
		temp = "min";
		bounds = metrics.getStringBounds(temp, null);  
		JLabel etTime = new JLabel();
		etTime.setName("etTime");
		setEtLabel(etTime, temp);
		etTime.setBounds(time.getX() + time.getWidth() + 5,
						 time.getY() + (time.getHeight() - (int) bounds.getHeight())/2,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		formulationPanel.add(etTime);
		
		System.out.println("ClientPanel: logUser" + ConfigureApp.logUser);
		
		if(!ConfigureApp.logUser.equals(JHDresserBar.getTextOnBar()))
			temp = ConfigureApp.logUser;
		else
			temp = "";
		bounds = metrics.getStringBounds(temp, null); 
		
		note = new JButtonWithout("note");
		note.setName("note");
		note.setLocation(description.getX() + description.getWidth() - note.getWidth(),
						 price.getY() + price.getHeight()/2 - note.getHeight()/2);
		note.setEnabled(false);
		note.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				MainWindow.notePanel.paintNote(customer);
				
			}
		});
		formulationPanel.add(note);
		
		JLabel etUser = new JLabel();
		etUser.setName("etUser");
		temp = "Pracownik: ";
		etUser.setText(temp);
		setEtLabel(etUser, temp);
		bounds = metrics.getStringBounds(temp, null);  
		etUser.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		etUser.setLocation(price.getX(), price.getY() + price.getHeight() + 15);
		formulationPanel.add(etUser);
		
		worker = new JLabel();
		worker.setName("worker");
		temp = serviceData.getUserFromString(ConfigureApp.logUserCard).toString();
		System.out.println("Pobrany pracownik: " + temp);
		setEtLabel(worker, temp);
		bounds = metrics.getStringBounds(temp, null); 
		worker.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		worker.setLocation(etUser.getX() + etUser.getWidth() + 5, etUser.getY());
		formulationPanel.add(worker);
		
		changeWorker = new JLabel();
		changeWorker.setName("worker");
		temp = "Zmie� >> ";
		setEtLabel(changeWorker, temp);
		bounds = metrics.getStringBounds(temp, null); 
		changeWorker.setSize((int) bounds.getWidth() + 20, (int) bounds.getHeight());
		changeWorker.setLocation(description.getX() + description.getWidth() - changeWorker.getWidth(), worker.getY());
		formulationPanel.add(changeWorker);
		
		changeWorker.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				changeWorker.setFont(fontEtCustomer);
				changeWorker.setForeground(ConfigureApp.etColorFont);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				changeWorker.setFont(fontEtCustomer.deriveFont(Font.BOLD, 14));
				changeWorker.setForeground(new Color(119,42,115));
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
				ConfigureWindow.bar.setActionBut(true);

				
			}
		});
		
		
		hairPanel = new JPanel();
		hairPanel.setName("hairPanel");
		hairPanel.setOpaque(false);
		hairPanel.setLayout(null);
		hairPanel.setSize(description.getWidth(),
						  formulationPanel.getHeight() - price.getY() - price.getHeight() + 5);
		hairPanel.setLocation(price.getX(),price.getY() + price.getHeight() + 5);
		formulationPanel.add(hairPanel);
		
		//setHairdresser(temp);
		
		
		
		
		if(customer!=null) {
			description.setEnabled(true);
			price.setEnabled(true);
			time.setEnabled(true);
		}
		
		if(!description.isEnabled()) {
			description.setBackground(backNotActive);
		}
		if(!price.isEnabled()) {
			price.setBackground(backNotActive);
		}
		if(!time.isEnabled()) {
			time.setBackground(backNotActive);
		}
		
		
	}
	
	public  void setHairdresser(String s) {
		
		/*hairPanel.removeAll();
		
		//listUser = servi
		
		hairdresser = new JLabel();
		hairdresser.setForeground(ConfigureApp.etColorFont);
		hairdresser.setFont(fontEtCustomer);
		hairdresser.setText(s);
		hairdresser.setBounds(0,
						 	  0,
						 	  hairPanel.getWidth(),
						      20);
		hairPanel.add(hairdresser);
		
		hairPanel.repaint();*/
		
		worker.setText(s);
		worker.repaint();
		
		
	}
	
	public void setAddButton() {
		
		addData = new JButtonOwn("but/blue", "Zapisz");
		addData.setLocation(formulationPanel.getX() - 5,
						    formulationPanel.getY() + formulationPanel.getHeight() + 10);

		h = h + addData.getHeight() + 10;
		addData.setEnabled(false);
		addData.setName("addData");
		
		newFormula = new JButtonOwn("but/blue", "Wyczy��");
		newFormula.setLocation(addData.getX() + addData.getWidth() + 25,
							   addData.getY());
		newFormula.setEnabled(false);
		
		addVisit = new JButtonOwn("but/orange", "Zaplanuj wizyt�");
		addVisit.setLocation(customerPanel.getX(),
							 newFormula.getY());
		addVisit.setEnabled(false);
		addVisit.setName("addVisit");
		System.out.println("Czy jest klient: " + customer);
		
		
		//mainPanel.add(newFormula);
		mainPanel.add(addData);
		mainPanel.add(addVisit);
		
	}
	
	public void setHistoryBarLab(JTextField date, JTextField content, 
								 JTextField time, JTextField price, JTextField hair) 
	{
		
		fontCustomer = ConfigureApp.font.deriveFont(Font.PLAIN, 14);
		FontMetrics metrics = new FontMetrics(fontCustomer) {};
		Rectangle2D bounds;
		
		String s = "Data";
		bounds = metrics.getStringBounds(s, null);
		etDate = new JLabel(s);
		etDate.setFont(fontCustomer);
		etDate.setForeground(ConfigureApp.etColorFont);
		etDate.setBounds(date.getWidth()/2 - (int) bounds.getWidth()/2,
						 historyBar.getHeight()/2 - (int) bounds.getHeight()/2 + 2,
						 (int) bounds.getWidth(),
						 (int) bounds.getHeight());
		
		historyBar.add(etDate);
		
		s = "Receptura";
		bounds = metrics.getStringBounds(s, null);
		etTreatment = new JLabel(s);
		etTreatment.setFont(fontCustomer);
		etTreatment.setForeground(ConfigureApp.etColorFont);
		etTreatment.setBounds(content.getX() + (content.getWidth()/2 - (int) bounds.getWidth()/2) - 10,
						 	  etDate.getY(),
						 	  (int) bounds.getWidth(),
						 	  (int) bounds.getHeight());
		
		historyBar.add(etTreatment);
		
		s = "Czas";
		bounds = metrics.getStringBounds(s, null);
		etTimeHis = new JLabel(s);
		etTimeHis.setFont(fontCustomer);
		etTimeHis.setForeground(ConfigureApp.etColorFont);
		etTimeHis.setBounds(time.getX() + (time.getWidth()/2 - (int) bounds.getWidth()/2),
						 	  etDate.getY(),
						 	  (int) bounds.getWidth(),
						 	  (int) bounds.getHeight());
		
		historyBar.add(etTimeHis);
		
		s = "Cena";
		bounds = metrics.getStringBounds(s, null);
		etPriceHis = new JLabel(s);
		etPriceHis.setFont(fontCustomer);
		etPriceHis.setForeground(ConfigureApp.etColorFont);
		etPriceHis.setBounds(price.getX() + (price.getWidth()/2 - (int) bounds.getWidth()/2),
						 	  etDate.getY(),
						 	  (int) bounds.getWidth(),
						 	  (int) bounds.getHeight());
		
		historyBar.add(etPriceHis);
		
		s = "Fryzjer";
		bounds = metrics.getStringBounds(s, null);
		etHairdresser = new JLabel(s);
		etHairdresser.setFont(fontCustomer);
		etHairdresser.setForeground(ConfigureApp.etColorFont);
		etHairdresser.setBounds(hair.getX() + (hair.getWidth()/2 - (int) bounds.getWidth()/2),
						 	  	etDate.getY(),
						 	  	(int) bounds.getWidth(),
						 	  	(int) bounds.getHeight());
		
		historyBar.add(etHairdresser);
		
		historyBar.repaint();
		
	}
	
	public void setHistory() {
		
		
		historyLabel = new JLabel();
		ImageIcon imgIc = new ImageIcon("resources/images/history.png");
		historyLabel.setIcon(imgIc);
		historyLabel.setBounds(customerPanel.getX(),
							   addData.getY() + addData.getHeight() + 15,
							   imgIc.getIconWidth(),
							   imgIc.getIconHeight());
		
		h  = h + historyLabel.getHeight() + 20;
		historyLabel.setName("historyLabel");
		
		mainPanel.add(historyLabel);
		
		int x = MainWindow.tabbedPanel.getHeight() - h - 30;
		
		System.out.println("Wysoko�� karty: " + x);
		
		historyPanel = new JPanel();
		historyPanel.setLayout(null);
		historyPanel.setBackground(ConfigureApp.backPanelColorForm);
		historyPanel.setName("historyPanel");
		int height = 0;
		
		
			
		
		
		mainPanel.add(historyPanel);
		
		historyBar = new JPanel();
		historyBar.setBackground(ConfigureApp.backPanelColorForm); 
		historyBar.setLayout(null);
		historyBar.setBounds(0,
							 0,
							 historyLabel.getWidth(),
							 21);
		
		historyPanel.add(historyBar);
		
		historyPanel.setBounds(historyLabel.getX(),
				   			   historyLabel.getY() + historyLabel.getHeight()-2,
				   			   historyLabel.getWidth(),
				   			   historyBar.getHeight());
		
		
		
	}
	
	public JTextField getHistoryField(int x, int y, int width, String text, int number) {
		
		
		int y_bounds = y + 1;
		if(number==0) 
			y_bounds = y + historyBar.getY() + historyBar.getHeight() + 1;
			
		Font tempFont = new Font("Arial", Font.PLAIN, 18);
		
		JTextField field = new JTextField();
		field.setBackground(ConfigureApp.additionalFieldColor);
		field.setFont(tempFont);
		field.setBorder(null);
		field.setForeground(ConfigureApp.foreFontColor);
		field.setBounds(x, 
						y_bounds, 
						width, 
						heightH);
		field.setEditable(false);
		field.setHighlighter(null);
		field.setHorizontalAlignment(JTextField.CENTER);
		field.setText(text);
		//field.setMargin(new Insets(0,0,0,0));
		//field.setHorizontalAlignment(JTextField.LEFT);
		
		historyPanel.add(field);
		
		return field;
		
	}
	
	
	
	public void setStartPoint() {
		
		int x = 10;
		int y = 20;
		if(MainWindow.cardPanel.getSize().width>1024) 
			x = (MainWindow.cardPanel.getSize().width - 1024)/2;
		if(MainWindow.cardPanel.getSize().height>768)
			y = (MainWindow.cardPanel.getSize().width - 768)/2;
		
		startPoint = new Point(x,y);
		
	}
	
	
	
	public JPanel getMainPanel() {
		
		return mainPanel;
	}

	public List<JTextComponent> getObligatoryFieldList() {
		return obligatoryFieldList;
	}

	public List<JTextComponent> getAdditionalFieldList() {
		return additionalFieldList;
	}

	public JTextField getName() {
		return name;
	}

	public JTextField getSurname() {
		return surname;
	}

	public JTextField getNumber() {
		return number;
	}

	public JTextField getNaturalColor() {
		return naturalColor;
	}

	public JTextField getGreyColor() {
		return greyColor;
	}

	public JTextArea getDescription() {
		return description;
	}

	public JTextField getPrice() {
		return price;
	}

	public JTextField getTime() {
		return time;
	}

	public JButton getAddData() {
		return addData;
	}

	public JPanel getHistoryPanel() {
		return historyPanel;
	}
	
	public JButton getNewFormula() {
		return newFormula;
	}

	public JPanel getCustomerPanel() {
		return customerPanel;
	}

	public JLabel getHairdresser() {
		return hairdresser;
	}
	
	
	public void clearBorder() {
		
		if(prevIndex!=null) {
			
			formulaList.get(prevIndex).setBorder(null);
			dateList.get(prevIndex).setBorder(null);
			priceList.get(prevIndex).setBorder(null);
			timeList.get(prevIndex).setBorder(null);
			
		}
		
	}
	
	private Boolean checkNumber(String text) {
		
		Boolean incorrect = serviceData.isGoodNumber(text);
		
		if(!incorrect) {
			
			number.setBorder(BorderFactory.createCompoundBorder(
					ConfigureApp.mistakeWrite,
		   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
			number.setBackground(new Color(249,203,201));
			
		}
		
		else {
			
			number.setBorder(BorderFactory.createCompoundBorder(
					ConfigureApp.notWrite,
					BorderFactory.createEmptyBorder(0, 3, 0, 0)));
			number.setBackground(ConfigureApp.additionalFieldColor);
			
		}
		
		
		return incorrect;
	}
	
	private Customer isCustomerExist() {
		
		
		return serviceData.checkClient(number.getText(), surname.getText(), name.getText());
	
		
		
		
	}
	
	private void showCard(Customer c) {
		
		int size = MainWindow.tabbedPanel.getTabCount();
		int i=0;
		Boolean isThere=false;
		while(i<size) {
			if(MainWindow.tabbedPanel.getTitleAt(i).equals(c.getSurname() + " " + c.getName())) {
				isThere = true;
				MainWindow.tabbedPanel.setSelectedIndex(i);
				break;
			}
				
			i++;
		}
		if(!isThere) {
			if(ConfigureApp.isTabs()) {
				
				ConfigureApp.countTab();
				MainWindow.currentCard=4;
				MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
				
				ClientPanel cardClient = new ClientPanel(MainWindow.cardPanel.getSize(),0,c);
				MainWindow.listPanels.add(cardClient);
				
				MainWindow.tabbedPanel.addTab(c.getSurname() + " "  + c.getName(), 
											  cardClient.getMainPanel());

				int selIndex = MainWindow.tabbedPanel.getTabCount()-1;

				MainWindow.tabbedPanel.setForegroundAt(selIndex, ConfigureApp.fontAtTabColor);
				MainWindow.tabbedPanel.setSelectedIndex(selIndex);

				MainWindow.setWidgetBar();
				
				
				
			}	
			else {
				comm.dont("Masz zbyt du�o otwartych zak�adek");
			}
		}
		
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		JTextComponent textField = (JTextComponent) e.getComponent();
		
		
		textField.setBorder(BorderFactory.createCompoundBorder(ConfigureApp.setWrite,
				  											   BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		Iterator<JTextComponent> it = obligatoryFieldList.iterator();
		Iterator<JTextComponent> it2 = additionalFieldList.iterator();
		Iterator<String> itString = fieldString.iterator();
		
		String text =  textField.getText();
		
		while(itString.hasNext()) {
			
			String name = itString.next();
			System.out.println("Name textField: " + name);
			System.out.println("Pobrany text: " + text);
			if(name.equals(text)) {
				 textField.setText("");
			}
			else {
				 textField.selectAll();
				 textField.setSelectionColor(new Color(106,184,207));
			}
			
		}
		
		String name = e.getComponent().getName();
		Boolean wasThere = false;
		
		while (it.hasNext()) {
			
			String temp = it.next().getName();
			if(temp.equals(name)) {
				 textField.setForeground(ConfigureApp.foreFontColor);
				 wasThere = true;
			}
			
		}
		
		if(!wasThere) {
			while (it2.hasNext()) {
				
				String temp = it2.next().getName();
				if((temp.equals("number")) || (temp.equals("greyColor")) || (temp.equals("naturalColor"))) { 
					if( !(this.name.getText().equals(""))
						&& !(this.surname.getText().equals(""))
						&& !(this.name.getText().equals("Obowi�zkowe"))
						&& !(this.surname.getText().equals("Obowi�zkowe"))) {
							description.setEnabled(true);
							description.setBackground(ConfigureApp.additionalFieldColor);
							price.setEnabled(true);
							price.setBackground(ConfigureApp.additionalFieldColor);
							time.setEnabled(true);
							time.setBackground(ConfigureApp.additionalFieldColor);
							addData.setEnabled(true);
							
					}
					
					
				}
					
				
				
				if (temp.equals(name)) {
					 textField.setForeground(ConfigureApp.foreFontColor);
				}

			}
		}
		
		
		
		
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		
		JTextComponent tempField = (JTextComponent) e.getComponent();
		tempField.setBorder(BorderFactory.createCompoundBorder(
				ConfigureApp.notWrite,
				BorderFactory.createEmptyBorder(0, 3, 0, 0)));
		
		Iterator<JTextComponent> it2 = additionalFieldList.iterator();
		
		String name = e.getComponent().getName();
		Boolean notcheck = false;
		
		Boolean incorrect;
		
		if(name.equals("number")) {
			incorrect = checkNumber(tempField.getText());
	
		}
		
		
	/*	if(name.equals("number")) {
			String s = tempField.getText();
			if(!serviceData.isGoodNumber(s)) {
				incorretNumber = true;
				tempField.setBorder(BorderFactory.createCompoundBorder(
						ConfigureApp.mistakeWrite,
			   			BorderFactory.createEmptyBorder(0, 3, 0, 0)));
				tempField.setBackground(new Color(249,203,201));
			}
			else {
				tempField.setBorder(BorderFactory.createCompoundBorder(
						ConfigureApp.notWrite,
						BorderFactory.createEmptyBorder(0, 3, 0, 0)));
				tempField.setBackground(ConfigureApp.additionalFieldColor);
				incorretNumber = false;
			}
			
			Customer c = serviceData.checkClient(number.getText(), surname.getText(), this.name.getText());
			if((c!=null && !notcheck)) {
				
				System.out.println("W bazie znajduje si� taki osobnik");
				Boolean yes = comm.youSure("W bazie znajduje si� klient o podanych danych\n" +
										   "Co chcesz zrobi�?", 
										   0, "Poka� kart�", "Kontynuuj dodawanie klienta");
				if(yes) {
					int size = MainWindow.tabbedPanel.getTabCount();
					int i=0;
					Boolean isThere=false;
					while(i<size) {
						if(MainWindow.tabbedPanel.getTitleAt(i).equals(c.getSurname() + " " + c.getName())) {
							isThere = true;
							MainWindow.tabbedPanel.setSelectedIndex(i);
							break;
						}
							
						i++;
					}
					if(!isThere) {
						if(ConfigureApp.isTabs()) {
							
							ConfigureApp.countTab();
							MainWindow.currentCard=4;
							MainWindow.cl.show(MainWindow.cardPanel, "" + (MainWindow.currentCard));
							
							ClientPanel cardClient = new ClientPanel(MainWindow.cardPanel.getSize(),0,c);
							MainWindow.listPanels.add(cardClient);
							
							MainWindow.tabbedPanel.addTab(c.getSurname() + " "  + c.getName(), 
														  cardClient.getMainPanel());

							int selIndex = MainWindow.tabbedPanel.getTabCount()-1;

							MainWindow.tabbedPanel.setForegroundAt(selIndex, ConfigureApp.fontAtTabColor);
							MainWindow.tabbedPanel.setSelectedIndex(selIndex);

							MainWindow.setWidgetBar();
							
							if (ConfigureWindow.bar.getText().equals("------------------------")) {
								MainWindow.pane.setVisible(true);
								MainWindow.choosePanel.setVisible(true);
							}
							
						}	
						else {
							comm.dont("Masz zbyt du�o otwartych zak�adek");
						}
					}
				}
			}
			
			
		}*/
		
		
			
		
		
		customerPanel.repaint();
		
		
	}

	

	@Override
	public void mouseClicked(MouseEvent evt) {
		
		
			
			clearBorder();
			
			int i=0;
			
			if(formulaList.contains(evt.getComponent()))  
				i = formulaList.indexOf(evt.getComponent());
			else if(dateList.contains(evt.getComponent())) 
				i = dateList.indexOf(evt.getComponent());
			else if(priceList.contains(evt.getComponent())) 
				i = priceList.indexOf(evt.getComponent());
			else if(timeList.contains(evt.getComponent())) 
				i = timeList.indexOf(evt.getComponent());
			else if(hairList.contains(evt.getComponent())) 
				i = hairList.indexOf(evt.getComponent());

			Formula f = formulasList.get((dateList.size() - 1) - i);
			
			formulaList.get(i).setBorder(BorderFactory.createLineBorder(new Color(63,24,53)));
			dateList.get(i).setBorder(BorderFactory.createLineBorder(new Color(63,24,53)));
			priceList.get(i).setBorder(BorderFactory.createLineBorder(new Color(63,24,53)));
			timeList.get(i).setBorder(BorderFactory.createLineBorder(new Color(63,24,53)));
			hairList.get(i).setBorder(BorderFactory.createLineBorder(new Color(63,24,53)));
			prevIndex = i;
			
			MainWindow.pane.setVisible(true);
			
			FormulaWindow formulaWindow = new FormulaWindow(MainWindow.mainGlassPanel.getSize());
			formulaWindow.setFormula(f, formulaList.get(i), priceList.get(i), 
												timeList.get(i));
			MainWindow.mainGlassPanel.setPanel(formulaWindow.getMainPanel());
			MainWindow.mainGlassPanel.setVisible(true);
			
			
			//description.setText(f.getContent());
			//time.setText(f.getTime());
			//price.setText(f.getPrice());
			//formulaTemp = f;
			//formula = f;
		
		
	}



	@Override
	public void mouseEntered(MouseEvent evt) {
		
		//formulaTemp = new Formula(description.getText(), time.getText(), price.getText());
		
		int i=0;
		if(formulaList.contains(evt.getComponent())) 
			i = formulaList.indexOf(evt.getComponent());
		else if(dateList.contains(evt.getComponent())) 
			i = dateList.indexOf(evt.getComponent());
		else if(priceList.contains(evt.getComponent())) 
			i = priceList.indexOf(evt.getComponent());
		else if(timeList.contains(evt.getComponent())) 
			i = timeList.indexOf(evt.getComponent());
		else if(hairList.contains(evt.getComponent())) 
			i = hairList.indexOf(evt.getComponent());
		else if(deleteList.contains(evt.getComponent())) 
			i = deleteList.indexOf(evt.getComponent());
			
		
		deleteFormula = deleteList.get(i).getFormula();	
		System.out.println("Dlaczego tutaj to nie dzia�a? Formula: " + deleteFormula);
		
		formulaList.get(i).setBackground(new Color(253,227,253));
		dateList.get(i).setBackground(new Color(253,227,253));
		priceList.get(i).setBackground(new Color(253,227,253));
		timeList.get(i).setBackground(new Color(253,227,253));
		hairList.get(i).setBackground(new Color(253,227,253));
		
		formulaList.get(i).setBorder(ConfigureApp.notWrite);
		dateList.get(i).setBorder(ConfigureApp.notWrite);
		priceList.get(i).setBorder(ConfigureApp.notWrite);
		timeList.get(i).setBorder(ConfigureApp.notWrite);
		hairList.get(i).setBorder(ConfigureApp.notWrite);
		
		i=0;
	}



	@Override
	public void mouseExited(MouseEvent evt) {
		
		//description.setText(formulaTemp.getContent());
		//time.setText(formulaTemp.getTime());
		//price.setText(formulaTemp.getPrice());
		
		int i=0;
		if(formulaList.contains(evt.getComponent())) 
			i = formulaList.indexOf(evt.getComponent());
		else if(dateList.contains(evt.getComponent())) 
			i = dateList.indexOf(evt.getComponent());
		else if(priceList.contains(evt.getComponent())) 
			i = priceList.indexOf(evt.getComponent());
		else if(timeList.contains(evt.getComponent())) 
			i = timeList.indexOf(evt.getComponent());
		else if(hairList.contains(evt.getComponent())) 
			i = hairList.indexOf(evt.getComponent());
		else if(deleteList.contains(evt.getComponent())) 
			i = deleteList.indexOf(evt.getComponent());
		
		deleteFormula = null;
		
		formulaList.get(i).setBackground(ConfigureApp.additionalFieldColor);
		dateList.get(i).setBackground(ConfigureApp.additionalFieldColor);
		priceList.get(i).setBackground(ConfigureApp.additionalFieldColor);
		timeList.get(i).setBackground(ConfigureApp.additionalFieldColor);
		hairList.get(i).setBackground(ConfigureApp.additionalFieldColor);
		
		formulaList.get(i).setBorder(null);
		dateList.get(i).setBorder(null);
		priceList.get(i).setBorder(null);
		timeList.get(i).setBorder(null);
		hairList.get(i).setBorder(null);
		
		i=0;
		
	}



	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static String getNameTab() {
		
		return nameTab;
		
	}
	
	public  ClientPanel getInstance() {
		
		return this;
		
	}
	
}
