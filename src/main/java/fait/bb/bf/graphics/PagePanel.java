package fait.bb.bf.graphics;

import java.util.List;

import javax.swing.JPanel;



public abstract class PagePanel<T> extends JPanel {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -6726030424605633103L;

	public abstract void setListPanel(List<T> list);

}
