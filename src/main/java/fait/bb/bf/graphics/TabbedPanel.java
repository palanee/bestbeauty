package fait.bb.bf.graphics;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fait.bb.bf.graphics.components.customUI.TabbedPaneUI;

public class TabbedPanel {
	
	
	private JTabbedPane tabbedPanel;
	private Dimension size;
	private int numberT;
	
	TabbedPanel(Dimension size) {
		
		this.size = size;
		int insetLeft = 150;
		int insetTop = 120;
		numberT = 0;
		
		//tabbedPanel = new JTabbedPane();
		tabbedPanel.setBackground(ConfigureApp.backgroundColor);
		tabbedPanel.setUI(new TabbedPaneUI());
		tabbedPanel.setBounds(insetLeft, 
							  insetTop,
							  size.width - insetLeft/2,
							  size.height - insetTop/2);
		tabbedPanel.setAutoscrolls(true);
		
		
	}

	public void addTabs(String name ) {
		
		//tabbedPanel.addTab();
		
	}

	public JTabbedPane getTabbedPanel() {
		return tabbedPanel;
	}
	
	

}
