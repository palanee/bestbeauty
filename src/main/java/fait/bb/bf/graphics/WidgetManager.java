package fait.bb.bf.graphics;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

public class WidgetManager {
	
	Dimension screen;
	List<JButton> list;
	List<JButton> tempList;
	
	
	//czesc standardowa klasy
	public WidgetManager(Dimension screen, List<JButton> list) {
		super();
		this.screen = screen;
		this.list = list;
		tempList = new ArrayList<JButton>();
		setBoundsList();
	}

	public Dimension getScreen() {
		return screen;
	}

	public void setScreen(Dimension screen) {
		this.screen = screen;
	}

	public List<JButton> getList() {
		return tempList;
	}

	public void setList(List<JButton> list) {
		this.list = list;
	}
	
	//czesc wlasciwa klasy
	private void setBoundsList() {
		
		int size=list.size();
		int i;
		Point central = new Point(screen.width/2, 
								  screen.height/2);
		
		switch(size) {
			case 1: 
				JButton w = list.get(0);
				
				w.setBounds((int)screen.width/2-(int) list.get(0).getSize().width/2,
									  (int)screen.height/2-(int) list.get(0).getSize().height/2,
									  (int) list.get(0).getSize().width,
									  (int) list.get(0).getSize().height);
				
				tempList.add(w);
				break;
			
			case 2:
				i=0;
				list.get(i).setLocation(central.x-20-list.get(i).getWidth(),
									  central.y-list.get(i).getHeight()/2);
				i++;
				list.get(i).setLocation(central.x+20,
						  				central.y-list.get(i).getHeight()/2);
				
				break;
			case 3:
				i=0;
				int height = 20;
				int width = (screen.width-100)/3;
				list.get(i).setLocation(width/2 - list.get(i).getWidth()/2,
										central.y-list.get(i).getHeight()/2 - height);
				i++;
				list.get(i).setLocation(width + width/2 - list.get(i).getWidth()/2,
										central.y-list.get(i).getHeight()/2 - height);
				i++;
				list.get(i).setLocation(2*width + width/2 - list.get(i).getWidth()/2,
										central.y-list.get(i).getHeight()/2 - height);
				
				
				
				
				break;
				
		}
		
		
		
		
		
	}
	

}
