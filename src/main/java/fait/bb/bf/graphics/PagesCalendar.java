package fait.bb.bf.graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import fait.bb.bf.graphics.calendar.CalendarPanelNew;
import fait.bb.bf.graphics.components.ButtonSite;

public class PagesCalendar<T> {
	
	private List<T> list;
	
	//liczba oznaczaj�ca ilo�� element�w na stron�
	private int elemPage;
	
	//panel, do kt�rego maj� zosta� dodane przyciski
	private JPanel panelButton;
	private List<ButtonSite> buttonList;
	private JButton left;
	private JButton right;
	
	private int numberPages;
	
	private PagePanel<T> pagePanel;
	public int currentSite;
	
	public PagesCalendar(List<T> list, int elemPage, JPanel panelButton) {
		
		
		
		this.list = list;
		System.out.println("Lista u�ytkownik�w: " + list.size() + " " + list);
		
		this.elemPage = elemPage;
		this.panelButton = panelButton;
		
		
		numberPages = 0;
		currentSite = 1;
		
	}
	
	public void setPagePanel(PagePanel<T> pagePanel) {
		
		this.pagePanel = pagePanel;
		
	}
	
	
	public void setButtons(JButton left, JButton right) {
		
		this.left = left;
		this.right = right;
		
	}
	
	
	public void setPages() {
		
		
		if(list.size()<=elemPage) {
			System.out.println("PagesCalendar line:68 - Wykonujemy pierwszy warunek");
			numberPages = 1;
			pagePanel.setListPanel(list);
		}
		else {
			int number = list.size()/elemPage;
			if(list.size()%elemPage!=0)
				number++;
			
			numberPages = number;
			System.out.println("Liczba stron: " + numberPages);
		
			
			buttonList = new ArrayList<ButtonSite>();
			int i=1;
			int length = 10;
			int x = 0;
			int width = panelButton.getWidth();
			if(left!=null && right!=null)
				width = right.getX() - left.getX() + left.getWidth();
			
			while(i<=number) {
				
				final ButtonSite temp;
				
				
				if(i==currentSite) {
					temp = new ButtonSite(i,1);
					//x = (width - number*temp.getWidth() - number*length)/2;
				}
				else {
					temp = new ButtonSite(i,0);
					//x = x + temp.getWidth() + length;
				}
				
				if(i==1) {
					x = (width - number*temp.getWidth() - number*length)/2;
				}
				else {
					x = x + temp.getWidth() + length;
				}
				
				if(left!=null) 
					temp.setLocation(x, (left.getHeight() - temp.getHeight())/2);
				else
					temp.setLocation(x, (panelButton.getHeight() - temp.getHeight())/2);
				
				temp.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent zdarz) {
						System.out.println("Klikni�to w button: " + temp.getNumber());
						pagePanel.removeAll();
						
						currentSite = temp.getNumber();
						Iterator<ButtonSite> it = buttonList.iterator();
						while(it.hasNext()) {
							ButtonSite temp = it.next();
							if(temp.getNumber()==currentSite)
								temp.changeIcon(1);
							else
								temp.changeIcon(0);
							
						}
						
					
						paintList();
						
						pagePanel.repaint();
						
						
						
					}
				});
				
				panelButton.add(temp);
				buttonList.add(temp);
				
				i++;
				
			}
			
			paintList();
		}
		
		pagePanel.repaint();
		
	}
	
	private void paintList() {
		
		if((list.size() - currentSite*elemPage)> 0) {
			pagePanel.setListPanel(list.subList((currentSite-1)*elemPage, currentSite*elemPage));
			System.out.println("List od: " + ((currentSite-1)*elemPage) + " do: " + (currentSite*elemPage));
			
		}
		else {
			pagePanel.setListPanel(list.subList((currentSite-1)*elemPage, list.size()));
			System.out.println("List od: " + ((currentSite-1)*elemPage) + " do: " + (list.size()));
		}
		
	}
	
	public int getNumberPages() {
		return numberPages;
	}

}
