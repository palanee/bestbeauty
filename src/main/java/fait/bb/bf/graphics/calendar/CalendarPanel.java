package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.RepaintManager;
import javax.swing.plaf.ComboBoxUI;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.customUI.CalendarComboBoxUI;
import fait.bb.bf.graphics.components.customUI.ComboBoxRenderer;
import fait.bb.bf.graphics.panels.glasspanels.AddDate;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;


public class CalendarPanel {
	
	private JPanel mainPanel;
	
	static JPanel whitePanel;

	private static JPanel navigationPanel;
	private JButton chooseButton;
	private JButton changeView;
	private JButton addDate;
	private JButton today;
	public static JComboBox comboBox;
	private String[] tabUser;
	
	public static JPanel calendarPanel;
	public static CalendarMonth calendarMonth;
	private static int currentSite = 0;
	
	private static JButton rightArrow;
	private static JButton leftArrow;
	
	private static int widthPanelArrow;
	private static int count = 0;
	public static Calendar currentDate = null;
	
	private ServiceData serviceData;
	private static CalendarApp calendarApp;
	
	private final static String nameTab = "CalendarPanel";
	
	private static List<CalendarDay> calendarDayList;
	
	public static int sizeTab;
	
	public CalendarPanel() {
		
		mainPanel = new JPanel();
		mainPanel.setBackground(ConfigureApp.backgroundCardColor);
		mainPanel.setLayout(null);
		
		serviceData = new ServiceData();
		calendarApp = new CalendarApp();
		currentDate = ConfigureApp.date.getCalDate();
		
		tabUser = serviceData.parseToString(serviceData.getUsers(" AND status!='ZWOLNIONY' "));
		sizeTab = tabUser.length;
		setComponents();
		
	}
	
	
	
	public void setComponents() {
		
		setWhitePanel();
		setNavigationPanel();
		setCalendarPanel();
		
		whitePanel.repaint();
	}
	
	private void setWhitePanel() {
		
		whitePanel = new JPanel();
		whitePanel.setBackground(ConfigureApp.backgroundColor);
		whitePanel.setBorder(ConfigureApp.panelBorder);
		whitePanel.setLayout(null);
		whitePanel.setBounds(10, 10,
							 MainWindow.tabbedPanel.getWidth() - 20,
							 MainWindow.tabbedPanel.getHeight()- 60);
		
		mainPanel.add(whitePanel);
		
		leftArrow = new JButtonOwn("buttons/leftCalendar");
		widthPanelArrow = leftArrow.getWidth() + 20;
		leftArrow.setLocation(widthPanelArrow - leftArrow.getWidth() - 5,
							  whitePanel.getHeight()/2 - leftArrow.getHeight()/2);
		leftArrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				
				//System.out.println("Przed klikni�ciem: " + currentDate.get(Calendar.DAY_OF_MONTH));
				currentDate.add(Calendar.DAY_OF_MONTH, -(count-1));
				System.out.println("Po klikni�ciu data: " + currentDate.get(Calendar.DAY_OF_MONTH));
				setCalendarPanel();
				calendarPanel.repaint();
				
			}
		});
		
		
		
		
		rightArrow = new JButtonOwn("buttons/rightCalendar");
		rightArrow.setLocation(whitePanel.getWidth() - widthPanelArrow + 5,
							  whitePanel.getHeight()/2 - rightArrow.getHeight()/2);
		
		rightArrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				calendarPanel.removeAll();
				//System.out.println("Przed klikni�ciem: " + currentDate.get(Calendar.DAY_OF_MONTH));
				currentDate.add(Calendar.DAY_OF_MONTH, count-1);
				System.out.println("Po klikni�ciu data: " + currentDate.get(Calendar.DAY_OF_MONTH));
				setCalendarPanel();
				calendarPanel.repaint();
				
			}
		});
		
		whitePanel.add(leftArrow);
		whitePanel.add(rightArrow);
		
		
		
	}

	/**
	 * comboBox umo�liwia wyb�r uzytkownika, dla kt�rego wy�wietlany
	 * jest kalendarz wizyt
	 * addDate umo�liwia dodanie nowej wizyty w kalendarzu
	 * Zweryfikowa� dzia�anie setFields i setCard
	 */
	
	private void setNavigationPanel() {
		
		int height = 60;
		int widthIcon = 15;
		
		navigationPanel = new JPanel();
		navigationPanel.setLayout(null);
		navigationPanel.setOpaque(false);
		navigationPanel.setBounds(0,
								  0,
								  whitePanel.getWidth() - 2,
								  height);
		
		whitePanel.add(navigationPanel);

		comboBox = new JComboBox(tabUser);
		comboBox.setUI((ComboBoxUI) CalendarComboBoxUI.createUI(comboBox));
		comboBox.setRenderer(new ComboBoxRenderer(0));
		comboBox.setFont(ConfigureApp.font.deriveFont(Font.BOLD, 15));
		comboBox.setForeground(new Color(119,42,115));
		comboBox.setBackground(ConfigureApp.backgroundColor);
		if(tabUser.length>0)
			comboBox.setSelectedIndex(0);
		comboBox.setBounds(leftArrow.getX() + leftArrow.getWidth(),
						   10,
						   280,
						   40);
		
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				calendarPanel.removeAll();
				setCalendarPanel();
				calendarPanel.repaint();
				
			}
		});
		
		comboBox.addContainerListener(new ContainerListener() {
			
			@Override
			public void componentRemoved(ContainerEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentAdded(ContainerEvent arg0) {
				System.out.println("YUPI!");
				comboBox.repaint();
				
			}
		});
		
		navigationPanel.add(comboBox);
		
		
		
		changeView = new JButtonOwn("changeView");
		changeView.setLocation(navigationPanel.getWidth()/2 + 50,
							   comboBox.getY() + comboBox.getHeight()/2 - changeView.getHeight()/2 + 2);
		changeView.setToolTipText("Zmie� widok kalendarza");
		changeView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				//calendarPanel.removeAll();
				if(currentSite==0) {
					currentSite=1;
					calendarPanel.removeAll();
					setCalendarPanelMonth();
					rightArrow.setVisible(false);
					leftArrow.setVisible(false);
					calendarPanel.repaint();
				}
				else {
					refreshCalendar();
				}
				
				
			}
		});

		
		navigationPanel.add(changeView);
		
		addDate = new JButtonOwn("addDate");
		addDate.setLocation(changeView.getX() + changeView.getWidth() + widthIcon,
							changeView.getY());
		addDate.setToolTipText("Dodaj wizyt�");
		addDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				
				//MainWindow.addDate.repaint();
				
				
				ConfigureApp.activeTab = nameTab;
				
				MainWindow.addDate.setFields(null, null, null, null);
				MainWindow.addDate.setCard("add", "user", true);
				//AddDate addDat = new AddDate();
				//addDat.setVisible(true);
				
				
			}
		});
		
		navigationPanel.add(addDate);
		
		today = new JButtonOwn("today");
		today.setLocation(addDate.getX() + addDate.getWidth() + widthIcon,
						  addDate.getY());
		today.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				if(currentSite==0) {
					calendarPanel.removeAll();
					//System.out.println("Przed klikni�ciem: " + currentDate.get(Calendar.DAY_OF_MONTH));
					currentDate = Calendar.getInstance();
					System.out.println("Po klikni�ciu data: " + currentDate.get(Calendar.DAY_OF_MONTH));
					setCalendarPanel();
					calendarPanel.repaint();
				}
				//je�li jeste�my na widoku miesi�ca
				else {
					currentSite=0;
					calendarPanel.removeAll();
					whitePanel.remove(calendarMonth);
					currentDate = Calendar.getInstance();
					setCalendarPanel();
					rightArrow.setVisible(true);
					leftArrow.setVisible(true);
					calendarPanel.repaint();
					whitePanel.repaint();
					
				}
				
				
				
			}
		});
		
		navigationPanel.add(today);
		
	}
	
	public static void refreshCalendar() {
		
		currentSite=0;
		
		calendarPanel.removeAll();
		whitePanel.remove(calendarMonth);
		setCalendarPanel();
		rightArrow.setVisible(true);
		leftArrow.setVisible(true);
		calendarPanel.repaint();
		whitePanel.repaint();
		
	}
	
	private void setCalendarPanelMonth() {
		
		
		Dimension size = new Dimension(whitePanel.getWidth() - 2*widthPanelArrow,
				  					   whitePanel.getHeight() - navigationPanel.getHeight() - 10);
		calendarMonth = new CalendarMonth(currentDate.get(Calendar.MONTH), 
														currentDate.get(Calendar.YEAR),
														size);
		calendarMonth.setSize(size);
		calendarMonth.setLocation(whitePanel.getWidth()/2 - calendarPanel.getWidth()/2,
					  navigationPanel.getY() + navigationPanel.getHeight() + 10);
		whitePanel.add(calendarMonth);
		
	}

	/**
	 * Tworzy panel kalendarza
	 */
	
	public static void setCalendarPanel() {
		
		calendarPanel = new JPanel();
		RepaintManager.currentManager(calendarPanel).markCompletelyClean(calendarPanel);
		calendarPanel.setLayout(null);
		calendarPanel.setOpaque(false);
		calendarPanel.setSize(whitePanel.getWidth() - 2*widthPanelArrow,
							  whitePanel.getHeight() - navigationPanel.getHeight() - 10);
		calendarPanel.setLocation(whitePanel.getWidth()/2 - calendarPanel.getWidth()/2,
								  navigationPanel.getY() + navigationPanel.getHeight() + 10);
		whitePanel.add(calendarPanel);
		
		
		//lista dni wy�wietlanych u�ytkownikowi
		calendarDayList = new ArrayList<CalendarDay>();
		
		//ustawiam, ile dni zmieni�ci si� na jednej stronie
		countDays(CalendarDay.getWidthDay());
		
		List<DateOfDay> list =  calendarApp.getStringTopDate(count, currentDate);
		
		currentDate.add(Calendar.DAY_OF_MONTH, -(count));
		
		/*if(currentDate==null) {
			list.get(0).getCalDate();
			currentDate = Calendar.getInstance();
		}*/
		
		
		
		int i = 0;
		
		
		int startX = getStartX(count);
		
		
		while(i<count) {
				
			CalendarDay temp = new CalendarDay(calendarPanel.getSize(), list.get(i), 
								(String) comboBox.getSelectedItem(),
								comboBox.getSelectedIndex());
			RepaintManager.currentManager(calendarPanel).markCompletelyClean(calendarPanel);
			temp.setIgnoreRepaint(true);
			temp.setLocation(startX, 0);
			calendarDayList.add(temp);
			calendarPanel.add(temp);
			
			i++;
			startX = temp.getX() + temp.getWidth();
			temp.repaint();
		}
		
		
		
	}
	
	
	public static void countDays(int width) {
		
		
		count = calendarPanel.getWidth()/width;		
		
		
	}
	
	public static int getStartX(int count) {
		
		
		int tempWidth = count * CalendarDay.getWidthDay();
		int startX = calendarPanel.getWidth()/2 - tempWidth/2;
		return startX;
		
	}
	
	public void updateValue() {
		
		tabUser = serviceData.parseToString(serviceData.getUsers(" AND status!='ZWOLNIONY' "));
		
	}

	public JPanel getMainPanel() {
		// TODO Auto-generated method stub
		return mainPanel;
	}
	
	public static String getNameTab() {
		
		return nameTab;
		
	}

}
