package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureApp.LabelType;
import fait.bb.bf.graphics.components.JButtonColor;
import fait.bb.bf.graphics.components.customUI.PopMenuItemUI;
import fait.bb.bf.graphics.panels.glasspanels.AddDate;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.Customer;
import fait.bb.bf.logic.ServiceData;


public class CalendarHourLabel extends JLabel implements MouseListener {
	
	Font font;
	FontMetrics metrics;
	Rectangle2D bounds;
	
	private Color backgroundPanel = new Color(255,244,230);
	
	private static int rowHeight = 31;
	private ServiceData serviceData;
	private float h;
	private ConfigureApp.LabelType type;
	private int count;
	
	private JButtonColor visit;
	private JLabel etVisit;
	private String titleString;
	
	public Boolean isClicked = false;
	
	private Boolean isFirst = true;
	
	private JPanel hourPanel;
	private JLabel etHour;
	
	private Boolean isCalendar = false;
	private Boolean isSetHalf = false;
	
	
	
	public CalendarHourLabel(float h, ConfigureApp.LabelType type, int count, JPanel hourPanel) {
		
		font = new Font("Arial", Font.BOLD, 11);
		//Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 12);
		metrics = new FontMetrics(font) {};
		setSize(215,rowHeight);
		setLayout(null);
		
		serviceData = new ServiceData();
		this.h = h;
		this.type = type;
		this.count = count;
		titleString = "Ilo�� wizyt: " + count;
		this.hourPanel = hourPanel;
		
		setLabel();
		
		addMouseListener(this);
		
		
		
	}
	
	private void setLabel() {
		
		final JLabel hour = new JLabel();
		hour.setOpaque(true);
		hour.setBackground(new Color(237,234,234));
		hour.setSize(43, rowHeight-1);
		add(hour);
		
		etHour = new JLabel();
		etHour.setFont(font);
		etHour.setForeground(new Color(112,109,109));
		etHour.setText(serviceData.roundNumber(2, h));
		bounds = metrics.getStringBounds(serviceData.roundNumber(2, h), null);
		etHour.setLocation(hour.getWidth()/2 - (int) bounds.getWidth()/2,
					   hour.getHeight()/2 - (int) bounds.getHeight()/2);
		etHour.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		hour.add(etHour);
		
	
		Dimension size = new Dimension(getWidth(), getHeight()-1);
		
		visit = new JButtonColor(type.getColor(), size);
		//visit.setBackground(type.getColor());
		visit.setSize(getWidth(),
					  getHeight()-1);
		visit.setLocation(hour.getX() + hour.getWidth(),
						  hour.getY());
		visit.setBorderPainted(false);
		visit.setFocusPainted(false);
		visit.setLayout(null);

		
		//visit.setForeground(type.getColorFont());
		add(visit);
		
		etVisit = new JLabel();
		String s = "Ilo�� wizyt: ";
		bounds = metrics.getStringBounds(titleString, null);
		etVisit.setForeground(type.getColorFont());
		etVisit.setText(titleString);
		etVisit.setFont(font);
		etVisit.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		etVisit.setLocation(visit.getWidth()/2 - etVisit.getWidth()/2,
							visit.getHeight()/2 - etVisit.getHeight()/2);
		
		visit.add(etVisit);
		
		
	}
	
	public void setListHalfHour() {
		
		List<String> list = new ArrayList<String>();
		
		list.add((int) h + ":" + "00");
		list.add((int) h + ":" + "15");
		list.add((int) h + ":" + "30");
		list.add((int) h + ":" + "45");
		
		
		final JPopupMenu popMenu = new JPopupMenu();
		popMenu.setBorderPainted(false);
		popMenu.setBorder(null);
		
		Iterator<String> it = list.iterator();
		popMenu.setPopupSize(this.getWidth(), 20 * list.size());
		
		while(it.hasNext()) {
			
			final String s = it.next();
			JMenuItem menuItem = new JMenuItem(s);
			menuItem.setUI(new PopMenuItemUI());
			menuItem.setFont(new Font("Arial", Font.BOLD, 14));
			menuItem.setForeground(new Color(224,132,13));
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					
					AddDate.hourString = s;
					etHour.setText(s);
					popMenu.hide();
					isSetHalf = false;
				}
			});

			popMenu.add(menuItem);
			
			
		}
		
		popMenu.show(hourPanel, this.getX(), this.getY() + this.getHeight());
		
	}
	
	//zmienia komponenty po klikni�ciu na g��wny komponent
	public void setClicked(Boolean isCalendar, Boolean isEdit) {
		
		this.isCalendar = isCalendar;
		
		AddDate.hourString = getHour() + ":00";
		
		if(!isFirst)
			setListHalfHour();
		if(isSetHalf)
			setListHalfHour();
		
		if(MainWindow.addDate.isTextFull())
			MainWindow.addDate.getSave().setEnabled(true);
		else
			MainWindow.addDate.getSave().setEnabled(false);
		
		
		isClicked = true;
		int index = AddDate.listHours.indexOf(this);
		
		AddDate.listHours.get(index).setIsClicked(isClicked);
		
		if(!isSetHalf && !isEdit)
			count++;

		font = new Font("Arial", Font.BOLD, 13);
		visit.setColor(new Color(32, 157, 7));
		etVisit.setForeground(Color.WHITE);
		titleString = "Ilo�� wizyt: " + count;
		etVisit.setText(titleString);
		
		
		
		metrics = new FontMetrics(font) {};
		etVisit.setFont(font);
		bounds = metrics.getStringBounds(titleString, null);
		etVisit.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		etVisit.setLocation(visit.getWidth() / 2 - etVisit.getWidth() / 2,
				visit.getHeight() / 2 - etVisit.getHeight() / 2);
		
	}
	
	public void setNotClicked() {
		
		AddDate.HOUR = null;
		isClicked = false;

		font = new Font("Arial", Font.BOLD, 11);

		etVisit.setForeground(type.getColorFont());
		visit.setColor(type.getColor());
		
		metrics = new FontMetrics(font) {
		};
		
		count--;
		titleString = "Ilo�� wizyt: " + count;
		etVisit.setText(titleString);
		etVisit.setFont(font);
		bounds = metrics.getStringBounds(titleString, null);
		etVisit.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		etVisit.setLocation(visit.getWidth() / 2 - etVisit.getWidth() / 2,
				visit.getHeight() / 2 - etVisit.getHeight() / 2);
	}
	
	public JButton getVisit() {
		return visit;
	}
	
	public Float getHour() {
		return h;
	}
	
	public void setIsClicked(Boolean bool) {
		
		isClicked = bool;
	}
	
	public Boolean getIsClicked() {
		
		return isClicked;
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
		
		
		if(AddDate.hourString==null) {
			
			if (!isClicked) {
		
				isFirst = false;
				setClicked(false, false);

			} 
			else {
				isSetHalf = true;
				isFirst = false;
				setClicked(true, false);
				
			}
		
		}
		
		else if(AddDate.hourString!=null) {
			
			if(isClicked) {
				
				setNotClicked();
				
			}
			
			else {
				
				
				
				Iterator<CalendarHourLabel> it = AddDate.listHours.iterator();
				while(it.hasNext()) {
					CalendarHourLabel temp = it.next();
					if(temp.getIsClicked()) {
						temp.setIsClicked(false);
						AddDate.hourString = null;
						temp.setNotClicked();
					}
				}
				isFirst = false;
				setClicked(false, false);
				
			}
			
		}
		
		
		
		
		
	}
	
	public static int getHeightW() {
		
		return rowHeight;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
