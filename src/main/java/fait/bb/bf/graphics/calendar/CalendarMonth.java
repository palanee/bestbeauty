package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.ServiceData;


public class CalendarMonth extends JPanel {
	
	private int currentMonth;
	private String monthName;
	private int currentYear;
	
	private JLabel etMonth;
	private JButton arrowLeft;
	private JButton arrowRight;
	
	private JPanel dayPanel;
	private JPanel titlePanel;
	private JPanel namesPanel;
	
	private JPanel mainPanel;
	private Color fontColor = new Color(119,42,115);
	
	public static Calendar chooseDate;
	public static Boolean isClicked = false;
	
	int width = 112;
	int height = 63;
	int space = 2;
	
	private Calendar cal;
	
	
	CalendarMonth(int currentMonth, int currentYear, Dimension size) {
		
		setLayout(null);
		setOpaque(false);
		setSize(size);
		setLocation(0,0);
		
		this.currentMonth = currentMonth;
		this.currentYear = currentYear;
		System.out.println("Aktualny miesi�c: " + currentMonth);
		
		cal = Calendar.getInstance();
		chooseDate = Calendar.getInstance();
		
		setComponents();
	}
	
	private void setComponents() {
		
		setMainPanel();
		
		setTitlePanel();
		setTitleMonth();
		
		setNamesPanel();
		setDayPanel();
		setDayOfMonth();
		setArrows();
	}
	
	private void setMainPanel() {
		
		arrowLeft = new JButtonOwn("buttons/leftMonth");
		
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setOpaque(false);
		mainPanel.setSize(width*7+6*space,height*6+4*space+120);
		mainPanel.setLocation(getWidth()/2 - mainPanel.getWidth()/2,
							  10);
		add(mainPanel);
		
		
		
	}
	
	private void setTitleMonth() {
		
		Font font = new Font("Arial", Font.BOLD, 22);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds;
		
		String s = CalendarApp.getNameOfMonth(currentMonth) + " " + currentYear;
		System.out.println("Tytu� panelu miesi�ca: " + s);
		bounds = metrics.getStringBounds(s, null);
		etMonth = new JLabel(s);
		etMonth.setFont(font);
		etMonth.setForeground(fontColor);
		etMonth.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
		etMonth.setLocation(mainPanel.getWidth()/2 - etMonth.getWidth()/2,
							5);
		titlePanel.add(etMonth);
		
		
		
		
		
	}
	
	
	
	
	private void setTitlePanel() {
		
		titlePanel = new JPanel();
		titlePanel.setLayout(null);
		titlePanel.setOpaque(false);
		titlePanel.setSize(width*7+6*space,
						   29);
		
		titlePanel.setLocation(0, 
							  0);
		mainPanel.add(titlePanel);
		
		
		
	}
	
	private void setArrows() {
		
		//arrowLeft = new JButtonOwn("buttons/leftMonth");
		
		arrowLeft.setLocation(mainPanel.getX() - 10 - arrowLeft.getWidth(),
							  mainPanel.getHeight()/2 - arrowLeft.getHeight()/2);
		//arrowLeft.setLocation(0,10);
		arrowLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				dayPanel.removeAll();
				titlePanel.removeAll();
				cal.add(Calendar.MONTH, -1);
				currentMonth = cal.get(Calendar.MONTH);
				currentYear = cal.get(Calendar.YEAR);
				setTitleMonth();
				setDayOfMonth();
				dayPanel.repaint();
				titlePanel.repaint();
			}
		});
		
		add(arrowLeft);
		
		arrowRight = new JButtonOwn("buttons/rightMonth");
		arrowRight.setLocation(mainPanel.getX() + mainPanel.getWidth() + 10,
							   mainPanel.getHeight()/2 - arrowRight.getHeight()/2);
		arrowRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				dayPanel.removeAll();
				titlePanel.removeAll();
				cal.add(Calendar.MONTH, 1);
				currentMonth = cal.get(Calendar.MONTH);
				currentYear = cal.get(Calendar.YEAR);
				setTitleMonth();
				setDayOfMonth();
				dayPanel.repaint();
				titlePanel.repaint();
			}
		});
		
		add(arrowRight);
		
	}
	
	private void setNamesPanel() {
		
		Font font = new Font("Arial", Font.BOLD, 18);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds;
		
		int i = 0;
		int j = i+1;
		CalendarApp.Day[] days = CalendarApp.Day.values();
		
		CalendarApp.Day temp = days[0];
		
		while(i<days.length) {
			if(j<days.length)
				days[i] = days[j];
			else
				days[i] = temp;
			i++;
			j++;
		}
		
		i = 0;
		
		int y = titlePanel.getY() + titlePanel.getHeight() + 20;
		
		while(i<days.length) {
			String s = days[i].getName();
			bounds = metrics.getStringBounds(s, null);
			JLabel lab = new JLabel();
			lab.setText(s);
			lab.setForeground(fontColor);
			lab.setFont(font);
			lab.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
			lab.setLocation((width+2)*i + width/2 - lab.getWidth()/2, 
							 y);
			mainPanel.add(lab);
			
			i++;
		}
		
	}
	
	private void setDayPanel() {
		
		dayPanel = new JPanel();
		dayPanel.setLayout(null);
		dayPanel.setOpaque(false);
		dayPanel.setSize(width*7+6*space,
						 mainPanel.getHeight() - titlePanel.getHeight() - 40);
		
		dayPanel.setLocation(mainPanel.getWidth()/2 - dayPanel.getWidth()/2, 
							 etMonth.getY() + etMonth.getHeight() + 40);
		mainPanel.add(dayPanel);
		
	}
	
	private void setDayOfMonth() {
		
		Font font = new Font("Arial", Font.PLAIN, 22);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds;
		
		int kolumn = 0;
		int wiersz = 0;
		
		int last = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		kolumn = cal.get(Calendar.DAY_OF_WEEK) - 2;
		if(kolumn<0)
			kolumn=6;
	
		
		
		
		for(int i=1; i<=last; i++) {
			
			JButton but = new JButtonOwn("buttons/backDay");
			but.setLayout(null);
			//but.setSize(width, height);
			but.setLocation(kolumn*(width+1),
							wiersz*(height+2));
			dayPanel.add(but);
			
			JLabel lab = new JLabel();
			String s = Integer.toString(i);
			bounds = metrics.getStringBounds(s, null);
			lab.setText(s);
			lab.setForeground(new Color(53,44,44));
			lab.setFont(font);
			lab.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
			
			lab.setLocation(but.getWidth()/2 - lab.getWidth()/2,
							but.getHeight()/2 - lab.getHeight()/2);
			but.add(lab);
			final int dayOfMonth = i;
			but.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					
					chooseDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
					chooseDate.set(Calendar.MONTH,cal.get(Calendar.MONTH));
					chooseDate.set(Calendar.YEAR, cal.get(Calendar.YEAR));
					CalendarPanelNew.currentDate = chooseDate;
					CalendarPanelNew.refreshCalendar();
					
				}
			});


			
			
			kolumn++;
			if(kolumn==7) {
				kolumn=0;
				wiersz++;
			}
			
			
		}
		
	}
	
	
}
