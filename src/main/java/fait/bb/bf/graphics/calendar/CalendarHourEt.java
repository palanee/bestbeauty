package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.DayWork;

public class CalendarHourEt extends JLabel {
	
	private final static int rowHeight = 38;
	private final int rowWidth = 75;
	
	private static int startHour;
	private static int endHour;
	private static int rows;
	
	private Boolean isFound = false;
	private DateOfDay date;
	
	
	public CalendarHourEt(DateOfDay date) {
		
		//this.user = user;
		this.date = date;
		
		setStartAndEnd();
		
		setPreferredSize(new Dimension(rowWidth, rows*rowHeight));
		setMinimumSize(new Dimension(rowWidth, rows*rowHeight));
		setMaximumSize(new Dimension(rowWidth, rows*rowHeight));
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(ConfigureApp.backgroundColor);
		//setBorder(BorderFactory.createLineBorder(new Color(194,115,12)));
		setBorder(null);
		
		paintRows();
		
	}
	
	CalendarHourEt() {
		
		//this.user = user;
		
		
		setMaxStartAndEnd();
		
		setPreferredSize(new Dimension(rowWidth, rows*rowHeight));
		setMinimumSize(new Dimension(rowWidth, rows*rowHeight));
		setMaximumSize(new Dimension(rowWidth, rows*rowHeight));
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(ConfigureApp.backgroundColor);
		//setBorder(BorderFactory.createLineBorder(new Color(194,115,12)));
		setBorder(null);
		
		paintRows();
		
	}
	
	private void setMaxStartAndEnd() {
		
		if(ConfigureApp.listDayWork==null || ConfigureApp.listDayWork.size()==0) {
			startHour = 8;
			endHour = 19; 
			
		}
		else {
			startHour = (int) ConfigureApp.minDayStart;
			endHour = ConfigureApp.maxDayEnd;
		}
		
		rows = (int) (endHour - startHour) * 4;
		
		
	}
	
	private void setStartAndEnd() {
		
		if(ConfigureApp.listDayWork==null || ConfigureApp.listDayWork.size()==0) {
			startHour = 8;
			endHour = 19; 
			isFound = true;
		}
		else {
			
			
			Iterator<DayWork> it = ConfigureApp.listDayWork.iterator();
			while(it.hasNext()) {
				DayWork plan = it.next();
				if(plan.getDay()==date.getDayWeek()) {
					isFound = true;
					startHour = plan.getStart();
					endHour = plan.getEnd();
				}
			}
		}
		
		if(!isFound) {
			startHour = (int) ConfigureApp.minDayStart;
			endHour = startHour + 10; 
		}
		rows = (int) (endHour - startHour) * 4;
		
	}
	
	
	
	
	private void paintRows() {
		
		
		String s;
		
		for(int i=startHour; i<endHour; i++) {
			
			for(int j=0; j<60; j=j+15) {
				
				
				if(j==0) 
					s = i + ":" + "00";
				else
					s = i + ":" + j;

				JLabel temp = paintSingleRow(s);
				add(temp);
				
			}
			
			
			
			
		}
		
		
	}
	
	public static int getLocationHour(int hour) {
		
		if(hour>=startHour && hour<endHour)
			return rowHeight * ((hour - startHour) * 4 - 1);
		else
			return rowHeight * rows;
	
			
	}
	
	private JLabel paintSingleRow(String s) {
		
		JLabel temp = new JLabel();
		temp.setLayout(null);
		//temp.setOpaque(true);
		//temp.setBackground(new Color(232,238,242));
		//temp.setOpaque(true);
		//temp.setBackground(Color.gray);
		temp.setPreferredSize(new Dimension(getPreferredSize().width, rowHeight));
		temp.setMinimumSize(new Dimension(getMinimumSize().width, rowHeight));
		temp.setMaximumSize(new Dimension(getMaximumSize().width, rowHeight));
		temp.setAlignmentX(JButton.CENTER_ALIGNMENT);
		
		Dimension size = temp.getPreferredSize();
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 12);
		FontMetrics metrics = new FontMetrics(font) {};
		
		JLabel back = new JLabel();
		back.setOpaque(true);
		back.setBackground(new Color(206,214,228));
		back.setSize(size.width, size.height-1);
		back.setLocation(0, 0);
		
		
		
		temp.add(back);
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		JLabel text = new JLabel(s);
		text.setFont(font);
		text.setForeground(new Color(117,121,128));
		text.setSize((int) bounds.getWidth(),
					 (int) bounds.getHeight());
		text.setLocation(back.getWidth()/2 - text.getWidth()/2,
						 back.getHeight()/2 - text.getHeight()/2);
		back.add(text);
		//temp.setSize(new Dimension(getWidth(), rowHeight));
		//temp.setLocation(0, y);
		
		return temp;
		
	}
	
	

}
