package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.FlowLayout;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;

public class CalendarHeader extends JPanel {
	
	
	
	
	CalendarHeader(List<User> users, int width) {
		
		
		setLayout(new FlowLayout());
		setBackground(ConfigureApp.backgroundColor);
		
		ServiceData serv = new ServiceData();
		
		add(new CalendarHeaderColumn());
		
		Iterator<User> it = users.iterator();
		while(it.hasNext()) {
			
			User u = it.next();
			
			CalendarHeaderColumn temp = new CalendarHeaderColumn(serv.parseToString(u), new Color(198,113,3), width);
			add(temp);
			
		}
		
		
		
		
		
	}
	
	CalendarHeader(List<DateOfDay> list, Boolean isDate) {
		
		Calendar cal = Calendar.getInstance();
		CalendarApp app = new CalendarApp();
		DateOfDay today = app.changeDate(cal);
		
		
		setLayout(new FlowLayout());
		setBackground(ConfigureApp.backgroundColor);
		
		
		add(new CalendarHeaderColumn());
		
		Iterator<DateOfDay> it =list.iterator();
		while(it.hasNext()) {
			
			DateOfDay d = it.next();
			CalendarHeaderColumn temp;
			
			if(d.getDate().equals(today.getDate())) 
				temp = new CalendarHeaderColumn(d.getDate(), new Color(224,132,13));
			
			else
				temp = new CalendarHeaderColumn(d.getDate(), new Color(119,42,115));
			
			
			
			add(temp);
			
		}
		
		
		
		
		
	}
	
	
	
	
		
		
		
	
	
	
	
	

}
