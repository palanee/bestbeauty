package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.RepaintManager;
import javax.swing.SwingConstants;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.ConfigureApp.LabelType;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.DayWork;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.Visit;


public class CalendarDay extends JPanel {
	
	final private static int widthDay = 253;
	
	private JLabel title;
	private JLabel etTitle;
	private Color backgroundPanel = new Color(255,244,230);
	
	private int freeHeight = 2;
	private int rows;
	private int rowHeight = 35;
	
	private List<JLabel> listHours = new ArrayList<JLabel>();
	private List<Visit> listVisit = new ArrayList<Visit>();
	
	private ServiceData serviceData;
	private DateOfDay date;
	private String user;
	private int combo;
	
	private float startHour;
	private float endHour;
	
	public static String nameTab = "CalendarDay";
	
	private Boolean isFound = false;
	
	
	CalendarDay(Dimension sizeMain, DateOfDay date, String user, int combo) {
		
		setSize(widthDay, sizeMain.height - 15);
		setLayout(null);
		setBackground(backgroundPanel);
		//setBorder(BorderFactory.createLineBorder(new Color(194,115,12)));
		setBorder(null);
		
		serviceData = new ServiceData();
		this.date = date;
		this.user = user;
		this.combo = combo;
		
		
		
		if(ConfigureApp.listDayWork==null || ConfigureApp.listDayWork.size()==0) {
			startHour = 8.00f;
			endHour = 19.00f; 
			isFound = true;
		}
		else {
			Iterator<DayWork> it = ConfigureApp.listDayWork.iterator();
			System.out.println("Rozmiar listy: " + ConfigureApp.listDayWork.size());
			while(it.hasNext()) {
				DayWork plan = it.next();
				if(plan.getDay()==date.getDayWeek()) {
					System.out.println("CalendarDay plan.getDay: " + plan.getDay());
					System.out.println("CalendarDay date.getDayWeek: " + date.getDayWeek());
					isFound = true;
					startHour = (float) plan.getStart();
					endHour = (float) plan.getEnd() + 1;
				}
			}
		}
		
		if(!isFound) {
			startHour = ConfigureApp.minDayStart;
			endHour = startHour + 10; 
		}
		rows = (int) (endHour - startHour);
		setComponents();
	}
	
	public void setComponents() {
		
		listVisit = serviceData.getListVisit(user, date.getDateDate());
		
		setTitile();
		//countRow();
		setHours();
	}
	
	private void setTitile() {
		
		title = new JLabel();
		title.setBorder(BorderFactory.createMatteBorder(
                0, 2, 0, 0, ConfigureApp.backgroundColor));
                
		if(date.getDayWeek()!=1)
			title.setBackground(new Color(119,42,115));
		else
			title.setBackground(new Color(164,39,1));
		title.setOpaque(true);
		
		title.setSize(getWidth(),
					  36);
		title.setLocation(0,0);
		add(title);
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 17);
		FontMetrics metrics = new FontMetrics(font) {};
		
		String s = date.getDate();
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		etTitle = new JLabel();
		etTitle.setForeground(backgroundPanel);
		etTitle.setText(s);
		etTitle.setFont(font);
		etTitle.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
		etTitle.setLocation(44,
							 title.getHeight()/2 - etTitle.getHeight()/2);
		title.add(etTitle);
		
	}
	
	private int getCount(float labH) {
		
		Iterator<Visit> it = listVisit.iterator();
		int count = 0;
		
		while(it.hasNext()) {
			
			
			Visit vis = it.next();
			
			
			if(vis.getHour()==labH) {
				count++;

			}
			
		}
		
		return count;
	}
	
	//metoda odpowiadaj�ca za ustawienie godzin
	private void setHours() {
		
		int i = 0;
		int startY = title.getY() + title.getHeight() + freeHeight + 
					(int) (startHour - ConfigureApp.minDayStart) * (CalendarHourLabel.getHeightW()+5);
		
		int size, count;
		
		if(listVisit!=null)
			size = listVisit.size();
		else
			size = 0;
		
		while(i<rows) {
			
			if(size>0)
				count = getCount(startHour);
			else
				count = 0;
			
			if((date.getDayWeek()==1) && (count==0))
				count=-1;
			
			System.out.println("date.getDayWeek = " + date.getDayWeek());
			System.out.println("count: " + count);
			
			JLabel temp = new JLabel();
			if(isFound) {
				temp = setLabelHour(startHour, ConfigureApp.getColor(count), count);
			}
			else {
				temp = setLabelHour(startHour, ConfigureApp.getColor(-1), count);
			}
			
			RepaintManager.currentManager(this).markCompletelyClean(this);
			temp.setLocation(0,startY);
			listHours.add(temp);
			add(temp);
			
			startHour+=1.00f;
			startY = temp.getY() + temp.getHeight() + 1;
			i++;
		}
		
	}

	/**
	 *
	 * @param h obslugiwana godzina
	 * @param type
	 * @param count
	 * @return zwraca pojedyncz� kom�rk� z godzin�
	 * CHECK: czy dzia�a: int visits = getCount(h);
	 * 						MainWindow.addDate.
	 */
	
	private JLabel setLabelHour(final float h, final ConfigureApp.LabelType type, int count) {
		
		if(count<0)
			count=0;
		
		final JLabel temp = new JLabel();
		Font font = new Font("Arial", Font.BOLD, 12);
		//Font font = ConfigureApp.font.deriveFont(Font.PLAIN, 12);
		FontMetrics metrics = new FontMetrics(font) {};
		Rectangle2D bounds = null;
		
		temp.setSize(getWidth(),
					 rowHeight);
		
		final JLabel hour = new JLabel();
		hour.setOpaque(true);
		hour.setBackground(new Color(237,234,234));
		hour.setSize(40, rowHeight);
		temp.add(hour);
		
		JLabel etHour = new JLabel();
		etHour.setFont(font);
		etHour.setForeground(new Color(112,109,109));
		etHour.setText(serviceData.roundNumber(2, h));
		bounds = metrics.getStringBounds(serviceData.roundNumber(2, h), null);
		etHour.setLocation(hour.getWidth()/2 - (int) bounds.getWidth()/2,
					   hour.getHeight()/2 - (int) bounds.getHeight()/2);
		etHour.setSize((int) bounds.getWidth(),
					   (int) bounds.getHeight());
		hour.add(etHour);
		
		JLabel visit = new JLabel() {
			/*public void paint(Graphics g) {
				
				setOpaque(true); 
				setBorder(null);
				setLayout(null);
				g.setColor(type.getColor());
				g.fillRect(0, 0, temp.getWidth()-hour.getWidth(), temp.getHeight());
				setUI(null);
				super.paint(g);  
			}
			
			public void update(Graphics g) {
				
			}*/
		};
		
		visit.setOpaque(true);
		visit.setBackground(type.getColor());
		visit.setSize(temp.getWidth()-hour.getWidth(),
					  temp.getHeight());
		visit.setLocation(hour.getX() + hour.getWidth(),
						  hour.getY());
		//visit.setBorderPainted(false);
		//visit.setFocusPainted(false);
		
		//visit.setForeground(type.getColorFont());
		temp.add(visit);
		
		JLabel etVisit = new JLabel();
		String s = "Um�wione wizyty: " + count;
		bounds = metrics.getStringBounds(s, null);
		etVisit.setForeground(type.getColorFont());
		etVisit.setText(s);
		etVisit.setFont(font);
		etVisit.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
		etVisit.setLocation(visit.getWidth()/2 - (int) bounds.getWidth()/2,
							visit.getHeight()/2 - (int) bounds.getHeight()/2);
		
		visit.add(etVisit);
		


		temp.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				ConfigureApp.activeTab = CalendarPanel.getNameTab();
				
				int visits = getCount(h);
						MainWindow.addDate.setFields(date.getDateDate(), serviceData.roundNumber(2, h), user.toString(), listVisit);
				if(visits>0)
					MainWindow.addDate.setCard("main", "user", true);
				else
					MainWindow.addDate.setCard("add", "user", true);
				
				//MainWindow.addDate.setHour(h);
			}
			
		});
		
		return temp;
		
	}
	
	private void countRow() {
		
		rows = (this.getHeight() - freeHeight*2 - title.getHeight())/rowHeight;
		
		
	}
	
	public static int getWidthDay() {
		return widthDay;
	}
	
	public void update(Graphics g) {
		
	}

}
