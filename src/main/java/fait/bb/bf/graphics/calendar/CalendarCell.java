package fait.bb.bf.graphics.calendar;

import java.util.ArrayList;
import java.util.List;

import fait.bb.bf.logic.Visit;

public class CalendarCell  {
	
	private List<Visit> visits;
	
	CalendarCell() {
		
		visits = new ArrayList<Visit>();
		
	}
	
	public void addVisit(Visit v) {
		
		visits.add(v);
		
	}
	
	public List<Visit> getVisits() {
		
		return visits;
		
	}
	

}
