package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import fait.bb.bf.graphics.ConfigureApp;

public class CalendarHeaderColumn extends JLabel {
	
	private final static int widthCol = 222;
	private final int rowHeight = 29;
	
	CalendarHeaderColumn(String title, Color color, int width) {
		
		/*setBorder(BorderFactory.createMatteBorder(
                0, 2, 0, 0, ConfigureApp.backgroundColor));*/
                
		
		setBackground(color);
		setOpaque(true);
		setLayout(null);
		
		setPreferredSize(new Dimension(width, rowHeight));
		setMinimumSize(new Dimension(width, rowHeight));
		setMaximumSize(new Dimension(width, rowHeight));
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 14);
		FontMetrics metrics = new FontMetrics(font) {};
		
		String s = title;
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		JLabel etTitle = new JLabel();
		etTitle.setForeground(ConfigureApp.backgroundColor);
		etTitle.setText(s);
		etTitle.setFont(font);
		etTitle.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
		etTitle.setLocation(width/2 - etTitle.getWidth()/2,
							rowHeight/2 - etTitle.getHeight()/2);
		add(etTitle);
		
	}
	
	CalendarHeaderColumn(String title, Color color) {
		
		/*setBorder(BorderFactory.createMatteBorder(
                0, 2, 0, 0, ConfigureApp.backgroundColor));*/
                
		
		setBackground(color);
		setOpaque(true);
		setLayout(null);
		
		setPreferredSize(new Dimension(widthCol, rowHeight));
		setMinimumSize(new Dimension(widthCol, rowHeight));
		setMaximumSize(new Dimension(widthCol, rowHeight));
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 14);
		FontMetrics metrics = new FontMetrics(font) {};
		
		String s = title;
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		JLabel etTitle = new JLabel();
		etTitle.setForeground(ConfigureApp.backgroundColor);
		etTitle.setText(s);
		etTitle.setFont(font);
		etTitle.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
		etTitle.setLocation(widthCol/2 - etTitle.getWidth()/2,
							rowHeight/2 - etTitle.getHeight()/2);
		add(etTitle);
		
	}
	
	CalendarHeaderColumn() {
		
		setBackground(new Color(171,175,183));
		setOpaque(true);
		setLayout(null);
		
		setPreferredSize(new Dimension(75, rowHeight));
		setMinimumSize(new Dimension(75, rowHeight));
		setMaximumSize(new Dimension(75, rowHeight));
		
		Font font = ConfigureApp.font.deriveFont(Font.BOLD, 14);
		FontMetrics metrics = new FontMetrics(font) {};
		
		String s = "Godziny";
		Rectangle2D bounds = metrics.getStringBounds(s, null);
		JLabel etTitle = new JLabel();
		etTitle.setForeground(ConfigureApp.backgroundColor);
		etTitle.setText(s);
		etTitle.setFont(font);
		etTitle.setSize((int) bounds.getWidth(),
						(int) bounds.getHeight());
		etTitle.setLocation(75/2 - etTitle.getWidth()/2,
							rowHeight/2 - etTitle.getHeight()/2);
		add(etTitle);
		
	}

}
