package fait.bb.bf.graphics.calendar;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fait.bb.bf.graphics.ConfigureApp;
import fait.bb.bf.graphics.components.JButtonOwn;
import fait.bb.bf.graphics.components.JButtonWithout;
import fait.bb.bf.graphics.panels.mainpanels.MainWindow;
import fait.bb.bf.logic.CalendarApp;
import fait.bb.bf.logic.DateOfDay;
import fait.bb.bf.logic.DayWork;
import fait.bb.bf.logic.ServiceData;
import fait.bb.bf.logic.User;
import fait.bb.bf.logic.Visit;

public class CalendarDayNew extends JPanel{

	private JPanel dayPanel;
	private final static int widthCol = 222;
	private final int rowHeight = 38;
	
	private int startHour;
	private int endHour;
	private int rows;
	private int count;
	private List<CalendarCell> cells;
	
	
	private Color backgroundPanel = new Color(255,244,230);
	
	private int widthColumn;
	
	private DateOfDay date;
	
	private User user;
	
	private Boolean isFound = false;
	
	private List<Visit> listVisit;
	
	private Boolean isWorked = true;
	private Boolean isFree = false;
	
	public static String nameTab = "CalendarDay";
	private int breakHour;
	private int breakHourEnd;
	
	
	
	public CalendarDayNew(DateOfDay date, User user) {
		
		
		this.date = date;
		ServiceData serviceData = new ServiceData();
		this.user = serviceData.getUserFromSession(user);
				
		listVisit = serviceData.getListVisit(user, date.getDateDate());
		cells = new ArrayList<CalendarCell>();
		
		isFree = this.user.isFreeDay(date.getDateDate());
		
		
		setStartAndEnd();
		
		String work = serviceData.getWork("daywork", date.getDayWeek());
		if(work.equals("w") || isFree)
			isWorked = false;
		
		count = rows;
		/*setPreferredSize(new Dimension(widthCol, (count)*rowHeight));
		setMinimumSize(new Dimension(widthCol, (count)*rowHeight));
		setMaximumSize(new Dimension(widthCol, (count)*rowHeight));*/
		
		widthColumn = widthCol;
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(backgroundPanel);
		//setBorder(BorderFactory.createLineBorder(new Color(194,115,12)));
		setBorder(null);
		
		//paintRows();

		repaint();
		
	}
	
	private Boolean setActiveAdding() {
		
		if(CalendarApp.diffInDays(Calendar.getInstance().getTime(), date.getDateDate())>=1) {
			return false;
		}
		else
			return true;
		
		
	}
	
	public void setWidth(int width) {
		
		widthColumn = width;
		setPreferredSize(new Dimension(widthColumn, (count)*rowHeight));
		setMinimumSize(new Dimension(widthColumn, (count)*rowHeight));
		setMaximumSize(new Dimension(widthColumn, (count)*rowHeight));
		setBorder(null);
		paintRows();
		repaint();
		revalidate();
		
	}
	
	CalendarDayNew(DateOfDay date, User user, Boolean isUser) {
		
		
		this.date = date;
		ServiceData serviceData = new ServiceData();
		this.user = serviceData.getUserFromSession(user);
		isFree = this.user.isFreeDay(date.getDateDate());
		
		listVisit = serviceData.getListVisit(user, date.getDateDate());
		
		String work = serviceData.getWork("daywork", date.getDayWeek());
		if(work.equals("w") || isFree)
			isWorked = false;
		
		cells = new ArrayList<CalendarCell>();
		setStartAndEnd();
		
		breakHour = (int) ((startHour - ConfigureApp.minDayStart) *4);
		breakHourEnd = (ConfigureApp.maxDayEnd - endHour) * 4;
		count = rows + breakHour + breakHourEnd;
		
		widthColumn = widthCol;
		
		setPreferredSize(new Dimension(widthColumn, (count)*rowHeight));
		setMinimumSize(new Dimension(widthColumn, (count)*rowHeight));
		setMaximumSize(new Dimension(widthColumn, (count)*rowHeight));
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(backgroundPanel);
		//setBorder(BorderFactory.createLineBorder(new Color(194,115,12)));
		setBorder(null);
		
		paintEmptyRows(breakHour);
		paintRows();
		paintEmptyRows(breakHourEnd);
		

		repaint();
		
	}
	
	
	private void setStartAndEnd() {
		
		if(ConfigureApp.listDayWork==null || ConfigureApp.listDayWork.size()==0) {
			startHour = ConfigureApp.minDayStart;
			endHour = ConfigureApp.maxDayEnd;
			isFound = true;
		}
		else {
			Iterator<DayWork> it = ConfigureApp.listDayWork.iterator();
			while(it.hasNext()) {
				DayWork plan = it.next();
				if(plan.getDay()==date.getDayWeek()) {
					isFound = true;
					startHour = plan.getStart();
					endHour = plan.getEnd();
					break;
				}
			}
		}
		
		if(!isFound) {
			startHour = ConfigureApp.minDayStart;
			endHour = ConfigureApp.maxDayEnd; 
		}
		rows = (int) (endHour - startHour) * 4;
		
		for(int i=0; i<rows; i++) {
			
			cells.add(new CalendarCell());
			
		}
		
		
	}
	
	private void paintEmptyRows(int breakHour) {
		
		
		
		if(breakHour>0) {
			for(int i=0; i<breakHour; i++) {
				JLabel temp = paintRow();
				temp.setVisible(true);
				add(temp);
			}
			revalidate();
		}
		
	}
	
	private void paintRows() {
		
		int y = 1;
		
		String hour="";	
		int countVisit = 0;
		
		int times = 0;
		int  diff = 0;
		int temp = 0;
		Visit visit = null;
		Boolean flagFirst = false;
		Boolean flagVisit = false;
		
		for(int i=0; i<(rows); i++) {
				
				countVisit = 0;
			
				Boolean isVisit = false;
				if(times==0) {
					visit = null;
					flagVisit = false;
				}
				
				int h = (int) (i/4 + startHour);
				int min = i%4 * 15;
				
				hour = h + ":" + min;
				if(min==0) 
					hour +="0";
				
				if(listVisit.size()!=0) {
					
					Iterator<Visit> it = listVisit.iterator();
					
					
					while(it.hasNext()) {
						
						Visit v = it.next();
						
						
						
						if((v.getCalendar().get(Calendar.HOUR_OF_DAY)==h && v.getCalendar().get(Calendar.MINUTE)==min)) {
							countVisit++;
							visit = v;
							int rest = visit.getDuration()%15;
							if(rest==0)
								temp = visit.getDuration()/15;
							else
								temp = visit.getDuration()/15 + 1;
							
							if(times>0 && times>temp) {
								diff = temp;
							}
							else if(times>0 && times==temp) {
								diff = temp;
								times = temp;
							}
							else if(times>0 && times<temp) {
								diff = times;
								times = temp;
							}
							else {
								diff = 0;
								times = temp;
							}
								
							
							flagFirst = true;
							flagVisit = true;
							
							
							for(int j=0; j<temp; j++) {
								if(j+i<cells.size())
									cells.get(i+j).addVisit(visit);
							}
							
							
								
							
							
							
						}
						
						
						
					}
				}
				
				
				JLabel tempek = paintSingleRow(i, countVisit, visit, hour, flagFirst, flagVisit, diff);
				tempek.setVisible(true);
				add(tempek);
				flagFirst = false;
				if(times>0)
					times--;
				if(diff>0)
					diff--;
		}
		revalidate();
		
		
	}
	
	private JLabel paintRow() {
		
		JLabel temp = new JLabel();
		temp.setLayout(null);
	
		//temp.setOpaque(true);
		//temp.setBackground(Color.gray);
		temp.setPreferredSize(new Dimension(widthColumn, rowHeight));
		temp.setMinimumSize(new Dimension(widthColumn, rowHeight));
		temp.setMaximumSize(new Dimension(widthColumn, rowHeight));
		temp.setAlignmentX(JButton.CENTER_ALIGNMENT);
		return temp;
		
	}
	
	private JLabel paintSingleRow(final int i, int countVisit, Visit visit, final String hour, Boolean flag, Boolean isVisit, int diff) {
		
		JLabel temp = new JLabel();
		temp.setLayout(null);
		//temp.setOpaque(true);
		//temp.setBackground(new Color(232,238,242));
		//temp.setOpaque(true);
		//temp.setBackground(Color.gray);
		temp.setPreferredSize(new Dimension(widthColumn, rowHeight));
		temp.setMinimumSize(new Dimension(widthColumn, rowHeight));
		temp.setMaximumSize(new Dimension(widthColumn, rowHeight));
		temp.setAlignmentX(JButton.CENTER_ALIGNMENT);
		
			
			Dimension size = temp.getPreferredSize();
			
			JLabel back = new JLabel();
			back.setOpaque(true);
			
			//ustawianie t�a dla przycisk�w, kt�re nie maj� wizyt
			if(countVisit==0) {
					if(isWorked) 		
							back.setBackground(ConfigureApp.backgroundColor);
					else
						back.setBackground(new Color(234,234,234));
			}
			
			else if(countVisit==1)
				back.setBackground(new Color(254,239,198));
			else 
				back.setBackground(new Color(253,227,155));
			
			back.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, new Color(206,214,228)));
			back.setSize(size.width, size.height);
			back.setLocation(0, 0);
			
			temp.add(back);
			
			if(flag)
				back.setBorder(BorderFactory.createMatteBorder(
                        1, 1, 0, 1, new Color(191,114,13)));
			
			if(countVisit==0 && !isVisit) {
				JButton addVisit;
				
				if(isWorked)
					addVisit = new JButtonOwn("addVisitPlus");
				else
					addVisit = new JButtonOwn("notWork");
				
			
				addVisit.setLocation(back.getWidth()/2 - addVisit.getWidth()/2, 
									 back.getHeight()/2 - addVisit.getHeight()/2);
				addVisit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				back.add(addVisit);
				
				addVisit.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						ConfigureApp.activeTab = CalendarPanelNew.getNameTab();
						System.out.println("Klikamy pierwszy warunek");
						
						
						MainWindow.addDate.setFields(date.getDateDate(), hour, user.toString(), cells.get(i).getVisits());
						MainWindow.addDate.setCard("add", "user", true);
						
						/*if(visits>0)
							MainWindow.addDate.setCard("main", "user");
						else
							MainWindow.addDate.setCard("add", "user");*/
						
						
					}
				});
				addVisit.setEnabled(setActiveAdding());
			}
			
			else if(countVisit>1 && flag){
				
				Font font = new Font("Arial", Font.BOLD, 13);
				FontMetrics metrics = new FontMetrics(font) {};
				
				if(diff>0) {
					
					back.setBackground(new Color(253,227,155));
				}
				
				String cust = "Liczba wizyt: " + countVisit;
				Rectangle2D bounds = metrics.getStringBounds(cust, null);
				
				JLabel customer = new JLabel(cust);
				customer.setFont(font);
				customer.setForeground(new Color(208,119,5));
				customer.setSize((int) bounds.getWidth(),
								 (int) bounds.getHeight());
				customer.setLocation(5, 
									 back.getHeight()/2 - customer.getHeight()/2);
				back.add(customer);
				
				back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
				back.addMouseListener(new MouseAdapter() {
					
					
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
						System.out.println("Klikamy drugi warunek");
						
						ConfigureApp.activeTab = CalendarPanelNew.getNameTab();
						MainWindow.addDate.setFields(date.getDateDate(), hour, user.toString(), cells.get(i).getVisits());
						
						MainWindow.addDate.setCard("main", "user", setActiveAdding());
						
					}
				});
				
			}
			
			else if(flag){
				
				Font font = new Font("Arial", Font.BOLD, 13);
				FontMetrics metrics = new FontMetrics(font) {};
				
				if(diff>0) {
					
					back.setBackground(new Color(253,227,155));
				}
				
				String cust = visit.getCustomer().getName() + " " + visit.getCustomer().getSurname();
				Rectangle2D bounds = metrics.getStringBounds(cust, null);
				
				JLabel customer = new JLabel(cust);
				customer.setFont(font);
				customer.setForeground(new Color(208,119,5));
				customer.setSize((int) bounds.getWidth(),
								 (int) bounds.getHeight());
				customer.setLocation(5, 
									 5);
				back.add(customer);
				
				
				font = new Font("Arial", Font.PLAIN, 12);
				metrics = new FontMetrics(font) {};
				
				
				
				if(visit.getTreatment()==null)
					cust = "";
				else
					cust = visit.getTreatment().getName();
				bounds = metrics.getStringBounds(cust, null);
				JLabel vis = new JLabel(cust);
				vis.setFont(font);
				vis.setForeground(new Color(208,119,5));
				vis.setSize(back.getWidth() - 5,
						   (int) bounds.getHeight());
				vis.setLocation(5, customer.getY() + customer.getHeight() + 2);
				back.add(customer);
				
				back.add(vis);
				back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
				back.addMouseListener(new MouseAdapter() {
					
					
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
						System.out.println("Klikamy trzeci warunek");
						
						ConfigureApp.activeTab = CalendarPanelNew.getNameTab();
						MainWindow.addDate.setFields(date.getDateDate(), hour, user.toString(), cells.get(i).getVisits());
						
						MainWindow.addDate.setCard("main", "user", setActiveAdding());
						
					}
				});
				
			}
		
	
			
			
			else {
				
				if(diff>0) 
					back.setBackground(new Color(253,227,155));
				else
					back.setBackground(new Color(254,239,198));
				back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				
				back.addMouseListener(new MouseAdapter() {
					
					
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
						ConfigureApp.activeTab = CalendarPanelNew.getNameTab();
						
						System.out.println("Klikamy czwarty warunek");
						
						MainWindow.addDate.setFields(date.getDateDate(), hour, user.toString(), cells.get(i).getVisits());
						
						MainWindow.addDate.setCard("main", "user", setActiveAdding());
						
					}
				});
				
				
			}
		
		
		
		
		
		//temp.setSize(new Dimension(getWidth(), rowHeight));
		//temp.setLocation(0, y);
	
		
		return temp;
		
	}
	
	public static int getWidthCol() {
		
		return widthCol;
		
	}
	
	public User getUser() {
		return user;
	}
	
	

}
