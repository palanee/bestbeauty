package fait.standard;

import java.net.MalformedURLException;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class Network {
	
	public Network() {
		
		try {
			
			Properties sysProperties = System.getProperties();
			HostnameVerifier hv = new HostnameVerifier()
		    {
		        public boolean verify(String urlHostName, SSLSession session)
		        {
		            System.out.println("Warning: URL Host: " + urlHostName + " vs. "
		                    + session.getPeerHost());
		            return true;
		        }

				
		    };
		    trustAllHttpsCertificates();
		    HttpsURLConnection.setDefaultHostnameVerifier(hv);
			
		    
		   
			
			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
	javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()
		{
			return null;
		}

		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)
		{
			return true;
		}

		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
			throws java.security.cert.CertificateException
		{
			return;
		}

		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
			throws java.security.cert.CertificateException
		{
			return;
		}
	}


	private static void trustAllHttpsCertificates() throws Exception
	{

		//  Create a trust manager that does not validate certificate chains:

		javax.net.ssl.TrustManager[] trustAllCerts =

			new javax.net.ssl.TrustManager[1];

		javax.net.ssl.TrustManager tm = new miTM();

		trustAllCerts[0] = tm;

		javax.net.ssl.SSLContext sc =

			javax.net.ssl.SSLContext.getInstance("SSL");

		sc.init(null, trustAllCerts, null);

		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(

				sc.getSocketFactory());

	}
	

}
