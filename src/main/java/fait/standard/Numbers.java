package fait.standard;

public class Numbers {
	
	String SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]{}|;':,./<>?";
	String FLOAT_SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]{}|;':/<>?";
	
	public Boolean isInteger(String text) {
		
		Boolean isGood = true;
		
		char[] tab = text.trim().toCharArray();
		int i = 0;
		while(i<tab.length) {
			if(Character.isLetter(tab[i]))
				return false;
			else if(Character.isWhitespace(tab[i]))
				return false;
			else if(SPECIAL_CHARACTERS.indexOf(tab[i])!=-1)
				return false;
				
			i++;
		}
		
		return isGood;
		
	}
	
	
	public Boolean isFloatOrInteger(String text) {
		
		Boolean isGood = true;
		
		char[] tab = text.trim().toCharArray();
		int i = 0;
		while(i<tab.length) {
			if(Character.isLetter(tab[i]))
				return false;
			else if(Character.isWhitespace(tab[i]))
				return false;
			else if(FLOAT_SPECIAL_CHARACTERS.indexOf(tab[i])!=-1)
				return false;
				
			i++;
		}

		
		return isGood;
		
	}

}
