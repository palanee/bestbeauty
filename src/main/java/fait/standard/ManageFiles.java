package fait.standard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.stream.FileImageInputStream;


public class ManageFiles {
	
	public String readFile(String name) {
		
		try {
			return new Scanner(new FileInputStream(new File(name)), "UTF-8").useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}

}
